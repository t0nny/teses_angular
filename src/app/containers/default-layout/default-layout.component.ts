import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import { OpcionesService } from '../../services/opciones/opciones.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  private changes: MutationObserver;
  public element: HTMLElement;
  dataOpciones: any = []
  dataOpcionesHijos: any = []
  dataOpcionesPadre: any = []
  loginUser: any = {}
  // dataOpcionTotal:any=[]
  constructor(@Inject(DOCUMENT) _document?: any, opcionesServicio?: OpcionesService, authService?: AuthService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
    //let usuario: string = localStorage.getItem('usuarioAct');

    authService.getUserInfo().subscribe(data => {
      this.loginUser = data;
      console.log(this.loginUser)
      opcionesServicio.getOpcionPaUsuario(this.loginUser.id).subscribe(dataPadre => {
        this.dataOpcionesPadre = dataPadre;
        opcionesServicio.getOpcionUsuario(this.loginUser.id).subscribe(dataHijo => {
          this.dataOpcionesHijos = dataHijo;
          if (dataPadre) {
            for (let i = 0; i < dataPadre.length; i++) {
              let opcion = {};
              if (dataPadre[i].hijo == true) {

                opcion = {
                  id: dataPadre[i].idOpcion,
                  name: dataPadre[i].nombreOpcion,
                  // url: dataPadre[i].urlOpcion,
                  url: 'XD',
                  icon: dataPadre[i].iconoOpcion,
                  variant: 'secondary', children: [],
                }
              } else {
                opcion = {
                  id: dataPadre[i].idOpcion,
                  name: dataPadre[i].nombreOpcion,
                  // url: dataPadre[i].urlOpcion,
                  url: 'XD',
                  icon: dataPadre[i].iconoOpcion,
                  variant: 'secondary'
                }
              }

              this.dataOpciones.push(opcion);
              // this.dataOpciones.push({ divider: true }, {
              //   title: true, name: 'Extras'
              // });

              this.buscarOpcionPadreHijo(dataPadre[i].idOpcion)

              if (dataPadre[i].idPadre != null) {
                let dataAux = []
                let id = 0;
                for (let j = 0; j < this.dataOpciones.length; j++) {
                  if (this.dataOpciones[j].id == dataPadre[i].idOpcion) {
                    id = this.dataOpciones[j].id;
                    dataAux = this.dataOpciones[j].children;
                    this.dataOpciones.splice(j, 1);
                  }

                }

                for (let j = 0; j < this.dataOpciones.length; j++) {
                  for (let k = 0; k < this.dataOpciones[j].children.length; k++) {
                    if (this.dataOpciones[j].children[k].id == id) {
                      this.dataOpciones[j].children[k].children = [];
                      this.dataOpciones[j].children[k].children.push(dataAux)
                    }
                  }
                }
              }
            }
            //  console.log("datos")
            //   console.log(this.dataOpciones)
            //  console.log("datos del navitem")
            this.navItems = this.dataOpciones
            //  console.log(this.navItems)

          }
        });
      })
    })
  }

  buscarOpcionPadreHijo(idOpcion: number) {

    for (let i = 0; i < this.dataOpcionesHijos.length; i++) {
      if (this.dataOpcionesHijos[i].idOpcionPadre == idOpcion) {
        for (let j = 0; j < this.dataOpciones.length; j++) {
          if (this.dataOpciones[j].id == idOpcion) {

            this.dataOpciones[j].children.push({
              id: this.dataOpcionesHijos[i].idOpcionHijo,
              idPadre: this.dataOpcionesHijos[i].idOpcionPadre,
              name: this.dataOpcionesHijos[i].nombreOpcionHijo,
              url: this.dataOpcionesHijos[i].urlOpcionHijo,
              icon: this.dataOpcionesHijos[i].iconoOpcionHijo,
              variant: 'info', show: false
            })



          }
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }
}
