import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { jqxMenuComponent, jqxMenuModule } from 'jqwidgets-ng/jqxmenu';
import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';
import { jqxInputModule } from 'jqwidgets-ng/jqxinput';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { jqxDateTimeInputComponent, jqxDateTimeInputModule } from 'jqwidgets-ng/jqxdatetimeinput';
import { jqxDropDownListComponent, jqxDropDownListModule  } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxListBoxModule  } from 'jqwidgets-ng/jqxlistbox';
import { jqxValidatorComponent, jqxValidatorModule } from 'jqwidgets-ng/jqxvalidator';
import { jqxExpanderComponent, jqxExpanderModule } from 'jqwidgets-ng/jqxexpander';
import { jqxComboBoxModule } from 'jqwidgets-ng/jqxcombobox';
import { jqxMaskedInputComponent, jqxMaskedInputModule } from 'jqwidgets-ng/jqxmaskedinput';
import { jqxNumberInputComponent, jqxNumberInputModule } from 'jqwidgets-ng/jqxnumberinput';
import { jqxCheckBoxComponent, jqxCheckBoxModule } from 'jqwidgets-ng/jqxcheckbox';
import { jqxButtonComponent, jqxButtonModule } from 'jqwidgets-ng/jqxbuttons';
import { jqxRadioButtonComponent, jqxRadioButtonModule} from 'jqwidgets-ng/jqxradiobutton';
import { jqxKanbanComponent, jqxKanbanModule} from 'jqwidgets-ng/jqxkanban';
import { jqxWindowComponent, jqxWindowModule} from 'jqwidgets-ng/jqxwindow';
import { ModalComponentComponent } from './modal-view/modal-component/modal-component.component';
import { ModalFormularioComponent } from './modal-view/modal-formulario/modal-formulario.component';
import { ModalFormularioMallaComponent } from './modal-view/modal-formulario-malla/modal-formulario-malla.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../interceptors/token.interceptor';
import { AuthService } from '../services/auth/auth.service';
import { Base64Util } from '../util/base-64-util';
import { AccountService } from '../services/auth/account.service';
import { TokenService } from '../services/auth/token.service';
import { AuthGuardService } from '../services/auth/auth-guard.service';
import { CacheService } from '../services/auth/cache.service';
import { OpcionesService } from '../services/opciones/opciones.service';
import { jqxTabsComponent, jqxTabsModule } from 'jqwidgets-ng/jqxtabs';
import { jqxTextAreaComponent, jqxTextAreaModule } from 'jqwidgets-ng/jqxtextarea';
import { jqxSchedulerComponent, jqxSchedulerModule } from 'jqwidgets-ng/jqxscheduler';
import { jqxTreeGridComponent, jqxTreeGridModule } from 'jqwidgets-ng/jqxtreegrid';
import { jqxPanelComponent, jqxPanelModule } from 'jqwidgets-ng/jqxPanel';



@NgModule({
  declarations: [

    ModalComponentComponent,
    ModalFormularioComponent,
    ModalFormularioMallaComponent,
  ],
  imports: [
    ModalModule.forRoot(),
    CommonModule,
    jqxMenuModule,
    jqxGridModule,
    jqxInputModule,
    FormsModule,
    jqxDateTimeInputModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    jqxDropDownListModule,
    jqxListBoxModule,
    jqxValidatorModule,
    jqxExpanderModule,
    jqxComboBoxModule,
    jqxMaskedInputModule,
    jqxNumberInputModule,
    jqxCheckBoxModule,
    jqxButtonModule,
    jqxRadioButtonModule,
    jqxKanbanModule,
    jqxWindowModule,
    jqxTabsModule,
    jqxTextAreaModule,
    jqxDateTimeInputModule,
    jqxSchedulerModule,
    jqxTreeGridModule,
    jqxPanelModule,
  ],
  exports: [ 
    ModalComponentComponent,
    ModalFormularioComponent,
    ModalFormularioMallaComponent,
    jqxGridModule,
    jqxButtonComponent,
    jqxMenuComponent,
    jqxInputModule,
    jqxCheckBoxModule,
    jqxCheckBoxComponent,
    jqxRadioButtonComponent, 
    jqxExpanderComponent,
    jqxCheckBoxComponent,
    jqxDropDownListComponent,
    jqxMaskedInputComponent,
    jqxMaskedInputComponent,
    jqxWindowComponent,
    jqxKanbanComponent,
    jqxTabsComponent,
    jqxTextAreaComponent,
    jqxDateTimeInputComponent,
    jqxValidatorComponent,
    jqxSchedulerComponent,
    jqxNumberInputComponent,
    jqxTreeGridComponent,
    jqxPanelComponent],
    providers: [
      Base64Util, AuthService, AccountService, TokenService, AuthGuardService, CacheService, OpcionesService,DatePipe,
      { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
      // LVT Fin 
    ],
})
export class GlobalComponentModule { }
