import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GlobalComponentModule } from '../global-component.module';
//import { jqxCheckBoxComponent } from 'jqwidgets-scripts/jqwidgets-ng/jqxcheckbox';
//import { DistributivosService } from '../../services/distributivos/distributivos.service';
//import { jqxMenuModule } from 'jqwidgets-scripts/jqwidgets-ng/jqxmenu';
@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    GlobalComponentModule,
 //   jqxMenuModule
  ],
  exports:[],
  
})
export class ModalViewModule { }
