import { Component, ViewChild, TemplateRef, ViewEncapsulation, OnInit, Output, ElementRef, EventEmitter, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { jqxInputComponent } from 'jqwidgets-ng/jqxinput';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { DistributivosService } from '../../../services/distributivos/distributivos.service';
import { jqxValidatorComponent } from 'jqwidgets-ng/jqxvalidator';
import { MallasService } from '../../../services/mallas/mallas.service';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxListBoxComponent } from 'jqwidgets-ng/jqxlistbox';
import { jqxNumberInputComponent } from 'jqwidgets-ng/jqxnumberinput';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { ModalComponentComponent } from '../../modal-view/modal-component/modal-component.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modal-formulario-malla',
  templateUrl: './modal-formulario-malla.component.html',
  styleUrls: ['./modal-formulario-malla.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ModalFormularioMallaComponent {


  @ViewChild('formNuevo') private formNuevo: ModalDirective;
  @ViewChild('dateTimeInput') dateTimeInput: jqxDateTimeInputComponent;
  @ViewChild('myValidator') myValidator: jqxValidatorComponent;
  @ViewChild('Events') Events: ElementRef;
  @ViewChild('controlFecha') controlFecha: jqxDateTimeInputComponent;
  @ViewChild('cmbOfertas') cmbOfertas: jqxDropDownListComponent;
  @ViewChild('inputNivelMin') inputNivelMin: jqxInputComponent;
  @ViewChild('inputDetalle') inputDetalle: jqxInputComponent;
  @ViewChild('listPeriodosCheck') listPeriodosCheck: jqxListBoxComponent;
  @ViewChild('inputMateriasA') inputMateriasA: jqxNumberInputComponent;
  @ViewChild('inputNiMax') inputNiMax: jqxNumberInputComponent;
  @ViewChild('inputNiMin') inputNiMin: jqxNumberInputComponent;
  @ViewChild('factorMultDocente') factorMultDocente: jqxNumberInputComponent;
  @ViewChild('inputCreditos') inputCreditos: jqxNumberInputComponent;
  @ViewChild('inputmMaterias') inputmMaterias: jqxNumberInputComponent;
  @ViewChild('inputHoras') inputHoras: jqxNumberInputComponent;
  @ViewChild('inputReglamento') inputReglamento: jqxInputComponent;
  @ViewChild(ModalComponentComponent) modal: ModalComponentComponent;
  @ViewChild("myCheckFecha") myCheckFecha: jqxCheckBoxComponent;
  @ViewChild('dateTimeFechaHasta') dateTimeFechaHasta: jqxDateTimeInputComponent;
  @ViewChild('dateTimeFechaDesde') dateTimeFechaDesde: jqxDateTimeInputComponent;
  @ViewChild("myCheckFactMult") myCheckFactMult: jqxCheckBoxComponent;

  sub: Subscription;

  @ViewChild('myDiv') myDiv: ElementRef;


  @Output() protected PassMethods = new EventEmitter();//variable para escuchar eventos del componente

  protected a: string;
  protected opcion: any = {};
  protected periodoCheck: any;
  //protected periodoMallaVersion:any=[];  

  //--------------------------------------------------------------------------------------

  /**Permite controlar que no salga del modal sin presionar algun boton de éste */
  protected config = { backdrop: true, ignoreBackdropClick: true };

  constructor(protected modalService: BsModalService,
    private distributivosService: DistributivosService,
    private router: Router,
    private route: ActivatedRoute,
    private mallasService: MallasService) { }
  idofertaMalla: any;
  periodoMallaVersion: any = [];
  id: any;
  idPeriodo_Academico: any;
  idMalla: any;
  idUsuarioIngreso: any;
  versionVal: any;
  ValControl: any;
  resultado: any;
  descripcion: any;
  inputNivelMax: any;
  ncreditos: any;
  nhorasmax: any;
  fechaAprobacion: any;
  fechaDesde: any;
  fechaHasta: any;
  minCreditosMallas: number = 0;
  maxCreditosMallas: number = 0;
  minHorasMallas: number = 0;
  maxHorasMallas: number = 0;
  minAsignaturaMallas: number = 0;
  maxAsignaturaMallas: number = 0;
  horasCreditos: number = 0;
  datosMalla: any = {};


  ngOnInit() {
    
  }

  sourceCar: any = {
    datatype: 'json',
    id: 'id',
    datafields: [
      { name: 'id', type: 'number' },
      { name: 'idOferta', type: 'number' },
      { name: 'oferta', type: 'string' },
    ]
  };
  dataAdapterCar: any = new jqx.dataAdapter(this.sourceCar);

  listOnSelect(event: any): void {
    if (event.args) {
      let item = event.args.item;
      if (item) {
        this.idofertaMalla = item.value
        let descripcion = "MALLA DE " + item.label
        this.opcion.descripcion = descripcion;
        this.inputDetalle.val(descripcion)
      }
    }
  };
  //CARGA PERIODOS VIGENTES
  sourceComboPeriodoVigente: any = {
    id: 'id',
    localdata: [
      { name: 'id' },
      { name: 'codigo' },
      { name: 'descripcion' },

    ]
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterPeridoVigente: any = new jqx.dataAdapter(this.sourceComboPeriodoVigente);

  listaPeriodosVigentes() {
    this.distributivosService.getPeriodoVigente().subscribe(data => {
      this.sourceComboPeriodoVigente.localdata = data;
      this.dataAdapterPeridoVigente.dataBind();
    })
  }
  ReglamentoVigente() {
    this.mallasService.getReglamentoVigente().subscribe(data => {
      this.sourceComboReglamentoVigente.localdata = data;
      if (data) {
        this.opcion.idReglamento = data[0].id
        this.inputReglamento.val(data[0].nombre);
        for (let i = 0; i < data.length; i++) {
          if (data[i].codigoValidacion == 'NHPC ') {
            this.horasCreditos = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMNCM') {
            this.minCreditosMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMXCM') {
            this.maxCreditosMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMNHM') {
            this.minHorasMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMXHM') {
            this.maxHorasMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMNAM') {
            this.minAsignaturaMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMXAM') {
            this.maxAsignaturaMallas = data[i].valorValidacion;
          }
        }
        this.inputCreditos.min(this.minCreditosMallas);
        this.inputCreditos.max(this.maxCreditosMallas);
        this.inputHoras.min(this.minHorasMallas);
        this.inputHoras.max(this.maxHorasMallas);

        this.inputMateriasA.min(this.minAsignaturaMallas);
        this.inputMateriasA.max(this.maxAsignaturaMallas);
      }

      this.dataAdapterReglamentoVigente.dataBind();
    })
  }
  BuscaReglamento(idReglamento: number) {
    this.mallasService.getBuscaReglamento(idReglamento).subscribe(data => {
      if (data) {
        this.inputReglamento.val(data[0].nombre);
        for (let i = 0; i < data.length; i++) {
          if (data[i].codigoValidacion == 'NHPC ') {
            this.horasCreditos = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMNCM') {
            this.minCreditosMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMXCM') {
            this.maxCreditosMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMNHM') {
            this.minHorasMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMXHM') {
            this.maxHorasMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMNAM') {
            this.minAsignaturaMallas = data[i].valorValidacion;
          }
          if (data[i].codigoValidacion == 'NMXAM') {
            this.maxAsignaturaMallas = data[i].valorValidacion;
          }
        }
      }
      this.sourceComboReglamentoVigente.localdata = data;
      this.dataAdapterReglamentoVigente.dataBind();
    })
  }
  sourceComboReglamentoVigente: any = {
    id: 'id',
    localdata: []
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterReglamentoVigente: any = new jqx.dataAdapter(this.sourceComboReglamentoVigente);
  calcularHoras() {
    if (this.inputCreditos.val() > 0) {
      let horas = this.inputCreditos.val() * this.horasCreditos;
      this.inputHoras.val(horas);
    }
  }

  checkChangeFecha(event: any): void {
    let checked = event.args.checked;
    if (checked) {
      this.dateTimeFechaHasta.disabled(false)
    } else {
      this.dateTimeFechaHasta.value(null)
      this.dateTimeFechaHasta.disabled(true)
    }

  }
  checkFactorMult(event: any): void {
    let checked = event.args.checked;
    if (checked) {
      this.factorMultDocente.disabled(false);
      this.factorMultDocente.val(0);
    } else {
      this.factorMultDocente.disabled(true);
      this.factorMultDocente.val(0);
    }

  }

  listaPeriodosFechaDesde(fechaDesde: Date) {
    console.log(fechaDesde)
    this.mallasService.getPeriodoDesde(fechaDesde).subscribe(data => {

      this.sourceComboPeriodoVigente.localdata = data;
      this.dataAdapterPeridoVigente.dataBind();

      this.cmbOfertas.disabled(true);
      this.periodoMallaVersion = [];

      for (let i = 0; i < this.opcion.periodoMallaVersion.length; i++) {
        for (let j = 0; j < this.dataAdapterPeridoVigente.records.length; j++) {
          if (this.opcion.periodoMallaVersion[i].idPeriodoAcademico == this.dataAdapterPeridoVigente.records[j].id) {
            let periodoMa = this.opcion.periodoMallaVersion[i];
            this.periodoMallaVersion.push(periodoMa);
            this.listPeriodosCheck.checkIndex(j);
          }
        }
      }
    })
  }

  formularioNuevo(opt: any, idDepartamento: number, idTipoOferta: number) {
    this.opcion = opt;
    this.mallasService.listarOfertaPorDepartamentoTipoOferta(idDepartamento, idTipoOferta).subscribe(data => {
      this.sourceCar.localdata = data
      this.dataAdapterCar.dataBind();
      this.cmbOfertas.source(this.dataAdapterCar)
      if (this.opcion.id != null) {
        console.log("entro por editar")
        this.BuscaReglamento(this.opcion.idReglamento);
        this.listaPeriodosFechaDesde(this.opcion.fechaDesde)
        for (let i = 0; i < this.dataAdapterCar.records.length; i++) {
          if (this.dataAdapterCar.records[i].id == this.opcion.idDepartamentoOferta) {
            this.cmbOfertas.val(this.opcion.idDepartamentoOferta)
          }
        }
        if (this.opcion.factorMultComponenteDocencia > 0) {
          this.factorMultDocente.disabled(false);
          this.myCheckFactMult.checked(true);

        } else {
          this.factorMultDocente.disabled(true);
          // this.myCheckFactMult.checked(false);
          this.myCheckFactMult.val(false)
        }
        this.ReglamentoVigente();
      } else {
        this.cmbOfertas.clearSelection()
        this.cmbOfertas.disabled(false);
        this.opcion.descripcion = "";

        this.inputDetalle.val("")
        // this.checkFactorMult.
        this.myCheckFactMult.checked(false)
        this.factorMultDocente.val(0)
        this.factorMultDocente.disabled(true)
        console.log(this.cmbOfertas.val())
        this.listaPeriodosVigentes();
        this.ReglamentoVigente();

      }
    });


    //this.opcion= opt;
    this.formNuevo.show();//Muesta el modal
    this.formNuevo.config = this.config;//Establece las configuraciones para el modal

  }
  validaDatos = (): boolean => {

    if (!this.cmbOfertas.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'SELECCIONE UNA CARRERA '
      });
      return false;
    } else if (!this.inputDetalle.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'ESCRIBA EL DETALLE DE LA MALLA'
      });
      return false;
    } else if (!this.inputReglamento.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'NO HA SELECCIONADO UN REGLAMENTO VIGENTE'
      });
      return false;
    } else if (!this.inputMateriasA.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA CANTIDAD DE MATERIAS'
      });
      return false;
    } else if (!this.inputmMaterias.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA CANTIDAD DE MATERIAS'
      });
      return false;
    } else if (!this.inputNivelMin.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA CANTIDAD MINIMA DE MATERIA'
      });
      return false;

    }
    else if (!this.inputCreditos.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA CANTIDAD DE CREDITOS'
      });
      return false;

    } else if (!this.inputHoras.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA CANTIDAD DE HORAS'
      });
      return false;

    }
    else if (!this.inputNiMin.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE EL NIVEL MINIMO DE HORAS'
      });
      return false;

    }
    else if (!this.inputNiMax.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE EL NIVEL MAXIMO DE HORAS'
      });
      return false;
    }
    else if (!this.dateTimeFechaDesde.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA FECHE DE CREACION DE LA MALLA'
      });
      return false;

    }
    else if (this.myCheckFactMult.val()&& this.factorMultDocente.val()==0) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'Ingrese el factor de multiplicación del componente de docencia'
      });
      return false;

    }
    else if (!this.controlFecha.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA FECHA DE CREACION'
      });
      return false;
    }
    else if (!this.controlFecha.val()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESE LA FECHA DE CREACION'
      });
      return false;
    }
    else if (this.myCheckFecha.checked() == true && !this.dateTimeFechaHasta.value()) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'INGRESSE UNA FECHA HASTA'
      });
      return false;
    }
    else if (this.dateTimeFechaHasta.value()) {
      var f = new Date();
      if (this.dateTimeFechaHasta.value() < f) {
        this.modal.alertMessage({
          title: 'Malla',
          msg: 'LA FECHA HASTA DEBE SER SUPERIOR A LA FECHA ACTUAL'
        });
        return false;
      }else if (this.controlFecha.value() && this.dateTimeFechaDesde.value()) {
        if (this.controlFecha.value() > this.dateTimeFechaDesde.value()) {
          this.modal.alertMessage({
            title: 'Malla',
            msg: 'LA FECHA DE APROBACIÓN DEBE SER INFERIOR A LA FECHA DESDE'
          });
          return false;
        }else if (this.dateTimeFechaDesde.value() > this.dateTimeFechaHasta.value()) {
          this.modal.alertMessage({
            title: 'Malla',
            msg: 'LA FECHA DESDE DEBE SER INFERIOR A LA FECHA HASTA'
          });
          return false;
        }
      }
    }
    else if (this.listPeriodosCheck.getCheckedItems().length < 1) {
      this.modal.alertMessage({
        title: 'Malla',
        msg: 'SELECCIONE AL MENOS UN PERIODO ACADEMICO '
      });
      return false;

    }

    return true;
  }

  protected submit() {
    if (this.validaDatos() == true) {
      this.generarJSON();
      this.modal.alertQuestion({
        title: 'Malla',
        msg: 'Desea grabar sus registros?',
        result: (k) => {
          if (k) {
            console.log(this.datosMalla)
            this.mallasService.grabarMalla(this.datosMalla).subscribe(result => {
              this.gotoList();
              this.formNuevo.hide();
            }, error => console.error(error));
          }
        }
      })
    }
    // else{
    //   this.modal.alertMessage({ title: 'Malla',  msg: ' ' });  
    // }

  }

  generarJSON() {
    this.datosMalla = {};
    this.datosMalla.id = this.opcion.id;
    this.datosMalla.descripcion = this.opcion.descripcion;
    this.datosMalla.idDepartamentoOferta = this.cmbOfertas.val();
    this.datosMalla.versionMalla = null;
    this.datosMalla.idDepartamentoOferta = this.idofertaMalla;
    this.datosMalla.idNivelMinAperturado = this.opcion.idNivelMinAperturado;
    this.datosMalla.idNivelMaxAperturado = this.opcion.idNivelMaxAperturado;
    this.datosMalla.idReglamento = this.opcion.idReglamento;
    this.datosMalla.numCreditos = this.opcion.numCreditos;
    this.datosMalla.numeroHorasMaximo = this.opcion.numeroHorasMaximo;
    this.datosMalla.numAsignaturas = this.opcion.numAsignaturas;
    this.datosMalla.numAsignaturasAprobar = this.opcion.numAsignaturasAprobar;
    this.datosMalla.numNiveles = this.opcion.numNiveles;
    this.datosMalla.fechaAprobacion = this.opcion.fechaAprobacion;
    this.datosMalla.fechaDesde = this.opcion.fechaDesde;
    this.datosMalla.fechaHasta = this.opcion.fechaHasta;
    this.datosMalla.estado = 'A';
    this.datosMalla.version = this.opcion.version;
    this.datosMalla.usuarioIngreso_id = 1;
    this.datosMalla.periodoMallaVersion = this.periodoMallaVersion;
    this.datosMalla.factorMultComponenteDocencia = this.opcion.factorMultComponenteDocencia
  }


  checkChange(event: any): void {

    //s let items = this.listPeriodosCheck.getCheckedItems();

    let value = event.args.value;
    if (event.args.checked) {
      let cont = 0
      if (this.periodoMallaVersion.length > 0) {
        for (let i = 0; i < this.periodoMallaVersion.length; i++) {
          if (value == this.periodoMallaVersion[i].idPeriodoAcademico) {
            this.periodoMallaVersion[i].estado = 'A'
            cont = cont + 1
          }
        }
        if (cont == 0) {
          this.periodoMallaVersion.push({ "id": null, "idPeriodoAcademico": value, "idMalla": this.opcion.id, "usuarioIngresoId": 1, "version": 0, "estado": 'A' });
        }
      } else {
        this.periodoMallaVersion.push({ "id": null, "idPeriodoAcademico": value, "idMalla": this.opcion.id, "usuarioIngresoId": 1, "version": 0, "estado": 'A' });
      }

      // eventsContainer.innerHTML = 'Checked: ' + event.args.label; 
    } else {
      for (let i = 0; i < this.periodoMallaVersion.length; i++) {
        if (value == this.periodoMallaVersion[i].idPeriodoAcademico && this.periodoMallaVersion[i].version > 0) {
          this.periodoMallaVersion[i].estado = 'I'
        } else {
          this.periodoMallaVersion.splice(i, 1)
        }
      }

    }
  };


  initialDate: Date = new Date(2019, 4, 1);
  sendButton(): void {
    this.myValidator.validate(document.getElementById('form'));
  }

  rules = [{
    input: '.userInput', message: 'Solo Numeros entre 1 y 10', action: 'keyup, blur',
    rule: (input: any, commit: any, event: any): any => {
      this.validaControles()
      if (this.resultado == 1) {
        return true;
      }
      return false;
    }
  },
  {
    input: '.birthInput', message: 'La fecha debe estar entre el 1/1/2010 and 1/1/2020.', action: 'valueChanged',
    rule: (input: any, commit: any): any => {
      let date = this.controlFecha.value();
      let result = date.getFullYear() >= 2010 && date.getFullYear() <= 2020;
      return result;
    }
  }
  ];

  validaControles() {
    if (this.inputNivelMin.val()) {
      this.ValControl = this.inputNivelMin.val();
      if (this.ValControl >= 1 && this.ValControl <= 10 && this.ValControl != "") {
        this.resultado = 1;
      }
      this.resultado = 0
    }
  }


  gotoList() {
    // this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
    this.router.navigate(['mallas/mallacarrera'],)//);
  }

  protected cancelar() {
    // this. gotoList(); 
    // this.sub.unsubscribe(); 
    this.formNuevo.hide();

  }

}
