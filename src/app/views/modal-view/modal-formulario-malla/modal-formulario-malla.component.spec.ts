import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFormularioMallaComponent } from './modal-formulario-malla.component';

describe('ModalFormularioComponent', () => {
  let component: ModalFormularioMallaComponent;
  let fixture: ComponentFixture<ModalFormularioMallaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFormularioMallaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFormularioMallaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
