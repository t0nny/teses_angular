import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { ModalComponentComponent } from '../../modal-view/modal-component/modal-component.component';
import { ValidadorService } from '../../../services/validacion/validador.service';
import { jqxButtonComponent } from 'jqwidgets-ng/jqxbuttons';
import { NgxExtendedPdfViewerComponent } from 'ngx-extended-pdf-viewer';
import { OpcionesAsignaturaService } from '../../../services/opciones-asignatura/opciones-asignatura.service';
import {Constants} from '../../../util/constants';

@Component({
  selector: 'app-bibliografia',
  templateUrl: './bibliografia.component.html',
  styleUrls: [
 // encapsulation: ViewEncapsulation.None
],
})
export class BibliografiaComponent implements OnInit {
  @ViewChild('gridBibliografias') gridBibliografias: jqxGridComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;
  @ViewChild('botonVolver') botonVolver: jqxButtonComponent;
  @ViewChild('myPdfViewer') myPdfViewer: NgxExtendedPdfViewerComponent;

  constructor(
    private router: Router, private route: ActivatedRoute,
    private opcionesAsignaturaService: OpcionesAsignaturaService,
    private validadorService: ValidadorService, ) { }


  pageinformation: any = {
    "page": "0",
    // "size":this.pagesize,
    "sortfield": "id",
    "direction": "DESC",
    "filterInformation": []
  };

  //Variable paa cargar datos del objeto 
  listaPacientes: Array<any>;
  rowindex: number = -1;
  banderaDepencia: boolean = false
  ocultarPdfViewer: boolean = true;
  ocultarGridReportes: boolean = false;
  pdfSrc: string = '';
  isCollapsed: boolean = false;

  ngOnInit() {
    this.listaTodasBibliografias()
  }

  ngAfterViewInit(): void {
    this.getStorageFormularioState();
  }

  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Editar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Eliminar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
  ];

  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };
  getAdapter(sourceMenu: any): any {
    return new jqx.dataAdapter(this.sourceMenu, { autoBind: true });
  };
  menuMatricula = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

  itemclick(event: any): void {
    // captura el id seleccionado
    const selectedrowindex = this.gridBibliografias.getselectedrowindex();
    const idRecursoBibliograficoSel = this.gridBibliografias.getcellvalue(selectedrowindex, 'idRecursoBibliografico');
    var opt = event.args.innerText;

    switch (opt) {
      case 'Nuevo':
        this.router.navigate(['planificacion-docente/editar-bibliografia', 0]);
        break;
      case 'Editar':
        if (idRecursoBibliograficoSel) {
          this.setFormularioState();
          this.router.navigate(['planificacion-docente/editar-bibliografia', idRecursoBibliograficoSel]);
        } else {
          this.myModal.alertMessage({ title: 'Registro de Recursos Bibliograficos', msg: 'Seleccione un Recurso Bibliográfico!' });
        }
        break;
      case 'Eliminar':
        if (idRecursoBibliograficoSel) {
          // if (this.banderaDepencia == false) {
            this.myModal.alertQuestion({
              title: 'Registro de Recursos Bibliograficos',
              msg: '¿Desea eliminar este registro?',
              result: (k) => {
                if (k) {
                  this.eliminarBibliografia(idRecursoBibliograficoSel)
                  this.myModal.alertMessage({ title: 'Registro de Recursos Bibliograficos', msg: 'Recurso Bibliográfico eliminado Correctamente!' });
                  this.gridBibliografias.clear()
                  this.gridBibliografias.clearselection()
                  this.listaTodasBibliografias()
                  this.gridBibliografias.refreshdata()
                }
              }
            })
          // } else {
          //   this.myModal.alertMessage({
          //     title: 'Registro de Recursos Bibliograficos',
          //     msg: 'No es posible eliminar este registro activo, por sus dependencias con otros registros!'
          //   });
          // }
        } else {
          this.myModal.alertMessage({ title: 'Registro de Recursos Bibliograficos', msg: 'Seleccione un Paciente!' });
        }
        break;
      default:
    }
  };

  eliminarBibliografia(idBibliografia: number) {
    this.opcionesAsignaturaService.borrarBibliografia(idBibliografia).subscribe(result => {
    }, error => console.error(error));
  }

  sourceBibliografia: any =
    {
      datatype: 'array',
      id: 'idRecursoBibliografico',
      datafields:
        [
          { name: 'idRecursoBibliografico', type: 'int' },
          { name: 'idTipoBibliografia', type: 'int' },
          { name: 'idAutor', type: 'int' },
          { name: 'idEditorial', type: 'int' },
          { name: 'edicion', type: 'int' },
          { name: 'titulo', type: 'string' },
          { name: 'descripcion', type: 'string' },
          { name: 'autor', type: 'string' },
          { name: 'tipoBibliografia', type: 'string' },
          { name: 'editorial', type: 'string' },
          { name: 'anioEdicion', type: 'string' },
          { name: 'isbn', type: 'string' },
        ],
      hierarchy:
      {
        keyDataField: { name: 'idRecursoBibliografico' },
        parentDataField: { name: 'padre_id' }
      }
    };
  dataAdapterBibliografia: any = new jqx.dataAdapter(this.sourceBibliografia);

  //metodo de reinderizado de filas del grid
  rendergridrows = (params: any): any[] => {
    return params.data;
  }

  listaTodasBibliografias() {
    this.opcionesAsignaturaService.getListaTodasBibliografias().subscribe(data => {
      this.listaPacientes = data;
      this.sourceBibliografia.localdata = data;
      this.dataAdapterBibliografia.dataBind();
      this.gridBibliografias.gotopage(this.pageinformation.page)
    });
  }

  columnsBibliografia: any[] =
    [
      { text: 'id RecursoBibliografico', datafield: 'idRecursoBibliografico', width: '5%', filtertype: 'none', hidden: true },
      { text: 'id TipoBibliografia', datafield: 'idTipoBibliografia', width: '5%', hidden: true, filtertype: 'none' },
      { text: 'Id Autor', datafield: 'idAutor', width: '5%', hidden: true, filtertype: 'none' },
      { text: 'id Editorial', datafield: 'idEditorial', width: '5%',  filtertype: 'none',cellsalign: 'left', left: 'left' , hidden: true},
      { text: 'Titulo', datafield: 'titulo', width: '20%', align: 'center' },
      { text: 'Descripcion', datafield: 'descripcion', width: '20%', align: 'center' },
      { text: 'Autor', datafield: 'autor', width: '15%', align: 'center'},
      { text: 'Editorial', datafield: 'editorial', width: '15%', align: 'center', cellsalign: 'center' },
      
      { text: 'Tipo Bibliografia', datafield: 'tipoBibliografia', width: '10%', align: 'center', cellsalign: 'center' },
      { text: 'Edicion', datafield: 'edicion', width: '5%', align: 'center', cellsalign: 'center' },
      { text: 'Año Edicion', datafield: 'anioEdicion', width: '5%', align: 'center', cellsalign: 'center' },
      { text: 'ISBN', datafield: 'isbn', width: '10%', align: 'center', cellsalign: 'center' },
    ];

  localization: any = Constants.getLocalization('es');

  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '90%';
    }
    return 850;
  }

  getEvents(event): void {
    if (event.val == "ok") {
      //this.save(this.distributivoDocente); 
    }
  }

  //graba el estado del grid y combox
  setFormularioState() {
    //Prepara estado de grabado del grid
    let gridState = JSON.stringify(this.gridBibliografias.savestate())
    this.pageinformation.page = JSON.parse(gridState).pagenum;
    localStorage.setItem('pageinformation', JSON.stringify(this.pageinformation));
    localStorage.setItem('gridPacientestate', gridState);
  }

  getStorageFormularioState() {
    if (localStorage.getItem('gridPacientestate')) {
      //carga el estado recuperado de los combobox y grid
      let gridState = JSON.parse(localStorage.getItem('gridPacientestate'));
      this.gridBibliografias.loadstate(gridState);
      this.pageinformation.page = JSON.stringify(gridState.pagenum)
      //recupera y asigana puntero fila del grid seleccionad
      this.gridBibliografias.gotopage(this.pageinformation.page)
      //borra la variable temporal de control de estados del grid
      localStorage.removeItem('gridPacientestate');
    }
  }

}
