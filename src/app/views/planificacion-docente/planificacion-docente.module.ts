import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanificacionDocenteRoutingModule } from './planificacion-docente-routing.module';
import { FormsModule } from '@angular/forms';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { OpcionesAsignaturaService } from '../../services/opciones-asignatura/opciones-asignatura.service';
import { BibliografiaComponent } from './bibliografia/bibliografia.component';
import { GlobalComponentModule } from '../global-component.module';

@NgModule({
  declarations: 
  [
    BibliografiaComponent,
  ],
  imports: 
  [
    CommonModule,
    PlanificacionDocenteRoutingModule,
    FormsModule,
    CommonModule,
    FormsModule,
    GlobalComponentModule
  ] ,providers: [OpcionesAsignaturaService]
})
export class PlanificacionDocenteModule { }


