import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ControlAutenticacion } from '../../security/control-autenticacion';
import { BibliografiaComponent } from './bibliografia/bibliografia.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Planificacion Docencia'
  },
  children: [{
    path: '',
    redirectTo: 'planificacion-docente',
    pathMatch: 'full'                                                                                                                                                                                        
  },
  {
    path: 'bibliografia',
    component: BibliografiaComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Listado de Recursos Bibliográficos'
    }
  },
  // {
  //   path: 'editar-bibliografia/:idRecursoBibliografico',
  //   component: EditarBibliografiaComponent,
  //   canActivate: [ControlAutenticacion],
  //   data: {
  //     title: 'Registro de Recursos Bibliográficos '
  //   }
  // },


  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanificacionDocenteRoutingModule { }