import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../../model/usuario'
import { AuthService } from '../../services/auth/auth.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
 })
export class LoginComponent implements OnInit {
  @Input() title = 'SIIA:Sistema Integrado de Información Académica UPSE';
  usuario: Usuario = new Usuario();

  @Input() mensajeNotificacion='Credenciales no válidas';
 // @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
 
  constructor(//public messagerService: MessagerService,
              private router: Router,
              private autenticacionService: AuthService) { }

  ngOnInit() {
    this.autenticacionService.logout();
  }

  gotoHome() {
   this.router.navigate(['/dashboard']);
  }

  login() {
    localStorage.setItem('usuarioActivo', JSON.stringify(this.usuario.usuario));
    
    localStorage.setItem('usuarioAct', this.usuario.usuario);
    this.autenticacionService.login(this.usuario.usuario, this.usuario.claveAuxiliar)
   
    .subscribe(data => {
      // Si se obtuvo el token lo almacena en memoria en notación JSON.

      localStorage.setItem('usuarioActual', JSON.stringify(data));
 

      this.gotoHome();
    }, error => {
      alert('Credenciales no válidas');
      /*  this.messagerService.alert({
        title: 'Credenciales no válidas',
        msg: 'Ingrese correctamente sus credenciales de acceso!'
       }); */
      console.log(error);
    });

  }

}
