import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DocentesService } from '../../../services/docentes/docentes.service';
import { ModalComponentComponent } from '../../modal-view/modal-component/modal-component.component';
import { jqxComboBoxComponent } from 'jqwidgets-ng/jqxcombobox';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import {Constants} from '../../../util/constants';

@Component({
  selector: 'app-docente-lista',
  templateUrl: './docente-lista.component.html',
  styleUrls: []
})
export class DocenteListaComponent implements OnInit {

  constructor(
    private router: Router, private route: ActivatedRoute,
    private DocentesService: DocentesService,
  ) { }

  @ViewChild('selectDepartamento', { read: false }) comboDepartamento: jqxComboBoxComponent;
  @ViewChild('selectOferta') comboOferta: jqxComboBoxComponent;
  @ViewChild('gridDocentesRegistrados') gridDocentesRegistrados: jqxGridComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;

  //Variable paa cargar datos del objeto 
  listaDocentes: Array<any>;
  periodo: Array<any>;
  facultad: Array<any>;
  carrera: Array<any>;
  rowindex: number = -1;
  idOferta: any;
  banderaDepencia: boolean;

  ngOnInit() {
    //this.ListarDepartamento();
    this.ListarAllDocentes();
  }

  ngAfterViewInit(): void {
    this.getStorageFormularioState();
  }



  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Editar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Eliminar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
  ];

  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };
  getAdapter(sourceMenu: any): any {
    return new jqx.dataAdapter(this.sourceMenu, { autoBind: true });
  };
  menu = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

  itemclick(event: any): void {
    // captura el id seleccionado
    const selectedrowindex = this.gridDocentesRegistrados.getselectedrowindex();
    const idPersonaSel = this.gridDocentesRegistrados.getcellvalue(selectedrowindex, 'idPersona');
    //let idComboOferta = this.comboOferta.val()
    var opt = event.args.innerText;

    switch (opt) {
      case 'Nuevo':
        this.router.navigate(['docentes/docente-edicion', 0]);
      case 'Editar':
        if (idPersonaSel != null) {
          console.log(idPersonaSel);
          this.router.navigate(['docentes/docente-edicion', idPersonaSel
            //,idComboOferta
          ]);
        } else {
          this.myModal.alertMessage({ title: 'Registro de Docentes', msg: 'Seleccione un Docente!' });
        }
        break;
      case 'Eliminar':
        if (idPersonaSel) {
          //if (this.banderaDepencia == false) {
          this.myModal.alertQuestion({
            title: 'Registro de docentes',
            msg: 'Desea eliminar este registro?',
            result: (k) => {
              if (k) {
                this.eliminarDocente(idPersonaSel)

                this.myModal.alertMessage({ title: 'Registro de Docentes', msg: 'Docente eliminado correctamente!' });
                this.gridDocentesRegistrados.clear()
                //this.gridDocentesRegistrados.clearselection()
                this.ListarAllDocentes();
                this.gridDocentesRegistrados.refreshdata();
                this.router.navigate(['/docentes/docente-lista']);

              }
            }
          })
          /*} /*else {
            this.myModal.alertMessage({
              title: 'Registro de Docentes',
              msg: 'No es posible eliminar este registro activo, por sus dependencias con otros registros!'
            });
          }*/
        } else {
          this.myModal.alertMessage({ title: 'Registro de Docentes', msg: 'Seleccione un Docentes!' });
        }

        break;
      default:
    }
  };



  ListarAllDocentes() {
    this.DocentesService.getListarTodosDocenteRegistrados().subscribe(data => {
      //alert(JSON.stringify(data));
      ;
      this.sourceDocentesRegistrados.localdata = data;
      this.dataAdapterDocentesRegistrados.dataBind();
    })

  }

  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceOferta: any =
    {
      datatype: 'json',
      id: 'idOferta',
      localdata:
        [
          { name: 'idOferta', type: 'int' },
          { name: 'oferta', type: 'string' },
          { name: 'estado', type: 'string' }

        ],
    };
  //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
  dataAdapterOferta: any = new jqx.dataAdapter(this.sourceOferta);

  sourceDocentesRegistrados: any =
    {
      datatype: 'array',
      id: 'idPersona',
      datafields:
        [
          { name: 'idPersona', type: 'int' },
          { name: 'identificacion', type: 'string' },
          { name: 'nombresCompletos', type: 'string' },
          { name: 'idDocente', type: 'int' },
          { name: 'emailInstitucional', type: 'string' },


        ],
      hierarchy:
      {
        keyDataField: { name: 'idPersona' },
        parentDataField: { name: 'padre_id' }
      }
    };
  dataAdapterDocentesRegistrados: any = new jqx.dataAdapter(this.sourceDocentesRegistrados);

  //metodo de reinderizado de filas del grid
  rendergridrows = (params: any): any[] => {
    return params.data;
  }

  columnsDocente: any[] =
    [
      { text: 'Id Persona', datafield: 'idPersona', width: '5%', filtertype: 'none', hidden: true },
      { text: 'Identificación', datafield: 'identificacion', width: '12%', cellsalign: 'center', center: 'center' },
      { text: 'Nombres', datafield: 'nombresCompletos', width: '40%' },
      { text: 'Email Institucional', datafield: 'emailInstitucional', width: '33%' },
      { text: 'Id Docente', datafield: 'idDocente', width: '5%', hidden: true, filtertype: 'none' },
      {
        text: 'Hoja de vida', columntype: 'button', width: '15%',
        cellsrenderer: (): string => {
          return 'Descargar';
        },
        buttonclick: (row: number): void => {
          let dataRecord = this.gridDocentesRegistrados.getrowdata(row);
          this.descargaHojaDeVida(dataRecord.idDocente);
        },

      },
    ];

    localization: any = Constants.getLocalization('es');

  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '90%';
    }
    return 850;
  }

  //Busca todas las oferta_distrbiutivo
  listarDocentes() {

    if (this.comboOferta.val() == "") {
      this.myModal.alertMessage({ title: 'Selección ', msg: 'Seleccione una facultad' });
      return;
    }

    this.DocentesService.getListarDocenteRegistrados(this.comboOferta.val()).subscribe(data => {

      this.listaDocentes = data;
      this.sourceDocentesRegistrados.localdata = data;
      this.dataAdapterDocentesRegistrados.dataBind();
    });
  }

  eliminarDocente(idPersona: number) {
    this.DocentesService.borrarDocente(idPersona).subscribe(result => {
    }, error => console.error(error));
  }

  getEvents(event): void {
    if (event.val == "ok") {

    }
  }

  //graba el estado del grid y combox
  setFormularioState() {
    //Prepara estado de grabado del grid
    let departamentoState = JSON.stringify(this.comboDepartamento.getSelectedItem());
    let ofertaState = JSON.stringify(this.comboOferta.getSelectedItem());
    let gridState = JSON.stringify(this.gridDocentesRegistrados.savestate())
    //graba en memoria estado de combobox y grid
    localStorage.setItem('cbxDepartamentoState', departamentoState);
    localStorage.setItem('cbxOfertaState', ofertaState);
    localStorage.setItem('gridDocenteState', gridState);
  }

  getStorageFormularioState() {
    if (localStorage.getItem('gridDocenteState')) {
      //recupera el estado del grid y combobox
      let departamentoState = JSON.parse(localStorage.getItem('cbxDepartamentoState'));
      let ofertaState = JSON.parse(localStorage.getItem('cbxOfertaState'));

      //carga el estado recuperado de los combobox y grid
      this.comboDepartamento.selectedIndex(departamentoState.index);
      this.comboOferta.selectedIndex(ofertaState.index);
      let gridState = JSON.parse(localStorage.getItem('gridDocenteState'));
      this.gridDocentesRegistrados.loadstate(gridState);
      //recupera y asigana puntero fila del grid seleccionada
      this.rowindex = gridState.selectedrowindex;
      //this.getselectedrowindex = gridState.selectedrowindex;
      //borra la variable temporal de control de estados del grid
      localStorage.removeItem('cbxDepartamentoState');
      localStorage.removeItem('cbxOfertaState');
      localStorage.removeItem('gridDocenteState');
      //muestra traza de ejecucion
      // this.loadedCharacter= gridState;
    }
  }


  //funcion que obtiene el reporte de pdf del reporte distributivo 
  descargaHojaDeVida(idDocente: number) {

    this.DocentesService.getReportHojaDeVida(idDocente).subscribe((blob: Blob) => {
      // Para navegadores de Microsoft.
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
      }
      let filename = "";
      filename = this.nombreArchivo();
      const objectUrl = window.URL.createObjectURL(blob);

      const enlace = document.createElement('a');
      enlace.href = objectUrl;
      enlace.download = objectUrl;
      enlace.setAttribute('download', filename);
      enlace.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        window.URL.revokeObjectURL(objectUrl);
      });
    });
  }

  nombreArchivo(): string {
    let filename = "";
    var fecha = new Date();
    filename = fecha.getHours() + "_" + fecha.getMinutes() + "_" + fecha.getDate() + "_" + fecha.getMonth() + "_" + fecha.getFullYear()
    return filename;
  }


}
