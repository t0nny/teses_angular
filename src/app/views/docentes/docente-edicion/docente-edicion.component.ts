import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalComponentComponent } from '../../modal-view/modal-component/modal-component.component';
import { jqxValidatorComponent } from 'jqwidgets-ng/jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { jqxInputComponent } from 'jqwidgets-ng/jqxinput';
import { ValidadorService } from '../../../services/validacion/validador.service';
import { jqxScrollViewComponent } from 'jqwidgets-ng/jqxscrollview';
import { jqxMaskedInputComponent } from 'jqwidgets-ng/jqxmaskedinput';
import { jqxTextAreaComponent } from 'jqwidgets-ng/jqxtextarea';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { DocentesService } from '../../../services/docentes/docentes.service';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import {Constants} from '../../../util/constants';

@Component({
  selector: 'app-docente-edicion',
  templateUrl: './docente-edicion.component.html',
  styleUrls: []
})


export class DocenteEdicionComponent implements OnInit {
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;
  @ViewChild('myValidator') myValidator: jqxValidatorComponent;
  @ViewChild('txtIdenficacion') txtIdenficacion: jqxInputComponent;
  @ViewChild('scrollViewFoto', { read: false }) scrollViewFoto: jqxScrollViewComponent;
  @ViewChild('txtApellidos') txtApellidos: jqxInputComponent;
  @ViewChild('txtNombres') txtNombres: jqxInputComponent;
  @ViewChild('dateFechaNace') dateFechaNace: jqxDateTimeInputComponent;
  @ViewChild('comboGenero') comboGenero: jqxDropDownListComponent;
  @ViewChild('masketTelefono') masketTelefono: jqxMaskedInputComponent;
  @ViewChild('masketCelular') masketCelular: jqxMaskedInputComponent;
  @ViewChild('txtEmailPersonal') txtEmailPersonal: jqxInputComponent;
  @ViewChild('txtEmailInstitucional') txtEmailInstitucional: jqxInputComponent;
  @ViewChild('txtAreaDireccion') txtAreaDireccion: jqxTextAreaComponent;
  @ViewChild('gridFormacionProfesional' ) gridFormacionProfesional:     jqxGridComponent;
  @ViewChild('gridDocenteHistorial' ) gridDocenteHistorial:     jqxGridComponent;
  @ViewChild('gridActualizacionProfesional' ) gridActualizacionProfesional:     jqxGridComponent;
  @ViewChild('checkBoxValCedula') checkBoxValCedula: jqxCheckBoxComponent;
  //@ViewChild(ModalFormacionProfesionalComponent)  formMantenedor: ModalFormacionProfesionalComponent;
  facultad: any;
  banderaExisteCedula: boolean;
  datosFormacionProfesional: Array<any>;
  universidades: Array<any>;
  niveles: Array<any>;
  areas: Array<any>;
  categorias: Array<any>;
  listaFormacionProfesionalEliminados: any=[];
  listaDocenteHistorialEliminados: any=[];
  listaActualizacionProfesionalEliminados: any=[];

  sub: Subscription;
  personaAng: any = {};
  banderaFormacion:number =0;
  banderaActualizacion:number =0;
  banderaHistorial:number =0;
  docente: any = {};
  idPersonaPar: number;
  
  labelOpcion: string;
  labelCarrera: string
  labelFacultad: string
  listaFormacionProfesional:  Array<any>;
  listaDocenteHistorial:  Array<any>;
  listaActualizacionProfesional:  Array<any>;
  fecha:any=null;
  algo:any;
  //variables usadas para recuperar los datos en la edición
  varFechaNacimiento: any = new Date();

  constructor(
    private router: Router, private route: ActivatedRoute,
    private docentesService: DocentesService,
    private validadorService: ValidadorService,
  ) { }

  
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.idPersonaPar = params['idPersona'];
      //this.idOfertaPar = params['idOferta'];
      if (this.idPersonaPar) {
        if (this.idPersonaPar != 0) {
          this.labelOpcion = 'Edición '
          this.buscarDocente(this.idPersonaPar)
          //this.recuperarMallaCarreraFacultad(this.idOfertaPar)
          this.listarFormacionProfesional(this.idPersonaPar)
          this.listarHistorialDocente(this.idPersonaPar)  
          this.listarActualizacionProfesional(this.idPersonaPar)         
        } else {
          this.labelOpcion = 'Registro '
          this.nuevoDocente()
          //this.recuperarMallaCarreraFacultad(this.idOfertaPar)
        }
        this.buscarUniversidades()
        this.buscarNiveles()
        this.buscarAreas()
        this.buscarDocenteCategorias()
      }
    });
  }

  data = [
    {
      'id': '1',
      'text': 'Grabar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'text': 'Cancelar',
      'id': '2',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },

  ];

  source1 =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.data
    };

  getAdapter(source1: any): any {
    return new jqx.dataAdapter(this.source1, { autoBind: true });
  };
  menus = this.getAdapter(this.source1).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

  gotoList() {
    this.router.navigate(['/docentes/docente-lista']);
  }

  itemclick(event: any): void {
    var opt = event.args.innerText;
    switch (opt) {
      case 'Grabar':
        this.myValidator.validate(document.getElementById('formDocente'));
        if (this.validaDatos() == true) {
          this.myValidator.hide();
          this.myModal.alertQuestion({
            title: 'Registro de Docentes',
            msg: 'Desea grabar este registro?',
            result: (k) => {
              if (k) {
                this.generarJsonFormacionDocente();
                this.generarJsonDocenteHistorial();  
                this.generarJsonActualizacionProfesional();    
                console.log(this.personaAng);
                this.docentesService.grabarDocente(this.personaAng).subscribe(result => {
                  this.gotoList();
                }, error => console.error(error)
                );
              }
            }
          })
        } else {
          this.myModal.alertMessage({
            title: 'Registro de Docentes',
            msg: 'Verifique que todos los campos esten llenados correctamente!'
          });
        }
        break;
      case 'Cancelar':
        this.myValidator.hide();
        this.gotoList();
        break; 
    }
  };

  nuevoDocente() {
    //PARA ARAMAR EL JASON PERSONA 
    this.personaAng.id = null
    this.personaAng.estado = "AC"
    this.personaAng.version = "0"
    //PARA ARAMAR EL JASON ESTUDIANTE 
    this.docente.id = 0
    this.docente.idPersona = null
    this.docente.estado = "A"
    this.docente.usuarioIngresoId = "1"
    this.docente.version = "0"
    this.personaAng.docente = this.docente;
  }

  buscarDocente(idPersona: number) {
    this.docentesService.getBuscarDocente(idPersona).subscribe(data => {
      if (data) {
        this.editarDocente(data)
      }    
    });
  }

  editarDocente(persona: any) {
    this.personaAng = persona;
    this.varFechaNacimiento = new Date(this.personaAng.fecha_nace);
    this.comboGenero.val(this.personaAng.genero);
    if (persona.docente) {
      this.docente = persona.docente;
      //this.recuperarMallaCarreraFacultad(this.idOfertaPar) 
    } else {
      this.personaAng.docente = this.docente;
    }
  }

  generarJsonFormacionDocente() {
    // genera el json distributivo docente
    let nuevaListaFormacionProfesional: any=[];
    this.personaAng.docente['idPersona'] = this.idPersonaPar;
    
    //insert en el json la tabla docente asignatura aprendizaje
    let gridFormacionProfesiona=this.gridFormacionProfesional.getrows();
    console.log(gridFormacionProfesiona)
    let tamano=gridFormacionProfesiona.length;
    if(tamano>0){
      for (let i = 0; i < tamano; i++) {
        let agregar : any={}
        let formacionProfesional: any = {};
        let formacionProfesionalEditar: any = {};
          if(gridFormacionProfesiona[i].idFormacionProfesional==0 ){
              formacionProfesional.id=0;
              formacionProfesional.idUniversidad=gridFormacionProfesiona[i].idUniversidad;
              formacionProfesional.idNivelAcademico= gridFormacionProfesiona[i].idNivelAcademico ;  
              formacionProfesional.idAreaConocimiento= gridFormacionProfesiona[i].idAreaConocimiento; 
              formacionProfesional.idDocente= this.personaAng.docente.id ; 
              formacionProfesional.fechaObtencion = gridFormacionProfesiona[i].fechaObtencion ; 
              formacionProfesional.titulo= gridFormacionProfesiona[i].titulo; 
              formacionProfesional.codigoSenescyt= gridFormacionProfesiona[i].codigoSenescyt ; 
              formacionProfesional.tiempoEstudio= gridFormacionProfesiona[i].tiempoEstudio ; 
              formacionProfesional.tituloPrincipal= gridFormacionProfesiona[i].tituloPrincipal ;
              formacionProfesional.version=0; 
              formacionProfesional.estado="A";
              formacionProfesional.usuarioIngresoId=1  ; 
              agregar = formacionProfesional;  
            }if(gridFormacionProfesiona[i].idFormacionProfesional!=0 ){
              for(let j = 0; j < this.listaFormacionProfesional.length; j++){
                if(this.listaFormacionProfesional[j].idFormacionProfesional==gridFormacionProfesiona[i].idFormacionProfesional){
                  formacionProfesionalEditar.id=gridFormacionProfesiona[i].idFormacionProfesional;
                  formacionProfesionalEditar.idUniversidad=gridFormacionProfesiona[i].idUniversidad;
                  formacionProfesionalEditar.idNivelAcademico=gridFormacionProfesiona[i].idNivelAcademico;
                  formacionProfesionalEditar.idAreaConocimiento=gridFormacionProfesiona[i].idAreaConocimiento; 
                  formacionProfesionalEditar.idDocente= gridFormacionProfesiona[i].idDocente ;
                  formacionProfesionalEditar.fechaObtencion =gridFormacionProfesiona[i].fechaObtencion ;
                  formacionProfesionalEditar.titulo= gridFormacionProfesiona[i].titulo;
                  formacionProfesionalEditar.codigoSenescyt=  gridFormacionProfesiona[i].codigoSenescyt ;
                  formacionProfesionalEditar.tiempoEstudio= gridFormacionProfesiona[i].tiempoEstudio ; 
                  formacionProfesionalEditar.usuarioIngresoId=1  ;  
                  formacionProfesionalEditar.tituloPrincipal= gridFormacionProfesiona[i].tituloPrincipal ;
                  formacionProfesionalEditar.estado= gridFormacionProfesiona[i].estado;
                  formacionProfesionalEditar.version=gridFormacionProfesiona[i].version; 
                  agregar = formacionProfesionalEditar;
                }
              }
        } 
        nuevaListaFormacionProfesional.push(agregar);
        agregar="";
      }
      let arreglo :any=[];
      let cabecera : any= {};
      arreglo = this.listaFormacionProfesionalEliminados;
      for(let i=0;i<arreglo.length;i++){
        nuevaListaFormacionProfesional.push(arreglo[i])
      } 
      cabecera=nuevaListaFormacionProfesional;
      this.personaAng.docente.formacionProfesionales=cabecera;
    }
  }

  generarJsonDocenteHistorial() {
    // genera el json Docente Historial
    let nuevoDocenteHistorial: any=[];
    this.personaAng.docente['idPersona'] = this.idPersonaPar;
    
    //insert en el json la tabla docente asignatura aprendizaje
    let gridDocenteHistoria=this.gridDocenteHistorial.getrows();
    let tamano=gridDocenteHistoria.length;
    if(tamano>0){
      for (let i = 0; i < tamano; i++) {
        let agregar : any={};
        let docenteHistorialNuevo: any = {};
        let docenteHistorialEdita: any = {};
        if(gridDocenteHistoria[i].id==0 ){
            docenteHistorialNuevo.id=0;
            docenteHistorialNuevo.idDocente=this.personaAng.docente.id 
            docenteHistorialNuevo.idDocenteCategoria= gridDocenteHistoria[i].idDocenteCategoria ;  
            docenteHistorialNuevo.fechaDesde = gridDocenteHistoria[i].fechaDesde; 
            docenteHistorialNuevo.fechaHasta= gridDocenteHistoria[i].fechaHasta; 
            docenteHistorialNuevo.observacion= gridDocenteHistoria[i].observacion ; 
            docenteHistorialNuevo.version=0; 
            docenteHistorialNuevo.estado="A";
            docenteHistorialNuevo.usuarioIngresoId=1  ; 
            agregar = docenteHistorialNuevo;
            
          }if(gridDocenteHistoria[i].id!=0 ){
            for(let j = 0; j < this.listaDocenteHistorial.length; j++){       
              if(this.listaDocenteHistorial[j].id=gridDocenteHistoria[i].id){
                docenteHistorialEdita.id=gridDocenteHistoria[i].id;
                docenteHistorialEdita.idDocente=gridDocenteHistoria[i].idDocente;
                docenteHistorialEdita.idDocenteCategoria=gridDocenteHistoria[i].idDocenteCategoria;
                docenteHistorialEdita.fechaDesde =gridDocenteHistoria[i].fechaDesde;
                docenteHistorialEdita.fechaHasta= gridDocenteHistoria[i].fechaHasta;
                docenteHistorialEdita.observacion=  gridDocenteHistoria[i].observacion;
                docenteHistorialEdita.usuarioIngresoId=1  ;  
                docenteHistorialEdita.estado=gridDocenteHistoria[i].estado;
                docenteHistorialEdita.version=gridDocenteHistoria[i].version; 
                agregar = docenteHistorialEdita;
                
              }
            }
      } 
      nuevoDocenteHistorial.push(agregar);
      agregar="";
    }
    let arreglo :any=[];
    let cabecera : any= {};
    cabecera=nuevoDocenteHistorial;
    arreglo = this.listaDocenteHistorialEliminados;
    for(let i=0;i<arreglo.length;i++){
      nuevoDocenteHistorial.push(arreglo[i])
    } 
    cabecera=nuevoDocenteHistorial;
    this.personaAng.docente.docentesHistorial=cabecera;
    }
  }

  generarJsonActualizacionProfesional() {
    // genera el json Actualizacion Profesional
    let nuevoActualizacionProfesional: any=[];
    let actualizacionProfesionalNuevo: any = {};
    let actualizacionProfesionalEdita: any = {};
    this.personaAng.docente['idPersona'] = this.idPersonaPar;
    //insert en el json la tabla docente asignatura aprendizaje
    let gridActualizacionProfesiona=this.gridActualizacionProfesional.getrows();
    let tamano=gridActualizacionProfesiona.length;
    if(tamano>0){
      for (let i = 0; i < tamano; i++) {
        if(gridActualizacionProfesiona[i].codigo==undefined) {
          
          if(gridActualizacionProfesiona[i].tipoCapacitacion=='Profesional'){
            gridActualizacionProfesiona[i].codigo='PRO'}else{
            gridActualizacionProfesiona[i].codigo='PED'
          }
        }
          
        
        let agregar : any={};
        if(gridActualizacionProfesiona[i].id==0 ){
            actualizacionProfesionalNuevo.id=0;
            actualizacionProfesionalNuevo.idDocente=this.personaAng.docente.id 
            actualizacionProfesionalNuevo.idAreaConocimiento= gridActualizacionProfesiona[i].idAreaConocimiento ; 
            //actualizacionProfesionalNuevo.idDocente=gridActualizacionProfesiona[i].idDocente; 
            actualizacionProfesionalNuevo.fechaDesde = gridActualizacionProfesiona[i].fechaDesde; 
            actualizacionProfesionalNuevo.fechaHasta= gridActualizacionProfesiona[i].fechaHasta; 
            actualizacionProfesionalNuevo.institucion = gridActualizacionProfesiona[i].institucion ; 
            actualizacionProfesionalNuevo.nombreCurso = gridActualizacionProfesiona[i].nombreCurso ; 
            actualizacionProfesionalNuevo.tipoEvento = gridActualizacionProfesiona[i].tipoEvento ; 
            actualizacionProfesionalNuevo.tipoCertificado = gridActualizacionProfesiona[i].tipoCertificado ; 
            actualizacionProfesionalNuevo.numeroDias = gridActualizacionProfesiona[i].numeroDias ; 
            actualizacionProfesionalNuevo.numeroHoras = gridActualizacionProfesiona[i].numeroHoras ; 
            actualizacionProfesionalNuevo.tipoCapacitacion = gridActualizacionProfesiona[i].codigo ; 
            actualizacionProfesionalNuevo.version=0; 
            actualizacionProfesionalNuevo.estado="A";
            actualizacionProfesionalNuevo.usuarioIngresoId=1  ; 
            agregar = actualizacionProfesionalNuevo;
            actualizacionProfesionalNuevo={}

          }if(gridActualizacionProfesiona[i].id!=0 ){
            for(let j = 0; j < this.listaActualizacionProfesional.length; j++){       
              if(this.listaActualizacionProfesional[j].id=gridActualizacionProfesiona[i].id){
                actualizacionProfesionalEdita.id=gridActualizacionProfesiona[i].id;
                actualizacionProfesionalEdita.idDocente=this.personaAng.docente.id 
                actualizacionProfesionalEdita.idAreaConocimiento=gridActualizacionProfesiona[i].idAreaConocimiento;
                actualizacionProfesionalEdita.fechaDesde =gridActualizacionProfesiona[i].fechaDesde;
                actualizacionProfesionalEdita.fechaHasta= gridActualizacionProfesiona[i].fechaHasta;
                actualizacionProfesionalEdita.institucion=  gridActualizacionProfesiona[i].institucion;
                actualizacionProfesionalEdita.nombreCurso =gridActualizacionProfesiona[i].nombreCurso;
                actualizacionProfesionalEdita.tipoEvento= gridActualizacionProfesiona[i].tipoEvento;
                actualizacionProfesionalEdita.tipoCertificado=  gridActualizacionProfesiona[i].tipoCertificado;
                actualizacionProfesionalEdita.tipoCapacitacion=  gridActualizacionProfesiona[i].codigo;
                actualizacionProfesionalEdita.numeroDias=  gridActualizacionProfesiona[i].numeroDias;
                actualizacionProfesionalEdita.numeroHoras=  gridActualizacionProfesiona[i].numeroHoras;
                actualizacionProfesionalEdita.usuarioIngresoId=1  ;  
                actualizacionProfesionalEdita.estado=gridActualizacionProfesiona[i].estado;
                actualizacionProfesionalEdita.version=gridActualizacionProfesiona[i].version; 
                agregar = actualizacionProfesionalEdita;
                actualizacionProfesionalEdita={}
              }
            }
          } 
        nuevoActualizacionProfesional.push(agregar);
        agregar="";
      }
      let arreglo :any=[];
      let cabecera : any= {};
      cabecera=nuevoActualizacionProfesional;
      arreglo = this.listaActualizacionProfesionalEliminados;
      for(let i=0;i<arreglo.length;i++){
        nuevoActualizacionProfesional.push(arreglo[i])
      } 
      cabecera=nuevoActualizacionProfesional;
      //no olvidar de agregar
      this.personaAng.docente.actualizacionProfesionales=cabecera;
    }
  }

  ChangeInputIdenficacion(event: any): void {
    this.getPersonaByCedula('2400245286')
  }

  getPersonaByCedula(cedula: string) {
    this.docentesService.getBuscarDocentePorCedula(cedula).subscribe(data => {  
      if (data) {  
        this.banderaExisteCedula = true
        this.myModal.alertMessage({
          title: 'Registro de Docentes',
          msg: 'Ya existe un estudiante con ese número de cédula!'
        });
      } else {
        this.banderaExisteCedula = false
      }
    });
  }

  valueChangedFechaNacimiento() {
    let fechaNacimiento: Date
    fechaNacimiento = this.dateFechaNace.value()
    this.varFechaNacimiento = fechaNacimiento
    if (this.personaAng) {
      this.personaAng.fecha_nace = fechaNacimiento
    }
  }

  sourceGenero = [{ "genero": "Masculino", "codigo": 'M' }, { "genero": "Femenino", "codigo": 'F' }];

  sourceTipoCapacitacion = [{ "tipoCapacitacion": "Profesional", "codigo": 'PRO' }, { "tipoCapacitacion": "Pedagógico", "codigo": 'PED' }];


  /*recuperarMallaCarreraFacultad(idOferta) {
    const id = idOferta;
    this.docentesService.getRecuperarMallaCarreraFacultad(idOferta).subscribe(data => {
      if (data.length > 0) {
       
        this.labelCarrera = data[0].carrera;
        this.labelFacultad = data[0].facultad;
      }
    })
  }*/

  //Busca todas las oferta_distrbiutivo
  listarFormacionProfesional(idPersonaPar:number) {
    this.docentesService.getListaFormacionProfesional(idPersonaPar).subscribe(data => {
    this.listaFormacionProfesional = data;
    this.sourceFormacionProfesional.localdata = data;
    this.dataAdapterFormacionProfesional.dataBind();    });  }


  //Formacion Profesional
  sourceFormacionProfesional: any =
    {
      datatype: 'array',
      id: 'idFormacionProfesional',
      datafields:
        [
          { name: 'idFormacionProfesional', type: 'int' },
          { name: 'idDocente', type: 'int' },
          { name: 'idPersona', type: 'int' },
          { name: 'idUniversidad', type: 'int' },
          { name: 'universidad', type: 'string' },
          { name: 'idPais', type: 'int' },
          { name: 'pais', type: 'string' },
          { name: 'titulo', type: 'string' },
          { name: 'idNivelAcademico', type: 'int' },
          { name: 'nivelAcademico', type: 'string' },
          { name: 'idAreaConocimiento', type: 'int' },
          { name: 'areaConocimiento', type: 'string' },
          { name: 'tiempoEstudio', type: 'int' },
          { name: 'fechaObtencion', type: 'date' },
          { name: 'codigoSenescyt', type: 'string' },
          { name: 'version', type: 'int' },
          { name: 'estado', type: 'string' },
          { name: 'tituloPrincipal', type: 'bool' },
        ],
      hierarchy:
      {
        keyDataField: { name: 'idFormacionProfesional' },
        parentDataField: { name: 'padre_id' }
      }
    };
  dataAdapterFormacionProfesional: any = new jqx.dataAdapter(this.sourceFormacionProfesional);
  
  columnsFormacionProfesional: any[] =
    [
      { text: 'Id Formacion Profesional', datafield: 'idFormacionProfesional', width: '5%', filtertype: 'none', hidden: true },
      { text: 'Id Docente', datafield: 'idDocente', width: '5%', cellsalign: 'center', center: 'center', hidden: true },
      { text: 'Id Persona', datafield: 'idPersona', width: '5%', hidden: true },
      { text: 'Universidad', datafield: 'idUniversidad',displayfield: 'universidad', width: '20%', cellsalign: 'left', center: 'center' , columntype: 'dropdownlist',
        enableHover: true,
        initeditor: (row: number, _value: any, editor: any): void => { 
          editor.jqxDropDownList({ autoOpen: true, autoDropDownHeight:false , source: this.dataAdapterUniversidades, displayMember: 'universidad', 
          valueMember: 'idUniversidad', animationType: 'slide', enableHover: true,autoItemsHeight:true, 
          });
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
          this.gridFormacionProfesional.hiderowdetails(row);
        }, 
      },
      { text: 'Id Pais', datafield: 'idPais', width: '5%', hidden: true, filtertype: 'none' },
      { text: 'Pais', datafield: 'pais', width: '10%', cellsalign: 'center', center: 'center',hidden: true  },
      { text: 'Titulo Obtenido', datafield: 'titulo', width: '10%', cellsalign: 'center', center: 'center' ,autoItemsHeight:true,columntype: '', },
      { text: 'Nivel Académico', datafield: 'idNivelAcademico',displayfield: 'nivelAcademico', width: '15%', cellsalign: 'center', center: 'center', columntype: 'dropdownlist', 
      enableHover: true,
        initeditor: (row: number, _value: any, editor: any): void => { 
          editor.jqxDropDownList({  autoDropDownHeight:false , source: this.dataAdapterNiveles, displayMember: 'nivelAcademico',
          valueMember: 'idNivelAcademico', animationType: 'slide', enableHover: true,autoItemsHeight:true
          });
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
          this.gridFormacionProfesional.hiderowdetails(row);
        },   
      },
      { text: 'Area de Conocimiento', datafield: 'idAreaConocimiento',displayfield: 'areaConocimiento', width: '15%', cellsalign: 'center', center: 'center', columntype: 'dropdownlist',
      enableHover: true,autoItemsHeight:true,
        initeditor: (row: number, _value: any, editor: any): void => { 
          editor.jqxDropDownList({ autoDropDownHeight:false , source: this.dataAdapterAreas, displayMember: 'areaConocimiento',
          valueMember: 'idAreaConocimiento', animationType: 'slide', enableHover: true,autoItemsHeight:true,
          });
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
          this.gridFormacionProfesional.hiderowdetails(row);
        }, 
      },
      { text: 'Fecha de Obtencion', datafield: 'fechaObtencion', width: '12%', cellsalign: 'center', center: 'center', columntype: 'datetimeinput',  cellsformat: 'dd/MM/yyyy', 
          validation: (cell: any, value: Date): any => {
            let max:any = new Date();
            if (value > max  ) {
                this.banderaFormacion = 1;
                return { result: false, message: 'Fecha no admitida' };
                
            }
            return true;
          },
          cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
            
            
            this.gridFormacionProfesional.hiderowdetails(row);
          },
      },
      { text: 'Codigo Senescyt', datafield: 'codigoSenescyt', width: '10%',cellsalign: 'left', filtertype: 'none',autoItemsHeight:true },
      { text: 'Tiempo de Estudio', datafield: 'tiempoEstudio', width: '7%',cellsalign: 'left', filtertype: 'none' ,autoItemsHeight:true, columntype: 'numberinput',
          validation: (cell: any, value: number): any => {
              if (value < 6 || value > 100) {
                this.banderaFormacion = 1;
                return { result: false, message: 'El tiempo de estudio debe ser en un intervalo de 6-100 meses' };
              }
              return true;
          },
          createeditor: (row: number, cellvalue: any, editor: any): void => {
              editor.jqxNumberInput({ decimalDigits: 0, digits: 3 });
          }
      },
      { text: 'Principal', datafield: 'tituloPrincipal',name:"principal", columntype: 'checkbox', width: '5%',cellsalign: 'center',
          validation: (cell: any, value: boolean): any => {
            if (value == true) {
              console.log("Aqui 1:"+this.gridFormacionProfesional.getrows());
              return { result: false, message: 'Debe marca un solo registro' };
            }
            return true;
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
          //console.log(this.gridFormacionProfesional.getrows()); 
          let bandera=0;
          for(let i=0; i<this.gridFormacionProfesional.getrows().length;i++){
            console.log(this.gridFormacionProfesional.getrowdata(i).tituloPrincipal);
            if(this.gridFormacionProfesional.getrowdata(i).tituloPrincipal==true){
              bandera=bandera+1;
            }
            if(bandera>=1){
              console.log("Mas de uno seleccionado,posicion: " +i );
              this.gridFormacionProfesional.setcellvalue(i,'tituloPrincipal',false);
            }
          }
          
          this.gridFormacionProfesional.hiderowdetails(row);
        }, 
        
        
      },
      { text: 'Version', datafield: 'version', width: '5%',cellsalign: 'left', filtertype: 'none' ,hidden: true },
      { text: 'Estado', datafield: 'estado', width: '5%',cellsalign: 'left', filtertype: 'none' ,hidden: true },
      { text: 'Accion', datafield: 'quitar', columntype: 'button', width: '6%', //disabled: this.rowEditAsig,
        buttonclick: (row: number): void => {
          let selectedrowindex = this.gridFormacionProfesional.getselectedrowindex();
          let rowscount = this.gridFormacionProfesional.getdatainformation().rowscount;
          let id = this.gridFormacionProfesional.getrowid(selectedrowindex);
        
          //datos para eliminar formacion docente
          let agregarEliminados : any={};    
          let formacionProfesionalEliminar: any = {}; 
          formacionProfesionalEliminar.id=this.gridFormacionProfesional.getrowdata(selectedrowindex).idFormacionProfesional;
          formacionProfesionalEliminar.idUniversidad=this.gridFormacionProfesional.getrowdata(selectedrowindex).idUniversidad;
          formacionProfesionalEliminar.idNivelAcademico=this.gridFormacionProfesional.getrowdata(selectedrowindex).idNivelAcademico;
          formacionProfesionalEliminar.idAreaConocimiento=this.gridFormacionProfesional.getrowdata(selectedrowindex).idAreaConocimiento; 
          formacionProfesionalEliminar.idDocente= this.gridFormacionProfesional.getrowdata(selectedrowindex).idDocente ;
          formacionProfesionalEliminar.fechaObtencion =this.gridFormacionProfesional.getrowdata(selectedrowindex).fechaObtencion ;
          formacionProfesionalEliminar.titulo= this.gridFormacionProfesional.getrowdata(selectedrowindex).titulo;
          formacionProfesionalEliminar.codigoSenescyt=  this.gridFormacionProfesional.getrowdata(selectedrowindex).codigoSenescyt ;
          formacionProfesionalEliminar.tiempoEstudio=this.gridFormacionProfesional.getrowdata(selectedrowindex).tiempoEstudio ; 
          formacionProfesionalEliminar.usuarioIngresoId=this.gridFormacionProfesional.getrowdata(selectedrowindex).usuarioIngresoId ;  
          formacionProfesionalEliminar.version=this.gridFormacionProfesional.getrowdata(selectedrowindex).version; 
          formacionProfesionalEliminar.estado="I";
          agregarEliminados =  formacionProfesionalEliminar;
          //agregar a lista de eliminados
          this.listaFormacionProfesionalEliminados.push(agregarEliminados);
          //elimina fila de formacion profesional
          this.gridFormacionProfesional.deleterow(id);
        }, cellsrenderer: (): String => {
          return 'Quitar';
        }
      },
    ];

    rowEditAsig = (row: number, datafield: string, columntype: any, value: any): void | boolean => {
      let dataRecord = this.gridFormacionProfesional.getrowdata(row)
      let detalle=dataRecord.detalle
      
     
      let columnAsignatura = this.gridFormacionProfesional.getcolumn(datafield).text;
      let columnParalelo = this.gridFormacionProfesional.getcolumn(datafield).text;
     
      if (columnAsignatura == 'Asignatura' ) {
        this.gridFormacionProfesional.setcellvalue(row,'idParalelo','')
        this.gridFormacionProfesional.setcellvalue(row,'numEstudiantes','')
        this.gridFormacionProfesional.hiderowdetails(row);
        
      }else if (columnParalelo=="Paralelo"){
        this.gridFormacionProfesional.setcellvalue(row,'numEstudiantes','')
        this.gridFormacionProfesional.hiderowdetails(row);
      }
     if (detalle=='ocultar detalle'){
      let column = this.gridFormacionProfesional.getcolumn('detalle');
      this.gridFormacionProfesional.setcellvalue(row, column.displayfield, 'ver detalle')
     }
    }

    localization: any = Constants.getLocalization('es');

  //Reglas de validación formulario
  rules =
    [
      { input: '.identificacionInput', message: 'Identificacion requerida!', action: 'keyup, blur', rule: 'required' },
      {
        input: '.identificacionInput', message: 'Identificación incorrecta',
        action: 'keyup, blur, change',
        rule: (input: any, commit: any): any => {
          return (this.validarCedulaPasaporte(this.txtIdenficacion.val()))
        }
      },
      { input: '.nombresInput', message: 'Nombres requeridos!', action: 'keyup, blur', rule: 'required' },
      { input: '.nombresInput', message: 'Nombres deben contener solo letras!', action: 'keyup', rule: 'notNumber' },
      { input: '.nombresInput', message: 'Nombres deben tener entre 3 e 30 caracteres!', action: 'keyup', rule: 'length=2,30' },
      { input: '.apellidosInput', message: 'Apellidos requeridos!', action: 'keyup, blur', rule: 'required' },
      { input: '.apellidosInput', message: 'Apellidos deben contener solo letras!', action: 'keyup', rule: 'notNumber' },
      { input: '.apellidosInput', message: 'Apellidos deben tener entre 3 e 30 caracteress!', action: 'keyup', rule: 'length=2,30' },
      {
        input: '.fecha_naceInput', message: 'La edad debe ser entre 20 y 90 años.', action: 'valueChanged',
        rule: (input: any, commit: any): any => {
          return (this.validaFechaNacimiento(this.dateFechaNace.value()));
        }
      },
      {
        input: '.generoInput', message: 'Seleccione Género', action: 'change',
        rule: (input: any, commit: any): any => {
          let selectedIndex = this.comboGenero.selectIndex
          if (selectedIndex) { return selectedIndex; };
        }
      },
      { input: '.email_personalInput', message: 'E-mail requerido!', action: 'keyup, blur', rule: 'required' },
      { input: '.email_personalInput', message: 'E-mail incorrecto!', action: 'keyup', rule: 'email' },
      { input: '.email_institucionalInput', message: 'E-mail requerido!', action: 'keyup, blur', rule: 'required' },
      { input: '.email_institucionalInput', message: 'E-mail incorrecto!', action: 'keyup', rule: 'email' },
      {
        input: '.telefonoInput', message: 'Ingrese 9 dígitos, utilice código de area ejemplo 042787123!', action: 'valuechanged, blur',
        rule: (input: any, commit: any): any => {
          return (this.validadorService.validaTelefono(this.masketTelefono.val()));
        }
      },
      {
        input: '.celularInput', message: 'Celular incorrecto! Ingrese 10 dígitos', action: 'valuechanged, blur',
        rule: (input: any, commit: any): any => {
          return (this.validadorService.validaCelular(this.masketCelular.val()));
        }
      },
      {
        input: '.ciudadInput', message: 'Ciudad requerida', action: 'keyup, blur',
        rule: (input: any, commit: any): any => {
          return (this.validadorService.validaDireccion(this.txtAreaDireccion.val()));
        }
      },
      {
        input: '.direccionInput', message: 'Dirección requerida! Sector, Avenida, Calle, referencia', action: 'keyup, blur',
        rule: (input: any, commit: any): any => {
          return (this.validadorService.validaDireccion(this.txtAreaDireccion.val()));
        }
      },
    ];

  validarCedulaPasaporte(cedula: string) {
    var esValido = true;
    var valCed = this.checkBoxValCedula.val();
    if (valCed == false) {
      if (!this.validadorService.validaCedula(cedula)) {
        esValido = false;
        this.myModal.alertMessage({
          title: 'Cédula incorrecta',
          msg: 'Ingrese correctamente la cédula!'
        });
      } 
    }
    if (valCed == true) {
      if (!this.validadorService.validaPasaporte(cedula)) {
        esValido = false;
        this.myModal.alertMessage({
          title: 'Pasaporte incorrecto',
          msg: 'Ingrese correctamente el pasaporte!'
        });
      }
    }
    return esValido;
  }

  validaDatos(): boolean {
    let valido = true
    //valida cedula o pasaporte
    if (!this.validarCedulaPasaporte(this.txtIdenficacion.val())) {
      valido = false
      return valido
    }
    //valida Apellidos
    if (!this.validadorService.validaApellidos(this.txtApellidos.val())) {
      valido = false
      return valido
    }
    //valida fecha de nacimiento
    if (!this.validaFechaNacimiento(this.dateFechaNace.value())) {
      valido = false
      return valido
    }
    //valida mails
    if (!this.validadorService.validaMails(this.txtEmailPersonal.val())) {
      valido = false
      return valido
    }
    //valida telefono
    if (!this.validadorService.validaTelefono(this.masketTelefono.val())) {
      valido = false
      return valido
    }
    //valida celular
    if (!this.validadorService.validaCelular(this.masketCelular.val())) {
      valido = false
      return valido
    }
    //valida direccion
    if (!this.validadorService.validaDireccion(this.txtAreaDireccion.val())) {
      valido = false
      return valido
    }
    return valido
  }

  validaFechaNacimiento(fecha: any) {
    //valida edad entre 16 y 70 años
    let fecha_actual = new Date();
    let yearTo = fecha_actual.getFullYear() - 20;
    let yearFrom = fecha_actual.getFullYear() - 90;
    let date = new Date(fecha)
    let result = date.getFullYear() >= yearFrom && date.getFullYear() <= yearTo;
    return result;
  }

generaterow(): any {
    let row = {};
    let productindex = 
    row['idFormacionProfesional'] = 0;
    return row;
}

generaterow2(): any {
  let row = {};
  let productindex = 
  row['id'] = 0;
   return row;
}

generaterow3(): any {
  let row = {};
 // let productindex = 
  //row['id'] = 0;
  return row;
}
  //prueba de menu grid
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer1 = document.createElement('div');
    buttonContainer1.id = 'buttonContainer1';
    buttonContainer1.style.cssText = 'float: left; margin-left: 5px';
    container.appendChild(buttonContainer1);
    toolbar[0].appendChild(container);
    let addRowButton = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: 100, value: 'Agregar',theme:'darkblue' });
    addRowButton.addEventHandler('click', () => {
        let datarow = this.generaterow();
        let gridFormacionProfesional = this.gridFormacionProfesional.getrows();
        let getSize = gridFormacionProfesional.length;
        this.gridFormacionProfesional.selectrow(getSize-1);
     
        if(getSize==0){
           //Agrega una nueva fila
           this.gridFormacionProfesional.addrow(null, datarow);
        }else{
       
          if(this.gridFormacionProfesional.getrowdata(getSize-1).idNivelAcademico >= 0 && this.gridFormacionProfesional.getrowdata(getSize-1).idUniversidad >= 0  &&  
          this.gridFormacionProfesional.getrowdata(getSize-1).idAreaConocimiento >=0 && this.gridFormacionProfesional.getrowdata(getSize-1).tiempoEstudio>=6 &&
          this.gridFormacionProfesional.getrowdata(getSize-1).tiempoEstudio<100 &&
          this.gridFormacionProfesional.getrowdata(getSize-1).fechaObtencion!=null && this.gridFormacionProfesional.getrowdata(getSize-1).codigoSenescyt!=null &&
          this.gridFormacionProfesional.getrowdata(getSize-1).titulo!=null
          ){
            if(this.banderaFormacion==0){
              //Agrega una nueva fila
              this.gridFormacionProfesional.addrow(null, datarow);
             
            }else{
              this.banderaFormacion=1;
            }
            
          }else{
            this.myModal.alertMessage({title: 'Formacion Profesional ',msg: 'Registro vacio o incompleto.'});
          return;}
       }
       this.banderaFormacion=0
    })
};

  buscarUniversidades(){
    this.docentesService.getAllUniversidades().subscribe(data => {
      this.universidades = data;
      this.sourceUniversidades.localdata = data;
      this.dataAdapterUniversidades.dataBind();  });  }

  buscarNiveles(){
    this.docentesService.getAllNiveles().subscribe(data => {
      this.niveles = data;
      this.sourceNiveles.localdata = data;
      this.dataAdapterNiveles.dataBind();       });  }

  buscarAreas(){
    this.docentesService.getAllAreas().subscribe(data => {
      this.areas = data;
      this.sourceAreas.localdata = data;
      this.dataAdapterAreas.dataBind();         });   }

  buscarDocenteCategorias(){
    this.docentesService.getAllDocentesCategorias().subscribe(data => {
      this.categorias = data;
      this.sourceCategorias.localdata = data;
      this.dataAdapterCategorias.dataBind();   });    }

  sourceUniversidades: any =
  {
      datatype: 'json',
      id: 'id',
      datafields:
      [  
          { name: 'idUniversidad',  type: 'int' },
          { name: 'universidad', type: 'string' }, 
      ]
  };
  //adaptador para enlace de datos del grid
  dataAdapterUniversidades: any = new jqx.dataAdapter(this.sourceUniversidades);

  sourceNiveles: any =
  {
      datatype: 'json',
      id: 'id',
      datafields:
      [  
          { name: 'idNivelAcademico',  type: 'int' },
          { name: 'nivelAcademico', type: 'string' },
      ]
  };
  //adaptador para enlace de datos del grid
  dataAdapterNiveles: any = new jqx.dataAdapter(this.sourceNiveles);

  sourceAreas: any =
  {
      datatype: 'json',
      id: 'id',
      datafields:
      [  
          { name: 'idAreaConocimiento',  type: 'int' },
          { name: 'areaConocimiento', type: 'string' },
      ]
  };
  //adaptador para enlace de datos del grid
  dataAdapterAreas: any = new jqx.dataAdapter(this.sourceAreas);

  sourceCategorias: any =
  {
      datatype: 'json',
      id: 'id',
      datafields:
      [  
          { name: 'idDocenteCategoria',  type: 'int' },
          { name: 'docenteCategoria', type: 'string' },
          //{ name: 'tipoRelacionLaboral', type: 'string' },
      ]
  };
  //adaptador para enlace de datos del grid
  dataAdapterCategorias: any = new jqx.dataAdapter(this.sourceCategorias);

  //prueba de menu grid
  createButtonsContainers = (statusbar: any): void => {
    let container2 = document.createElement('div');
    container2.style.margin = '5px';
    let buttonContainer3 = document.createElement('div');
    buttonContainer3.id = 'buttonContainer3';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
    container2.appendChild(buttonContainer3);
    statusbar[0].appendChild(container2);
    let addRowButton1 = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 100, value: 'Agregar',theme:'darkblue' });
    addRowButton1.addEventHandler('click', () => {
        let datarow = this.generaterow2();
        let gridDocenteHistorial = this.gridDocenteHistorial.getrows();
        let getSize = gridDocenteHistorial.length;
        this.gridDocenteHistorial.selectrow(getSize-1);
          if(getSize==0){
            //Agrega una nueva fila
            this.gridDocenteHistorial.addrow(null, datarow);
          }else{
            if(this.gridDocenteHistorial.getrowdata(getSize-1).idDocenteCategoria >= 0 && this.gridDocenteHistorial.getrowdata(getSize-1).fechaDesde !=null
            
            ){
              if(this.banderaHistorial==0){
              //Agrega una nueva fila
              this.gridDocenteHistorial.addrow(null, datarow);
              }else{
                this.banderaHistorial==1
                this.myModal.alertMessage({title: 'Historial Docente ',msg: 'Revise que los campos esten ingresados correctamente.'});
              }
            }
            else{
            
            this.myModal.alertMessage({title: 'Historial Docente ',msg: 'Registro vacio o incompleto.'});
            return;}
          }
          this.banderaHistorial=0
    })
  };

 //Busca historial de docente
  listarHistorialDocente(idPersonaPar:number) {
    this.docentesService.getListaDocenteHistorial(idPersonaPar).subscribe(data => {
    this.listaDocenteHistorial = data;
    this.sourceDocenteHistorial.localdata = data;
    this.dataAdapterDocenteHistorial.dataBind();
    });
  }
  //Docente Historial
  sourceDocenteHistorial: any =
    {
      datatype: 'array',
      id: 'id',
      datafields:
        [
          { name: 'idPersona', type: 'int' },
          { name: 'idDocente', type: 'int' },
          { name: 'id', type: 'int' },
          { name: 'idDocenteCategoria', type: 'int' },
          { name: 'docenteCategoria', type: 'string' },
          { name: 'fechaDesde', type: 'date' },
          { name: 'fechaHasta', type: 'date' },
          { name: 'observacion', type: 'string' },
          { name: 'estado', type: 'string' },
          
        ],
      hierarchy:
      {
        keyDataField: { name: 'id' },
        parentDataField: { name: 'padre_id' }
      }
    };
    dataAdapterDocenteHistorial: any = new jqx.dataAdapter(this.sourceDocenteHistorial);

    columnsDocenteHistorial: any[] =
    [
      { text: 'Id ', datafield: 'id', width: '5%', filtertype: 'none', hidden: true },
      { text: 'Id Docente', datafield: 'idDocente', width: '5%', cellsalign: 'center', center: 'center', hidden: true },
      { text: 'Id Persona', datafield: 'idPersona', width: '5%', hidden: true },
      { text: 'Descripcion', datafield: 'idDocenteCategoria',displayfield: 'docenteCategoria', width: '18%', cellsalign: 'left', center: 'center' , columntype: 'dropdownlist',
        enableHover: true,
        initeditor: (row: number, _value: any, editor: any): void => { 
          editor.jqxDropDownList({ autoDropDownHeight:false , source: this.dataAdapterCategorias, displayMember: 'docenteCategoria',
          valueMember: 'idDocenteCategoria', enableHover: true,autoItemsHeight:true, 
          });
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
         
          this.gridDocenteHistorial.hiderowdetails(row);
          
        }, 
      },     
      { text: 'Fecha Desde', datafield: 'fechaDesde', row: 'row',width: '10%', cellsalign: 'center', center: 'center', columntype: 'datetimeinput',  cellsformat: 'dd/MM/yyyy', autoItemsHeight:true, 
     
        
      },
      { text: 'Fecha Hasta', datafield: 'fechaHasta', row: 'row',width: '10%', cellsalign: 'center', center: 'center', columntype: 'datetimeinput',  cellsformat: 'dd/MM/yyyy',autoItemsHeight:true,
          validation: (cell: any, value: Date): any => {
            if(this.gridDocenteHistorial.getrowdata(cell.row).fechaDesde!=null){

              if ( value < this.gridDocenteHistorial.getrowdata(cell.row).fechaDesde) {
                this.banderaHistorial=1;
                return { result: false, message: 'Fecha no admitida, debe ser posterior a la fecha de inicio' };
            }
            return true;
            }
            
          },

          cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
           
          },
         
      },
      { text: 'Observacion', datafield: 'observacion', width: '39%', cellsalign: 'center', center: 'center'  },
      {
        text: 'Accion', datafield: 'quitar', columntype: 'button', width: '6%', //disabled: this.rowEditAsig,
        buttonclick: (row: number): void => {
          let selectedrowindex = this.gridDocenteHistorial.getselectedrowindex();
          let rowscount = this.gridDocenteHistorial.getdatainformation().rowscount;
          let id = this.gridDocenteHistorial.getrowid(selectedrowindex);
          //DATOS DE DOCENTE HISTORIAL A ELIMINAR
          let agregarEliminados : any={};    
          let docenteHistorialEliminar: any = {}; 
          docenteHistorialEliminar.id=this.gridDocenteHistorial.getrowdata(selectedrowindex).id;
          docenteHistorialEliminar.idDocente=this.gridDocenteHistorial.getrowdata(selectedrowindex).idDocente;
          docenteHistorialEliminar.idDocenteCategoria=this.gridDocenteHistorial.getrowdata(selectedrowindex).idDocenteCategoria;
          docenteHistorialEliminar.fechaDesde =this.gridDocenteHistorial.getrowdata(selectedrowindex).fechaDesde;
          docenteHistorialEliminar.fechaHasta= this.gridDocenteHistorial.getrowdata(selectedrowindex).fechaHasta;
          docenteHistorialEliminar.observacion=  this.gridDocenteHistorial.getrowdata(selectedrowindex).observacion;
          docenteHistorialEliminar.usuarioIngresoId=1  ;  
          docenteHistorialEliminar.estado="I";
          docenteHistorialEliminar.version=this.gridDocenteHistorial.getrowdata(selectedrowindex).version; 
          agregarEliminados = docenteHistorialEliminar;
          //agregar a lista de eliminados
          this.listaDocenteHistorialEliminados.push(agregarEliminados);
          //ELIMINA LA FILA DE GRID
          this.gridDocenteHistorial.deleterow(id);
        }, cellsrenderer: (): String => {
          return 'Quitar';
        }
      },
    ];

  //Busca historial de docente
  listarActualizacionProfesional(idPersonaPar:number) {
    this.docentesService.getListaActualizacionProfesionalPorIdPersona(idPersonaPar).subscribe(data => {
    this.listaActualizacionProfesional = data;
    this.sourceActualizacionProfesional.localdata = data;
    this.dataAdapterActualizacionProfesional.dataBind();
    });
  }

  //Docente Historial
  sourceActualizacionProfesional: any =
  {
    datatype: 'array',
    id: 'id',
    datafields:
      [
        { name: 'id', type: 'int' },
        { name: 'idDocente', type: 'int' },
        { name: 'idPersona', type: 'int' },
        { name: 'idAreaConocimiento', type: 'int' },
        { name: 'areaConocimiento', type: 'string' },
        { name: 'institucion', type: 'string' },
        { name: 'nombreCurso', type: 'string' },
        { name: 'tipoEvento', type: 'string' },
        { name: 'tipoCertificado', type: 'string' },
        { name: 'numeroDias', type: 'int' },
        { name: 'numeroHoras', type: 'int' },
        { name: 'tipoCapacitacion', type: 'string' },
        { name: 'fechaDesde', type: 'date' },
        { name: 'fechaHasta', type: 'date' },
        { name: 'estado', type: 'string' },
        { name: 'version', type: 'int' },
      ],
    hierarchy:
    {
      keyDataField: { name: 'id' },
      parentDataField: { name: 'padre_id' }
    }
  };
  dataAdapterActualizacionProfesional: any = new jqx.dataAdapter(this.sourceActualizacionProfesional);

  countries: any[] = [
    { value: 'PRO', label: 'PROFESIONALIZACION' },
    { value: 'PED', label: 'PEDAGOGICA' },
  ]

  countriesSource: any =
    {
        datatype: 'array',
        datafields: [
            { name: 'label', type: 'string' },
            { name: 'value', type: 'string' }
        ],
        localdata: this.countries
    };
  countriesAdapter: any = new jqx.dataAdapter(this.countriesSource, { autoBind: true });

  columnsActualizacionProfesional: any[] =
  [
     { text: 'id', datafield: 'id', hidden:true },
     { text: 'Id Docente', datafield: 'idDocente', width: '5%', cellsalign: 'center', center: 'center', hidden: true },
     { text: 'Id Persona', datafield: 'idPersona', width: '5%', hidden: true },
     { text: 'Institución', datafield: 'institucion',filtertype: 'input',width: '14%' },
     { text: 'Nombre Curso', datafield: 'nombreCurso',filtertype: 'input',width: '14%' },
     { text: 'Area de Conocimiento', datafield: 'idAreaConocimiento',displayfield: 'areaConocimiento', width: '14%', cellsalign: 'center', center: 'center', columntype: 'dropdownlist',
      enableHover: true,autoItemsHeight:true,
        initeditor: (row: number, _value: any, editor: any): void => { 
          editor.jqxDropDownList({ autoDropDownHeight:false , source: this.dataAdapterAreas, displayMember: 'areaConocimiento',
          valueMember: 'idAreaConocimiento', animationType: 'slide', enableHover: true,autoItemsHeight:true,
          });
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
          this.gridFormacionProfesional.hiderowdetails(row);
        }, 
      },
     { text: 'Tipo Evento', datafield: 'tipoEvento',filtertype: 'input',width: '10%' },
     { text: 'Tipo Certificado', datafield: 'tipoCertificado',filtertype: 'input',width: '10%' },

    { text: 'Tipo de Formación', datafield: 'codigo',displayfield: 'tipoCapacitacion', width: '7%', cellsalign: 'center', center: 'center', columntype: 'dropdownlist',
      enableHover: true,autoItemsHeight:true,
        initeditor: (row: number, _value: any, editor: any): void => { 
          editor.jqxDropDownList({ autoDropDownHeight:false , source: this.sourceTipoCapacitacion, displayMember: 'tipoCapacitacion',
          valueMember: 'codigo', animationType: 'slide', enableHover: true,autoItemsHeight:true,
          });
        },
        cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
          this.gridFormacionProfesional.hiderowdetails(row);
        }, 
      },
     { text: 'N° Días', datafield: 'numeroDias',filtertype: 'input',width: '5%',columntype: 'numberinput',
        validation: (cell: any, value: number): any => {
          if (value <= 0) {
            this.banderaActualizacion = 1;
            return { result: false, message: 'El número de dias debe ser mayor a 0' };
          }
          return true;
        },
        createeditor: (row: number, cellvalue: any, editor: any): void => {
            editor.jqxNumberInput({ decimalDigits: 0, digits: 3 });
        } 
     },
     { text: 'N° Horas', datafield: 'numeroHoras',filtertype: 'input',width: '6%',columntype: 'numberinput',
          validation: (cell: any, value: number): any => {
            if (value <= 0) {
              this.banderaActualizacion = 1;
              return { result: false, message: 'El número de horas debe ser mayor a 0' };
            }
            return true;
        },
        createeditor: (row: number, cellvalue: any, editor: any): void => {
          
            editor.jqxNumberInput({ decimalDigits: 0, digits: 3 });
        }
      },
     { text: 'Desde',columngroup: 'Fecha', datafield: 'fechaDesde', width: '7%', cellsalign: 'center', center: 'center', columntype: 'datetimeinput',  cellsformat: 'dd/MM/yyyy', autoItemsHeight:true,
   
    },
     { text: 'Hasta',columngroup: 'Fecha', datafield: 'fechaHasta', width: '7%', cellsalign: 'center', center: 'center', columntype: 'datetimeinput',  cellsformat: 'dd/MM/yyyy',autoItemsHeight:true,
    
      validation: (cell: any, value: Date): any => {
        if ( value < this.gridActualizacionProfesional.getrowdata(cell.row).fechaDesde) {
            this.banderaActualizacion=1;
            return { result: false, message: 'Fecha no admitida, debe ser posterior a la fecha de inicio' };
        }
        return true;
    },
    },
     {
      text: 'Accion', datafield: 'quitar', columntype: 'button', width: '6%', //disabled: this.rowEditAsig,
      buttonclick: (row: number): void => {
        let selectedrowindex = this.gridActualizacionProfesional.getselectedrowindex();
        
        let id = this.gridActualizacionProfesional.getrowid(selectedrowindex);
        //DATOS DE DOCENTE HISTORIAL A ELIMINAR
        let agregarEliminados : any={};    
        let actualizacionProfesionalEliminar: any = {}; 
        actualizacionProfesionalEliminar.id=this.gridActualizacionProfesional.getrowdata(selectedrowindex).id;
        actualizacionProfesionalEliminar.idDocente=this.gridActualizacionProfesional.getrowdata(selectedrowindex).idDocente;
        actualizacionProfesionalEliminar.idAreaConocimiento=this.gridActualizacionProfesional.getrowdata(selectedrowindex).idAreaConocimiento;
        actualizacionProfesionalEliminar.institucion=  this.gridActualizacionProfesional.getrowdata(selectedrowindex).institucion;
        actualizacionProfesionalEliminar.nombreCurso=  this.gridActualizacionProfesional.getrowdata(selectedrowindex).nombreCurso;
        actualizacionProfesionalEliminar.tipoEvento=this.gridActualizacionProfesional.getrowdata(selectedrowindex).tipoEvento;
        actualizacionProfesionalEliminar.tipoCertificado=this.gridActualizacionProfesional.getrowdata(selectedrowindex).tipoCertificado;
        actualizacionProfesionalEliminar.tipoCapacitacion=this.gridActualizacionProfesional.getrowdata(selectedrowindex).tipoCapacitacion;

        actualizacionProfesionalEliminar.fechaDesde =this.gridActualizacionProfesional.getrowdata(selectedrowindex).fechaDesde;
        actualizacionProfesionalEliminar.fechaHasta= this.gridActualizacionProfesional.getrowdata(selectedrowindex).fechaHasta;
        actualizacionProfesionalEliminar.numeroHoras=  this.gridActualizacionProfesional.getrowdata(selectedrowindex).numeroHoras;
        actualizacionProfesionalEliminar.numeroDias=  this.gridActualizacionProfesional.getrowdata(selectedrowindex).numeroDias;
        actualizacionProfesionalEliminar.usuarioIngresoId=1  ;  
        actualizacionProfesionalEliminar.estado="I";
        actualizacionProfesionalEliminar.version=this.gridActualizacionProfesional.getrowdata(selectedrowindex).version; 
        agregarEliminados = actualizacionProfesionalEliminar;
        //agregar a lista de eliminados
        this.listaActualizacionProfesionalEliminados.push(agregarEliminados);
        //ELIMINA LA FILA DE GRID
        this.gridActualizacionProfesional.deleterow(id);
      }, cellsrenderer: (): String => {
        return 'Quitar';
      }
    },
     
  ];
  columngroups: any[] =
  [
      
      { text: 'Fecha', align: 'center', name: 'Fecha' },
      
  ];

    actualizacionProfesionalMenu = (toolbar: any): void => {
      let _container = document.createElement('div');
      _container.style.margin = '5px';
      let _buttonContainer = document.createElement('div');
      _buttonContainer.id = '_buttonContainer';
      _buttonContainer.style.cssText = 'float: left; margin-left: 5px';
      _container.appendChild(_buttonContainer);
      toolbar[0].appendChild(_container);
      let addRowButton = jqwidgets.createInstance('#_buttonContainer', 'jqxButton', { width: 100, value: 'Agregar',theme:'darkblue' });
      addRowButton.addEventHandler('click', () => {
          let datarow = this.generaterow3();
          let gridActualizacionProfesional = this.gridActualizacionProfesional.getrows();
          let getSize = gridActualizacionProfesional.length;
          this.gridActualizacionProfesional.selectrow(getSize-1);
          
          if(getSize==0){
            //Agrega una nueva fila
            this.gridActualizacionProfesional.addrow(null, datarow);
          } else{
            if(this.gridActualizacionProfesional.getrowdata(getSize-1).idAreaConocimiento>= 0 && this.gridActualizacionProfesional.getrowdata(getSize-1).institucion !=null  &&  
            this.gridActualizacionProfesional.getrowdata(getSize-1).nombreCurso !=null && this.gridActualizacionProfesional.getrowdata(getSize-1).tipoEvento!=null &&
            this.gridActualizacionProfesional.getrowdata(getSize-1).tipoCertificado!=null && this.gridActualizacionProfesional.getrowdata(getSize-1).numeroDias>=1 &&
            this.gridActualizacionProfesional.getrowdata(getSize-1).numeroHoras>=1 &&  this.gridActualizacionProfesional.getrowdata(getSize-1).tipoCapacitacion!=null &&
            this.gridActualizacionProfesional.getrowdata(getSize-1).fechaDesde!=null && this.gridActualizacionProfesional.getrowdata(getSize-1).fechaHasta!=null
            ){
              if(this.banderaActualizacion==0){
                   //Agrega una nueva fila
                this.gridActualizacionProfesional.addrow(null, datarow);
              }else{
                this.banderaActualizacion=1;
                this.myModal.alertMessage({title: 'Actualizacion Profesional ',msg: 'Revise que los campos esten ingresados correctamente..'});
              }
            }else{
            this.myModal.alertMessage({title: 'Actualizacion Profesional ',msg: 'Registro vacio o incompleto.'});
            return;
            }
          }
          this.banderaActualizacion=0;
      })
  };

  }
