import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocenteEdicionComponent } from './docente-edicion.component';

describe('DocenteEdicionComponent', () => {
  let component: DocenteEdicionComponent;
  let fixture: ComponentFixture<DocenteEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocenteEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocenteEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
