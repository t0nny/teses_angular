import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ControlAutenticacion } from '../../security/control-autenticacion';
import { DocenteListaComponent } from './docente-lista/docente-lista.component';
import { DocenteEdicionComponent } from './docente-edicion/docente-edicion.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Docente'
    },
    children: [{
      path: '',
      redirectTo: 'docentes',
      pathMatch: 'full'                                                                                                                                                                                        
    },
    {
      path: 'docente-lista',
      component: DocenteListaComponent,
      canActivate: [ControlAutenticacion],
      data: {
        title: 'Listado de Docentes'
      }
    },
    {
      path: 'docente-edicion/:idPersona'
      ///:idOferta'
      ,
      component: DocenteEdicionComponent,
      canActivate: [ControlAutenticacion],
      data: {
        title: 'Edición de Docente'
      }
    },
      

    ]
    
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocentesRoutingModule { }
