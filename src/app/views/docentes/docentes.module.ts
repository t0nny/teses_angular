import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocentesService } from '../../services/docentes/docentes.service';
import { FormsModule } from '@angular/forms'
import { DocentesRoutingModule } from './docentes-routing.module';
import { DocenteListaComponent } from './docente-lista/docente-lista.component';
import { DocenteEdicionComponent } from './docente-edicion/docente-edicion.component';
import { GlobalComponentModule } from '../global-component.module';

@NgModule({
  declarations: [
    DocenteListaComponent,
    DocenteEdicionComponent,
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    GlobalComponentModule,
    DocentesRoutingModule,
    
  ],providers: [DocentesModule, DocentesService]
})
export class DocentesModule { }
