import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../services/auth/account.service";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";

@Component({
  selector: 'app-asignar-tareas',
  templateUrl: './asignar-tareas.component.html',
  //styleUrls: ['./modal-tareas.component.scss']
})
export class AsignarTareasComponent implements OnInit {

  constructor(private periodoConvocatoria: VinculacionService, private accountService: AccountService) { }

  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
  @ViewChild('gridActividadesParticipantes') gridActividadesParticipantes: jqxGridComponent;
  @ViewChild('dropDownParticipante') dropDownParticipante: jqxDropDownListComponent;
  @ViewChild('dropDownTareas') dropDownTareas: jqxDropDownListComponent;
  @ViewChild('dropDownActividades') dropDownActividades: jqxDropDownListComponent;
  @ViewChild('dropDownTipoParticipante') dropDownTipoParticipante: jqxDropDownListComponent;
  @ViewChild('dropDownCarrera') dropDownCarrera: jqxDropDownListComponent;
  @ViewChild('dropDownProyectos') dropDownProyectos: jqxDropDownListComponent;

  //Variable paa cargar datos del objeto 
  rowindex: number = -1;
  _sub: Subscription;
  Titulo: string;
  dataActividadesTareas: any = {};
  usuarioActual: any = {};//variable para guardar el usuario actual
  tareas: any = {};
  docentesParticipantes: any = {};
  EstudiantesParticipantes: any = {};
  proyectoParticipanteDelete: any = [];

  ngOnInit(): void {
    this.MostrarTitulo();
  }

  MostrarTitulo() {
    //aqui nuevamente
    this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
    this._sub = this.periodoConvocatoria.cargarTipoUsuario(this.usuarioActual.id).subscribe(data1 => {
      if (data1.length > 0) {
        this._sub = this.periodoConvocatoria.cargarTituloFacultad('4', data1[0].identificacion).subscribe(data => {
          //console.log("titulo "+ JSON.stringify(data));
          this.Titulo = data.nombre;
          this.ListarCarreras(data.idDepartamento);
        }, error => console.error("h " + error));
      }
    }, error => console.error("h " + error));

  }

  /*****************llenar combo carreras**********************************************************/
  ListarCarreras(id: any) {
    this._sub = this.periodoConvocatoria.cargarCarreras('4', id).subscribe(data => {
      //console.log(JSON.stringify(data));        
      this.sourceCarrera.localdata = data;
      this.dataAdapterCarrera.dataBind();
    }, error => console.error("h " + error));

  }
  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceCarrera: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descripcion', type: 'string' },
      { name: 'estado', type: 'string' }

    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
  dataAdapterCarrera: any = new jqx.dataAdapter(this.sourceCarrera);
  /*************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO PROYECTOS, LISTA PROYECTOS**************************************************************/

  ListarProyectos() {
    this._sub = this.periodoConvocatoria.listarProyectosIdDepOferta(this.dropDownCarrera.val()).subscribe(data => {
      this.sourceProyectos.localdata = data;
      this.dataAdapterProyecto.dataBind();
    });
  }

  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceProyectos: any = {
    datatype: 'json',
    id: 'idProyecto',
    localdata: [
      { name: 'idProyecto', type: 'int' },
      { name: 'tituloProyecto', type: 'string' },
      { name: 'estadoProyecto', type: 'string' },
      { name: 'estado', type: 'string' }

    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
  dataAdapterProyecto: any = new jqx.dataAdapter(this.sourceProyectos);

  /********************************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO TAREAS, LISTA TAREAS**************************************************************/

  ListarTareas(event: any) {
    this.tareas = this.dataActividadesTareas.filter((dats) => { return dats.otIdObjetivoActividad == event.args.item.value });
    this.sourcetareas.localdata = this.tareas;
    this.dataAdapterTareas.dataBind();
  }

  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourcetareas: any = {
    datatype: 'json',
    id: 'idObjetivoTarea',
    localdata: [
      { name: 'idObjetivoTarea', type: 'int' },
      { name: 'descripcionTarea', type: 'string' },
      { name: 'estado', type: 'string' }

    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
  dataAdapterTareas: any = new jqx.dataAdapter(this.sourcetareas);

  /********************************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO ACTIVIDADES, LISTA ACTIVIDADES**************************************************************/

  ListarActividades(event: any) {
    let arrayProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
    if (arrayProyecto.estadoProyecto.trim() != 'APROBADO') {
      this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Proyecto no se encuentra aprobado!' });
      this.docentesParticipantes = [];
      this.EstudiantesParticipantes = [];
      this.sourceParticipante.localdata = this.docentesParticipantes;
      this.dataAdapterParticipante.dataBind();

      this.sourceActividad.localdata = [];
      this.dataAdapterActividades.dataBind();

      this.sourcetareas.localdata = [];
      this.dataAdapterTareas.dataBind();

      this.gridActividadesParticipantes.clear();
    } else {
      this._sub = this.periodoConvocatoria.listarActividadesProyecto(arrayProyecto.idProyecto).subscribe(data => {
        this.dataActividadesTareas = data;
        let hash = {};
        //viene de la bd un array con los datos de actividad y tareas, la siguiente funcion filtra y obtiene solo
        //las actividades 
        let data1 = data.filter(o => hash[o.idObjetivoActividad] ? false : hash[o.idObjetivoActividad] = true);
        this.sourceActividad.localdata = data1;
        this.dataAdapterActividades.dataBind();
        //console.log(JSON.stringify(data1));
        /*const resultado = Array.from(new Set(data.map(s => s.idObjetivoActividad)))
        .map(idObjetivoActividad => {
            return {
              idObjetivoActividad: idObjetivoActividad,
                descripcionActividad: data.find(s => s.idObjetivoActividad === idObjetivoActividad).descripcionActividad
            };
        });
        console.log(JSON.stringify(resultado));*/
        this.cargarParticipantesCombo(arrayProyecto.idProyecto);
      });
      this.listarAsignaciones(arrayProyecto.idProyecto);
    }
  }

  listarAsignaciones(idProyecto: any) {
    this._sub = this.periodoConvocatoria.listarAsignacionesTareas(idProyecto).subscribe(data => {
      this.aggGripParticipantesProyecto(data);
    });
  }


  cargarParticipantesCombo(idProyecto: any) {
    this._sub = this.periodoConvocatoria.listarDocentesProyecto(idProyecto).subscribe(dataDocente => {
      this.docentesParticipantes = dataDocente;
      let hash = {};
      this.docentesParticipantes = this.docentesParticipantes.filter(o => hash[o.idPersona] ? false : hash[o.idPersona] = true);
    });
    this._sub = this.periodoConvocatoria.listarEstudianteProyecto(idProyecto).subscribe(dataEstudiante => {
      this.EstudiantesParticipantes = dataEstudiante;
      let hash = {};
      this.EstudiantesParticipantes = this.EstudiantesParticipantes.filter(o => hash[o.idPersona] ? false : hash[o.idPersona] = true);
    });
  }

  /*************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO ACTIVIDADES, LISTA ACTIVIDADES**************************************************************/

  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceActividad: any = {
    datatype: 'json',
    id: 'idObjetivoActividad',
    localdata: [
      { name: 'idObjetivoActividad', type: 'int' },
      { name: 'descripcionActividad', type: 'string' },
      { name: 'estado', type: 'string' }

    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
  dataAdapterActividades: any = new jqx.dataAdapter(this.sourceActividad);

  /*************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO TIPO PARTICIPANTE, LISTA TIPO PARTICIPANTE**************************************************************/
  dataTipoParticipante =
    [
      {
        'id': '1',
        'descripcion': 'DOCENTE'
      },
      {
        'id': '2',
        'descripcion': 'ESTUDIANTE'
      }
    ]
  //FUENTE DE DATOS PARA EL COMBOBOX DE TIPO PARTICIPANTE
  sourceTipoParticipante: any = {
    datatype: 'json',
    id: 'id',
    datafields: [
      { name: 'id' },
      { name: 'descripcion' }
    ],
    localdata: this.dataTipoParticipante
  };
  //CARGAR ORIGEN DEL COMBOBOX DE TIPO PARTICIPANTE
  dataAdapterTipoParticipante: any = new jqx.dataAdapter(this.sourceTipoParticipante);

  /*************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO TIPO PARTICIPANTE, LISTA TIPO PARTICIPANTE**************************************************************/

  tipoParticipante(event: any) {
    if (event.args.item.value == 1) {
      this.sourceParticipante.localdata = this.docentesParticipantes;
      this.dataAdapterParticipante.dataBind();
    }
    if (event.args.item.value == 2) {
      this.sourceParticipante.localdata = this.EstudiantesParticipantes;
      this.dataAdapterParticipante.dataBind();
    }
  }

  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceParticipante: any = {
    datatype: 'json',
    id: 'idPersona',
    localdata: [
      { name: 'idPersona', type: 'int' },
      { name: 'nombres', type: 'string' },
      { name: 'estado', type: 'string' }

    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
  dataAdapterParticipante: any = new jqx.dataAdapter(this.sourceParticipante);

  /********************************************************************************************************************************** */
  /*****************crea el boton y el evento para agregar DOCENTES PARTICIPANTES AL GRIP*************************************************************/

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer1 = document.createElement('div');
    buttonContainer1.id = 'buttonContainer1';
    buttonContainer1.style.cssText = 'float: left; margin-left: 5px';

    let buttonContainer2 = document.createElement('div');
    buttonContainer2.id = 'buttonContainer2';
    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer1);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);
    let addRowButton = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: 100, value: 'Agregar', theme: 'darkblue' });
    addRowButton.addEventHandler('click', () => {
      if (this.dropDownParticipante.val() == '') {
        this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Seleccione un participante!' });
      } else {
        if (this.dropDownTareas.val() == '') {
          this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Seleccione una tarea!' });
        } else {
          let gridActividadesParticipantes = this.gridActividadesParticipantes.getrows();
          let getSize = gridActividadesParticipantes.length;
          this.gridActividadesParticipantes.selectrow(getSize - 1);
          let arrayOfProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
          let arrayOfTareas = JSON.parse(JSON.stringify(this.dropDownTareas.getSelectedItem())).originalItem;
          let arrayOfTipoParticipante = JSON.parse(JSON.stringify(this.dropDownTipoParticipante.getSelectedItem())).originalItem;
          let arrayOfParticipante = JSON.parse(JSON.stringify(this.dropDownParticipante.getSelectedItem())).originalItem;
          let arrDatosAsiganciones: any = [];
          let objDatosAsiganciones: any = {};
          objDatosAsiganciones.idProyecto = arrayOfProyecto.idProyecto;
          objDatosAsiganciones.tituloProyecto = arrayOfProyecto.tituloProyecto;
          objDatosAsiganciones.descripcionActividad = arrayOfTareas.descripcionActividad;
          objDatosAsiganciones.descripcionTarea = arrayOfTareas.descripcionTarea;
          objDatosAsiganciones.fechaDesde = arrayOfTareas.fechaDesde;
          objDatosAsiganciones.fechaHasta = arrayOfTareas.fechaHasta;
          objDatosAsiganciones.idObjetivoTarea = arrayOfTareas.idObjetivoTarea;
          objDatosAsiganciones.idObjetivoActividad = arrayOfTareas.idObjetivoActividad;
          objDatosAsiganciones.descripcion = arrayOfTipoParticipante.descripcion;
          objDatosAsiganciones.idPersona = arrayOfParticipante.idPersona;
          objDatosAsiganciones.nombres = arrayOfParticipante.nombres;
          arrDatosAsiganciones.push(objDatosAsiganciones);
          if (getSize == 0) {
            //Agrega una nueva fila
            this.aggGripParticipantesProyecto(arrDatosAsiganciones);
          } else {
            let data1 = gridActividadesParticipantes.filter((dats) => { return (dats.idPersona == this.dropDownParticipante.val()) && (dats.idObjetivoTarea == this.dropDownTareas.val()) })
            if (data1.length > 0) {
              this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Participante ya ha sido agregado!' });
              this.gridActividadesParticipantes.clearselection();
            } else {
              this.aggGripParticipantesProyecto(arrDatosAsiganciones);
            }
          }
        }
      }
    })

    let addRowButton1 = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 100, value: 'Grabar', theme: 'darkblue' });
    addRowButton1.addEventHandler('click', () => {
      let gridActividadesParticipantes = this.gridActividadesParticipantes.getrows();
      let getSize = gridActividadesParticipantes.length;
      this.gridActividadesParticipantes.selectrow(getSize - 1);
      if (getSize == 0) {
        this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'No hay datos para guardar!' });
      } else {
        let asignacionActividades = this.formarJsonAsignarPersonal();
        this.guardar(asignacionActividades);
        this.borrarParticipanteProyecto();
      }
    })
  };

  /*************************************************************************************************************** */
  /********************************************PARAMETROS DE LA GRILA DE pPARTICIPANTES DE PROYECTOS******************************************************************* */

  sourceActividadesParticipantes: any = {
    datatype: 'array',
    id: 'id',
    datafields: [
      { name: 'id', type: 'int' },
      { name: 'idAsignacion', type: 'int' },
      { name: 'idPersona', type: 'int' },
      { name: 'idProyecto', type: 'int' },
      { name: 'idObjetivoActividad', type: 'int' },
      { name: 'idObjetivoTarea', type: 'int' },
      { name: 'tituloProyecto', type: 'string' },
      { name: 'nombres', type: 'string' },
      { name: 'descripcionTarea', type: 'string' },
      { name: 'descripcionActividad', type: 'string' },
      { name: 'fechaDesde', type: 'date' },
      { name: 'fechaHasta', type: 'date' },
      { name: 'descripcion', type: 'string' },
    ]
  };
  dataAdapterActividadesParticipantes: any = new jqx.dataAdapter(this.sourceActividadesParticipantes);

  columnsActividadesParticipantes: any[] = [
    { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
    { text: 'Id', datafield: 'idAsignacion', width: '2%', filtertype: 'none', hidden: true },
    { text: 'Id', datafield: 'idPersona', width: '2%', filtertype: 'none', hidden: true },
    { text: 'Id', datafield: 'idProyecto', width: '2%', filtertype: 'none', hidden: true },
    { text: 'Id', datafield: 'idObjetivoActividad', width: '2%', filtertype: 'none', hidden: true },
    { text: 'Id', datafield: 'idObjetivoTarea', width: '2%', filtertype: 'none', hidden: true },
    { text: 'Proyecto', datafield: 'tituloProyecto', width: '16%' },
    { text: 'Actividad', datafield: 'descripcionActividad', width: '16%' },
    { text: 'Tarea', datafield: 'descripcionTarea', width: '15%' },
    { text: 'Fecha Desde', datafield: 'fechaDesde', width: '10%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
    { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '10%', cellsformat: 'yyyy-MM-dd' },
    { text: 'Tipo', datafield: 'descripcion', width: '10%' },
    { text: 'Participante', datafield: 'nombres', width: '16%' },
    {
      text: 'Quitar', columntype: 'button', width: '7%',
      cellsrenderer: (): string => {
        return 'Quitar';
      },
      buttonclick: (row: number): void => {
        let idAsigancion = this.gridActividadesParticipantes.getcellvalue(this.rowindex, 'idAsignacion');
        if (idAsigancion != null) {
          this.proyectoParticipanteDelete.push(idAsigancion);
          let id = this.gridActividadesParticipantes.getcellvalue(this.rowindex, 'id');
          this.gridActividadesParticipantes.deleterow(id);
        } else {
          let id = this.gridActividadesParticipantes.getcellvalue(this.rowindex, 'id');
          this.gridActividadesParticipantes.deleterow(id);
        }
        this.rowindex = -1;
      },
    },
  ];

  /*************************************************************************************************************** */
  /*****************evento para eliminar participante proyecto guardada en la bd**********************************************************/

  borrarParticipanteProyecto() {
    if (this.proyectoParticipanteDelete.length > 0 && this.proyectoParticipanteDelete != undefined) {
      for (let index = 0; index < this.proyectoParticipanteDelete.length; index++) {
        console.log('proyectoParticipanteDelete' + this.proyectoParticipanteDelete[index]);
        this._sub = this.periodoConvocatoria.borrarParticipanteProyectoGrid(this.proyectoParticipanteDelete[index]).subscribe(data => {
        }, error => console.error("Error al eliminar participante del proyecto " + error));
      }

    }

  }

  /********************************************************************************************************************************** */
  /***********metodo para obtner el id de la fila seleccionada en la grilla de DOCENTES PARTICIPANTES*************************************************************/

  Rowselect(event: any): void {
    if (event.args) {
      this.rowindex = event.args.rowindex;
    }
  }

  /********************************************************************************************************************************** */
  /************************agg participantes al grip***************************************************/

  aggGripParticipantesProyecto(arrayOfAsignaciones: any) {
    for (let index = 0; index < arrayOfAsignaciones.length; index++) {
      let datarow = {};
      datarow['id'] = arrayOfAsignaciones[index].id;
      datarow['idProyecto'] = arrayOfAsignaciones[index].idProyecto;
      datarow['tituloProyecto'] = arrayOfAsignaciones[index].tituloProyecto;
      datarow['idAsignacion'] = arrayOfAsignaciones[index].idAsignacion;
      datarow['idObjetivoActividad'] = arrayOfAsignaciones[index].idObjetivoActividad;
      datarow['idObjetivoTarea'] = arrayOfAsignaciones[index].idObjetivoTarea;
      datarow['idPersona'] = arrayOfAsignaciones[index].idPersona;
      datarow['descripcionActividad'] = arrayOfAsignaciones[index].descripcionActividad;
      datarow['descripcionTarea'] = arrayOfAsignaciones[index].descripcionTarea;
      datarow['fechaDesde'] = new Date(arrayOfAsignaciones[index].fechaDesde);
      datarow['fechaHasta'] = new Date(arrayOfAsignaciones[index].fechaHasta);
      datarow['descripcion'] = arrayOfAsignaciones[index].descripcion;
      datarow['nombres'] = arrayOfAsignaciones[index].nombres;
      this.gridActividadesParticipantes.addrow(arrayOfAsignaciones[index].id, datarow);
    }
    this.dropDownParticipante.clearSelection();
    this.dropDownTareas.clearSelection();
    this.dropDownActividades.clearSelection();
    this.dropDownTipoParticipante.clearSelection();
  }

  formarJsonAsignarPersonal() {
    let arrParticipantes: any = [];
    let objParticipantes: any = {};

    //proyecto participante
    let gridParticipantes = this.gridActividadesParticipantes.getrows();
    let getSize1 = gridParticipantes.length;
    if (getSize1 > 0) {
      for (let index = 0; index < gridParticipantes.length; index++) {
        objParticipantes = {};
        objParticipantes.id = gridParticipantes[index].idAsignacion;
        objParticipantes.idProyecto = gridParticipantes[index].idProyecto;
        objParticipantes.idPersona = gridParticipantes[index].idPersona;
        objParticipantes.idObjetivoActividad = gridParticipantes[index].idObjetivoActividad;
        objParticipantes.idObjetivoTarea = gridParticipantes[index].idObjetivoTarea;
        objParticipantes.tipoParticipante = gridParticipantes[index].descripcion;
        objParticipantes.usuarioIngresoId = this.usuarioActual.id;
        objParticipantes.estado = 'A';
        arrParticipantes.push(objParticipantes);
      }
    }
    return arrParticipantes;
  }

  guardar(asignacionActividades: any) {
    this._sub = this.periodoConvocatoria.grabarAsignacionActividades(asignacionActividades).subscribe(result => {
      this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Datos Guardados Correctamente!' });
    }, error => {
      this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Error al grabar Asignacion Actividades!' + error });
    });
  }


}

