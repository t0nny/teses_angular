import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";

@Component({
    selector: 'app-modal-detalles-participantes',
    templateUrl: './modal-detalles-participantes.component.html',
    //styleUrls: ['./modal-tareas.component.scss']
})
export class ModalDetallesParticipantesComponent implements OnInit {

    constructor(private periodoConvocatoria: VinculacionService) { }

    @ViewChild('modalDetallesParticipantes') private modalDetallesParticipantes: ModalDirective;
    @ViewChild('modalDetallesEstudianteParticipantes') private modalDetallesEstudianteParticipantes: ModalDirective;

    _sub: Subscription;
    participante: any = {};
    estudianteparticipante: any = {};

    ngOnInit(): void {
        throw new Error("Method not implemented.");
    }

    mostrarDetalleParticipantes(idDocente: any): void {
        this._sub = this.periodoConvocatoria.buscarDirectorProyecto(idDocente).subscribe(data => {
            //console.log(JSON.stringify(data));
            this.participante = data[0];
            this.modalDetallesParticipantes.show();
        });
    }

    mostrarDetalleEstudianteParticipantes(idPeriodoAcademico: any, idNivel: any, idEstudiante: any): void {
        this._sub = this.periodoConvocatoria.buscarEstudianteSemestre(idPeriodoAcademico, '0', idNivel, '0', idEstudiante).subscribe(data => {
            //console.log(JSON.stringify(data));
            this.estudianteparticipante = data[0];
            this.modalDetallesEstudianteParticipantes.show();
        });
    }





}