import { Component, OnInit, ViewChild } from "@angular/core";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../services/auth/account.service";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";
import { ModalInformeActividadesComponent } from "../modal-informe-actividades/modal-informe-actividades.component";

@Component({
    selector: 'app-generar-informe',
    templateUrl: './generar-informe.component.html',
    //styleUrls: ['./modal-tareas.component.scss']
})
export class GenerarInformeComponent implements OnInit {

    constructor(private periodoConvocatoria: VinculacionService, private accountService: AccountService) { }

    @ViewChild(ModalInformeActividadesComponent) formNuevoInformeActividad: ModalInformeActividadesComponent;
    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild('dropDownCarrera') dropDownCarrera: jqxDropDownListComponent;
    @ViewChild('dropDownProyectos') dropDownProyectos: jqxDropDownListComponent;
    @ViewChild('dropDownConsolidado') dropDownConsolidado: jqxDropDownListComponent;
    @ViewChild('gridActividadesRealizadas') gridActividadesRealizadas: jqxGridComponent;
    @ViewChild('gridActividadesRealizadasEst') gridActividadesRealizadasEst: jqxGridComponent;

    //Variable paa cargar datos del objeto 
    rowindex: number = -1;
    usuarioActual: any = {};//variable para guardar el usuario actual
    _sub: Subscription;
    facultad: string;
    participanteProyecto: string;
    depOfertaActividad: any = {};

    ngOnInit(): void {
        this.MostrarTitulo();
    }

    MostrarTitulo() {
        this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
        this.participanteProyecto = this.usuarioActual.nombres + ' ' + this.usuarioActual.apellidos;
        this._sub = this.periodoConvocatoria.cargarTipoUsuario(this.usuarioActual.id).subscribe(data1 => {
            if (data1.length > 0) {
                this._sub = this.periodoConvocatoria.cargarTituloFacultad('4', data1[0].identificacion).subscribe(data => {
                    //console.log("titulo "+ JSON.stringify(data));
                    this.facultad = data.nombre;
                    this.ListarCarreras(data.idDepartamento);
                }, error => console.error("h " + error));
            }
        }, error => console.error("h " + error));

    }

    /*****************llenar combo carreras**********************************************************/
    ListarConsolidados(id: any) {
        this._sub = this.periodoConvocatoria.listarSeguimientoActividad().subscribe(data => {
            //console.log(JSON.stringify(data));        
            this.sourceConsolidado.localdata = data;
            this.dataAdapterConsolidado.dataBind();
        }, error => console.error("h " + error));

    }
    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourceConsolidado: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descripcion', type: 'string' },
            { name: 'estado', type: 'string' }

        ],
    };//CARGAR ORIGEN DEL COMBOBOX DE CONSOLIDADO
    dataAdapterConsolidado: any = new jqx.dataAdapter(this.sourceConsolidado);


    /*****************llenar combo carreras**********************************************************/
    ListarCarreras(id: any) {
        this._sub = this.periodoConvocatoria.cargarCarreras('4', id).subscribe(data => {
            //console.log(JSON.stringify(data));        
            this.sourceCarrera.localdata = data;
            this.dataAdapterCarrera.dataBind();
        }, error => console.error("h " + error));

    }
    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourceCarrera: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descripcion', type: 'string' },
            { name: 'estado', type: 'string' }

        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
    dataAdapterCarrera: any = new jqx.dataAdapter(this.sourceCarrera);
    /*************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO PROYECTOS, LISTA PROYECTOS**************************************************************/

    ListarProyectos() {
        this._sub = this.periodoConvocatoria.listarProyectosIdDepOferta(this.dropDownCarrera.val()).subscribe(data => {
            this.sourceProyectos.localdata = data;
            this.dataAdapterProyecto.dataBind();
            this.sourceConsolidado.localdata = [];
            this.dataAdapterConsolidado.dataBind();
        });
    }

    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourceProyectos: any = {
        datatype: 'json',
        id: 'idProyecto',
        localdata: [
            { name: 'idProyecto', type: 'int' },
            { name: 'tituloProyecto', type: 'string' },
            { name: 'estadoProyecto', type: 'string' },
            { name: 'estado', type: 'string' }

        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
    dataAdapterProyecto: any = new jqx.dataAdapter(this.sourceProyectos);

    /********************************************************************************************************************************** */
    /*****************crea el boton y el evento para agregar actividades realizadas AL GRIP docente*************************************************************/

    rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer = document.createElement('div');
        buttonContainer.id = 'buttonContainer';
        buttonContainer.style.cssText = 'float: left; margin-left: 5px';

        let buttonContainer1 = document.createElement('div');
        buttonContainer1.id = 'buttonContainer1';
        buttonContainer1.style.cssText = 'float: left; margin-left: 5px';

        let buttonContainer3 = document.createElement('div');
        buttonContainer3.id = 'buttonContainer3';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(buttonContainer);
        container.appendChild(buttonContainer1);
        container.appendChild(buttonContainer3);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer', 'jqxButton', { width: '10%', height: 25, disabled: false, value: 'Nuevo', theme: 'ui-redmond' });
        addRowButton.addEventHandler('click', () => {
            if (this.depOfertaActividad.estadoEnvio == 'INI' || this.depOfertaActividad.estadoEnvio == undefined) {
                if (this.dropDownProyectos.val() == '') {
                    this.myModal.alertMessage({ title: 'Informe de Actividades', msg: 'Seleccione un proyecto!' });
                } else {
                    let arrayProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
                    if (arrayProyecto.estadoProyecto.trim() != 'APROBADO') {
                        this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Proyecto no se encuentra aprobado!' });
                    } else {
                        this.formNuevoInformeActividad.nuevaActividad(arrayProyecto.idProyecto, 'DOCENTE',this.depOfertaActividad);
                    }
                }
            } else {
                this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades ya fue enviado al Coordinador de Proyectos!' });
                this.gridActividadesRealizadasEst.clearselection();
            }
            /*if (this.dropDownParticipante.val() == '') {
                this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Seleccione un participante!' });
            } else {
                if (this.dropDownTareas.val() == '') {
                    this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Seleccione una tarea!' });
                } else {
                    let gridActividadesParticipantes = this.gridActividadesParticipantes.getrows();
                    let getSize = gridActividadesParticipantes.length;
                    this.gridActividadesParticipantes.selectrow(getSize - 1);
                    let arrayOfProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
                    let arrayOfTareas = JSON.parse(JSON.stringify(this.dropDownTareas.getSelectedItem())).originalItem;
                    let arrayOfTipoParticipante = JSON.parse(JSON.stringify(this.dropDownTipoParticipante.getSelectedItem())).originalItem;
                    let arrayOfParticipante = JSON.parse(JSON.stringify(this.dropDownParticipante.getSelectedItem())).originalItem;
                    let arrDatosAsiganciones: any = [];
                    let objDatosAsiganciones: any = {};
                    objDatosAsiganciones.idProyecto = arrayOfProyecto.idProyecto;
                    objDatosAsiganciones.tituloProyecto = arrayOfProyecto.tituloProyecto;
                    objDatosAsiganciones.descripcionActividad = arrayOfTareas.descripcionActividad;
                    objDatosAsiganciones.descripcionTarea = arrayOfTareas.descripcionTarea;
                    objDatosAsiganciones.fechaDesde = arrayOfTareas.fechaDesde;
                    objDatosAsiganciones.fechaHasta = arrayOfTareas.fechaHasta;
                    objDatosAsiganciones.idObjetivoTarea = arrayOfTareas.idObjetivoTarea;
                    objDatosAsiganciones.idObjetivoActividad = arrayOfTareas.idObjetivoActividad;
                    objDatosAsiganciones.descripcion = arrayOfTipoParticipante.descripcion;
                    objDatosAsiganciones.idPersona = arrayOfParticipante.idPersona;
                    objDatosAsiganciones.nombres = arrayOfParticipante.nombres;
                    arrDatosAsiganciones.push(objDatosAsiganciones);
                    if (getSize == 0) {
                        //Agrega una nueva fila
                        this.aggGripParticipantesProyecto(arrDatosAsiganciones);
                    } else {
                        let data1 = gridActividadesParticipantes.filter((dats) => { return (dats.idPersona == this.dropDownParticipante.val()) && (dats.idObjetivoTarea == this.dropDownTareas.val()) })
                        if (data1.length > 0) {
                            this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Participante ya ha sido agregado!' });
                            this.gridActividadesParticipantes.clearselection();
                        } else {
                            this.aggGripParticipantesProyecto(arrDatosAsiganciones);
                        }
                    }
                }
            }*/
        })

        let addRowButton1 = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: '10%', height: 25, disabled: false, value: 'Editar', theme: 'ui-redmond' });
        addRowButton1.addEventHandler('click', () => {
            if (this.depOfertaActividad.estadoEnvio == 'INI' || this.depOfertaActividad.estadoEnvio == undefined) {
                if (this.dropDownProyectos.val() == '') {
                    this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Seleccione un proyecto!' });
                } else {
                    let arrayProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
                    if (arrayProyecto.estadoProyecto.trim() != 'APROBADO') {
                        this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Proyecto no se encuentra aprobado!' });
                    } else {
                        if (this.rowindex != -1) {
                            let getRowObject = this.gridActividadesRealizadas.getrowdata(this.rowindex);
                            let id = getRowObject.id;
                            this.formNuevoInformeActividad.editarActividad(arrayProyecto.idProyecto, id, 'DOCENTE',this.depOfertaActividad);
                            this.gridActividadesRealizadas.clearselection();
                            this.rowindex = -1;
                        } else {
                            this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Seleccione un registro!' });
                        }
                    }
                }
            } else {
                this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades ya fue enviado al Coordinador de Proyectos!' });
                this.gridActividadesRealizadasEst.clearselection();
            }
        })

        let addRowButton3 = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: '15%', height: 25, disabled: false, value: 'Enviar Informe', theme: 'ui-redmond' });
        addRowButton3.addEventHandler('click', () => {
            let gridActividadesRealizadas = this.gridActividadesRealizadas.getrows();
            let getSize = gridActividadesRealizadas.length;
            let gridActividadesRealizadasEst = this.gridActividadesRealizadasEst.getrows();
            let getSizeEst = gridActividadesRealizadasEst.length;
            
            if (getSize == 0) {
                this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Ingrese las actividades realizadas por los Docentes!' });
                                
            } else {
                if (getSizeEst == 0) {
                    this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Ingrese las actividades realizadas por los Estudiantes!' });
                                    
                } else {
                    if (this.depOfertaActividad.estadoEnvio == 'INI' || this.depOfertaActividad.estadoEnvio == undefined) {
                        this._sub = this.periodoConvocatoria.updateEstadoDepOfetaActividades(this.depOfertaActividad.id).subscribe(data => {
                            this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades enviado al Coordinador de Proyectos!' });
                            this.ListarActividadesRealizadas();
                        });
                    } else {
                        this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades ya fue enviado al Coordinador de Proyectos!' });
                    }
                }
            }
             
        })
    };

    /********************************************************************************************************************************** */
    /*****************crea el boton y el evento para agregar actividades realizadas AL GRIP docente*************************************************************/

    rendertoolbar1 = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer4 = document.createElement('div');
        buttonContainer4.id = 'buttonContainer4';
        buttonContainer4.style.cssText = 'float: left; margin-left: 5px';

        let buttonContainer5 = document.createElement('div');
        buttonContainer5.id = 'buttonContainer5';
        buttonContainer5.style.cssText = 'float: left; margin-left: 5px';

        let buttonContainer6 = document.createElement('div');
        buttonContainer6.id = 'buttonContainer6';
        buttonContainer6.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(buttonContainer4);
        container.appendChild(buttonContainer5);
        container.appendChild(buttonContainer6);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer4', 'jqxButton', { width: '10%', height: 25, disabled: false, value: 'Nuevo', theme: 'ui-redmond' });
        addRowButton.addEventHandler('click', () => {
            console.log('thsisdata' + this.depOfertaActividad.estadoEnvio)
            if (this.depOfertaActividad.estadoEnvio == 'INI' || this.depOfertaActividad.estadoEnvio == undefined) {
                if (this.dropDownProyectos.val() == '') {
                    this.myModal.alertMessage({ title: 'Informe de Actividades', msg: 'Seleccione un proyecto!' });
                } else {
                    let arrayProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
                    if (arrayProyecto.estadoProyecto.trim() != 'APROBADO') {
                        this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Proyecto no se encuentra aprobado!' });
                    } else {
                        this.formNuevoInformeActividad.nuevaActividad(arrayProyecto.idProyecto, 'ESTUDIANTE',this.depOfertaActividad);
                    }
                }
            } else {
                this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades ya fue enviado al Coordinador de Proyectos!' });
                this.gridActividadesRealizadasEst.clearselection();
            }
            /*if (this.dropDownParticipante.val() == '') {
                this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Seleccione un participante!' });
            } else {
                if (this.dropDownTareas.val() == '') {
                    this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Seleccione una tarea!' });
                } else {
                    let gridActividadesParticipantes = this.gridActividadesParticipantes.getrows();
                    let getSize = gridActividadesParticipantes.length;
                    this.gridActividadesParticipantes.selectrow(getSize - 1);
                    let arrayOfProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
                    let arrayOfTareas = JSON.parse(JSON.stringify(this.dropDownTareas.getSelectedItem())).originalItem;
                    let arrayOfTipoParticipante = JSON.parse(JSON.stringify(this.dropDownTipoParticipante.getSelectedItem())).originalItem;
                    let arrayOfParticipante = JSON.parse(JSON.stringify(this.dropDownParticipante.getSelectedItem())).originalItem;
                    let arrDatosAsiganciones: any = [];
                    let objDatosAsiganciones: any = {};
                    objDatosAsiganciones.idProyecto = arrayOfProyecto.idProyecto;
                    objDatosAsiganciones.tituloProyecto = arrayOfProyecto.tituloProyecto;
                    objDatosAsiganciones.descripcionActividad = arrayOfTareas.descripcionActividad;
                    objDatosAsiganciones.descripcionTarea = arrayOfTareas.descripcionTarea;
                    objDatosAsiganciones.fechaDesde = arrayOfTareas.fechaDesde;
                    objDatosAsiganciones.fechaHasta = arrayOfTareas.fechaHasta;
                    objDatosAsiganciones.idObjetivoTarea = arrayOfTareas.idObjetivoTarea;
                    objDatosAsiganciones.idObjetivoActividad = arrayOfTareas.idObjetivoActividad;
                    objDatosAsiganciones.descripcion = arrayOfTipoParticipante.descripcion;
                    objDatosAsiganciones.idPersona = arrayOfParticipante.idPersona;
                    objDatosAsiganciones.nombres = arrayOfParticipante.nombres;
                    arrDatosAsiganciones.push(objDatosAsiganciones);
                    if (getSize == 0) {
                        //Agrega una nueva fila
                        this.aggGripParticipantesProyecto(arrDatosAsiganciones);
                    } else {
                        let data1 = gridActividadesParticipantes.filter((dats) => { return (dats.idPersona == this.dropDownParticipante.val()) && (dats.idObjetivoTarea == this.dropDownTareas.val()) })
                        if (data1.length > 0) {
                            this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Participante ya ha sido agregado!' });
                            this.gridActividadesParticipantes.clearselection();
                        } else {
                            this.aggGripParticipantesProyecto(arrDatosAsiganciones);
                        }
                    }
                }
            }*/
        })

        let addRowButton1 = jqwidgets.createInstance('#buttonContainer5', 'jqxButton', { width: '10%', height: 25, disabled: false, value: 'Editar', theme: 'ui-redmond' });
        addRowButton1.addEventHandler('click', () => {
            if (this.depOfertaActividad.estadoEnvio == 'INI' || this.depOfertaActividad.estadoEnvio == undefined) {
                if (this.dropDownProyectos.val() == '') {
                    this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Seleccione un proyecto!' });
                } else {
                    let arrayProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
                    if (arrayProyecto.estadoProyecto.trim() != 'APROBADO') {
                        this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Proyecto no se encuentra aprobado!' });
                    } else {
                        if (this.rowindex != -1) {
                            let getRowObject = this.gridActividadesRealizadasEst.getrowdata(this.rowindex);
                            let id = getRowObject.id;
                            this.formNuevoInformeActividad.editarActividad(arrayProyecto.idProyecto, id, 'ESTUDIANTE',this.depOfertaActividad);
                            this.gridActividadesRealizadasEst.clearselection();
                            this.rowindex = -1;
                        } else {
                            this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Seleccione un registro!' });
                        }
                    }
                }
            } else {
                this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades ya fue enviado al Coordinador de Proyectos!' });
                this.gridActividadesRealizadasEst.clearselection();
            }
        })

        let addRowButton3 = jqwidgets.createInstance('#buttonContainer6', 'jqxButton', { width: '15%', height: 25, disabled: false, value: 'Enviar Informe', theme: 'ui-redmond' });
        addRowButton3.addEventHandler('click', () => {
            let gridActividadesRealizadas = this.gridActividadesRealizadas.getrows();
            let getSize = gridActividadesRealizadas.length;
            let gridActividadesRealizadasEst = this.gridActividadesRealizadasEst.getrows();
            let getSizeEst = gridActividadesRealizadasEst.length;
            
            if (getSize == 0) {
                this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Ingrese las actividades realizadas por los Docentes!' });
                                
            } else {
                if (getSizeEst == 0) {
                    this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Ingrese las actividades realizadas por los Estudiantes!' });
                                    
                } else {
                    if (this.depOfertaActividad.estadoEnvio == 'INI' || this.depOfertaActividad.estadoEnvio == undefined) {
                        this._sub = this.periodoConvocatoria.updateEstadoDepOfetaActividades(this.depOfertaActividad.id).subscribe(data => {
                            this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades enviado al Coordinador de Proyectos!' });
                            this.ListarActividadesRealizadas();
                        });
                    } else {
                        this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Informe de actividades ya fue enviado al Coordinador de Proyectos!' });
                    }
                }
            }
        })


    };

    /*************************************************************************************************************** */
    /********************************************PARAMETROS DE LA GRILA DE ACTIVIDADES REALIZADAS DOCENTES******************************************************************* */

    sourceActividadesParticipantes: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idPersona', type: 'int' },
            { name: 'idObjetivoTarea', type: 'int' },
            { name: 'nombres', type: 'string' },
            { name: 'descripcionTarea', type: 'string' },
            { name: 'descripcionActividad', type: 'string' },
            { name: 'horas', type: 'string' },
            { name: 'lugar', type: 'string' },
            { name: 'fechaDesde', type: 'date' },
            { name: 'fechaHasta', type: 'date' },
        ]
    };
    dataAdapterActividadesRealizadas: any = new jqx.dataAdapter(this.sourceActividadesParticipantes);

    columnsActividadesRealizadas: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idPersona', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idObjetivoTarea', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Actividad', datafield: 'descripcionActividad', width: '15%' },
        { text: 'Tarea', datafield: 'descripcionTarea', width: '15%' },
        { text: 'Participante', datafield: 'nombres', width: '15%' },
        { text: 'Fecha Desde', datafield: 'fechaDesde', width: '9%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
        { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '9%', cellsformat: 'yyyy-MM-dd' },
        { text: 'H.trabajadas', datafield: 'horas', width: '15%' },
        { text: 'Lugar', datafield: 'lugar', width: '15%' },
        {
            text: 'Quitar', columntype: 'button', width: '7%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                let idActividadRealizada = this.gridActividadesRealizadas.getcellvalue(this.rowindex, 'id');
                if (idActividadRealizada != null) {
                    this._sub = this.periodoConvocatoria.borrarActividadesRealizadas(idActividadRealizada).subscribe(data => {
                        let id = this.gridActividadesRealizadas.getcellvalue(this.rowindex, 'id');
                        this.gridActividadesRealizadas.deleterow(id);
                        this.rowindex = -1;
                    });
                }

            },
        },
    ];

    /*************************************************************************************************************** */
    /********************************************PARAMETROS DE LA GRILA DE ACTIVIDADES REALIZADAS ESTUDIANTES******************************************************************* */
    sourceActividadesParticipantesEst: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idPersona', type: 'int' },
            { name: 'idObjetivoTarea', type: 'int' },
            { name: 'nombres', type: 'string' },
            { name: 'descripcionTarea', type: 'string' },
            { name: 'descripcionActividad', type: 'string' },
            { name: 'horas', type: 'string' },
            { name: 'lugar', type: 'string' },
            { name: 'fechaDesde', type: 'date' },
            { name: 'fechaHasta', type: 'date' },
        ]
    };

    dataAdapterActividadesRealizadasEst: any = new jqx.dataAdapter(this.sourceActividadesParticipantesEst);

    columnsActividadesRealizadasEst: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idPersona', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idObjetivoTarea', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Actividad', datafield: 'descripcionActividad', width: '15%' },
        { text: 'Tarea', datafield: 'descripcionTarea', width: '15%' },
        { text: 'Participante', datafield: 'nombres', width: '15%' },
        { text: 'Fecha Desde', datafield: 'fechaDesde', width: '9%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
        { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '9%', cellsformat: 'yyyy-MM-dd' },
        { text: 'H.trabajadas', datafield: 'horas', width: '15%' },
        { text: 'Lugar', datafield: 'lugar', width: '15%' },
        {
            text: 'Quitar', columntype: 'button', width: '7%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                let idActividadRealizada = this.gridActividadesRealizadasEst.getcellvalue(this.rowindex, 'id');
                if (idActividadRealizada != null) {
                    this._sub = this.periodoConvocatoria.borrarActividadesRealizadas(idActividadRealizada).subscribe(data => {
                        let id = this.gridActividadesRealizadasEst.getcellvalue(this.rowindex, 'id');
                        this.gridActividadesRealizadasEst.deleterow(id);
                        this.rowindex = -1;
                    });
                }

            },
        },
    ];


    /********************************************************************************************************************************** */
    /***********metodo para obtner el id de la fila seleccionada en la grilla de ACTIVIDADES REALIZADAS*************************************************************/

    Rowselect(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }

    RowselectEst(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }

    /********************************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO ACTIVIDADES, LISTA ACTIVIDADES**************************************************************/

    ListarActividadesRealizadas() {
        let arrayProyecto = JSON.parse(JSON.stringify(this.dropDownProyectos.getSelectedItem())).originalItem;
        if (arrayProyecto.estadoProyecto.trim() != 'APROBADO') {
            this.myModal.alertMessage({ title: 'Asignación de Tareas', msg: 'Proyecto no se encuentra aprobado!' });
            this.gridActividadesRealizadas.clear();
            this.gridActividadesRealizadasEst.clear();
        } else {
            this._sub = this.periodoConvocatoria.buscarActividadesRealizadas(arrayProyecto.idProyecto, this.dropDownConsolidado.val()).subscribe(data => {
                console.log(JSON.stringify(data));
                let docente = data.filter((dats) => { return dats.idTipoUsuarioProyecto == 2 });
                this.sourceActividadesParticipantes.localdata = docente;
                this.dataAdapterActividadesRealizadas.dataBind();

                let estudiante = data.filter((dats) => { return dats.idTipoUsuarioProyecto == 4 });
                this.sourceActividadesParticipantesEst.localdata = estudiante;
                this.dataAdapterActividadesRealizadasEst.dataBind();
                this.buscarDepOfertaActividades(this.dropDownCarrera.val(), this.dropDownConsolidado.val())
            });
        }
    }

    buscarDepOfertaActividades(idDepartamentoOferta: any, idSeguimientoActividad: any) {
        this.depOfertaActividad = {};
        this._sub = this.periodoConvocatoria.buscarDepOfertaActividad(idDepartamentoOferta, idSeguimientoActividad).subscribe(data => {
            //permite conocer si el conjusto de actividades ya ha sido enviado al coodinador de proyectos de la carrera
            console.log('como '+JSON.stringify(data));
            if (data != false) {
                this.depOfertaActividad = data;
            }else{
                this.depOfertaActividad.idSeguimientoActividad=this.dropDownConsolidado.val();
                this.depOfertaActividad.idDepartamentoOferta=this.dropDownCarrera.val();
                this.depOfertaActividad.estadoEnvio='INI';
                this.depOfertaActividad.usuarioIngresoId=this.usuarioActual.id;
                this.depOfertaActividad.estado='A';
            }
        }, error => console.error("h " + error));
    }

    /*************************************************************************************************************** */
    /**************************evento recibido desde el hijo(modalInformeActividades)*************************************************************/

    //actualizaal padre desde el hijo
    procesaPassMethods(h) {
        if (h.val == 'actualizar') {
            this.ListarActividadesRealizadas();
        }
    }

}