import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxNumberInputComponent } from 'jqwidgets-ng/jqxnumberinput';
import { jqxTextAreaComponent } from 'jqwidgets-ng/jqxtextarea';
import { ModalDirective, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { AccountService } from '../../../../services/auth/account.service';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';

@Component({
  selector: 'app-modal-tareas',
  templateUrl: './modal-tareas.component.html',
  styleUrls: ['./modal-tareas.component.scss']
})
export class ModalTareasComponent implements OnInit {

  @ViewChild('formNuevoTarea') private formNuevoTarea: ModalDirective;
  @ViewChild('myfechaDesde') myfechaDesde: jqxDateTimeInputComponent;
  @ViewChild('myfechaHasta') myfechaHasta: jqxDateTimeInputComponent;
  @ViewChild('dropDownObjetivo') dropDownObjetivo: jqxDropDownListComponent;
  @ViewChild('dropDownActividad') dropDownActividad: jqxDropDownListComponent;
  @ViewChild('myinputDescripcion') myinputDescripcion: jqxTextAreaComponent;
  @ViewChild('comuInput') comuInput: jqxNumberInputComponent;
  @ViewChild('coopInput') coopInput: jqxNumberInputComponent;
  @ViewChild('autoGestionInput') autoGestionInput: jqxNumberInputComponent;
  @ViewChild('creditoFIInput') creditoFIInput: jqxNumberInputComponent;
  @ViewChild('fiscalesInput') fiscalesInput: jqxNumberInputComponent;
  @ViewChild('creditoFEInput') creditoFEInput: jqxNumberInputComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

  @Input() proyecto: any = {};
  _sub: Subscription;
  tareas: any = {};
  @Output() PassMethods: EventEmitter<any> = new EventEmitter();
  usuarioActual: any = {};//variable para guardar el usuario actual
  idActividad: any = null;
  protected config = { backdrop: true, ignoreBackdropClick: true };

  constructor(protected modalService: BsModalService, private periodoConvocatoria: VinculacionService,
    public datePipe: DatePipe, private accountService: AccountService) { }

  ngOnInit() {
    this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
  }

  NuevaTarea(Fecha_Hora: any, dataComponente: any): void {
    console.log(Fecha_Hora);
    let COM = dataComponente.filter((dats) => { return dats.codigo == 'COM' });
    this.sourceObjetivos.localdata = COM;
    this.dataAdapterObjetivo.dataBind();

    let fecha = new Date(Fecha_Hora)
    this.myfechaDesde.value(fecha);
    let fechaFin = new Date(fecha.setMinutes(fecha.getMinutes() + 30))
    this.myfechaHasta.value(fechaFin);
    this.tareas.idObjetivoTarea = null;
    this.formNuevoTarea.config = this.config;//Establece las configuraciones para el modal
    this.formNuevoTarea.show();
  }

  editarTarea(appoint: any, dataComponente: any, dataTarea: any): void {
    let COM = dataComponente.filter((dats) => { return dats.codigo == 'COM' });
    this.sourceObjetivos.localdata = COM;
    this.dataAdapterObjetivo.dataBind();

    let COM1 = COM.filter((dats) => { return dats.imlDescripcionCorta.trim() == appoint.args.appointment.originalData.calendar.trim() });
    this.dropDownObjetivo.val(COM1[0].idItemMarcoLogico);
    let ACTIVIDAD = dataTarea.filter((dats) => { return dats.idObjetivoTarea == appoint.args.appointment.originalData.idObjetivoTarea });
    this.idActividad = ACTIVIDAD[0].idObjetivoActividad;
    let fechaDesde = new Date(appoint.args.appointment.originalData.fechaDesde);
    let fechaHasta = new Date(appoint.args.appointment.originalData.fechaHasta);
    this.myfechaDesde.value(fechaDesde);
    this.myfechaHasta.value(fechaHasta);
    this.tareas.idObjetivoTarea = appoint.args.appointment.originalData.idObjetivoTarea;
    this.myinputDescripcion.val(appoint.args.appointment.originalData.subject)
    this.formNuevoTarea.config = this.config;//Establece las configuraciones para el modal
    this.formNuevoTarea.show();
  }

  protected Submit() {
    /*this.tareas.resourceId = this.dropDownObjetivo.getSelectedItem().label;
    this.PassMethods.emit({ val: 'actualizar'});
    this.limpiar();
    this.formNuevoTarea.ngOnDestroy();*/
    if (this.funValidar() == true) {
      let datosTareas = this.formarJsonProyecto();
      this.guardar(datosTareas);
      this.myModal.alertMessage({ title: 'Registro Nuevo Tareas', msg: 'Datos Guardados Correctamente!' });
      this.limpiar();
      this.formNuevoTarea.ngOnDestroy();
    }
  }

  guardar(datosTareas: void) {
    this._sub = this.periodoConvocatoria.grabarObjetivoTarea(datosTareas).subscribe(result => {
      //para actulizar grid del padre(registro_proyecto)
      this.PassMethods.emit({ val: 'actualizar' });
    }, error => console.error("h " + error));
  }

  formarJsonProyecto() {
    let objetivoTarea: any = {};
    objetivoTarea.descripcion = this.myinputDescripcion.val();
    objetivoTarea.id = this.tareas.idObjetivoTarea;
    objetivoTarea.idObjetivoActividad = this.dropDownObjetivo.val();
    objetivoTarea.creditoInt = this.creditoFIInput.val();
    objetivoTarea.creditoExt = this.creditoFEInput.val();
    objetivoTarea.cooperacion = this.coopInput.val();
    objetivoTarea.estadoTarea = 'NE';
    //objetivoTarea.fechaDesde = this.datePipe.transform(this.myfechaDesde.value(), "yyyy-MM-dd HH:mm:ss");
    //objetivoTarea.fechaHasta = this.datePipe.transform(this.myfechaHasta.value(), "yyyy-MM-dd HH:mm:ss");
    objetivoTarea.fechaDesde = this.tareas.fechaDesde;
    objetivoTarea.fechaHasta = this.tareas.fechaHasta;
    //objetivoTarea.estado;
    objetivoTarea.usuarioIngresoId = this.usuarioActual.id;
    objetivoTarea.autogestion = this.autoGestionInput.val();
    objetivoTarea.fiscales = this.fiscalesInput.val();
    objetivoTarea.aporteComunit = this.comuInput.val();
    //json    
    return objetivoTarea;
  }

  funValidar() {
    let validar = true;
    if (this.dropDownObjetivo.val() == "") {
      validar = false;
      this.myModal.alertMessage({ title: 'Registro de Nueva Tarea', msg: 'Seleccione un Objetivo!' });
    }
    if (this.dropDownActividad.val() == "") {
      validar = false;
      this.myModal.alertMessage({ title: 'Registro de Nueva Tarea', msg: 'Seleccione un Actividad!' });
    }
    if (this.myinputDescripcion.val() == "") {
      this.myModal.alertMessage({ title: 'Registro de Nueva Tarea', msg: 'Ingrese una descripción para la tarea!' });
      validar = false;
    }
    if ((this.myfechaDesde.value() != null) && (this.myfechaHasta.value() != null)) {
      var dateFechadesde = String(this.myfechaDesde.value());
      var dateFechaHasta = String(this.myfechaHasta.value());
      //convierte las fechasen numeros
      var d = Date.parse(dateFechadesde);
      var h = Date.parse(dateFechaHasta);
      if (d >= h) {
        console.log("mayor " + dateFechadesde);
        this.myModal.alertMessage({ title: 'Registro de Nueva Tarea', msg: 'Fecha Fin debe ser mayor a fecha Inicial!' });
        validar = false;
      }
    }
    //convierte las fechasen numeros
    var pfd = Date.parse(this.proyecto.fechaDesde);
    var pfh = Date.parse(this.proyecto.fechaHasta);
    var tfd = Date.parse(String(this.myfechaDesde.value()));
    var tfh = Date.parse(String(this.myfechaHasta.value()));
    debugger
    if ((tfd >= pfd && tfd <= pfh)&&(tfh >= pfd && tfh <= pfh)) {
    }else{
      this.myModal.alertMessage({ title: 'Registro de Nueva Tarea', msg: 'La tarea debe estar entres las fechas inicio y fin del proyecto!' });
      validar = false;
      this.cancelar();       
        
    }
    return validar;
  }

  cancelar() {
    this.limpiar();
    this.formNuevoTarea.ngOnDestroy();
  }

  //**************************listar combox objetivo*************************************************************

  sourceObjetivos: any = {
    datatype: 'json',
    id: 'idItemMarcoLogico',
    localdata: [
      { name: 'estado', type: 'string' },
      { name: 'imlDescripcionCorta', type: 'string' },
      { name: 'idItemMarcoLogico', type: 'int' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterObjetivo: any = new jqx.dataAdapter(this.sourceObjetivos);

  mostrarDropActividades(event: any) {
    this._sub = this.periodoConvocatoria.buscarActividad(event.args.item.value).subscribe(data => {
      //console.log(JSON.stringify(data));
      if (this.idActividad != null) {
        this.sourceActividades.localdata = data;
        this.dataAdapterActividad.dataBind();
        this.dropDownActividad.val(this.idActividad);
      } else {
        this.sourceActividades.localdata = data;
        this.dataAdapterActividad.dataBind();
      }
    });
  }
  /*************************************************************************************************************** */
  //**************************listar combox actividades*************************************************************
  sourceActividades: any = {
    datatype: 'json',
    id: 'idObjetivoActividad',
    localdata: [
      { name: 'estado', type: 'string' },
      { name: 'oaDescripcion', type: 'string' },
      { name: 'idObjetivoActividad', type: 'int' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterActividad: any = new jqx.dataAdapter(this.sourceActividades);

  limpiar() {
    this.tareas.idObjetivoTarea = undefined;
    this.tareas.resourceId = "";
    this.dropDownObjetivo.clearSelection();
    this.dropDownActividad.clearSelection();
    this.tareas.subject = "";
    this.myinputDescripcion.val("");
    this.creditoFEInput.val("");
    this.coopInput.val("");
    this.creditoFIInput.val("");
    this.autoGestionInput.val("");
    this.comuInput.val("");
    this.fiscalesInput.val("");
    this.idActividad = null;
    this.sourceActividades.localdata = [];
    this.dataAdapterActividad.dataBind();
  }
}
