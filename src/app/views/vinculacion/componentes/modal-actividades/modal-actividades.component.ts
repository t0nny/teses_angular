import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { jqxInputComponent } from "jqwidgets-ng/jqxinput";
import { jqxNumberInputComponent } from "jqwidgets-ng/jqxnumberinput";
import { jqxTextAreaComponent } from "jqwidgets-ng/jqxtextarea";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../services/auth/account.service";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";

@Component({
  selector: 'app-modal-actividades',
  templateUrl: './modal-actividades.component.html',
  //styleUrls: ['./modal-tareas.component.scss']
})
export class ModalActividadesComponent implements OnInit {

  @ViewChild('formNuevoActividad') private formNuevoActividad: ModalDirective;
  @ViewChild('gridActividades') gridActividades: jqxGridComponent;
  @ViewChild('inputDescripcionAct') inputDescripcionAct: jqxTextAreaComponent;
  @ViewChild('inputDescripcion') inputDescripcion: jqxTextAreaComponent;
  @ViewChild('inputIndicador') inputIndicador: jqxTextAreaComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
  @ViewChild('lineaBaseInput') lineaBaseInput: jqxNumberInputComponent;
  @ViewChild('metaProgramadaInput') metaProgramadaInput: jqxNumberInputComponent;
  @ViewChild('tiempoPlaneadoInput') tiempoPlaneadoInput: jqxNumberInputComponent;
  @ViewChild('dropDownTipoObjetivo') dropDownTipoObjetivo: jqxDropDownListComponent;
  @ViewChild('inputDescripcionCorta') inputDescripcionCorta: jqxInputComponent;
  idItemMarcoLogicoPadre: any;
  rowindexUpdate: number = -1;

  constructor(private periodoConvocatoria: VinculacionService, private accountService: AccountService) { }

  _sub: Subscription;
  componente: any = {};
  dataDelGridComponentes: any = {};
  rowindex: number = -1;
  gridActividadesDelete: any = [];//almacenara los id de proyectosDocentes a eliminar
  public mostrarActividades = true;
  /**Permite controlar que no salga del modal sin presionar algun boton de éste */
  protected config = { backdrop: true, ignoreBackdropClick: true };
  usuarioActual: any = {};//variable para guardar el usuario actual
  @Output() PassMethods: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
    this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
  }

  nuevaActividad(dataComponentes: any): void {
    //let datata = componentesData.filter((dats) => { return dats.idItemMarcoLogico == idItemMarcoLogico })
    this.dataDelGridComponentes = dataComponentes;
    this.formNuevoActividad.show();
    this.formNuevoActividad.config = this.config;//Establece las configuraciones para el modal
    this.dropDownTipoObjetivo.disabled(false);
  }

  editarActividad(idItemMarcoLogico: any, dataComponentes: any): void {
    this.dropDownTipoObjetivo.disabled(false);
    if (idItemMarcoLogico != -1) {
      this.dataDelGridComponentes = dataComponentes;
      this.cargarActividades(idItemMarcoLogico);
    }
    this.formNuevoActividad.show();
    this.formNuevoActividad.config = this.config;//Establece las configuraciones para el modal

  }

  cargarActividades(idItemMarcoLogico: any) {
    //let datata = componentesData.filter((dats) => { return dats.idItemMarcoLogico == idItemMarcoLogico });
    //this.componente=datata[0];
    this._sub = this.periodoConvocatoria.buscarActividad(idItemMarcoLogico).subscribe(data => {
      //console.log(JSON.stringify(data));
      this.componente = data[0];
      this.inputIndicador.val(data[0].indicador);
      this.inputDescripcion.val(data[0].imlDescripcion);
      this.inputDescripcionCorta.val(data[0].imlDescripcionCorta);
      this.metaProgramadaInput.setDecimal(data[0].metaProgramada);
      this.dropDownTipoObjetivo.val(data[0].idTipoObjetivoProyecto);
      this.lineaBaseInput.setDecimal(data[0].lineaBase);
      this.tiempoPlaneadoInput.setDecimal(data[0].tiempoPlaneado);
      this.sourceActividades.localdata = data;
      this.dataAdapterActividades.dataBind();
      this.dropDownTipoObjetivo.disabled(true);
    });

  }

  agregarActividad(event: any) {

    let gridActividades = this.gridActividades.getrows();
    let getSize = gridActividades.length;
    this.gridActividades.selectrow(getSize - 1);
    let i = 1;
    //let arrayOfIdEstudiantesCo = JSON.parse(JSON.stringify(this.dropDownEstudiantesCo.getSelectedItem())).originalItem;
    if (this.inputDescripcionAct.val() != "") {
      if (getSize == 0) {
        //Agrega una nueva fila
        //this.aggGripEstudianteProyectoCo();
        let datarow = {};
        datarow['id'] = i;
        datarow['idOaItemMarcoLogico'] = this.componente.idOaItemMarcoLogico;
        datarow['oaDescripcion'] = this.inputDescripcionAct.val();
        this.gridActividades.addrow(i, datarow);
        this.inputDescripcionAct.val("");
      } else {
        if (this.rowindexUpdate == -1) {
          let data1 = gridActividades.filter((dats) => { return dats.oaDescripcion.trim() == this.inputDescripcionAct.val() })
          if (data1.length > 0) {
            this.myModal.alertMessage({ title: 'Registro Componente', msg: 'Actividad ya ha sido agregado!' });
            this.gridActividades.clearselection();
          } else {
            i++;
            let datarow = {};
            datarow['id'] = i;
            datarow['idOaItemMarcoLogico'] = this.componente.idOaItemMarcoLogico;
            datarow['oaDescripcion'] = this.inputDescripcionAct.val();
            this.gridActividades.addrow(i, datarow);
            // this.aggGripEstudianteProyectoCo(arrayOfIdEstudiantesCo);
            this.inputDescripcionAct.val("");
          }
        }else{
          let data1 = gridActividades.filter((dats) => { return dats.oaDescripcion.trim() == this.inputDescripcionAct.val() })
          if (data1.length > 0) {
            this.myModal.alertMessage({ title: 'Registro Componente', msg: 'Actividad ya ha sido agregado!' });
            this.gridActividades.clearselection();
          } else {
            let idActividad = this.gridActividades.getcellvalue(this.rowindexUpdate, 'idObjetivoActividad');
            let datarow = {};
            datarow['idObjetivoActividad'] = idActividad;
            datarow['idOaItemMarcoLogico'] = this.componente.idOaItemMarcoLogico;
            datarow['oaDescripcion'] = this.inputDescripcionAct.val();
            this.gridActividades.updaterow(this.rowindexUpdate,datarow);
            this.rowindexUpdate = -1;
            this.inputDescripcionAct.val("");
          }
        }
      }
    } else {
      this.myModal.alertMessage({ title: 'Registro Componente', msg: 'Ingrese un descripcion para la actividad!' });
    }

  }

  sourceActividades: any =
    {
      datatype: 'array',
      id: 'id',
      datafields:
        [
          { name: 'id', type: 'int' },
          { name: 'idObjetivoActividad', type: 'int' },
          { name: 'idOaItemMarcoLogico', type: 'int' },
          { name: 'oaDescripcion', type: 'string' },
        ]
    };
  dataAdapterActividades: any = new jqx.dataAdapter(this.sourceActividades);

  columnsActividades: any[] =
    [
      { text: 'Id Actividad', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Id Actividad', datafield: 'idObjetivoActividad', width: '2%', filtertype: 'none', hidden: true },
      { text: 'id Item Marco Logico', datafield: 'idOaItemMarcoLogico', width: '20%', filtertype: 'none', hidden: true },
      { text: 'Descripcion', datafield: 'oaDescripcion', width: '90%' },
      {
        text: 'Quitar', columntype: 'button', width: '10%',
        cellsrenderer: (): string => {
          return 'Quitar';
        },
        buttonclick: (row: number): void => {
          let idActividad = this.gridActividades.getcellvalue(this.rowindex, 'idObjetivoActividad');
          if (idActividad != null) {
            this.gridActividadesDelete.push(idActividad);
            let id = this.gridActividades.getcellvalue(this.rowindex, 'uid');
            this.gridActividades.deleterow(id);
          } else {
            let id = this.gridActividades.getcellvalue(this.rowindex, 'id');
            this.gridActividades.deleterow(id);
          }
          this.rowindex = -1;
        },
      },
    ];

  /********************************************************************************************************************************** */
  /***********metodo para obtner el id de la fila seleccionada en la grilla de DOCENTES PARTICIPANTES*************************************************************/

  Rowselect(event: any): void {
    if (event.args) {
      this.rowindex = event.args.rowindex;
    }
  }

  cancelar() {
    this.limpiar()
    this.sourceActividades.localdata = [];
    this.dataAdapterActividades.dataBind();
    this.formNuevoActividad.ngOnDestroy();
  }

  limpiar() {
    this.dropDownTipoObjetivo.clearSelection();
    this.componente.imlDescripcion = "";
    this.componente.imlDescripcionCorta = "";
    this.componente.indicador = "";;
    this.componente.metaProgramada = 0;
    this.componente.tiempoPlaneado = 0;
    this.componente.lineaBase = 0;
    this.lineaBaseInput.val("");
    this.metaProgramadaInput.val("");
    this.tiempoPlaneadoInput.val("");
    this.inputDescripcion.val("");
    this.inputDescripcionCorta.val("");
    this.inputDescripcionAct.val("");
    this.inputIndicador.val("");
    this.mostrarActividades = true;
    this.componente = [];
  }

  tipoObjetivo: any[] = [
    { id: 1, descripcion: 'FIN' },
    { id: 2, descripcion: 'PROPÓSITO' },
    { id: 3, descripcion: 'COMPONENTE' },
  ]

  tipoObjetivoSource: any =
    {
      datatype: 'array',
      datafields: [
        { name: 'descripcion', type: 'string' },
        { name: 'id', type: 'int' }
      ],
      localdata: this.tipoObjetivo
    };
  dataAdapterTipoObjetivo: any = new jqx.dataAdapter(this.tipoObjetivoSource, { autoBind: true });

  mostrarGridActividades(event: any) {
    this.idItemMarcoLogicoPadre = null;
    let FIN = this.dataDelGridComponentes.filter((dats) => { return dats.idTipoObjetivoProyecto == 1 });
    let PRO = this.dataDelGridComponentes.filter((dats) => { return dats.idTipoObjetivoProyecto == 2 });
    if (this.dropDownTipoObjetivo.getSelectedItem().label == 'COMPONENTE') {
      this.mostrarActividades = false;
      this.idItemMarcoLogicoPadre = PRO[0].idItemMarcoLogico;
    } else {
      this.mostrarActividades = true;
      if (this.dropDownTipoObjetivo.getSelectedItem().label == 'PROPÓSITO') {
        this.idItemMarcoLogicoPadre = FIN[0].idItemMarcoLogico;
      }
    }
  }

  protected Submit() {
    //this.a = 'ok';
    //this.PassMethods.emit({ val: this.a });
    //this.modalForm.hide();
    //this.formNuevo.hide();
    if (this.funValidar() == true) {
      let itemMarcoLogico = this.formarJsonitemMarcoLogico();
      this.guardar(itemMarcoLogico);
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Datos Guardados Correctamente!' });
      //this.formNuevo.hide();      
      this.cancelar();
    }
  }

  guardar(datosProyecto: any) {
    this._sub = this.periodoConvocatoria.grabarItemMarcoLogico(datosProyecto).subscribe(result => {
      //para actulizar grid del padre(evaluar_proyecto)
      this.PassMethods.emit({ val: 'actualizar' });
    }, error => console.error("Error al grabar Proyecto Marco Lógico " + error));
  }

  formarJsonitemMarcoLogico() {
    let datosProyecto: any = {};
    let arrItemMarcoLogico: any = [];
    let arrObjetivoActividad: any = [];
    let objObjetivoActividad: any = {};
    let objItemMarcoLogico: any = {};
    //proyecto MARCO LOGICO
    let gridActividades = this.gridActividades.getrows();
    let getSize = gridActividades.length;
    if (getSize > 0) {
      for (let index = 0; index < gridActividades.length; index++) {
        objObjetivoActividad = {};
        objObjetivoActividad.id = gridActividades[index].idObjetivoActividad;
        objObjetivoActividad.descripcion = gridActividades[index].oaDescripcion;
        objObjetivoActividad.version = 1;
        objObjetivoActividad.usuarioIngresoId = this.usuarioActual.id;
        objObjetivoActividad.estado = 'A';
        arrObjetivoActividad.push(objObjetivoActividad);
      }
    }
    objItemMarcoLogico.id = this.componente.idItemMarcoLogico;
    objItemMarcoLogico.idItemMarcoLogicoPadre = this.idItemMarcoLogicoPadre;
    objItemMarcoLogico.idTipoObjetivoProyecto = Number(this.dropDownTipoObjetivo.val());
    objItemMarcoLogico.indicador = this.inputIndicador.val();
    objItemMarcoLogico.descripcion = this.inputDescripcion.val();
    objItemMarcoLogico.descripcionCorta = this.inputDescripcionCorta.val();
    objItemMarcoLogico.tiempoPlaneado = this.tiempoPlaneadoInput.val();
    objItemMarcoLogico.metaProgramada = this.metaProgramadaInput.val();
    objItemMarcoLogico.lineaBase = this.lineaBaseInput.val();
    objItemMarcoLogico.objetivoActividad = arrObjetivoActividad;
    objItemMarcoLogico.version = 1;
    objItemMarcoLogico.usuarioIngresoId = this.usuarioActual.id;
    objItemMarcoLogico.estado = 'A';
    arrItemMarcoLogico.push(objItemMarcoLogico);

    datosProyecto.id = this.dataDelGridComponentes[0].idProyecto;
    datosProyecto.itemMarcoLogico = arrItemMarcoLogico;

    return datosProyecto;
  }

  funValidar() {
    let validar = true;
    if (this.componente.idItemMarcoLogico != undefined) {
      let descripcion = this.dataDelGridComponentes.filter((dats) => { return dats.imlDescripcion.trim() == this.inputDescripcion.val() });
      if (descripcion.length > 0) {
        if (descripcion[0].idItemMarcoLogico != this.componente.idItemMarcoLogico) {
          this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'El proyecto ya cuenta con una descripción similar!' });
          validar = false;
        }
      }
    } else {
      if (this.dropDownTipoObjetivo.val() == "") {
        validar = false;
        this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Seleccione tipo de objetivo!' });
      } else {
        validar = this.funValidarObjetivos(this.dropDownTipoObjetivo.val());
      }
      if (this.inputDescripcion.val() == "") {
        this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Ingrese la descripción del objetivo!' });
        validar = false;
      }
      if (this.inputDescripcionCorta.val() == "") {
        this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Ingrese la descripción del objetivo!' });
        validar = false;
      }
      if (this.inputIndicador.val() == "") {
        this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Ingrese un indicador meta!' });
        validar = false;
      }
      if (this.metaProgramadaInput.val() == 0) {
        this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Ingrese una meta programada!' });
        validar = false;
      }
      if (this.tiempoPlaneadoInput.val() == 0) {
        this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Ingrese un tiempo planeado!' });
        validar = false;
      }
      if (this.dropDownTipoObjetivo.val() == 3) {
        let gridActividades = this.gridActividades.getrows();
        let getSize = gridActividades.length;
        if (getSize == 0) {
          this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'Ingrese alguna actividad!' });
          validar = false;
        }
      }
    }
    return validar;
  }

  funValidarObjetivos(tipoObjetivo: any): boolean {
    let validar = true;
    switch (tipoObjetivo) {
      //Opciones de Crud  
      case '1':
        console.log('OBJETIVO DESARROLLO');
        let FIN = this.dataDelGridComponentes.filter((dats) => { return dats.idTipoObjetivoProyecto == 1 });
        if (FIN.length > 0) {
          this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'El proyecto ya cuenta con un objetivo de Desarrollo!' });
          validar = false;
        }
        break;
      case '2':
        console.log('OBJETIVO GENERAL')
        let PRO = this.dataDelGridComponentes.filter((dats) => { return dats.idTipoObjetivoProyecto == 2 });
        if (PRO.length > 0) {
          this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'El proyecto ya cuenta con un objetivo general!' });
          validar = false;
        }
        break;
      case '3':
        console.log('OBJETIVO ESPECIFICO')
        let COM = this.dataDelGridComponentes.filter((dats) => { return dats.imlDescripcion.trim() == this.inputDescripcion.val() });
        if (COM.length > 0) {
          this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'El proyecto ya cuenta con un objetivo general!' });
          validar = false;
        }
        let COM1 = this.dataDelGridComponentes.filter((dats) => { return dats.imlDescripcionCorta.trim() == this.inputDescripcionCorta.val() });
        if (COM1.length > 0) {
          this.myModal.alertMessage({ title: 'Matriz de Marco Lógico', msg: 'El proyecto ya cuenta con la descripcion corta!' });
          validar = false;
        }
        break;
      default:
      //default code block
    }
    return validar;
  }

  Celldoubleclick(event: any): void {
    if (event.args) {
      this.rowindexUpdate = event.args.rowindex;
      this.inputDescripcionAct.val(event.args.value)
    }
  }

}


