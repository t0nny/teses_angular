import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { data } from "autoprefixer";
import { jqxDateTimeInputComponent } from "jqwidgets-ng/jqxdatetimeinput";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { jqxInputComponent } from "jqwidgets-ng/jqxinput";
import * as moment from "moment";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../services/auth/account.service";
import { FileService } from "../../../../services/file/file.service";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";

@Component({
    selector: 'app-modal-informe-actividades',
    templateUrl: './modal-informe-actividades.component.html',
    //styleUrls: ['./modal-tareas.component.scss']
})
export class ModalInformeActividadesComponent implements OnInit {

    constructor(private fileArchivo: FileService,
        private periodoConvocatoria: VinculacionService, private accountService: AccountService) { }

    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild('formNuevoInformeActividad') private formNuevoInformeActividad: ModalDirective;
    @ViewChild('myfechaHasta') myfechaHasta: jqxDateTimeInputComponent;
    @ViewChild('myfechaDesde ') myfechaDesde: jqxDateTimeInputComponent;
    @ViewChild('dropDownParticipante') dropDownParticipante: jqxDropDownListComponent;
    @ViewChild('dropDownActividad') dropDownActividad: jqxDropDownListComponent;
    @ViewChild('dropDownObjetivo') dropDownObjetivo: jqxDropDownListComponent;
    @ViewChild('dropDownTarea') dropDownTarea: jqxDropDownListComponent;
    @ViewChild('myinputLugar') myinputLugar: jqxInputComponent;

    /**Permite controlar que no salga del modal sin presionar algun boton de éste */
    protected config = { backdrop: true, ignoreBackdropClick: true };
    usuarioActual: any = {};//variable para guardar el usuario actual
    total: number = 0;
    _sub: Subscription;
    depOfertaActividad: any = {};
    dataParticipantes: any = {};
    dataActividadesTareas: any = {};
    fileSelecionado: any = '';
    documentoSoporteUrl: string = '';
    actividadParticipante: any = {};
    validarFecha: boolean = false;
    participante: any = '';
    editar: boolean = false;
    ban: boolean = false;
    @Output() PassMethods: EventEmitter<any> = new EventEmitter();

    ngOnInit(): void {
        this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
    }

    ValueChanged(event: any): void {
        this.newMethod();

    }

    private newMethod() {
        console.log('1');
        if (this.ban) {
            if (this.myfechaDesde.val() != undefined) {
                console.log('2');
                // Do Something
                var d = Date.parse(this.myfechaDesde.val());
                var h = Date.parse(this.myfechaHasta.val());
                if (d >= h) {
                    console.log('3');
                    this.myModal.alertMessage({ title: 'Registro de Actividades', msg: 'Fecha Hasta debe ser mayor a fecha Inicial!' });
                } else {
                    this.validarFecha = true;
                    var fecha1 = moment(this.myfechaDesde.val(), "YYYY-MM-DD HH:mm:ss");
                    var fecha2 = moment(this.myfechaHasta.val(), "YYYY-MM-DD HH:mm:ss");
                    console.log('4');
                    var diff = fecha2.diff(fecha1, 'd'); // Diff in days
                    console.log(diff);

                    var diff = fecha2.diff(fecha1, 'h'); // Diff in hours
                    this.total = diff;
                    console.log(diff);
                }
            }
        }
    }

    nuevaActividad(idProyecto: any, participante: any, depOfertaActividad: any): void {
        this.ban=true;
        //console.log('depOfertaActividad'+JSON.stringify(depOfertaActividad));
        //let datata = componentesData.filter((dats) => { return dats.idItemMarcoLogico == idItemMarcoLogico })
        // this.dataDelGridComponentes = dataComponentes;
        this.depOfertaActividad = depOfertaActividad;
        let fecha = new Date()
        this.myfechaDesde.value(fecha);
        this.myfechaHasta.value(fecha);
        this.participante = participante;
        this.cargarParticipantesCombo(idProyecto, participante);
        this.llenarCombos(idProyecto)
        this.dropDownParticipante.disabled(false);
        this.dropDownObjetivo.disabled(false);
        this.dropDownActividad.disabled(false);
        this.dropDownTarea.disabled(false);
        this.formNuevoInformeActividad.show();
        this.formNuevoInformeActividad.config = this.config;//Establece las configuraciones para el modal
        //this.dropDownTipoObjetivo.disabled(false);
    }

    editarActividad(idProyecto: any, idActividadParticipante: any, participante: any, depOfertaActividad: any): void {
        //console.log('depOfertaActividad'+JSON.stringify(depOfertaActividad));
        this.ban=true;
        this.depOfertaActividad = depOfertaActividad;
        this.editar = true;
        this.participante = participante;
        this.cargarParticipantesCombo(idProyecto, participante);
        this.llenarCombos(idProyecto)
        setTimeout(() => {

            this._sub = this.periodoConvocatoria.editarActividadesRealizadas(idActividadParticipante).subscribe(data => {
                this.actividadParticipante.id = data[0].id;
                this.dropDownParticipante.checkItem(data[0].idPersona);
                this.dropDownObjetivo.val(data[0].idObjetivo);
                this.dropDownActividad.val(data[0].idObjetivoActividad);
                this.dropDownTarea.val(data[0].idObjetivoTarea);
                this.documentoSoporteUrl = data[0].documentoSoporteUrl;
                this.actividadParticipante.observacion = data[0].observaciones;
                this.total = data[0].horas;
                this.actividadParticipante.lugar = data[0].lugar;
                let fechaDesde = new Date(data[0].fechaDesde);
                let fechaHasta = new Date(data[0].fechaHasta);
                this.myfechaDesde.value(fechaDesde);
                this.myfechaHasta.value(fechaHasta);
                this.dropDownParticipante.disabled(true);
                this.dropDownObjetivo.disabled(true);
                this.dropDownActividad.disabled(true);
                this.dropDownTarea.disabled(true);
                this.formNuevoInformeActividad.show();
                this.formNuevoInformeActividad.config = this.config;//Establece las configuraciones para el modal

            });
        });

    }

    cargarParticipantesCombo(idProyecto: any, participante: any) {
        if (participante == 'DOCENTE') {
            this.dataParticipantes = {};
            this._sub = this.periodoConvocatoria.listarDocentesProyecto(idProyecto).subscribe(dataDocente => {
                this.dataParticipantes = dataDocente;
                let hash = {};
                this.dataParticipantes = this.dataParticipantes.filter(o => hash[o.idPersona] ? false : hash[o.idPersona] = true);
                this.sourceParticipante.localdata = this.dataParticipantes;
                this.dataAdapterParticipante.dataBind();
            });
        }
        if (participante == 'ESTUDIANTE') {
            this.dataParticipantes = {};
            this._sub = this.periodoConvocatoria.listarEstudianteProyecto(idProyecto).subscribe(dataEstudiante => {
                this.dataParticipantes = dataEstudiante;
                let hash = {};
                this.dataParticipantes = this.dataParticipantes.filter(o => hash[o.idPersona] ? false : hash[o.idPersona] = true);
                this.sourceParticipante.localdata = this.dataParticipantes;
                this.dataAdapterParticipante.dataBind();
            });
        }
    }

    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourceParticipante: any = {
        datatype: 'json',
        id: 'idPersona',
        localdata: [
            { name: 'idPersona', type: 'int' },
            { name: 'nombres', type: 'string' },
            { name: 'estado', type: 'string' }

        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
    dataAdapterParticipante: any = new jqx.dataAdapter(this.sourceParticipante);

    /********************************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO TAREAS, LISTA TAREAS**************************************************************/


    mostrarTareas(event: any) {
        if (this.editar == false) {
            let tareas = this.dataActividadesTareas.filter((dats) => { return dats.otIdObjetivoActividad == event.args.item.value && dats.estadoTarea == 'NE' });
            this.sourcetareas.localdata = tareas;
            this.dataAdapterTareas.dataBind();
        } else {
            let tareas = this.dataActividadesTareas.filter((dats) => { return dats.otIdObjetivoActividad == event.args.item.value });
            this.sourcetareas.localdata = tareas;
            this.dataAdapterTareas.dataBind();
        }

    }

    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourcetareas: any = {
        datatype: 'json',
        id: 'idObjetivoTarea',
        localdata: [
            { name: 'idObjetivoTarea', type: 'int' },
            { name: 'descripcionTarea', type: 'string' },
            { name: 'estadoTarea', type: 'string' }

        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
    dataAdapterTareas: any = new jqx.dataAdapter(this.sourcetareas);

    llenarCombos(idProyecto: any) {
        this._sub = this.periodoConvocatoria.listarActividadesProyecto(idProyecto).subscribe(data => {
            this.dataActividadesTareas = data;
            let hash = {};
            //viene de la bd un array con los datos de actividad y tareas, la siguiente funcion filtra y obtiene solo
            //las actividades 
            let data1 = data.filter(o => hash[o.idItemMarcoLogico] ? false : hash[o.idItemMarcoLogico] = true);
            this.sourceObjetivo.localdata = data1;
            this.dataAdapterObjetivo.dataBind();

        });
    }

    /*************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO objetivo, LISTA objetivo**************************************************************/

    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourceObjetivo: any = {
        datatype: 'json',
        id: 'idItemMarcoLogico',
        localdata: [
            { name: 'idItemMarcoLogico', type: 'int' },
            { name: 'descripcionItemMarcoLogico', type: 'string' },
            { name: 'estado', type: 'string' }

        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
    dataAdapterObjetivo: any = new jqx.dataAdapter(this.sourceObjetivo);

    /*************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO ACTIVIDADES, LISTA ACTIVIDADES**************************************************************/

    mostrarActividades(event: any) {
        let actividad = this.dataActividadesTareas.filter((dats) => { return dats.idItemMarcoLogico == event.args.item.value });
        this.sourceActividad.localdata = actividad;
        this.dataAdapterActividad.dataBind();
    }

    //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
    sourceActividad: any = {
        datatype: 'json',
        id: 'idObjetivoActividad',
        localdata: [
            { name: 'idObjetivoActividad', type: 'int' },
            { name: 'descripcionActividad', type: 'string' },
            { name: 'estado', type: 'string' }

        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE ACTIVIDADES
    dataAdapterActividad: any = new jqx.dataAdapter(this.sourceActividad);

    cancelar() {
        this._sub.unsubscribe();
        this.limpiar();
        this.formNuevoInformeActividad.hide();
    }

    limpiar() {
        //this.tareas.idObjetivoTarea = undefined;
        this.documentoSoporteUrl = "";
        this.actividadParticipante.lugar = "";
        this.actividadParticipante.observacion = "";
        this.dropDownObjetivo.clearSelection();
        this.dropDownActividad.clearSelection();
        this.dropDownParticipante.clearSelection();
        this.dropDownTarea.clearSelection();
        let fecha = new Date()
        this.myfechaDesde.value(fecha);
        this.myfechaHasta.value(fecha);
        this.total = 0;
        this.myinputLugar.val('');
        this.participante = '';
        this.editar = false;
        this.depOfertaActividad = {};
        this.ban=false;
    }

    onUploadStart(event: FileList): void {
        console.log('asi');
        let fileOld = event.item(0);
        let fileNew: FormData = new FormData();
        let namefileSinExtension: string = fileOld.name.split('?')[0].split('.').shift();
        let fileExtension: string = fileOld.name.split('?')[0].split('.').pop();
        this.documentoSoporteUrl = namefileSinExtension + '.' + fileExtension;
        fileNew.append('file', fileOld, this.documentoSoporteUrl);
        this.fileSelecionado = fileNew;
    };

    protected Submit() {
        if (this.funValidar() == true) {
            let datosActividadParticipantes = this.formarJsonProyecto();
            this.guardar(datosActividadParticipantes);
        }
    }

    guardar(datosActividadParticipantes: void) {
        if (this.fileSelecionado != '') {
            this._sub = this.fileArchivo.pushFileToStoragePP(this.fileSelecionado).subscribe(result => {
                console.log('resuk ' + result);
            }, error => {
                console.error("Error al subir file al server " + error)
                this.myModal.alertMessage({ title: 'Registro de Actividades', msg: 'Error al subir file al server!' });
                return;
            });
        }

        this._sub = this.periodoConvocatoria.grabarActividadParticipante(datosActividadParticipantes).subscribe(result => {
            this.PassMethods.emit({ val: "actualizar" });
            this.myModal.alertMessage({ title: 'Registro de Actividades', msg: 'Infornme guardado correctamente!' });
            this.formNuevoInformeActividad.ngOnDestroy();
            this.limpiar();
        }, error => {
            console.error("Error al guardar informe" + error)
            this.myModal.alertMessage({ title: 'Registro de Actividades', msg: 'Error al guardar informe!' });
            return;
        });
    }

    formarJsonProyecto() {
        //json para guardar informe de docente
        let arrActividadesParticipantes: any = [];
        let objActividadesParticipantes: any = {};
        let arrayOfParticipantes: any = []
        arrayOfParticipantes = this.dropDownParticipante.getCheckedItems();
        let arrayOfParticipantes1 = arrayOfParticipantes.map(arrayOfIdParticipantes => arrayOfIdParticipantes.originalItem);

        for (let index = 0; index < arrayOfParticipantes1.length; index++) {
            objActividadesParticipantes = {};
            objActividadesParticipantes.idPersona = arrayOfParticipantes1[index].idPersona;
            if (this.participante == 'DOCENTE') {
                objActividadesParticipantes.idTipoUsuarioProyecto = 2;
            }
            if (this.participante == 'ESTUDIANTE') {
                objActividadesParticipantes.idTipoUsuarioProyecto = 4;
            }
            objActividadesParticipantes.id = this.actividadParticipante.id;
            objActividadesParticipantes.idObjetivoTarea = this.dropDownTarea.val();
            objActividadesParticipantes.fechaDesde = this.actividadParticipante.fechaDesde;
            objActividadesParticipantes.fechaHasta = this.actividadParticipante.fechaHasta;
            objActividadesParticipantes.horasTrabajadas = this.total;
            objActividadesParticipantes.lugar = this.actividadParticipante.lugar;
            objActividadesParticipantes.observaciones = this.actividadParticipante.observacion;
            objActividadesParticipantes.documentoSoporteUrl = this.documentoSoporteUrl;
            objActividadesParticipantes.version = 1;
            objActividadesParticipantes.usuarioIngresoId = this.usuarioActual.id;
            objActividadesParticipantes.estado = 'A';
            arrActividadesParticipantes.push(objActividadesParticipantes);
        }
        this.depOfertaActividad.actividadParticipante = arrActividadesParticipantes;
        return this.depOfertaActividad;
    }

    funValidar() {
        let validar = true;
        if (this.dropDownParticipante.val() == "") {
            validar = false;
            this.myModal.alertMessage({ title: 'Registro de Actividad', msg: 'Seleccione un Participante!' });
        }
        if (this.dropDownObjetivo.val() == "") {
            validar = false;
            this.myModal.alertMessage({ title: 'Registro de Actividad', msg: 'Seleccione un Objetivo!' });
        }
        if (this.dropDownActividad.val() == "") {
            validar = false;
            this.myModal.alertMessage({ title: 'Registro de Actividad', msg: 'Seleccione un Actividad!' });
        }
        if (this.dropDownTarea.val() == "") {
            validar = false;
            this.myModal.alertMessage({ title: 'Registro de Actividad', msg: 'Seleccione una tarea!' });
        }

        if (this.actividadParticipante.lugar == "") {
            this.myModal.alertMessage({ title: 'Registro de Actividad', msg: 'Ingrese un lugar!' });
            validar = false;
        }
        this.newMethod();
        if (this.validarFecha == false) {
            this.myModal.alertMessage({ title: 'Registro de Actividad', msg: 'Fecha hasta debe ser mayor a fecha desde!' });
            validar = false;
        }
        return validar;
    }

}