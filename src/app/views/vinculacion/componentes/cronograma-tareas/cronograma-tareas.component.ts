import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { jqxSchedulerComponent } from 'jqwidgets-ng/jqxscheduler';
import { Constants } from '../../../../util/constants';
import { ModalTareasComponent } from '../modal-tareas/modal-tareas.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-cronograma-tareas',
  templateUrl: './cronograma-tareas.component.html',
  styleUrls: ['./cronograma-tareas.component.scss']
})
export class CronogramaTareasComponent implements OnInit {

  @ViewChild('schedulerReference') scheduler: jqxSchedulerComponent;
  @ViewChild(ModalTareasComponent) modalTareas: ModalTareasComponent;

  constructor(public datePipe: DatePipe) { }
  appointmentsObj: any  = {}
  appointmentsArray: Array<any>  = new Array();
  localization: any = Constants.getLocalization('es'); 
  ngOnInit() {
  }

  CellDoubleClick(event: any): void {
    let Fecha_Hora = event.args.cell.getAttribute("data-date");
    //this.modalTareas.NuevaTarea(Fecha_Hora);
    let value =this.scheduler.getSelection();
    let fechaSeleccionada = this.datePipe.transform(value.from.dateData, 'yyyy-MM-dd HH:mm');
  
    console.log(fechaSeleccionada)
    // Do Something
  }
  EditDialogCreate(event: any): void 
    {
      var args = event.args;
    var dialog = args.dialog;
    var fields = args.fields;
    var appointment = args.appointment;
      console.log(event)
        // Do Something
    }

  



  generateAppointments(): any {
    let appointments = new Array();
    let appointment1 = {
      id_tarea: "id1",
      descripcion: "George brings projector for presentations.",
      location: "",
      subject: "Quarterly Project Review Meeting",
      calendar: "Room 10",
      fechaDesde: new Date(2020, 10, 23, 9, 0, 0),
      fechaHasta: new Date(2020, 10, 23, 16, 0, 0)
    };
    let appointment2 = {
      id: "id2",
      description: "",
      location: "",
      subject: "IT Group Mtg.",
      calendar: "Room 20",
      start: new Date(2020, 10, 24, 10, 0, 0),
      end: new Date(2020, 10, 24, 15, 0, 0)
    };
    let appointment3 = {
      id: "id3",
      description: "",
      location: "",
      subject: "Course Social Media",
      calendar: "Room 30",
      start: new Date(2020, 10, 27, 11, 0, 0),
      end: new Date(2020, 10, 27, 13, 0, 0)
    };
    let appointment4 = {
      id: "id4",
      description: "",
      location: "",
      subject: "New Projects Planning",
      calendar: "Room 20",
      start: new Date(2020, 10, 23, 16, 0, 0),
      end: new Date(2020, 10, 23, 18, 0, 0)
    };
    let appointment5 = {
      id: "id5",
      description: "",
      location: "",
      subject: "Interview with James",
      calendar: "Room 10",
      start: new Date(2020, 10, 25, 15, 0, 0),
      end: new Date(2020, 10, 25, 17, 0, 0)
    };
    let appointment6 = {
      id_tarea: "id6",
      descripcion: "",
      location: "",
      subject: "Interview with Nancy",
      calendar: "Room 40",
      fechaDesde: new Date(2020, 10, 26, 14, 0, 0),
      fechaHasta: new Date(2020, 10, 26, 16, 0, 0)
    };
    this.appointmentsArray.push(appointment1);
    this.appointmentsArray.push(appointment2);
    this.appointmentsArray.push(appointment3);
    this.appointmentsArray.push(appointment4);
    this.appointmentsArray.push(appointment5);
    this.appointmentsArray.push(appointment6);
    console.log(this.appointmentsArray);
    //this.appointmentsArray=this.appointmentsObj;
    return this.appointmentsArray;
  };
  source: any =
    {
      dataType: "array",
      dataFields: [
        { name: 'id_tarea', type: 'int' },
        { name: 'descripcion', type: 'string' },
        { name: 'location', type: 'string' },
        { name: 'subject', type: 'string' },
        { name: 'calendar', type: 'string' },
        { name: 'subjectLabel', type: 'string' },
        { name: 'fechaDesde', type: 'date' },
        { name: 'res_estado_cita', type: 'string' },
        { name: 'fechaHasta', type: 'date' }
      ],
      id: 'id_tarea',
      localData: this.generateAppointments()
    };
  dataAdapter: any = new jqx.dataAdapter(this.source);
  date: any = new jqx.date(new Date());
  appointmentDataFields: any =
    {
      from: "fechaDesde",
      to: "fechaHasta",
      id: "id_tarea",
      description: "descripcion",
      subjectLabel: "res_estado_cita",
      location: "location",
      subject: "subject",
      resourceId: "calendar"
    };
  resources: any =
    {
      colorScheme: "scheme05",
     // dataField: "calendar",
      source: new jqx.dataAdapter(this.source)
    };
  //VISTA DE DIA-MES-AÑO
  views: any[] =
    [
      {
        type: 'dayView', showWeekends: false,  //ocultar fin de semana
        workTime: { fromDayOfWeek: 1, toDayOfWeek: 5, fromHour: 7, toHour: 25 }, //define hora de inicio y fin de para las reservaciones
        timeRuler: { scaleStartHour: 7, scaleEndHour: 24 }  //define hora de inicio y fin del calendario
      },
      {
        type: 'weekView', showWeekends: false,
        workTime: { fromDayOfWeek: 1, toDayOfWeek: 5, fromHour: 7, toHour: 25 },
        timeRuler: { scaleStartHour: 7, scaleEndHour: 24 }
      },
      {
        type: 'monthView', showWeekends: false,
        workTime: { fromDayOfWeek: 1, toDayOfWeek: 5, fromHour: 7, toHour: 25 },
        timeRuler: { scaleStartHour: 7, scaleEndHour: 24 }
      }
    ];

  //actualizaal padre desde el hijo
  procesaPassMethods(h: any) {
    if (h.val.id_tarea != undefined) {
      console.log("aq");
    this.generarAppointmentAdd(h.val);
      
   }
    
  }

  generarAppointmentAdd(event: any): void {
    let appointments = new Array();
    
    let appointment = {
      /*id_tarea: 7,
      descripcion: "eventdescripcion",
      location: "",
      subject: "ellaNoTeAma",
      calendar: "Room 3",
      fechaDesde: new Date(2020, 10, 24, 10, 0, 0),
      fechaHasta: new Date(2020, 10, 24, 15, 0, 0),*/
      id_tarea: event.id_tarea,
      descripcion: event.descripcion,
      location: "",
      subject: event.subject,
      calendar: "Room 3",
      fechaDesde: event.fechaDesde,
      fechaHasta: event.fechaHasta,
      //resizable: false,
      //draggable: false,
      //readOnly: true
      /*from: new Date(2020, 10, 24, 10, 0, 0),
      to: new Date(2020, 10, 24, 15, 0, 0),
      id: 7,
      description: 'eventdescripcion',
      subjectLabel: "res_estado_cita",
      location: "location",
      subject: "subject",*/
    };
    //this.appointmentsObj.push(appointment);
    //this.appointmentsArray=this.appointmentsObj;
    this.appointmentsArray.push(appointment);
    console.log(this.appointmentsArray[0])
    //this.scheduler.addAppointment(appointments[0]);
    /*this.source.localdata = appointment;
      console.log(this.source.localdata)
      this.dataAdapter.dataBind();*/
      this.source.localdata = this.appointmentsArray;
    //this.dataAdapter.dataBind();
    this.dataAdapter=new jqx.dataAdapter(this.source);
    /*this.resources.colorScheme='scheme05'
    this.resources.dataField= "calendar";
    this.resources.source=this.appointmentsArray;*/
    this.scheduler.beginAppointmentsUpdate();
    this.scheduler.endAppointmentsUpdate();
    console.log(this.scheduler.getAppointments());
      console.log(this.appointmentsArray);
      //this.ngAfterViewInit();
      /*this.scheduler.setAppointmentProperty(this.appointments[0].id, "subject", "RESERVADO");
    /this.scheduler.setAppointmentProperty(appointments[0].id, 'draggable', false);
        this.scheduler.setAppointmentProperty(appointments[0].id, 'resizable', false);
        this.scheduler.setAppointmentProperty(appointments[0].id, 'readOnly', true);
      this.scheduler.beginAppointmentsUpdate();
      this.scheduler.endAppointmentsUpdate();
    //console.log(appointment);
    console.log(this.scheduler.getAppointments());
    console.log(this.scheduler.getAppointmentProperty("id","id6"));*/
   
  }

  
  

}
