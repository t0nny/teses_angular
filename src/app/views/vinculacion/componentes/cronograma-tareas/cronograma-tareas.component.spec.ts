import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CronogramaTareasComponent } from './cronograma-tareas.component';

describe('CronogramaTareasComponent', () => {
  let component: CronogramaTareasComponent;
  let fixture: ComponentFixture<CronogramaTareasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CronogramaTareasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CronogramaTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
