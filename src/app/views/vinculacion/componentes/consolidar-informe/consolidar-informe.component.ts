import { Component, OnInit, ViewChild } from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../services/auth/account.service";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";
import { ModalInformeActividadesComponent } from "../modal-informe-actividades/modal-informe-actividades.component";

@Component({
    selector: 'app-consolidar-informe',
    templateUrl: './consolidar-informe.component.html',
    //styleUrls: ['./modal-tareas.component.scss']
})
export class ConsolidarInformeComponent implements OnInit {

    constructor(private periodoConvocatoria: VinculacionService, private accountService: AccountService) { }

    //Variable paa cargar datos del objeto 
    rowindex: number = -1;
    usuarioActual: any = {};//variable para guardar el usuario actual
    _sub: Subscription;
    facultad: string;
    participanteProyecto: string;

    ngOnInit(): void {
        this.MostrarTitulo();
    }

    @ViewChild(ModalInformeActividadesComponent) formNuevoInformeActividad: ModalInformeActividadesComponent;
    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild('gridActividadesRealizadas') gridActividadesRealizadas: jqxGridComponent;
    @ViewChild('gridActividadesRealizadasEst') gridActividadesRealizadasEst: jqxGridComponent;

    MostrarTitulo() {
        this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
        this.participanteProyecto = this.usuarioActual.nombres + ' ' + this.usuarioActual.apellidos;
        this._sub = this.periodoConvocatoria.cargarTipoUsuario(this.usuarioActual.id).subscribe(data1 => {
            if (data1.length > 0) {
                this._sub = this.periodoConvocatoria.cargarTituloFacultad('4', data1[0].identificacion).subscribe(data => {
                    //console.log("titulo "+ JSON.stringify(data));
                    this.facultad = data.nombre;
                }, error => console.error("h " + error));
            }
        }, error => console.error("h " + error));

    }

    /*************************************************************************************************************** */
    /********************************************PARAMETROS DE LA GRILA DE ACTIVIDADES REALIZADAS DOCENTES******************************************************************* */

    sourceActividadesParticipantes: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idPersona', type: 'int' },
            { name: 'idObjetivoTarea', type: 'int' },
            { name: 'nombres', type: 'string' },
            { name: 'descripcionTarea', type: 'string' },
            { name: 'descripcionActividad', type: 'string' },
            { name: 'horas', type: 'string' },
            { name: 'lugar', type: 'string' },
            { name: 'fechaDesde', type: 'date' },
            { name: 'fechaHasta', type: 'date' },
        ]
    };
    dataAdapterActividadesRealizadas: any = new jqx.dataAdapter(this.sourceActividadesParticipantes);

    columnsActividadesRealizadas: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idPersona', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idObjetivoTarea', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Actividad', datafield: 'descripcionActividad', width: '15%' },
        { text: 'Tarea', datafield: 'descripcionTarea', width: '15%' },
        { text: 'Participante', datafield: 'nombres', width: '15%' },
        { text: 'Fecha Desde', datafield: 'fechaDesde', width: '9%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
        { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '9%', cellsformat: 'yyyy-MM-dd' },
        { text: 'H.trabajadas', datafield: 'horas', width: '15%' },
        { text: 'Lugar', datafield: 'lugar', width: '15%' },
        {
            text: 'Quitar', columntype: 'button', width: '7%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                let idActividadRealizada = this.gridActividadesRealizadas.getcellvalue(this.rowindex, 'id');
                if (idActividadRealizada != null) {
                    this._sub = this.periodoConvocatoria.borrarActividadesRealizadas(idActividadRealizada).subscribe(data => {
                        let id = this.gridActividadesRealizadas.getcellvalue(this.rowindex, 'id');
                        this.gridActividadesRealizadas.deleterow(id);
                        this.rowindex = -1;
                    });
                }

            },
        },
    ];

    /*************************************************************************************************************** */
    /********************************************PARAMETROS DE LA GRILA DE ACTIVIDADES REALIZADAS ESTUDIANTES******************************************************************* */
    sourceActividadesParticipantesEst: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idPersona', type: 'int' },
            { name: 'idObjetivoTarea', type: 'int' },
            { name: 'nombres', type: 'string' },
            { name: 'descripcionTarea', type: 'string' },
            { name: 'descripcionActividad', type: 'string' },
            { name: 'horas', type: 'string' },
            { name: 'lugar', type: 'string' },
            { name: 'fechaDesde', type: 'date' },
            { name: 'fechaHasta', type: 'date' },
        ]
    };

    dataAdapterActividadesRealizadasEst: any = new jqx.dataAdapter(this.sourceActividadesParticipantesEst);

    columnsActividadesRealizadasEst: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idPersona', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idObjetivoTarea', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Actividad', datafield: 'descripcionActividad', width: '15%' },
        { text: 'Tarea', datafield: 'descripcionTarea', width: '15%' },
        { text: 'Participante', datafield: 'nombres', width: '15%' },
        { text: 'Fecha Desde', datafield: 'fechaDesde', width: '9%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
        { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '9%', cellsformat: 'yyyy-MM-dd' },
        { text: 'H.trabajadas', datafield: 'horas', width: '15%' },
        { text: 'Lugar', datafield: 'lugar', width: '15%' },
        {
            text: 'Quitar', columntype: 'button', width: '7%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                let idActividadRealizada = this.gridActividadesRealizadasEst.getcellvalue(this.rowindex, 'id');
                if (idActividadRealizada != null) {
                    this._sub = this.periodoConvocatoria.borrarActividadesRealizadas(idActividadRealizada).subscribe(data => {
                        let id = this.gridActividadesRealizadasEst.getcellvalue(this.rowindex, 'id');
                        this.gridActividadesRealizadasEst.deleterow(id);
                        this.rowindex = -1;
                    });
                }

            },
        },
    ];

    /********************************************************************************************************************************** */
    /***********metodo para obtner el id de la fila seleccionada en la grilla de ACTIVIDADES REALIZADAS*************************************************************/

    Rowselect(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }

    RowselectEst(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }

     /********************************************************************************************************************************** */
    /*****************crea el boton y el evento para agregar actividades realizadas AL GRIP docente*************************************************************/

    rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer = document.createElement('div');
        buttonContainer.id = 'buttonContainer';
        buttonContainer.style.cssText = 'float: left; margin-left: 5px';

        let buttonContainer1 = document.createElement('div');
        buttonContainer1.id = 'buttonContainer1';
        buttonContainer1.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(buttonContainer);
        container.appendChild(buttonContainer1);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer', 'jqxButton', { width: 60, value: 'Nuevo', theme: 'darkblue' });
        addRowButton.addEventHandler('click', () => {
            
        })

        let addRowButton1 = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: 60, value: 'Editar', theme: 'darkblue' });
        addRowButton1.addEventHandler('click', () => {
            
                    if (this.rowindex != -1) {
                        let getRowObject = this.gridActividadesRealizadas.getrowdata(this.rowindex);
                        let id = getRowObject.id;
                        //this.formNuevoInformeActividad.editarActividad(arrayProyecto.idProyecto, id, 'DOCENTE');
                        this.gridActividadesRealizadas.clearselection();
                        this.rowindex = -1;
                    } else {
                        this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Seleccione un registro!' });
                    }
        })
    };

    /********************************************************************************************************************************** */
    /*****************crea el boton y el evento para agregar actividades realizadas AL GRIP docente*************************************************************/

    rendertoolbar1 = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer4 = document.createElement('div');
        buttonContainer4.id = 'buttonContainer4';
        buttonContainer4.style.cssText = 'float: left; margin-left: 5px';

        let buttonContainer5 = document.createElement('div');
        buttonContainer5.id = 'buttonContainer5';
        buttonContainer5.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(buttonContainer4);
        container.appendChild(buttonContainer5);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer4', 'jqxButton', { width: 60, value: 'Nuevo', theme: 'darkblue' });
        addRowButton.addEventHandler('click', () => {
            
        })

        let addRowButton1 = jqwidgets.createInstance('#buttonContainer5', 'jqxButton', { width: 60, value: 'Editar', theme: 'darkblue' });
        addRowButton1.addEventHandler('click', () => {
            
                    if (this.rowindex != -1) {
                        let getRowObject = this.gridActividadesRealizadasEst.getrowdata(this.rowindex);
                        let id = getRowObject.id;
                        //this.formNuevoInformeActividad.editarActividad(arrayProyecto.idProyecto, id, 'ESTUDIANTE');
                        this.gridActividadesRealizadasEst.clearselection();
                        this.rowindex = -1;
                    } else {
                        this.myModal.alertMessage({ title: 'Seguimientos de Actividades', msg: 'Seleccione un registro!' });
                    }
               
        })


    };

}