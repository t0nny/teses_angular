import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoEdicionComponent } from './proyecto-edicion.component';

describe('ProyectoEdicionComponent', () => {
  let component: ProyectoEdicionComponent;
  let fixture: ComponentFixture<ProyectoEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
