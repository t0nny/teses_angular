import { Component, OnInit, ViewChild } from '@angular/core';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-ng/jqxvalidator';
import { NgxExtendedPdfViewerComponent } from 'ngx-extended-pdf-viewer';
import { jqxButtonComponent } from 'jqwidgets-ng/jqxbuttons';
import { jqxMenuComponent } from 'jqwidgets-ng/jqxmenu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proyecto-edicion',
  templateUrl: './proyecto-edicion.component.html',
  styleUrls: ['./proyecto-edicion.component.scss']
})
export class ProyectoEdicionComponent implements OnInit {

  @ViewChild('gridVersionMatriz') gridVersionMatriz: jqxGridComponent;
  @ViewChild('gridVersionProyectos') gridVersionProyectos: jqxGridComponent;
  @ViewChild('gridDatosPrueba') gridDatosPrueba: jqxGridComponent;  
  @ViewChild('myValidator') myValidator: jqxValidatorComponent; 
  @ViewChild('myPdfViewer') myPdfViewer: NgxExtendedPdfViewerComponent; 
  @ViewChild('myButtonVolver')      myButtonVolver:     jqxButtonComponent;
  @ViewChild('myButtonVer')      myButtonVer:     jqxButtonComponent;
  @ViewChild('jqxMenu')      jqxMenu:     jqxMenuComponent;
  

  isCollapsedPdf: boolean = true;
  isCollapsed: boolean = false;
  constructor(private router: Router,) { }

  ngOnInit() {
  }
  controlIsCollapsedVer(){
    /* this.habilitarCombos();
    this.isCollapsed = !this.isCollapsed;
    this.pdfSrc=""; */
    
    this.isCollapsedPdf=!this.isCollapsedPdf;
  }

  controlIsCollapsedVolver(){
    /* this.habilitarCombos();
    this.isCollapsed = !this.isCollapsed;
    this.pdfSrc=""; */
    this.isCollapsedPdf=!this.isCollapsedPdf
  }

  columnsVercionesProyectos: any[] =
  [
  
    { text: 'id', datafield: 'idDepartamento',width: '5%', hidden:true },
    { text: 'Descripcíon', datafield: 'departamento',width: '75%'}, 
     { text: 'Descargar',columntype: 'button', width: '25%',cellsrenderer: (): string => {
      return 'Descargar';
      },
      buttonclick: (row: number): void => {
        //get the data and append in to the inputs
       /*  this.editrow = row;
        let dataRecord = this.myGrid.getrowdata(this.editrow);
        this.descargaMallaHorasSemanalesPorPeriodo(dataRecord.idDepartamento); */
        
      } ,
    },
  ];
  columnsVersionesMatriz: any[] =
  [
  
    { text: 'id', datafield: 'idDepartamento',width: '5%', hidden:true },
    { text: 'Descripcíon', datafield: 'departamento',width: '65%'}, 
    { text: 'ver',columntype: 'button', width: '10%',cellsrenderer: (): string => {
    return 'ver';
    },
    buttonclick: (row: number): void => {
      //get the data and append in to the inputs
     /*  this.editrow = row;
      let dataRecord = this.myGrid.getrowdata(this.editrow);
      this.desabilitarCombos();
      this.verMallaHorasSemanalesPorPeriodo(dataRecord.idDepartamento);
     
      this.myGrid.virtualmode(false); */
    } ,
  },

   { text: 'Descargar',columntype: 'button', width: '25%',cellsrenderer: (): string => {
      return 'Descargar';
      },
      buttonclick: (row: number): void => {
        //get the data and append in to the inputs
       /*  this.editrow = row;
        let dataRecord = this.myGrid.getrowdata(this.editrow);
        this.descargaMallaHorasSemanalesPorPeriodo(dataRecord.idDepartamento); */
        
      } ,
    },
  ];

  // ------------------------------ jqxMenu -----------------------------------------
   //datos de jqxMenu
   data = [
    {
        'id': '1',
        'text': 'Grabar',
        'parentid': '-1',
        'subMenuWidth': '25%'
    },
    {
        'text': 'Cancelar',
        'id': '2',
        'parentid': '-1',
        'subMenuWidth': '25%'
    }, 

  ];
    // prepare the data
    source1 =
    {
        datatype: 'json',
        datafields: 
        [
            { name: 'id' },
            { name: 'parentid' },
            { name: 'text' },
            { name: 'subMenuWidth' }
        ],
        id: 'id',
        localdata: this.data
    };
    getAdapter(source1: any): any {
      // create data adapter and perform data
      return new jqx.dataAdapter(this.source1, { autoBind: true });
  };

  menus = this.getAdapter(this.source1).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);


/**
 *  description Evento del `jqxMenu`
 */
  itemclick(event: any): void 
  {
      var opt=event.args.innerText;
      switch (opt)
      {            
        case 'Grabar':
          //this.allValidation();
          break;
        case 'Cancelar':
          this.gotoList();
          break;
        default:
            //default code block
      }
  };
  //---------------------------------------------------------------------------------
  /**
   * Método que me permite regresar a la pantalla anterior sin actualizar ésta
   */
  gotoList(): void{
    this.router.navigate(['vinculacion/docente/registro-proyecto']);
  }
  //---------------------------------------------------------------------------------

}
