import { Component, ContentChildren, QueryList, AfterContentInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { TabComponent } from './tab.component';
import { DynamicTabsDirective } from './dynamic-tabs.directive';

@Component({
  selector: 'my-tabs',
  template: `
    <ul class="nav nav-tabs"> 
      <li *ngFor="let tab of dynamicTabs" (click)="selectTab(tab)" [class.active]="tab.active" class="nav-item">
        <a class="nav-link">{{tab.title}}</a>
      </li>
    </ul>
    <div class="card">
     
      <ng-template dynamic-tabs #container></ng-template> 
    </div>
    
  `,
  styles: [
    `
    ul.tabs li.active,  li.active a{
      background: rgb(68, 154, 201);
      }
    `
  ]
})
export class TabsComponent implements AfterContentInit {
  dynamicTabs: TabComponent[] = [];
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
  @ViewChild(DynamicTabsDirective) dynamicTabPlaceholder: DynamicTabsDirective;

  constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

  // contentChildren are set
  ngAfterContentInit() {
    // obtiene todos los Tabs activas
    const activeTabs = this.tabs.filter(tab => tab.active);

    // si no hay un conjunto de pestañas activo, active la primera
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  openTab(title: string, template, data, isCloseable = false) {
    // Obtenga una fábrica de componentes para nuestro TabComponent
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(
      TabComponent
    );

    // obtener la referencia del contenedor de vista de nuestra directiva de anclaje
    const viewContainerRef = this.dynamicTabPlaceholder.viewContainer;

    // crear una instancia del componente
    const componentRef = viewContainerRef.createComponent(componentFactory);

    // establecer las propiedades correspondientes en nuestra instancia del componente
    const instance: TabComponent = componentRef.instance as TabComponent;
    instance.title = title;
    instance.template = template;
    instance.dataContext = data;
    instance.isCloseable = isCloseable;

    // recuerde el componente dinámico para renderizar el
    // encabezado de navegacion del Tab
    this.dynamicTabs.push(componentRef.instance as TabComponent);
    // activa el tab
    this.selectTab(this.dynamicTabs[this.dynamicTabs.length - 1]);
    
  }

  selectTab(tab: TabComponent) {
    // desactiva todo los tabs
    this.dynamicTabs.forEach(tab => (tab.active = false));
    // Activa el Tab que se ha realizado el clic.
    tab.active = true;
  }

  activeTab() {
    const activeTabs = this.dynamicTabs.filter(tab => tab.active);
    if (activeTabs.length > 0) {
      // close the 1st active tab (should only be one at a time)
      this.selectTab(this.dynamicTabs[0]);
    }
  }
}
