/**
 * Una sola página de pestaña. Representa la plantilla pasada
 * a través de las propiedades @Input usando ngTemplateOutlet
 * y directivas ngTemplateOutletContext.
 */

import { Component, Input } from '@angular/core';

@Component({
  selector: 'my-tab',
  styles: [
    `
    .pane{
      padding: 1em;
    }
  `
  ],
  template: `
    <div [hidden]="!active" class="pane">
      
      <ng-container *ngIf="template"
        [ngTemplateOutlet]="template"
      >
      </ng-container>
    </div>
  `
})
export class TabComponent {
  @Input('tabTitle') title: string;
  @Input() active = false;
  @Input() isCloseable = false;
  @Input() template;
  @Input() dataContext;
}
