import { DatePipe } from "@angular/common";
import { Component, DebugElement, Input, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { jqxSchedulerComponent } from "jqwidgets-ng/jqxscheduler";
import { Subscription } from "rxjs";
import { VinculacionService } from "../../../../../../services/vinculacion/vinculacion.service";
import { Constants } from "../../../../../../util/constants";
import { ModalComponentComponent } from "../../../../../modal-view/modal-component/modal-component.component";
import { ModalActividadesComponent } from "../../../../componentes/modal-actividades/modal-actividades.component";
import { ModalTareasComponent } from "../../../../componentes/modal-tareas/modal-tareas.component";

@Component({
    selector: 'app-marco-logico',
    templateUrl: './marco-logico.component.html',
    //styleUrls: ['./marco-logico.component.css']
})

export class MarcoLogicoComponent implements OnInit {

    @ViewChild('myGridComponentes') myGridComponentes: jqxGridComponent;
    @ViewChild(ModalActividadesComponent) formNuevoActividad: ModalActividadesComponent;
    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild('schedulerReference') scheduler: jqxSchedulerComponent;
    @ViewChild(ModalTareasComponent) modalTareas: ModalTareasComponent;
    
    dataComponentes: any = {};
    @Input() proyecto: any = {};
    appointmentsArray: Array<any> = new Array();
    localization: any = Constants.getLocalization('es');
    dataLeyenda: any = null;
    i: any = 0;
    dataTareas: any = {};
    appointChange: any;
    
    constructor(private router: Router, private periodoConvocatoria: VinculacionService,
        public datePipe: DatePipe) { }
    //Variable paa cargar datos del objeto 
    rowindex: number = -1;
    _sub: Subscription;
    objetivoSelec: any;

    ngOnInit(): void {
        console.log("ngOnInitmarcoLogico")
        let data: any = [];
        let objObjetivoActividad: any = {};
        objObjetivoActividad.calendar = 'Actividades:';
        data.push(objObjetivoActividad);
        this.mostrarLeyenda(data);
    }

  
    mostrarLeyenda(data: any) {
        this.source.localdata = data;
        this.source.localdata = this.source.localdata;
        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.scheduler.beginAppointmentsUpdate();
        this.scheduler.endAppointmentsUpdate();
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    /**************************************************seccion marco logico************************************************************* */
    /*************************************************jqxMenuObjetivos************************************************************** */

    //datos de jqxMenu
    data1 = [
        {
            'id': '1',
            'text': 'Nuevo',
            'parentid': '-1',
            'subMenuWidth': '25%'
        },
        {
            'text': 'Editar',
            'id': '2',
            'parentid': '-1',
            'subMenuWidth': '25%'
        },
    ];
    // prepare the data
    source2 =
        {
            datatype: 'json',
            datafields:
                [
                    { name: 'id' },
                    { name: 'parentid' },
                    { name: 'text' },
                    { name: 'subMenuWidth' }
                ],
            id: 'id',
            localdata: this.data1
        };
    getAdapter2(source2: any): any {
        // create data adapter and perform data
        return new jqx.dataAdapter(this.source2, { autoBind: true });
    };

    menusObjetivo = this.getAdapter2(this.source2).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

    /*************************************************************************************************************** */
    /*************************************************evento del jqxMenuObjetivos************************************************************** */

    itemclickObjetivo(event: any): void {
        var opt = event.args.innerText;
        let idItemMarcoLogico = this.myGridComponentes.getcellvalue(this.rowindex, 'idItemMarcoLogico');

        switch (opt) {
            case 'Nuevo':
                console.log('nuevo objetivo');
                //let idItemMarcoLogico = this.myGridComponentes.getcellvalue(this.rowindex, 'idItemMarcoLogico');
                this.formNuevoActividad.nuevaActividad(this.dataComponentes);
                break;
            case 'Editar':
                console.log('editar objetivo');
                if (this.rowindex != -1) {
                    this.formNuevoActividad.editarActividad(idItemMarcoLogico, this.dataComponentes);
                } else {
                    this.myModal.alertMessage({ title: 'Marco Logico', msg: 'Seleccione un objetivo!' });
                }
                break;
            case 'Eliminar':
                console.log('eliminar objetivo');
                this.router.navigate(['vinculacion/componetes/cronograma-tareas/', 0]);
                break;
            default:
            //default code block
        }
    }

    /*************************************************************************************************************** */
    /*************************************************jqxMenu************************************************************** */

    //datos de jqxMenu
    data = [
        {
            'id': '1',
            'text': 'Grabar',
            'parentid': '-1',
            'subMenuWidth': '25%'
        },
        {
            'text': 'Cancelar',
            'id': '2',
            'parentid': '-1',
            'subMenuWidth': '25%'
        },
    ];
    // prepare the data
    source1 =
        {
            datatype: 'json',
            datafields:
                [
                    { name: 'id' },
                    { name: 'parentid' },
                    { name: 'text' },
                    { name: 'subMenuWidth' }
                ],
            id: 'id',
            localdata: this.data
        };
    getAdapter(source1: any): any {
        // create data adapter and perform data
        return new jqx.dataAdapter(this.source1, { autoBind: true });
    };

    menus = this.getAdapter(this.source1).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

    /*************************************************************************************************************** */
    //**************************listar grid programas*************************************************************

    ListarGridComponentes(idProyecto: any) {
        this._sub = this.periodoConvocatoria.listarGridComponentes(idProyecto).subscribe(data => {
            //console.log(JSON.stringify(data));
            this.dataComponentes = data;
            let hash = {};
            let array = data.filter(o => hash[o.idItemMarcoLogico] ? false : hash[o.idItemMarcoLogico] = true);
            //console.log(JSON.stringify(array)); 
            let dataConvertida = [];
            array.forEach(element => {
                dataConvertida.push({
                    idItemMarcoLogico: element.idItemMarcoLogico,
                    idItemMarcoLogicoPadre: element.idItemMarcoLogicoPadre,
                    imlDescripcion: element.imlDescripcion,
                    imlDescripcionCorta: element.imlDescripcionCorta,
                    indicador: element.indicador,
                    tiempoPlaneado: element.tiempoPlaneado,
                    lineaBase: element.lineaBase + '%',
                    metaProgramada: element.metaProgramada + '%',
                    tiopDescripcion: element.tiopDescripcion,
                })
            });
            this.sourceGridComponentes.localdata = dataConvertida;
            this.dataGridComponentes.dataBind();

            /*let rooms = data.filter((dats) => { return dats.idTipoObjetivoProyecto == 3 });
            if (rooms.length > 0) {
                rooms.forEach(element => {
                    let appointment = {
                        calendar: element.imlDescripcionCorta,
                    };
                    this.source.localdata.push(appointment);

                });
               
                this.source.localdata = this.source.localdata;
                this.dataAdapter = new jqx.dataAdapter(this.source);
                this.scheduler.beginAppointmentsUpdate();
                this.scheduler.endAppointmentsUpdate();           

            }*/

        });
        this.obtenerTareasScheluder(idProyecto);
    }

    /*************************************************************************************************************** */
    /*****************estructura para grid objetivos especificos(componente)**********************************************************/


    sourceGridComponentes: any =
        {
            datatype: 'array',
            id: 'id',
            datafields: [
                { name: 'idItemMarcoLogico', type: 'int' },
                { name: 'idItemMarcoLogicoPadre', type: 'int' },
                { name: 'imlDescripcion', type: 'string' },
                { name: 'imlDescripcionCorta', type: 'string' },
                { name: 'indicador', type: 'string' },
                { name: 'lineaBase', type: 'string' },
                { name: 'tiempoPlaneado', type: 'string' },
                { name: 'metaProgramada', type: 'string' },
                { name: 'tiopDescripcion', type: 'string' },
            ]
        };
    dataGridComponentes: any = new jqx.dataAdapter(this.sourceGridComponentes);

    columnsComponente: any[] = [
        { text: 'idItemMarcoLogico', datafield: 'idItemMarcoLogico', width: '5%', filtertype: 'none', hidden: true },
        { text: 'idItemMarcoLogicoPadre', datafield: 'idItemMarcoLogicoPadre', width: '5%', filtertype: 'none', hidden: true },
        { text: 'Descripcion', datafield: 'imlDescripcion', width: '27%', hidden: true },
        { text: 'Descripcion', datafield: 'imlDescripcionCorta', width: '27%' },
        { text: 'Indicador', datafield: 'indicador', width: '18%' },
        { text: 'Linea Base ', datafield: 'lineaBase', width: '8%', cellsformat: 'p4' },
        { text: 'Tiempo Planeado', datafield: 'tiempoPlaneado', width: '13%' },
        { text: 'Meta Programada ', datafield: 'metaProgramada', width: '11%', cellsformat: 'p4' },
        { text: 'Tipo Objetivo ', datafield: 'tiopDescripcion', width: '8%' },
        {
            text: 'Quitar', columntype: 'button', width: '15%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                //this.formNuevoActividad.nuevaActividad();
            },

        },
    ];

    private obtenerTareasScheluder(idProyecto: any) {
        this._sub = this.periodoConvocatoria.buscarObjetivoTareas(idProyecto).subscribe(data => {
            console.log(JSON.stringify(data));
            this.dataTareas = data;
            data.forEach(element => {
                this.generarAppointmentAdd(element);
            });
        });
    }

    /********************************************************************************************************************************** */
    /***********metodo para obtner el id de la fila seleccionada en la grilla de DOCENTES PARTICIPANTES*************************************************************/

    Rowselect(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }

    //actualizaal padre desde el hijo
    procesaPassMethodsActividades(h) {
        if (h.val == 'actualizar') {
            this.ListarGridComponentes(this.dataComponentes[0].idProyecto);
        }
    }

    /********************************************************************************************************************************** */
    /***********parametros para el scheluder*************************************************************/

    source: any =
        {
            dataType: "array",
            dataFields: [
                { name: 'idObjetivoTarea', type: 'int' },
                { name: 'descripcion', type: 'string' },
                { name: 'location', type: 'string' },
                { name: 'subject', type: 'string' },
                { name: 'calendar', type: 'string' },
                { name: 'subjectLabel', type: 'string' },
                { name: 'fechaDesde', type: 'date', format:'yyyy-MM-dd HH:mm:ss' },
                { name: 'res_estado_cita', type: 'string' },
                { name: 'fechaHasta', type: 'date', format:'yyyy-MM-dd HH:mm:ss' }
            ],
            id: 'idObjetivoTarea',
            localdata: []
            //localData: this.generateAppointments()
        };
    dataAdapter: any = new jqx.dataAdapter(this.source);
    date: any = new jqx.date(new Date());
    appointmentDataFields: any =
        {
            from: "fechaDesde",
            to: "fechaHasta",
            id: "idObjetivoTarea",
            description: "descripcion",
            subjectLabel: "res_estado_cita",
            location: "location",
            subject: "subject",
            resourceId: "calendar"
        };
    resources: any =
        {
            colorScheme: "scheme05",
            dataField: "calendar",
            source: new jqx.dataAdapter(this.source)
        };

    /* resource: any = {
         dataType: 'array',
         dataField: [{name: 'id', type: 'number'},{name: 'name', type: 'string'}],
         id: 'id',
         localData : [{id: 1, name: 'Phone Only'}, {id: 2,name: 'Video/Phone'}, {id: 3,name: 'Overflow'}, {id: 4,name: 'Unavailable'}]
         }
         resources: any = {
         colorScheme: "scheme05",
         dataField: "resource",
         source: new jqx.dataAdapter(this.source)
         };*/



    views: any[] = [
        {
            type: 'dayView', text: 'DIA', showWeekends: false,  //ocultar fin de semana
            workTime: { fromDayOfWeek: 1, toDayOfWeek: 5, fromHour: 7, toHour: 25 }, //define hora de inicio y fin de para las reservaciones
            timeRuler: { scaleStartHour: 7, scaleEndHour: 24 }  //define hora de inicio y fin del calendario
        },
        {
            type: 'weekView', text: 'SEMANA', showWeekends: false,
            workTime: { fromDayOfWeek: 1, toDayOfWeek: 5, fromHour: 7, toHour: 25 },
            timeRuler: { scaleStartHour: 7, scaleEndHour: 24 }
        },
        {
            type: 'monthView', text: 'MES', showWeekends: false,
            workTime: { fromDayOfWeek: 1, toDayOfWeek: 5, fromHour: 7, toHour: 25 },
            timeRuler: { scaleStartHour: 7, scaleEndHour: 24 }
        }
    ];

    CellDoubleClick(event: any): void {
        let Fecha_Hora = event.args.cell.getAttribute("data-date");
        this.modalTareas.NuevaTarea(Fecha_Hora, this.dataComponentes);
        let value = this.scheduler.getSelection();
        let fechaSeleccionada = this.datePipe.transform(value.from.dateData, 'yyyy-MM-dd HH:mm');

        console.log(fechaSeleccionada)
        // Do Something
    }

    AppointmentDoubleClick(event: any): void {
        let fechaSeleccionada = this.datePipe.transform(event.args.appointment.to.dateData, 'yyyy-MM-dd HH:mm:ss')
        this.appointChange=event;        
        this.modalTareas.editarTarea(event, this.dataComponentes,this.dataTareas);
        console.log(event)
    }    
      

    
    //actualizaal padre desde el hijo
    procesaPassMethodsTareas(h: any) {
        /*if (h.val.idObjetivoTarea != undefined) {
            if (h.val.idObjetivoTarea == null) {
                console.log("crear Tarea");
                this.generarAppointmentAdd(h.val);
            } else {
                console.log("actualizar");
                let appointment = h.val; //obtiene la cita a modificada
                console.log(appointment);
                this.appointChange
                this.scheduler.setAppointmentProperty(appointment.idObjetivoTarea, "subject", appointment.subject);
                this.scheduler.setAppointmentProperty(appointment.idObjetivoTarea, "resourceId", appointment.resourceId);
                this.scheduler.setAppointmentProperty(appointment.idObjetivoTarea, "from", appointment.fechaDesde);
                this.scheduler.setAppointmentProperty(appointment.idObjetivoTarea, "to", appointment.fechaHasta);
                this.appointChange
                console.log('aqui')
            }
        }*/
        if (h.val == 'actualizar') {
            this.appointmentsArray=[]
            this.mostrarLeyenda(this.appointmentsArray);
            this.obtenerTareasScheluder(this.dataComponentes[0].idProyecto);
        }        
    }

    generarAppointmentAdd(event: any): void {
        let appointment = {
            idObjetivoTarea: event.idObjetivoTarea,
            descripcion: "",
            location: "",
            subject: event.subject,
            calendar: event.resourceId,
            fechaDesde: this.datePipe.transform(event.fechaDesde, 'yyyy-MM-dd HH:mm:ss'),
            fechaHasta: this.datePipe.transform(event.fechaHasta, 'yyyy-MM-dd HH:mm:ss'),
            //fechaDesde: event.fechaDesde,
            //fechaHasta: event.fechaHasta,
        };
        this.appointmentsArray.push(appointment);
        console.log(this.appointmentsArray)
        this.source.localdata = this.appointmentsArray;
        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.scheduler.beginAppointmentsUpdate();
        this.scheduler.endAppointmentsUpdate();
    }

    seccionMarcoLogico() {
        let validar = true;
        if (this.myGridComponentes.getrows().length <= 3) {
          this.myModal.alertMessage({ title: 'Marco Lógico', msg: 'Agrege al menos un objetivo componente!' });
          validar = false;
        }
       
        return validar;
      }


}