import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { Subscription } from 'rxjs';
import { AccountService } from '../../../../../../services/auth/account.service';
import { VinculacionService } from '../../../../../../services/vinculacion/vinculacion.service';
import { ModalComponentComponent } from '../../../../../modal-view/modal-component/modal-component.component';

@Component({
  selector: 'app-datos-generales',
  templateUrl: './datos-generales.component.html',
  //styleUrls: ['./registro-proyecto.component.scss']
})

export class DatosGeneralesComponent implements OnInit {

  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

  //SECCION AREA GEOGRAFICA
  @ViewChild('checkBoxParroquia') checkBoxParroquia: jqxCheckBoxComponent;
  @ViewChild('checkBoxComuna') checkBoxComuna: jqxCheckBoxComponent;
  @ViewChild('dropDownProvincia') dropDownProvincia: jqxDropDownListComponent;
  @ViewChild('dropDownCanton') dropDownCanton: jqxDropDownListComponent;
  @ViewChild('dropDownParroquia') dropDownParroquia: jqxDropDownListComponent;
  @ViewChild('dropDownCarrera') dropDownCarrera: jqxDropDownListComponent;
  @ViewChild('dropDownSemestre') dropDownSemestre: jqxDropDownListComponent;
  @ViewChild('gridAsignaturaProyecto') gridAsignaturaProyecto: jqxGridComponent;
  @ViewChild('dropDownAsigantura') dropDownAsigantura: jqxDropDownListComponent;
  
  constructor(private periodoConvocatoria: VinculacionService,
    private accountService: AccountService) {}

  //mecanismo de enlace entre el hijo y el padre
  @Input() proyecto: any;
  @Input() proyectoVersion: any;
  @Input() proyectoPresupuesto: any;
  @Input() proyectoAreaGeografica: any;
  @Input() institucionBeneficiaria: any;
  @Input() proyectoAsignatura: any;
  @Input() dataSemestre: any;

  // Allow decimal numbers. The \. is only allowed once to occur
  private regex: RegExp = new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g);
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home'];

  //Variable paa cargar datos del objeto 
  rowindex: number = -1;
  _sub: Subscription;

  public mostrarParroquia = false;
  Barrio: string = 'Barrio';
  areaGeografica: any;
  asignaturaSelec: any;
  proyectoAsignaturaDelete: any = [];//almacenara los id de proyectosAsiganturas a eliminar
  fileSelecionado: any = '';
  total: number;
  usuarioActual: any = {};//variable para guardar el usuario actual

  ngOnInit(): void {
    this.usuarioActual=this.accountService.getUsuario(); //captura del usuario actual
    this.listarProvinciasCantones();
    this.ListarSemestres();
    this.listarCarreras();
  } 

  ngOnDestroy() {
    this._sub.unsubscribe();
  }

  /********************************************************************************************************************************** */
  /**************************************SERVICIO, TRAE LISTA AREAGEOGRAFICA**************************************************************/

  listarProvinciasCantones() {
    this._sub = this.periodoConvocatoria.buscarListaAreaGeografica().subscribe(data => {
      this.areaGeografica = data;
      let PROVINCIAS = data.filter((dats) => { return dats.idPadre == 1 })
      this.sourceProvincias.localdata = PROVINCIAS;
      this.dataAdapterProvincia.dataBind();
    });   
  }

  /********************************************************************************************************************************** */
  /**************************************PARAMETROS PARA LLENAR EL COMBO PROVINCIAS**************************************************************/

  sourceProvincias: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descipcionn', type: 'string' },
      { name: 'idPadre', type: 'int' },
      { name: 'estado', type: 'string' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PROVINCIAS
  dataAdapterProvincia: any = new jqx.dataAdapter(this.sourceProvincias);

  /********************************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO PROVINCIA, LISTA CANTONES**************************************************************/

  ListarCantones(event: any) {
    let CANTONES = this.areaGeografica.filter((dats) => { return dats.idPadre == event.args.item.value })
    this.sourceCantones.localdata = CANTONES;
    this.dataAdapterCanton.dataBind();
  }

  /********************************************************************************************************************************** */
  /**************************************PARAMETROS PARA LLENAR EL COMBO CANTONES**************************************************************/

  sourceCantones: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descipcionn', type: 'string' },
      { name: 'idPadre', type: 'int' },
      { name: 'estado', type: 'string' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE CANTONES
  dataAdapterCanton: any = new jqx.dataAdapter(this.sourceCantones);

  /********************************************************************************************************************************** */
  /**************************************EVENTOS DE COMBO CANTON, LISTA PARROQUIAS**************************************************************/

  ListarParroquias(event: any) {
    let PARROQUIAS = this.areaGeografica.filter((dats) => { return dats.idPadre == event.args.item.value })
    this.sourceParroquias.localdata = PARROQUIAS;
    this.dataAdapterParroquia.dataBind();
  }

  /********************************************************************************************************************************** */
  /**************************************PARAMETROS PARA LLENAR EL COMBO CANTONES**************************************************************/

  sourceParroquias: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descipcionn', type: 'string' },
      { name: 'idPadre', type: 'int' },
      { name: 'estado', type: 'string' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE CANTONES
  dataAdapterParroquia: any = new jqx.dataAdapter(this.sourceParroquias);


  /********************************************************************************************************************************** */
  //metodo para actualizar datos enviados desde seguimientos-proyecto a datos-generales
  listarCarreras() {
    let dataCarrera = JSON.parse(localStorage.getItem('dataCarrera')); //captura la data de carreras
    this.sourceCarrera.localdata = dataCarrera;
    this.dataAdapterCarrera.dataBind();
  }

  //FUENTE DE DATOS PARA EL COMBOBOX DE CARRERAS
  sourceCarrera: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descripcion', type: 'string' },
      { name: 'estado', type: 'string' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE CARRERA
  dataAdapterCarrera: any = new jqx.dataAdapter(this.sourceCarrera);

  /********************************************************************************************************************************** */
  /*********************************Cargar combo semestres*******************************************************************/

  ListarSemestres() {
    this.sourceSemestre.localdata = this.dataSemestre;
    this.dataAdapterSemestre.dataBind();
  }

  sourceSemestre: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descripcion', type: 'string' },
      { name: 'estado', type: 'string' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE SEMESTRE
  dataAdapterSemestre: any = new jqx.dataAdapter(this.sourceSemestre);


  procesaPassCombosAreaGeo(h) {
    if (h != undefined) {
      this.llenarCombosAreaGeo(h);
    }
  }

  procesaPassGridAsignaturas(h) {
    if (h != undefined) {
      this.llenarGridAsignaturas(h);
    }
  }

  procesaPassCheckBoxParroquia(h) {
    if (h != undefined) {
      if (h == true) {
        this.checkBoxParroquia.checked(true);
      } else {
        this.checkBoxComuna.checked(true);
      }
    }
  }

  /********************************************************************************************************************************** */
  
  llenarGridAsignaturas(idProyecto: any) {
    this._sub = this.periodoConvocatoria.buscarAsignaturasProyectosGrid(idProyecto).subscribe(data => {
      console.log(JSON.stringify(data));
      this.aggGriAsignaturaProyecto(data);
    });
  }

  /********************************************************************************************************************************** */
  /*********************************Cargar combo areGeografica*******************************************************************/

  llenarCombosAreaGeo(idAreaGeografica: any) {
    console.log("idAreaGeografica " + idAreaGeografica);
    let PARROQUIA = this.areaGeografica.filter((dats) => { return dats.id == idAreaGeografica })
    if (PARROQUIA[0].idPadre != null) {
      let CANTON = this.areaGeografica.filter((dats) => { return dats.id == PARROQUIA[0].idPadre })
      if (CANTON[0].idPadre != null) {
        let PROVINCIA = this.areaGeografica.filter((dats) => { return dats.id == CANTON[0].idPadre })
        if (PROVINCIA[0].idPadre != null) {
          let PAIS = this.areaGeografica.filter((dats) => { return dats.id == PROVINCIA[0].idPadre })
          if (PAIS[0].idPadre == null) {
            this.dropDownProvincia.val(PROVINCIA[0].id);
            this.dropDownCanton.val(CANTON[0].id);
            this.dropDownParroquia.val(PARROQUIA[0].id);
          }
        } else {
          this.dropDownProvincia.val(CANTON[0].id);
          this.dropDownCanton.val(PARROQUIA[0].id);
        }
      }
    }
  }

  /********************************************************************************************************************************** */
  /**************************************EVENTOS DE LOS CHECKS PARROQUIA Y COMUNA**************************************************************/

  checkParroquia(event: any): void {
    let checked = event.args.checked;
    if (checked) {
      this.mostrarParroquia = !checked;
      this.checkBoxComuna.checked(!checked);
      this.Barrio = 'Barrio'
    }
  }

  checkComuna(event: any): void {
    let checked = event.args.checked;
    if (checked) {
      this.mostrarParroquia = checked;
      this.checkBoxParroquia.checked(!checked);
      this.Barrio = 'Nombre'
    }
  }

  /*************************************************************************************************************** */
  //****evento de los comboCarrera y comboSemtres de la seccion Asiganturas del proyecto *************************************************************

  listarAsignaturas(event: any): void {
    if (event.args) {
      let item = event.args.item;
      if (item) {
        if (this.dropDownCarrera.val() && this.dropDownSemestre.val()) {
          this.llenarComboAsignaturas();
        }
      }
    }
  }

  llenarComboAsignaturas() {
    console.log('carrear ' + this.dropDownCarrera.val());
    console.log('SEMESTRE ' + this.dropDownSemestre.val());
    this._sub = this.periodoConvocatoria.buscarAsignaturasProyectos(this.dropDownCarrera.val(), this.dropDownSemestre.val()).subscribe(data => {
      console.log(JSON.stringify(data));
      this.sourceAsignaturas.localdata = data;
      this.dataAdapterAsignatura.dataBind();
    }, error => console.error("Error al llenar combo asignaturas proyecto " + error));
  }

  /********************************************************************************************************************************** */
  /**************************************PARAMETROS PARA LLENAR EL COMBO CANTONES**************************************************************/

  sourceAsignaturas: any = {
    datatype: 'json',
    id: 'idMallaAsignatura',
    localdata: [
      { name: 'idMallaAsignatura', type: 'int' },
      { name: 'descipcionn', type: 'string' },
      { name: 'semestre', type: 'string' },
      { name: 'estado', type: 'string' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE CANTONES
  dataAdapterAsignatura: any = new jqx.dataAdapter(this.sourceAsignaturas);

  /*************************************************************************************************************** */
  /********************************************PARAMETROS DE LA GRILA DE ASIGNATURAS PROYECTO******************************************************************* */

  sourceAsignaturasProyecto: any =
    {
      datatype: 'array',
      id: 'id',
      datafields:
        [
          { name: 'id', type: 'int' },
          { name: 'idProyectoAsignatura', type: 'int' },
          { name: 'idMallaAsignatura', type: 'int' },
          { name: 'descripcion', type: 'string' },
          { name: 'semestre', type: 'string' },
        ]
    };
  dataAdapterAsignaturaProyecto: any = new jqx.dataAdapter(this.sourceAsignaturasProyecto);

  columnsAsignaturaProyecto: any[] =
    [
      { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Id', datafield: 'idProyectoAsignatura', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Id', datafield: 'idMallaAsignatura', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Asignatura', datafield: 'descripcion', width: '40%' },
      { text: 'Semestre', datafield: 'semestre', width: '40%' },
      {
        text: 'Quitar', columntype: 'button', width: '20%',
        cellsrenderer: (): string => {
          return 'Quitar';
        },
        buttonclick: (row: number): void => {
          let idProyectoAsignatura = this.gridAsignaturaProyecto.getcellvalue(this.rowindex, 'idProyectoAsignatura');
          if (idProyectoAsignatura != null) {
            this.proyectoAsignaturaDelete.push(idProyectoAsignatura);
            let id = this.gridAsignaturaProyecto.getcellvalue(this.rowindex, 'id');
            this.gridAsignaturaProyecto.deleterow(id);
          } else {
            let id = this.gridAsignaturaProyecto.getcellvalue(this.rowindex, 'id');
            this.gridAsignaturaProyecto.deleterow(id);
          }
          this.rowindex = -1;
        },
      },
    ];

  /*************************************************************************************************************** */
  /*****************evento para seleccionar del combo asignatura Proyecto**********************************************************/

  asignaturaSeleccionada(event: any): void {
    if (event.args) {
      let item = event.args.item;
      if (item) {
        this.asignaturaSelec = item.value;
      }
    }
  };

  /*************************************************************************************************************** */
  /*****************evento para eliminar asignatura guardada en la bd**********************************************************/

  borrarAsignatura(idProyectoAsignatura: any) {
    this._sub = this.periodoConvocatoria.borrarAsignaturaGrid(idProyectoAsignatura).subscribe(data => {
    }, error => console.error("Error al eliminar asignaturas proyecto " + error));
  }

  /********************************************************************************************************************************** */
  /***********metodo para obtner el id de la fila seleccionada en la grilla de Asignatura proyecto*************************************************************/

  RowselectAsig(event: any): void {
    if (event.args) {
      this.rowindex = event.args.rowindex;
      console.log('this.rowindex ' + this.rowindex);
    }
  }

  /********************************************************************************************************************************** */
  /*********************************crea el boton y el evento para agregar ASIGNATURAPROYECTO AL GRIP*******************************************************************/

  rendertoolbarAsig = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer4 = document.createElement('div');
    buttonContainer4.id = 'buttonContainer4';
    buttonContainer4.style.cssText = 'float: left; margin-left: 5px';
    container.appendChild(buttonContainer4);
    toolbar[0].appendChild(container);
    let addRowButton = jqwidgets.createInstance('#buttonContainer4', 'jqxButton', { width: 100, value: 'Agregar', theme: 'darkblue' });
    addRowButton.addEventHandler('click', () => {
      if (this.asignaturaSelec == undefined) {
        this.myModal.alertMessage({ title: 'Asignatura Proyecto', msg: 'Seleccione una asignatura!' });
      } else {
        let gridAsignaturaProyecto = this.gridAsignaturaProyecto.getrows();
        let getSize = gridAsignaturaProyecto.length;
        this.gridAsignaturaProyecto.selectrow(getSize - 1);
        let arrayOfAsignaturas: any = this.dropDownAsigantura.getSelectedItem();
        let arrayOfAsignaturas1: any = []
        arrayOfAsignaturas1.push(arrayOfAsignaturas.originalItem);
        arrayOfAsignaturas1[0].idProyectoAsignatura = null;
        if (getSize == 0) {
          //Agrega una nueva fila
          this.aggGriAsignaturaProyecto(arrayOfAsignaturas1);
        } else {
          let data1 = gridAsignaturaProyecto.filter((dats) => { return dats.idMallaAsignatura == this.dropDownAsigantura.val() })
          if (data1.length > 0) {
            this.myModal.alertMessage({ title: 'Asignatura Proyecto', msg: 'Asignatura ya ha sido agregado!' });
            this.gridAsignaturaProyecto.clearselection();
          } else {
            this.aggGriAsignaturaProyecto(arrayOfAsignaturas1);
          }
        }
      }
    })
  };

  /********************************************************************************************************************************** */
  /*********************************EVENTOS PARA EL BOTON AGG ESTUDIANES PARTICIPANTES AL GRIP*******************************************************************/

  aggGriAsignaturaProyecto(data: any) {
    for (let index = 0; index < data.length; index++) {
      let datarow = {};
      datarow['id'] = data[index].id;
      datarow['idProyectoAsignatura'] = data[index].idProyectoAsignatura;
      datarow['idMallaAsignatura'] = data[index].idMallaAsignatura;
      datarow['descripcion'] = data[index].descripcion;
      datarow['semestre'] = data[index].semestre;
      this.gridAsignaturaProyecto.addrow(data[index].id, datarow);
    }
  }
  
  /********************************************************************************************************************************** */
  /**************************************EVENTOS VALIDA QUE SE INGRSEN NUMEROS DECIMALES**************************************************************/

  keyPress(event: any) {
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    let current: string = event.target.value;
    // We need this because the current value on the DOM element
    // is not yet updated with the value from this event
    let next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }

  /********************************************************************************************************************************** */
  /**************************************EVENTOS DE PRESUPUESTO SUMA LOS TOTALES**************************************************************/

  Change(event: any): void {
    this.total = 0;
    this.total = this.total + Number(this.proyectoPresupuesto.aporteUniversidad);
    this.total = this.total + Number(this.proyectoPresupuesto.entidadAuspiciante);
    this.total = this.total + Number(this.proyectoPresupuesto.comunidadBeneficiaria);
    this.total = this.total + Number(this.proyectoPresupuesto.autogestion);
    
  }   

  /*****************************VALIDACIONES DE LA SECCION DATOS GENERALES************************************************************** */
  
  funValidar() {
    let validar = true;
    if (this.proyecto.titulo == "") {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese un título!' });
      validar = false;
    }
    if (this.proyecto.descripcion == "") {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese una descripción!' });
      validar = false;
    }
    if (this.proyecto.fechaDesde == null) {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese una Fecha Inicio!' });
      validar = false;
    }
    if (this.proyecto.fechaHasta == null) {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese una Fecha Fin!' });
      validar = false;
    }
    if (this.proyectoVersion.url == '') {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Seleccione un Archivo!' });
      validar = false;
    }
    if ((this.proyecto.fechaDesde != null) && (this.proyecto.fechaHasta != null)) {
      var dateFechadesde = this.proyecto.fechaDesde;
      var dateFechaHasta = this.proyecto.fechaHasta;
      //convierte las fechasen numeros
      var d = Date.parse(dateFechadesde);
      var h = Date.parse(dateFechaHasta);
      if (d >= h) {
        console.log("mayor " + dateFechadesde);
        this.myModal.alertMessage({ title: 'Proyecto', msg: 'Fecha Fin debe ser mayor a fecha Inicial!' });
        validar = false;
      }
    }
    return validar;
  };
  
  seccionAsignaturasProycto() {
    let validar = true;
    let gridAsignaturaProyecto = this.gridAsignaturaProyecto.getrows();
    let getSize = gridAsignaturaProyecto.length;
    if (getSize == 0) {
      //Agrega una nueva fila
      this.myModal.alertMessage({ title: 'Asignatura Proyecto', msg: 'Ingrese una asignatura!' });
      validar = false;
    }

    if (this.proyectoAsignaturaDelete.length > 0) {
      for (let index = 0; index < this.proyectoAsignaturaDelete.length; index++) {
        console.log('idProyectoAsiganatura' + this.proyectoAsignaturaDelete[index]);
        this.borrarAsignatura(this.proyectoAsignaturaDelete[index]);
      }
      validar = true;
    }
    return validar;
  }

  seccionAreaGeografica() {
    let validar = true;
    if (this.dropDownProvincia.val() == "") {
      this.myModal.alertMessage({ title: 'Area Geográfica', msg: 'Seleccione una provincia!' });
      validar = false;
    }
    if (this.dropDownCanton.val() == "") {
      this.myModal.alertMessage({ title: 'Area Geográfica', msg: 'Seleccione un canton!' });
      validar = false;
    }
    if (this.proyectoAreaGeografica.descripcion == "") {
      this.myModal.alertMessage({ title: 'Area Geográfica', msg: 'ingrese el nombre del barrio o la comuna!' });
      validar = false;
    }
    if (this.checkBoxParroquia.checked()) {
      this.proyectoAreaGeografica.barrioComuna = this.checkBoxParroquia.checked();//true
      this.proyectoAreaGeografica.idAreaGeografica = this.dropDownParroquia.val();
    } else {
      this.proyectoAreaGeografica.barrioComuna = this.checkBoxParroquia.checked();//false
      this.proyectoAreaGeografica.idAreaGeografica = this.dropDownCanton.val();
    }
    return validar;
  }

 /*************************************************evento del boton subir documento************************************************************** */

  onUploadStart(event: FileList): void {
    let fileOld = event.item(0);
    
    /*if (event.length >0) {
      var theSplit = event.target.value.split('\\');
      this.proyectoVersion.url = theSplit[theSplit.length - 1];
      let fileNew = new FormData();
      fileNew.append('file', event.target.files[0], this.proyectoVersion.url);
      this.fileSelecionado = fileNew;      
    } else {
      this.proyectoVersion.url;
    }*/
    let fileNew:FormData = new FormData();
    fileNew.append('file', fileOld,this.proyectoVersion.url);
    this.fileSelecionado = fileNew; 
  };
  enviarFileProyecto(){
    return this.fileSelecionado;
  }

  formarJsonDatosGenerales() {
    let datosProyecto: any = {};
    let arrProyectoVersion: any = [];
    let objProyectoVersion: any = {};
    let arrProyectoPresupuesto: any = [];
    let objProyectoPresupuesto: any = {};
    let arrProyectoAsignatura: any = [];
    let objProyectoAsignatura: any = {};
    let arrInstitucionVeneficiaria: any = [];
     
     //datos generales del pryecto
    datosProyecto.id = this.proyecto.id;
    datosProyecto.idPrograma = this.proyecto.idPrograma;
    datosProyecto.idProyectoConvocatoria = this.proyecto.idProyectoConvocatoria;    
    datosProyecto.descripcion = this.proyecto.descripcion;
    datosProyecto.titulo = this.proyecto.titulo;
    datosProyecto.codigo = this.proyecto.codigo;
    datosProyecto.estadoProyecto = this.proyecto.estadoProyecto;
    datosProyecto.estado = 'A';
    datosProyecto.fechaDesde = this.proyecto.fechaDesde;
    datosProyecto.fechaHasta = this.proyecto.fechaHasta;
    datosProyecto.usuarioIngresoId = this.usuarioActual.id;
    datosProyecto.file = this.fileSelecionado;

    //version del proyecto
    objProyectoVersion.id = this.proyectoVersion.id;
    objProyectoVersion.url = this.proyectoVersion.url;
    objProyectoVersion.estadoProyectoVersion = this.proyectoVersion.estadoProyectoVersion;
    objProyectoVersion.usuarioIngresoId = this.usuarioActual.id;
    objProyectoVersion.estado = 'A';
    arrProyectoVersion.push(objProyectoVersion);
    
    
    //proyecto asignatura
    let gridAsignaturaProyecto = this.gridAsignaturaProyecto.getrows();
    let getSize = gridAsignaturaProyecto.length;
    if(getSize >0){
      for (let index = 0; index < gridAsignaturaProyecto.length; index++) {
        objProyectoAsignatura= {};
        objProyectoAsignatura.id=gridAsignaturaProyecto[index].idProyectoAsignatura;
        objProyectoAsignatura.idMallaAsignatura=gridAsignaturaProyecto[index].idMallaAsignatura;
        objProyectoAsignatura.usuarioIngresoId = this.usuarioActual.id;
        objProyectoAsignatura.estado = 'A';
        arrProyectoAsignatura.push(objProyectoAsignatura);        
      }
    }    

    //proyecto presupuesto
    objProyectoPresupuesto.id = this.proyectoPresupuesto.id;
    objProyectoPresupuesto.aporteUniversidad = this.proyectoPresupuesto.aporteUniversidad;
    objProyectoPresupuesto.entidadAuspiciante = this.proyectoPresupuesto.entidadAuspiciante;
    objProyectoPresupuesto.comunidadBeneficiaria = this.proyectoPresupuesto.comunidadBeneficiaria;
    objProyectoPresupuesto.autogestion = this.proyectoPresupuesto.autogestion;
    objProyectoPresupuesto.usuarioIngresoId = this.usuarioActual.id;
    objProyectoPresupuesto.estado = 'A';
    arrProyectoPresupuesto.push(objProyectoPresupuesto);

   // objProyectoAreGeografica
    
    datosProyecto.proyectoVersion = arrProyectoVersion;
    this.institucionBeneficiaria.proyectoAreaGeografica=this.proyectoAreaGeografica;
    arrInstitucionVeneficiaria.push(this.institucionBeneficiaria);
    datosProyecto.institucionBeneficiaria = arrInstitucionVeneficiaria;

    datosProyecto.proyectoPresupuesto=arrProyectoPresupuesto;
    datosProyecto.proyectoAsignatura=arrProyectoAsignatura;
    //json
    return datosProyecto;
  }   
}