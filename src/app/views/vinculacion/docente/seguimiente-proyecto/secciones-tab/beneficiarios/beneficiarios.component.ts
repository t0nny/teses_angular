import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { jqxCheckBoxComponent } from "jqwidgets-ng/jqxcheckbox";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { ValidadorService } from "../../../../../../services/validacion/validador.service";
import { ModalComponentComponent } from "../../../../../modal-view/modal-component/modal-component.component";

@Component({
    selector: 'app-beneficiarios',
    templateUrl: './beneficiarios.component.html',
    //styleUrls: ['./registro-proyecto.component.scss']
})

export class BeneficiariosComponent implements OnInit {
    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

    //SECCION BENEFICIARIOS
    @ViewChild('comboGenero') comboGenero: jqxDropDownListComponent;
    sourceGenero = [{ "genero": "Masculino", "codigo": 'M' }, { "genero": "Femenino", "codigo": 'F' }];
    @ViewChild('checkBoxValCedula') checkBoxValCedula: jqxCheckBoxComponent;

    constructor(private validadorService: ValidadorService) { }

    //mecanismo de enlace entre el hijo y el padre
    @Input() proyectoPersona: any;
    @Input() institucionBeneficiaria: any;

    ngOnInit(): void {
        console.log("ngOnInitbeneficiarios")
    }

    seccionRepresententeLegal() {
        let validar = true;
        if (!this.checkBoxValCedula.checked()) {
            if (!this.validadorService.validaCedula(this.proyectoPersona.identificacion)) {
                validar = false;
                this.myModal.alertMessage({
                    title: 'Cédula incorrecta',
                    msg: 'Ingrese correctamente la cédula!'
                });
            }
        }
        if (this.proyectoPersona.identificacion == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese la celuda del representante legal!' });
            validar = false;
        }
        if (this.proyectoPersona.apellidos == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese los apellidos del representante legal!' });
            validar = false;
        }
        if (this.proyectoPersona.nombres == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese los nombres del representante legal!' });
            validar = false;
        }
        if (this.proyectoPersona.celular == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese numero celular del representante legal!' });
            validar = false;
        }
        return validar;
    }   

    seccionOrganizacionBeneficiaria() {
        let validar = true;
        if (this.institucionBeneficiaria.razonSocial == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese nombre de la organización!' });
            validar = false;
        }
        if (this.institucionBeneficiaria.direccion == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese dirección de la organización!' });
            validar = false;
        }
        if (this.institucionBeneficiaria.benefDirectos == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese numero de beneficiarios directos!' });
            validar = false;
        }
        if (this.institucionBeneficiaria.benefIndirectos == "") {
            this.myModal.alertMessage({ title: 'Organización Beneficiaria', msg: 'Ingrese numero de beneficiarios indirectos!' });
            validar = false;
        }
        return validar;
    }   
}