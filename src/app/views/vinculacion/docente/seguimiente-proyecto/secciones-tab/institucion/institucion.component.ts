import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../../../services/auth/account.service";
import { VinculacionService } from "../../../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../../../modal-view/modal-component/modal-component.component";

@Component({
    selector: 'app-institucion',
    templateUrl: './institucion.component.html',
    //styleUrls: ['./registro-proyecto.component.scss']
})

export class InstitucionComponent implements OnInit {

    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

    //SECCION INSTITUCION
    @ViewChild('dropDownArea') dropDownArea: jqxDropDownListComponent;
    @ViewChild('dropDownSubArea') dropDownSubArea: jqxDropDownListComponent;
    @ViewChild('dropDownSubAreaEsp') dropDownSubAreaEsp: jqxDropDownListComponent;
    @ViewChild('dropDownDominioAcad') dropDownDominioAcad: jqxDropDownListComponent;
    @ViewChild('dropDownLineaInv') dropDownLineaInv: jqxDropDownListComponent;
    @ViewChild('dropDownSubLinea') dropDownSubLinea: jqxDropDownListComponent;

    constructor(private periodoConvocatoria: VinculacionService,
        private accountService: AccountService) { }
    
    @Input() lineasInvestigacion: any;
    @Input() proyectoLineasInv: any = {};
    @Input() proyectoDominiosAcad: any = {};
    @Input() proyectoAreaUns: any = {};
    areas: any;
    _sub: Subscription;
    usuarioActual: any = {};//variable para guardar el usuario actual

    ngOnInit(): void {
        this.usuarioActual=this.accountService.getUsuario(); //captura del usuario actual    
        this.listarUnesco();
        this.listarDominiosAcad();
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    procesaPassCombosUnesco(idAreaUnesco: any) {
        let SUB_AREA_ESP = this.areas.filter((dats) => { return dats.id == idAreaUnesco })
        if (SUB_AREA_ESP[0].idPadre != 0) {
            let SUB_AREA = this.areas.filter((dats) => { return dats.id == SUB_AREA_ESP[0].idPadre })
            if (SUB_AREA[0].idPadre != 0) {
                let AREA = this.areas.filter((dats) => { return dats.id == SUB_AREA[0].idPadre })
                if (AREA[0].idPadre == 0) {
                    this.dropDownArea.val(AREA[0].id);
                    this.dropDownSubArea.val(SUB_AREA[0].id);
                    this.dropDownSubAreaEsp.val(SUB_AREA_ESP[0].id);
                }
            }
        }
    }

    procesaPassCombosLineasInv(idSubLineaInv: any) {
        let SUB_AREA_ESP = this.lineasInvestigacion.filter((dats) => { return dats.idSubLinea == idSubLineaInv })
        this.dropDownLineaInv.val(SUB_AREA_ESP[0].idLineaInv);
        this.dropDownSubLinea.val(idSubLineaInv);
    }

    procesaPassDropDownDominioAcad(idDominiosAcademicos: any) {
        this.dropDownDominioAcad.val(idDominiosAcademicos);

    }

    seccionInstitucion() {
        let validar = true;
        if (this.dropDownArea.val() == "") {
            this.myModal.alertMessage({ title: 'Institución', msg: 'Seleccione una área de la unesco!' });
            validar = false;
        }
        if (this.dropDownSubArea.val() == "") {
            this.myModal.alertMessage({ title: 'Institución', msg: 'Seleccione un subárea de la unesco!' });
            validar = false;
        }
        if (this.dropDownDominioAcad.val() == "") {
            this.myModal.alertMessage({ title: 'Institución', msg: 'Seleccione dominios acádemicos!' });
            validar = false;
        }
        if (this.dropDownSubAreaEsp.val() == "") {
            this.myModal.alertMessage({ title: 'Institución', msg: 'Seleccione un subárea especifica de la unesco!' });
            validar = false;
        }
        if (this.dropDownLineaInv.val() == "") {
            this.myModal.alertMessage({ title: 'Institución', msg: 'Seleccione lineas de investigación!' });
            validar = false;
        }
        if (this.dropDownSubLinea.val() == "") {
            this.myModal.alertMessage({ title: 'Institución', msg: 'Seleccione una sublinea de investigación!' });
            validar = false;
        }
        return validar;
    }

    /********************************************************************************************************************************** */
    /**************************************SECCION INSTITUCION**************************************************************/
    /**************************************SERVICIO, TRAE LISTA AREAGEOGRAFICA**************************************************************/

    listarUnesco() {
        this._sub = this.periodoConvocatoria.buscarListaAreaUnesco().subscribe(data => {
            this.areas = data;
            let AREAS = data.filter((dats) => { return dats.idPadre == 0 });
            this.sourceArea.localdata = AREAS;
            this.dataAdapterArea.dataBind();
        });
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO AREA**************************************************************/

    sourceArea: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descipcionn', type: 'string' },
            { name: 'idPadre', type: 'int' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE AREA
    dataAdapterArea: any = new jqx.dataAdapter(this.sourceArea);

    /********************************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO AREA, LISTA SUB_AREAS**************************************************************/

    ListarSubArea(event: any) {
        let SUB_AREA = this.areas.filter((dats) => { return dats.idPadre == event.args.item.value });
        this.sourceSubArea.localdata = SUB_AREA;
        this.dataAdapterSubArea.dataBind();
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO SUB_AREA**************************************************************/

    sourceSubArea: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descipcionn', type: 'string' },
            { name: 'idPadre', type: 'int' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE SUB_AREA
    dataAdapterSubArea: any = new jqx.dataAdapter(this.sourceSubArea);

    /********************************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO AREA, LISTA SUB_AREAS**************************************************************/

    ListarSubAreaEsp(event: any) {
        let SUB_AREA_ESP = this.areas.filter((dats) => { return dats.idPadre == event.args.item.value });
        this.sourceSubAreaEsp.localdata = SUB_AREA_ESP;
        this.dataAdapterSubAreaEsp.dataBind();
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO SUB_AREA_ESP**************************************************************/

    sourceSubAreaEsp: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descipcionn', type: 'string' },
            { name: 'idPadre', type: 'int' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE SUB_AREA_ESP
    dataAdapterSubAreaEsp: any = new jqx.dataAdapter(this.sourceSubAreaEsp);

    /********************************************************************************************************************************** */
    /**************************************SERVICIOS PARA LLENAR COMBOS**************************************************************/

    procesaPasslistarLineasInvestigacion(data: any) {
        let result = Array.from(new Set(data.map(s => s.idLineaInv)))
            .map(idLineaInv => {
                return {
                    idLineaInv: idLineaInv,
                    descripcionLinea: data.find(s => s.idLineaInv === idLineaInv).descripcionLinea
                };
            });
        this.lineasInvestigacion = data;
        this.sourceLineasInv.localdata = result;
        this.dataAdapterLineaInv.dataBind();
    }

    listarDominiosAcad() {
        this._sub = this.periodoConvocatoria.buscarDominiosAcademicos().subscribe(data => {
            this.sourceDominiosAcad.localdata = data;
            this.dataAdapterDominioAcad.dataBind();
        });
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO DOMINIOS ACAD**************************************************************/

    sourceDominiosAcad: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descipcionn', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE DOMINIOS_ACAD
    dataAdapterDominioAcad: any = new jqx.dataAdapter(this.sourceDominiosAcad);

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO LINEAS INV**************************************************************/

    sourceLineasInv: any = {
        datatype: 'json',
        id: 'idLineaInv',
        localdata: [
            { name: 'idLineaInv', type: 'int' },
            { name: 'descripcionLinea', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE LINEA_INV
    dataAdapterLineaInv: any = new jqx.dataAdapter(this.sourceLineasInv);

    /********************************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO LINEAS_INV, LISTA SUB_LINEAS_INV**************************************************************/

    ListarSubLineas(event: any) {
        let SUB_AREA_ESP = this.lineasInvestigacion.filter((dats) => { return dats.idLineaInv == event.args.item.value })
        this.sourceSubLineasInv.localdata = SUB_AREA_ESP;
        this.dataAdapterSubLinea.dataBind();
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO SUB LINEAS INV**************************************************************/

    sourceSubLineasInv: any = {
        datatype: 'json',
        id: 'idSubLinea',
        localdata: [
            { name: 'idSubLinea', type: 'int' },
            { name: 'descripcionSubLi', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE LINEA_INV
    dataAdapterSubLinea: any = new jqx.dataAdapter(this.sourceSubLineasInv);
   
    formarJsonInstitucion() {
    let datosProyecto: any = {};
     let objProyectoAreaUnesco: any = {};
    let arrProyectoAreaUnesco: any = [];
    let objDominiosAcad: any = {};
    let arrDominiosAcad: any = [];
    let objLineasInv: any = {};
    let arrLineasInv: any = [];
     
    //proyecto dominios academicoa
    objDominiosAcad.id = this.proyectoDominiosAcad.id;
    objDominiosAcad.idDominiosAcademicos = this.dropDownDominioAcad.val();
    objDominiosAcad.usuarioIngresoId = this.usuarioActual.id;
    objDominiosAcad.estado = 'A';
    arrDominiosAcad.push(objDominiosAcad);

    //proyecto Area Unesco
    objProyectoAreaUnesco.id = this.proyectoAreaUns.id;
    objProyectoAreaUnesco.idAreaUnesco = this.dropDownSubAreaEsp.val();
    objProyectoAreaUnesco.usuarioIngresoId = this.usuarioActual.id;
    objProyectoAreaUnesco.estado = 'A';
    arrProyectoAreaUnesco.push(objProyectoAreaUnesco);

    //proyecto Lineas Investigacion
    objLineasInv.id = this.proyectoLineasInv.id;
    objLineasInv.idSubLineaInv = this.dropDownSubLinea.val();
    objLineasInv.usuarioIngresoId = this.usuarioActual.id;
    objLineasInv.estado = 'A';
    arrLineasInv.push(objLineasInv);    
    
    datosProyecto.proyectoDominiosAcademicos=arrDominiosAcad;
    datosProyecto.proyectoLineasInv=arrLineasInv;
    datosProyecto.proyectoAreaUnesco=arrProyectoAreaUnesco;
    //json
    return datosProyecto;
  }
}
