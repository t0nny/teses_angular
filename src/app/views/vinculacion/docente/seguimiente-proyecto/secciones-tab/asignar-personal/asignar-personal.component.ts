import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { Subscription } from "rxjs";
import { VinculacionService } from "../../../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../../../modal-view/modal-component/modal-component.component";
import { AccountService } from '../../../../../../services/auth/account.service';
import { ModalDetallesParticipantesComponent } from "../../../../componentes/modal-detalles-participantes/modal-detalles-participantes.component";

@Component({
    selector: 'app-asignar-personal',
    templateUrl: './asignar-personal.component.html',
    //styleUrls: ['./registro-proyecto.component.scss']
})

export class AsignarPersonalComponent implements OnInit {

    constructor(private periodoConvocatoria: VinculacionService,
        private accountService: AccountService) { }

    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild(ModalDetallesParticipantesComponent) detalleParticipante: ModalDetallesParticipantesComponent;
    //@ViewChild('gridDirectorProyecto') gridDirectorProyecto: jqxGridComponent;
    @ViewChild('gridDocenteParticipantes') gridDocenteParticipantes: jqxGridComponent;
    //@ViewChild('gridEstudianteCoordinador') gridEstudianteCoordinador: jqxGridComponent;
    @ViewChild('gridEstudianteParticipantes') gridEstudianteParticipantes: jqxGridComponent;

    @ViewChild('dropDownCarreraDP') dropDownCarreraDP: jqxDropDownListComponent;
    @ViewChild('dropDownDocentes') dropDownDocentes: jqxDropDownListComponent;

    @ViewChild('dropDownCarreraEC') dropDownCarreraEC: jqxDropDownListComponent;
    @ViewChild('dropDownSemestreEC') dropDownSemestreEC: jqxDropDownListComponent;
    @ViewChild('dropDownParaleloEC') dropDownParaleloEC: jqxDropDownListComponent;
    @ViewChild('dropDownEstudiantesCo') dropDownEstudiantesCo: jqxDropDownListComponent;

    @ViewChild('dropDownCarreraEP') dropDownCarreraEP: jqxDropDownListComponent;
    @ViewChild('dropDownSemestreEP') dropDownSemestreEP: jqxDropDownListComponent;
    @ViewChild('dropDownParaleloEP') dropDownParaleloEP: jqxDropDownListComponent;
    @ViewChild('dropDownEstudiantesPa') dropDownEstudiantesPa: jqxDropDownListComponent;

    //Variable paa cargar datos del objeto 
    rowindex: number = -1;
    _sub: Subscription;

    @Input() dataSemestre: any;

    proyectoDocenteDelete: any = [];//almacenara los id de proyectosDocentes a eliminar
    proyectoEstudianteDelete: any = [];

    idCarreraCo: any;
    idSemestreCo: any;
    idCarreraPa: any;
    idSemestrePa: any;
    estudiantePaSelec: any;
    estudianteCoSelec: any;
    estudiantesCoData: any;
    estudiantesPaData: any;
    arrayOfIdEstudiantesPa: any;
    usuarioActual: any = {};//variable para guardar el usuario actual

    ngOnInit(): void {
        this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
        this.limparCombos();
        this.ListarSemestres();
        this.ListarParalelos();
        this.listarCarreras();
    }

    /********************************************************************************************************************************** */
    //metodo para actualizar datos enviados desde seguimientos-proyecto a datos-generales
    listarCarreras() {
        let dataCarrera = JSON.parse(localStorage.getItem('dataCarrera')); //captura la data de carreras
        this.sourceCarrera.localdata = dataCarrera;
        this.dataAdapterCarrera.dataBind();
    }

    /*procesaPassDirectorProyecto(h) {
        if (h != undefined) {
            //this.ListarDirectorProyecto(h);
        }
    }

    procesaPassGripDocentesProyecto(h) {
        if (h != undefined) {
            //this.aggGripDocentesProyecto(h);
        }
    }*/

    procesaPassGripDocentesParticipantes(h) {
        if (h != undefined) {
            this.aggGripDocentesParticipantes(h);
        }
    }
    /*
        procesaPassGripEstudianteProyectoCo(h) {
            if (h != undefined) {
                this.aggGripEstudianteProyectoCo(h);
            }
        }*/

    procesaPassGripEstudianteProyectoPa(h) {
        if (h != undefined) {
            this.aggGripEstudianteProyectoPa(h);
        }
    }

    /********************************************************************************************************************************** */

    //FUENTE DE DATOS PARA EL COMBOBOX DE CARRERAS
    sourceCarrera: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descripcion', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE CARRERA
    dataAdapterCarrera: any = new jqx.dataAdapter(this.sourceCarrera);

    /********************************************************************************************************************************** */
    /*********************************Cargar combo semestres*******************************************************************/

    ListarSemestres() {
        this.sourceSemestre.localdata = this.dataSemestre;
        this.dataAdapterSemestre.dataBind();
    }

    sourceSemestre: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'descripcion', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE SEMESTRE
    dataAdapterSemestre: any = new jqx.dataAdapter(this.sourceSemestre);

    /********************************************************************************************************************************** */
    /*********************************Cargar combo semestres*******************************************************************/

    ListarParalelos() {
        this._sub = this.periodoConvocatoria.listarParalelos().subscribe(data => {
            this.sourceParalelos.localdata = data;
            this.dataAdapterParalelo.dataBind();
        }, error => console.error("Error al obtener los paralelos " + error));
    }

    sourceParalelos: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'id', type: 'int' },
            { name: 'label', type: 'string' },
            { name: 'state', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE PARALELO
    dataAdapterParalelo: any = new jqx.dataAdapter(this.sourceParalelos);

    /*************************************************************************************************************** */
    /************************agg DIRECTOR DE PROYECTOS al grip***************************************************/

    /*ListarDirectorProyecto(idDocente: any) {
        this._sub = this.periodoConvocatoria.buscarDirectorProyecto(idDocente).subscribe(data => {
            data[0].idTipoUsuarioProyecto = 1;//id 1 porque es docente director
            this.sourceDirectoRegistrado.localdata = data;
            this.dataAdapterDirectorProyecto.dataBind();
        });
    }

    /*************************************************************************************************************** */
    /*******************PARAMETROS DE LA GRILA DE DIRECTOR DE PROYECTOS*****************************************************/

    /*sourceDirectoRegistrado: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },//es un id para controlar la eliminacion o actualizacion(no se guarda ni viene dela db)
            { name: 'idProyectoDocente', type: 'int' },
            { name: 'idTipoUsuarioProyecto', type: 'int' },
            { name: 'idDocente', type: 'int' },
            { name: 'identificacion', type: 'string' },
            { name: 'nombres', type: 'string' },
            { name: 'apellidos', type: 'string' },
            { name: 'tituloGrado', type: 'string' },
            { name: 'tituloPosgrado', type: 'date', format: 'd' },
            { name: 'email', type: 'date', format: 'd' },
            { name: 'cedular', type: 'string' },
        ]
    };
    dataAdapterDirectorProyecto: any = new jqx.dataAdapter(this.sourceDirectoRegistrado);

    columnsDirectorProyecto: any[] = [
        { text: 'Id', datafield: 'id', width: '5%', filtertype: 'none', hidden: true },
        { text: 'IdProyectoDocente', datafield: 'idProyectoDocente', width: '5%', filtertype: 'none', hidden: true },
        { text: 'IdDocente', datafield: 'idDocente', width: '5%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idTipoUsuarioProyecto', width: '5%', filtertype: 'none', hidden: true },
        { text: 'Identificación', datafield: 'identificacion', width: '10%' },
        { text: 'Nombres', datafield: 'nombres', width: '15%' },
        { text: 'Apellidos', datafield: 'apellidos', width: '15%' },
        { text: 'Título Grado', datafield: 'tituloGrado', width: '10%' },
        { text: 'Título Posgrado', datafield: 'tituloPosgrado', width: '20%', cellsalign: 'center', center: 'center' },
        { text: 'Email', datafield: 'email', width: '20%' },
        { text: 'Cedular', datafield: 'cedular', width: '10%' },
    ];

    /*************************************************************************************************************** */
    //**************************evento del comboCarreras para agg DOCENTES PARTICIPANTES al combo *************************************************************

    ListarDocentes(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.ListarDocenteCombo(4, item.value)
            }
        }
    }

    /*************************************************************************************************************** */
    /*****************llenar combo docentes PARTICIPANTES**********************************************************/

    ListarDocenteCombo(id: any, idDepOferta: any) {
        this._sub = this.periodoConvocatoria.buscarDocentesPorIdDepOferta('4', idDepOferta).subscribe(data => {
            //console.log(JSON.stringify(data));
            this.sourceDocente.localdata = data;
            this.dataAdapterDocentes.dataBind();
        }, error => console.error("h " + error));
    }

    //FUENTE DE DATOS PARA EL COMBOBOX DE DOCENTE PARTICIPANTES
    sourceDocente: any = {
        datatype: 'json',
        id: 'idDocente',
        localdata: [
            { name: 'idDocente', type: 'int' },
            { name: 'nombres', type: 'string' },
            { name: 'estado', type: 'string' },
            { name: 'identificacion', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DOCENTE PARTICIPANTES
    dataAdapterDocentes: any = new jqx.dataAdapter(this.sourceDocente);

    /********************************************************************************************************************************** */
    /*****************crea el boton y el evento para agregar DOCENTES PARTICIPANTES AL GRIP*************************************************************/

    rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer1 = document.createElement('div');
        buttonContainer1.id = 'buttonContainer1';
        buttonContainer1.style.cssText = 'float: left; margin-left: 5px';
        container.appendChild(buttonContainer1);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: 100, value: 'Agregar', theme: 'darkblue' });
        addRowButton.addEventHandler('click', () => {
            let gridDocenteParticipantes = this.gridDocenteParticipantes.getrows();
            let getSize = gridDocenteParticipantes.length;
            this.gridDocenteParticipantes.selectrow(getSize - 1);
            let arrayOfIdDocentePart = JSON.parse(JSON.stringify(this.dropDownDocentes.getSelectedItem())).originalItem;

            if (getSize == 0) {
                //Agrega una nueva fila
                this.aggGripDocentesProyecto(arrayOfIdDocentePart);
            } else {
                let data1 = gridDocenteParticipantes.filter((dats) => { return dats.idDocente == this.dropDownDocentes.val() })
                if (data1.length > 0) {
                    this.myModal.alertMessage({ title: 'Seguimiento de Proyectos', msg: 'Docente ya ha sido agregado!' });
                    this.gridDocenteParticipantes.clearselection();
                } else {
                    this.aggGripDocentesProyecto(arrayOfIdDocentePart);
                }
            }
        })
    };

    /*************************************************************************************************************** */
    /************************agg docnetes participantes al grip***************************************************/

    aggGripDocentesParticipantes(idProyecto: any) {
        this._sub = this.periodoConvocatoria.listarDocentesProyecto(idProyecto).subscribe(dataDocente => {
            const data = Object.keys(dataDocente).map(index => {
                let person = dataDocente[index];
                return person;
            });
            //console.log(JSON.stringify(data));
            for (let index = 0; index < data.length; index++) {
                console.log("docente participane");
                let datarow = {};
                datarow['id'] = data[index].id;
                datarow['idProyectoDocente'] = data[index].idProyectoDocente;
                datarow['idDocente'] = data[index].idDocente;
                datarow['identificacion'] = data[index].identificacion;
                datarow['nombres'] = data[index].nombres;
                datarow['descripcion'] = data[index].descripcion;
                datarow['idTipoUsuarioProyecto'] = data[index].idTipoUsuarioProyecto;; //id 2 porque es docente participante
                this.gridDocenteParticipantes.addrow(data[index].id, datarow);
            }
        });

    }

    /*************************************************************************************************************** */
    /************************agg docnetes participantes al grip***************************************************/

    aggGripDocentesProyecto(idDocente: any) {
        //this._sub = this.periodoConvocatoria.buscarDirectorProyecto(idDocente).subscribe(data => {
        //console.log(JSON.stringify(data));
        //if (data.length > 0 && data[0].estado != 'E') {
        console.log("docente participane");
        let datarow = {};
        datarow['id'] = idDocente.id;
        datarow['idProyectoDocente'] = idDocente.idProyectoDocente;
        datarow['idDocente'] = idDocente.idDocente;
        datarow['identificacion'] = idDocente.identificacion;
        datarow['nombres'] = idDocente.nombres;
        datarow['descripcion'] = 'Participante';
        datarow['idTipoUsuarioProyecto'] = 2; //id 2 porque es docente participante
        this.gridDocenteParticipantes.addrow(idDocente.id, datarow);
        // }
        // });
    }
    /********************************************************************************************************************************** */
    /*******************PARAMETROS DE LA GRILA DE DOCENTES PARTICIPANTES*****************************************************/

    sourceDocenteParticipantes: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idProyectoDocente', type: 'int' },
            { name: 'idDocente', type: 'int' },
            { name: 'idTipoUsuarioProyecto', type: 'int' },
            { name: 'identificacion', type: 'string' },
            { name: 'nombres', type: 'string' },
            { name: 'descripcion', type: 'string' },
        ]
    };
    dataAdapterDocenteParticipantes: any = new jqx.dataAdapter(this.sourceDocenteParticipantes);

    columnsDocenteParticipantes: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'IdProyectoDocente', datafield: 'idProyectoDocente', width: '2%', filtertype: 'none', hidden: true },
        { text: 'IdDocente', datafield: 'idDocente', width: '2%', filtertype: 'none', hidden: true },
        { text: 'IdTipoUser', datafield: 'idTipoUsuarioProyecto', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Identificación', datafield: 'identificacion', width: '20%' },
        { text: 'Nombres', datafield: 'nombres', width: '42%' },
        { text: 'Típo Usuario', datafield: 'descripcion', width: '20%' },
        {
            text: 'Detalles', columntype: 'button', width: '10%',
            cellsrenderer: (): string => {
                return 'Detalles';
            },
            buttonclick: (row: number): void => {
                console.log('entrecreo');
                let idDocente = this.gridDocenteParticipantes.getcellvalue(this.rowindex, 'idDocente');
                if (idDocente != null) {
                    this.detalleParticipante.mostrarDetalleParticipantes(idDocente);
                    //abrir modal
                    /*this._sub = this.periodoConvocatoria.buscarDirectorProyecto(idDocente).subscribe(data => {
                        console.log(JSON.stringify(data));
                     
                    });*/
                }
                this.rowindex = -1;
            },
        },
        {
            text: 'Quitar', columntype: 'button', width: '8%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                debugger
                console.log('entrecreo');
                let tipoParticipante = this.gridDocenteParticipantes.getcellvalue(this.rowindex, 'descripcion');
                let idProyectoDocente = this.gridDocenteParticipantes.getcellvalue(this.rowindex, 'idProyectoDocente');
                if (tipoParticipante != 'Director') {
                    if (idProyectoDocente != null) {
                        this.proyectoDocenteDelete.push(idProyectoDocente);
                        let id = this.gridDocenteParticipantes.getcellvalue(this.rowindex, 'id');
                        this.gridDocenteParticipantes.deleterow(id);
                    } else {
                        let id = this.gridDocenteParticipantes.getcellvalue(this.rowindex, 'id');
                        this.gridDocenteParticipantes.deleterow(id);
                    }
                } else {
                    this.myModal.alertMessage({ title: 'Docente Participantes', msg: 'Docente director no puede ser retirado!' });

                }
                this.rowindex = -1;
            },
        },
    ];

    /********************************************************************************************************************************** */
    /***********metodo para obtner el id de la fila seleccionada en la grilla de DOCENTES PARTICIPANTES*************************************************************/

    Rowselect(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }

    /*************************************************************************************************************** */
    /*****************evento para eliminar proyecto_docente guardada en la bd**********************************************************/

    borrarProyectoDocente(idProyectoDocente: any) {
        this._sub = this.periodoConvocatoria.borrarProyectoDocenteGrid(idProyectoDocente).subscribe(data => {
        }, error => console.error("Error al eliminar docente del proyecto " + error));
    }

    /********************************************************************************************************************************** */
    /***************EVENTOS DE LOS COMBOS CARRERAS Y SEMESTRE de estudiantesCOOLABORADOR*******************************************************************/

    /*itemCarreraCoSele(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.idCarreraCo = item.value;
                this.dropDownParaleloEC.clearSelection();
                this.dropDownParaleloEC.unselectIndex(0);
                this.dropDownSemestreEC.clearSelection();
                this.dropDownSemestreEC.unselectIndex(0);
                this.dropDownEstudiantesCo.clearSelection();
                this.dropDownEstudiantesCo.unselectIndex(0);
                this.sourceEstudiantes.localdata = [];
                this.dataAdapterEstudiantesCo.dataBind();
                this.idSemestreCo = null;
            }
        }
    };

    itemSemestreCoSele(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.idSemestreCo = item.value;
                this.dropDownParaleloEC.clearSelection();
                this.dropDownParaleloEC.unselectIndex(0);
            }
        }
    };

    /********************************************************************************************************************************** */
    /**************************************evento del combo paralelo estudiantesColaborador**************************************************************/

    /*ListarEstudiantesCo(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                if (this.idCarreraCo) {
                    if (this.idSemestreCo) {
                        this.ListarEstudianteCoCombo(5, this.idCarreraCo, this.idSemestreCo, item.value);
                    } else {
                        this.myModal.alertMessage({ title: 'Estudiante Colaborador', msg: 'Seleccione un Semestre!' });
                        this.dropDownParaleloEC.clearSelection();
                        this.dropDownParaleloEC.unselectIndex(0);
                    }
                } else {
                    this.myModal.alertMessage({ title: 'Estudiante Colaborador', msg: 'Seleccione una Carrera!' });
                    this.dropDownParaleloEC.clearSelection();
                    this.dropDownParaleloEC.unselectIndex(0);
                    this.dropDownSemestreEC.clearSelection();
                    this.dropDownSemestreEC.unselectIndex(0);
                    this.idSemestreCo = null;
                }
            }
        }
    }

    /********************************************************************************************************************************** */
    /**************************************EVENTOS PARA LLENAR EL COMBO ESTUDIANES COLABORADOR**************************************************************/

    /*ListarEstudianteCoCombo(idPerAcademico: any, idDepOferta: any, idSemestre: any, idParalelo: any) {
        //el parametro 0 es porque ese es para el id de estudiante
        this._sub = this.periodoConvocatoria.buscarEstudianteSemestre(idPerAcademico, idDepOferta, idSemestre, idParalelo, 0).subscribe(data => {
            console.log(JSON.stringify(data));
            this.sourceEstudiantes.localdata = data;
            this.dataAdapterEstudiantesCo.dataBind();
            this.estudiantesCoData = data;
            const names = data.map(arrayOfIdEstudiantesCo => arrayOfIdEstudiantesCo.id);
            console.log('creofinal ' + names)
        }, error => console.error("Error al llenar combo estudiante colaborador " + error));
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO ESTUDIANES COLABORADOR**************************************************************/

    /*sourceEstudiantes: any = {
        datatype: 'json',
        id: 'idEstudiante',
        localdata: [
            { name: 'idEstudiante', type: 'int' },
            { name: 'nombreCompleto', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
    dataAdapterEstudiantesCo: any = new jqx.dataAdapter(this.sourceEstudiantes);

    /*************************************************************************************************************** */
    /*****************evento del combo estudiantes COLABORADOR**********************************************************/

    /*estudianteCoSelect(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.estudianteCoSelec = item.value;
            }
        }
    };

    /********************************************************************************************************************************** */
    /*********************************crea el boton y el evento para agregar ESTUDIANES COORDINADOR AL GRIP*******************************************************************/

    /*rendertoolbarEC = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer2 = document.createElement('div');
        buttonContainer2.id = 'buttonContainer2';
        buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
        container.appendChild(buttonContainer2);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 100, value: 'Agregar', theme: 'darkblue' });
        addRowButton.addEventHandler('click', () => {
            if (this.estudianteCoSelec == undefined) {
                this.myModal.alertMessage({ title: 'Estudiantes Colaborador', msg: 'Seleccione un estudiante!' });
            } else {
                let gridEstudianteCoordinador = this.gridEstudianteCoordinador.getrows();
                let getSize = gridEstudianteCoordinador.length;
                this.gridEstudianteCoordinador.selectrow(getSize - 1);
                let arrayOfIdEstudiantesCo = JSON.parse(JSON.stringify(this.dropDownEstudiantesCo.getSelectedItem())).originalItem;
                if (getSize == 0) {
                    //Agrega una nueva fila
                    this.aggGripEstudianteProyectoCo(arrayOfIdEstudiantesCo);
                } else {
                    let data1 = gridEstudianteCoordinador.filter((dats) => { return dats.idEstudiante == this.dropDownEstudiantesCo.val() })
                    if (data1.length > 0) {
                        this.myModal.alertMessage({ title: 'Estudiantes Colaborador', msg: 'Estudiante ya ha sido agregado!' });
                        this.gridEstudianteCoordinador.clearselection();
                    } else {
                        this.aggGripEstudianteProyectoCo(arrayOfIdEstudiantesCo);
                    }
                }
            }
        })
    };
    /********************************************************************************************************************************** */
    /*********************************EVENTOS PARA EL BOTON AGG ESTUDIANES COLABORADOR AL GRIP*******************************************************************/

    /*aggGripEstudianteProyectoCo(idEstudiante: any) {
        let arrayOfIdEstudiantesCo = idEstudiante;
        let datarow = {};
        datarow['id'] = arrayOfIdEstudiantesCo.id;
        datarow['idProyectoEstudiante'] = arrayOfIdEstudiantesCo.idProyectoEstudiante;
        datarow['identificacion'] = arrayOfIdEstudiantesCo.identificacion;
        datarow['idNivel'] = arrayOfIdEstudiantesCo.idNivel;
        datarow['idEstudiante'] = arrayOfIdEstudiantesCo.idEstudiante;
        datarow['nombreCompleto'] = arrayOfIdEstudiantesCo.nombreCompleto;
        datarow['cursoSemestre'] = arrayOfIdEstudiantesCo.cursoSemestre;
        datarow['telefono'] = arrayOfIdEstudiantesCo.telefono;
        datarow['email'] = arrayOfIdEstudiantesCo.email;
        datarow['idTipoUsuarioProyecto'] = 3; //id 3 porque es estudiante colaborador
        this.gridEstudianteCoordinador.addrow(arrayOfIdEstudiantesCo.id, datarow);

        this.dropDownParaleloEC.clearSelection();
        this.dropDownParaleloEC.unselectIndex(0);
        this.dropDownSemestreEC.clearSelection();
        this.dropDownSemestreEC.unselectIndex(0);
        this.dropDownEstudiantesCo.clearSelection();
        this.dropDownEstudiantesCo.unselectIndex(0);
        this.sourceEstudiantes.localdata = [];
        this.dataAdapterEstudiantesCo.dataBind();
    }

    /*************************************************************************************************************** */
    /********************************************PARAMETROS DE LA GRILA DE ESTUDIANTES COLABORADOR******************************************************************* */

    /*sourceEstudianteCoordinador: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idNivel', type: 'int' },
            { name: 'idEstudiante', type: 'int' },
            { name: 'identificacion', type: 'string' },
            { name: 'nombreCompleto', type: 'string' },
            { name: 'cursoSemestre', type: 'string' },
            { name: 'email', type: 'date', format: 'd' },
            { name: 'telefono', type: 'string' },
            { name: 'idTipoUsuarioProyecto', type: 'int' },
        ]
    };
    dataAdapterEstudianteCoordinador: any = new jqx.dataAdapter(this.sourceEstudianteCoordinador);

    columnsEstudianteCoordinador: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idNivel', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idEstudiante', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idTipoUsuarioProyecto', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Identificación', datafield: 'identificacion', width: '15%' },
        { text: 'Nombres', datafield: 'nombreCompleto', width: '22%' },
        { text: 'Curso', datafield: 'cursoSemestre', width: '25%' },
        { text: 'Email', datafield: 'email', width: '20%' },
        { text: 'Celular', datafield: 'telefono', width: '10%' },
        {
            text: 'Quitar', columntype: 'button', width: '8%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                let idEstudiante = this.gridEstudianteCoordinador.getcellvalue(this.rowindex, 'idEstudiante');
                if (idEstudiante != null) {
                    this.proyectoEstudianteDelete.push(idEstudiante);
                    let id = this.gridEstudianteCoordinador.getcellvalue(this.rowindex, 'id');
                    this.gridEstudianteCoordinador.deleterow(id);
                } else {
                    let id = this.gridEstudianteCoordinador.getcellvalue(this.rowindex, 'id');
                    this.gridEstudianteCoordinador.deleterow(id);
                }
                this.rowindex = -1;
            },
        },
    ];

    /********************************************************************************************************************************** */
    /***********metodo para obtner el id de la fila seleccionada en la grilla de ESTUDIANTES COLABORADOR*************************************************************/

    /*RowselectEC(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
            console.log('this.rowindexEC ' + this.rowindex);
        }
    }

    /*************************************************************************************************************** */
    /*****************evento para eliminar proyecto_docente guardada en la bd**********************************************************/

    borrarProyectoEstudiante(idProyectoEstudiante: any) {
        this._sub = this.periodoConvocatoria.borrarProyectoEstudianteGrid(idProyectoEstudiante).subscribe(data => {
        }, error => console.error("Error al eliminar estudiante del proyecto " + error));
    }

    /*************************************************************************************************************** */
    /*****************EVENTOS DE LOS COMBOS CARRERAS Y SEMESTRE estudiantesPARTICIPANTES**********************************************************/

    itemCarreraPaSele(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.idCarreraPa = item.value;
                this.dropDownParaleloEP.clearSelection();
                this.dropDownParaleloEP.unselectIndex(0);
                this.dropDownSemestreEP.clearSelection();
                this.dropDownSemestreEP.unselectIndex(0);
                this.idSemestrePa = null;
            }
        }
    };

    itemSemestrePaSele(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.idSemestrePa = item.value;
                this.dropDownParaleloEP.clearSelection();
                this.dropDownParaleloEP.unselectIndex(0);
            }
        }
    };

    /********************************************************************************************************************************** */
    /**************************************evento del combo paralelo estudiantesPARTICIPANTES**************************************************************/

    ListarEstudiantesPa(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                if (this.idCarreraPa) {
                    if (this.idSemestrePa) {
                        this.ListarEstudiantePaCombo(5, this.idCarreraPa, this.idSemestrePa, item.value)
                    } else {
                        this.myModal.alertMessage({ title: 'Detalles de Proyectos', msg: 'Seleccione un Semestre!' });
                        this.dropDownParaleloEP.clearSelection();
                        this.dropDownParaleloEP.unselectIndex(0);
                    }
                } else {
                    this.myModal.alertMessage({ title: 'Detalles de Proyectos', msg: 'Seleccione una Carrera!' });
                    this.dropDownParaleloEP.clearSelection();
                    this.dropDownParaleloEP.unselectIndex(0);
                    this.dropDownSemestreEP.clearSelection();
                    this.dropDownSemestreEP.unselectIndex(0);
                    this.idSemestrePa = null;
                }
            }
        }
    }

    /********************************************************************************************************************************** */
    /**************************************EVENTOS PARA LLENAR EL COMBO ESTUDIANES PARTICIPANTES**************************************************************/

    ListarEstudiantePaCombo(idPerAcademico: any, idDepOferta: any, idSemestre: any, idParalelo: any) {
        this._sub = this.periodoConvocatoria.buscarEstudianteSemestre(idPerAcademico, idDepOferta, idSemestre, idParalelo, 0).subscribe(data => {
            //console.log(JSON.stringify(data));
            this.sourceEstudiantesPa.localdata = data;
            this.dataAdapterEstudiantesPa.dataBind();
            this.estudiantesPaData = data;
        }, error => console.error("Error al llenar combo estudiante participantes" + error));
    }

    /********************************************************************************************************************************** */
    /**************************************PARAMETROS PARA LLENAR EL COMBO ESTUDIANES PARTICIPANTES**************************************************************/

    sourceEstudiantesPa: any = {
        datatype: 'json',
        id: 'idEstudiante',
        localdata: [
            { name: 'idEstudiante', type: 'int' },
            { name: 'idNivel', type: 'int' },
            { name: 'idProyectoEstudiante', type: 'int' },
            { name: 'nombreCompleto', type: 'string' },
            { name: 'telefono', type: 'string' },
            { name: 'cursoSemestre', type: 'string' },
            { name: 'email', type: 'string' },
            { name: 'identificacion', type: 'string' },
            { name: 'estado', type: 'string' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE ESTUDIANTES PARTICIPANTES
    dataAdapterEstudiantesPa: any = new jqx.dataAdapter(this.sourceEstudiantesPa);

    /*************************************************************************************************************** */
    /*****************evento del combo estudiantesPARTICIPANTES**********************************************************/

    estudiantePaSelect(event: any): void {
        if (event.args) {
            let item = event.args.item;
            if (item) {
                this.estudiantePaSelec = item.value;
            }
        }
    };

    /*************************************************************************************************************** */
    /*****************crea el boton y el evento para agregar estudiantesPARTICIPANTES a la grila**********************************************************/

    rendertoolbarEP = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer3 = document.createElement('div');
        buttonContainer3.id = 'buttonContainer3';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
        container.appendChild(buttonContainer3);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 100, value: 'Agregar', theme: 'darkblue' });
        addRowButton.addEventHandler('click', () => {
            if (this.estudiantePaSelec == undefined) {
                this.myModal.alertMessage({ title: 'Estudiantes Participantes', msg: 'Seleccione un estudiante!' });
            } else {
                let gridEstudianteParticipantes = this.gridEstudianteParticipantes.getrows();
                let getSize = gridEstudianteParticipantes.length;
                this.gridEstudianteParticipantes.selectrow(getSize - 1);
                let arrayOfEstudiantePa: any = []
                arrayOfEstudiantePa = this.dropDownEstudiantesPa.getCheckedItems();
                let arrayOfEstudiantePa1 = arrayOfEstudiantePa.map(arrayOfIdEstudiantesPa => arrayOfIdEstudiantesPa.originalItem);
                if (getSize == 0) {
                    //Agrega una nueva fila
                    this.aggGripEstudianteProyectoPa(arrayOfEstudiantePa1);
                    this.limpiarCombosEstudiantes();
                } else {
                    let data1 = gridEstudianteParticipantes.filter((dats) => { return dats.idEstudiante == this.dropDownEstudiantesPa.val() })
                    if (data1.length > 0) {
                        this.myModal.alertMessage({ title: 'Estudiantes Participantes', msg: 'Estudiante ya ha sido agregado!' });
                        this.gridEstudianteParticipantes.clearselection();
                    } else {
                        this.aggGripEstudianteProyectoPa(arrayOfEstudiantePa1);
                        this.limpiarCombosEstudiantes();
                    }
                }
            }
        })
    };

    /********************************************************************************************************************************** */
    /*********************************EVENTOS PARA EL BOTON AGG ESTUDIANES PARTICIPANTES AL GRIP*******************************************************************/

    aggGripEstudianteProyectoPa(idEstudiante: any) {
        //this.arrayOfIdEstudiantesPa = this.dropDownEstudiantesPa.getCheckedItems();
        this.arrayOfIdEstudiantesPa = idEstudiante;
        for (let index = 0; index < this.arrayOfIdEstudiantesPa.length; index++) {
            let datarow = {};
            if (this.arrayOfIdEstudiantesPa[index].idTipoUsuarioProyecto == undefined) {
                this.arrayOfIdEstudiantesPa[index].idTipoUsuarioProyecto = 4;
                this.arrayOfIdEstudiantesPa[index].descripcion = 'Participante';
            }
            datarow['id'] = this.arrayOfIdEstudiantesPa[index].id;
            datarow['idProyectoEstudiante'] = this.arrayOfIdEstudiantesPa[index].idProyectoEstudiante;
            datarow['identificacion'] = this.arrayOfIdEstudiantesPa[index].identificacion;
            datarow['idNivel'] = this.arrayOfIdEstudiantesPa[index].idNivel;
            datarow['idEstudiante'] = this.arrayOfIdEstudiantesPa[index].idEstudiante;
            datarow['nombreCompleto'] = this.arrayOfIdEstudiantesPa[index].nombreCompleto;
            datarow['idTipoUsuarioProyecto'] = this.arrayOfIdEstudiantesPa[index].idTipoUsuarioProyecto; //id 4 porque es estudiante participante
            datarow['descripcion'] = this.arrayOfIdEstudiantesPa[index].descripcion;
            this.gridEstudianteParticipantes.addrow(this.arrayOfIdEstudiantesPa[index].id, datarow);
        }
        //this.arrayOfIdEstudiantesPa = null;

    }

    limpiarCombosEstudiantes() {
        this.dropDownParaleloEP.clearSelection();
        this.dropDownParaleloEP.unselectIndex(0);
        this.dropDownSemestreEP.clearSelection();
        this.dropDownSemestreEP.unselectIndex(0);
        this.dropDownEstudiantesPa.clearSelection();
        this.dropDownEstudiantesPa.unselectIndex(0);
        this.sourceEstudiantesPa.localdata = [];
        this.dataAdapterEstudiantesPa.dataBind();
    }

    /*************************************************************************************************************** */
    /********************************************PARAMETROS DE LA GRILA DE ESTUDIANTES PARTTICIPANTES******************************************************************* */

    sourceEstudianteParticipantes: any = {
        datatype: 'array',
        id: 'id',
        datafields: [
            { name: 'id', type: 'int' },
            { name: 'idProyectoEstudiante', type: 'int' },
            { name: 'idNivel', type: 'int' },
            { name: 'idEstudiante', type: 'int' },
            { name: 'identificacion', type: 'string' },
            { name: 'nombreCompleto', type: 'string' },
            { name: 'idTipoUsuarioProyecto', type: 'int' },
            { name: 'descripcion', type: 'string' },
        ]
    };
    dataAdapterEstudianteParticipantes: any = new jqx.dataAdapter(this.sourceEstudianteParticipantes);

    columnsEstudianteParticipantes: any[] = [
        { text: 'Id', datafield: 'id', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idNivel', width: '2%', filtertype: 'none', hidden: true },
        { text: 'IdProyectoEstudiante', datafield: 'idProyectoEstudiante', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Id', datafield: 'idEstudiante', width: '2%', filtertype: 'none', hidden: true },
        { text: 'Identificación', datafield: 'identificacion', width: '15%' },
        { text: 'Nombres', datafield: 'nombreCompleto', width: '42%' },
        {
            text: 'Tipo Usuario', datafield: 'idTipoUsuarioProyecto', displayfield: 'descripcion', width: '20%', cellsalign: 'left', center: 'center', columntype: 'dropdownlist',
            enableHover: true,
            initeditor: (row: number, _value: any, editor: any): void => {
                editor.jqxDropDownList({
                    autoOpen: true, autoDropDownHeight: false, source: this.dataTipoUsuarioParticipantes, displayMember: 'descripcion',
                    valueMember: 'idTipoUsuarioProyecto', animationType: 'slide', enableHover: true, autoItemsHeight: true, theme: 'ui-redmond',
                });
            },
            cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                this.gridEstudianteParticipantes.hiderowdetails(row);
            },
        },
        {
            text: 'Detalles', columntype: 'button', width: '15%',
            cellsrenderer: (): string => {
                return 'Detalles';
            },
            buttonclick: (row: number): void => {
                let idEstudiante = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'idEstudiante');
                let idNivel = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'idNivel');
                if (idEstudiante != null) {
                    //5 es el id de periodo academico, se debe realizar una funcion el cual obtenga el id del periodo academico vigente
                    this.detalleParticipante.mostrarDetalleEstudianteParticipantes('5',idNivel, idEstudiante);
                    /*this.proyectoEstudianteDelete.push(idProyectoEstudiante);
                    let id = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'id');
                    this.gridEstudianteParticipantes.deleterow(id);
                } else {
                    let id = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'id');
                    this.gridEstudianteParticipantes.deleterow(id);*/
                }
                this.rowindex = -1;
            },
        },
        {
            text: 'Quitar', columntype: 'button', width: '8%',
            cellsrenderer: (): string => {
                return 'Quitar';
            },
            buttonclick: (row: number): void => {
                let idProyectoEstudiante = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'idProyectoEstudiante');
                if (idProyectoEstudiante != null) {
                    this.proyectoEstudianteDelete.push(idProyectoEstudiante);
                    let id = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'id');
                    this.gridEstudianteParticipantes.deleterow(id);
                } else {
                    let id = this.gridEstudianteParticipantes.getcellvalue(this.rowindex, 'id');
                    this.gridEstudianteParticipantes.deleterow(id);
                }
                this.rowindex = -1;
            },
        },
    ];
    /*************************************************************************************************************** */
    /**************************************EVENTOS DE COMBO TIPO PARTICIPANTE, LISTA TIPO PARTICIPANTE**************************************************************/
    dataTipoParticipante =
        [
            {
                'idTipoUsuarioProyecto': '3',
                'descripcion': 'Coordinador'
            },
            {
                'idTipoUsuarioProyecto': '4',
                'descripcion': 'Participante'
            }
        ]
    sourceEstaudiantesParticipantes: any =
        {
            datatype: 'json',
            id: 'idTipoUsuarioProyecto',
            datafields:
                [
                    { name: 'idTipoUsuarioProyecto', type: 'int' },
                    { name: 'descripcion', type: 'string' },
                ],
            localdata: this.dataTipoParticipante
        };
    //adaptador para enlace de datos del grid
    dataTipoUsuarioParticipantes: any = new jqx.dataAdapter(this.sourceEstaudiantesParticipantes);

    /********************************************************************************************************************************** */
    /***********metodo para obtner el id de la fila seleccionada en la grilla de ESTUDIANTES PARTTICIPANTES*************************************************************/

    RowselectEP(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
            console.log('this.rowindex ' + this.rowindex);
        }
    }

    /*************************************************************************************************************** */
    /*************************************************limpia los combos que tienen check************************************************************** */

    limparCombos() {
        this.sourceEstudiantesPa.localdata = [];
        this.dataAdapterEstudiantesPa.dataBind();
    }

    seccionAsigPersonal() {
        let validar = true;
        let gridDocenteParticipantes = this.gridDocenteParticipantes.getrows();
        let getSize = gridDocenteParticipantes.length;
        if (getSize == 0) {
            //Agrega una nueva fila
            this.myModal.alertMessage({ title: 'Docentes participantes', msg: 'Ingrese docentes participantes!' });
            validar = false;
        }
        if (this.proyectoDocenteDelete.length > 0 && this.proyectoDocenteDelete != undefined) {
            for (let index = 0; index < this.proyectoDocenteDelete.length; index++) {
                console.log('idProyectodoente' + this.proyectoDocenteDelete[index]);
                this.borrarProyectoDocente(this.proyectoDocenteDelete[index]);
            }
            validar = true;
        }

        /*let gridEstudianteCoordinador = this.gridEstudianteCoordinador.getrows();
        let getSize1 = gridEstudianteCoordinador.length;
        if (getSize1 == 0) {
            //Agrega una nueva fila
            this.myModal.alertMessage({ title: 'Estudiante coordinador', msg: 'Ingrese estudiante coordinador!' });
            validar = false;
        }*/
        if (this.proyectoEstudianteDelete.length > 0 && this.proyectoEstudianteDelete != undefined) {
            for (let index = 0; index < this.proyectoEstudianteDelete.length; index++) {
                console.log('idProyectoEstCoord' + this.proyectoEstudianteDelete[index]);
                this.borrarProyectoEstudiante(this.proyectoEstudianteDelete[index]);
            }
            validar = true;
        }

        let gridEstudianteParticipantes = this.gridEstudianteParticipantes.getrows();
        let getSize2 = gridEstudianteParticipantes.length;
        if (getSize2 == 0) {
            //Agrega una nueva fila
            this.myModal.alertMessage({ title: 'Estudiante participante', msg: 'Ingrese estudiante participante!' });
            validar = false;
        }
        let getSize3 = gridEstudianteParticipantes.length;
        let ban = 0;
        if (getSize3 > 0) {
            for (let index = 0; index < gridEstudianteParticipantes.length; index++) {
                if (gridEstudianteParticipantes[index].idTipoUsuarioProyecto == 3) {
                    ban = ban + 1;
                }
            }
            if (ban > 1) {
                this.myModal.alertMessage({ title: 'Estudiantes Participantes', msg: 'Mas de un estudiante coordinador seleccionado!' });
                validar = false;
            }
            if (ban == 0) {
                this.myModal.alertMessage({ title: 'Estudiantes Participantes', msg: 'Debe ingresar un estudiante coordinador!' });
                validar = false;
            }
        }
        return validar;
    }

    formarJsonAsignarPersonal() {
        let datosProyecto: any = {};
        let arrDocenteParticipantes: any = [];
        let objDocenteParticipantes: any = {};
        let objProyectoEstudiante: any = {};
        let arrProyectoEstudiante: any = [];

        //proyecto docente
        let gridDocenteParticipantes = this.gridDocenteParticipantes.getrows();
        let getSize1 = gridDocenteParticipantes.length;
        if (getSize1 > 0) {
            for (let index = 0; index < gridDocenteParticipantes.length; index++) {
                objDocenteParticipantes = {};
                objDocenteParticipantes.id = gridDocenteParticipantes[index].idProyectoDocente;
                objDocenteParticipantes.idDocente = gridDocenteParticipantes[index].idDocente;
                objDocenteParticipantes.idTipoUsuarioProyecto = gridDocenteParticipantes[index].idTipoUsuarioProyecto;
                objDocenteParticipantes.usuarioIngresoId = this.usuarioActual.id;
                objDocenteParticipantes.estado = 'A';
                arrDocenteParticipantes.push(objDocenteParticipantes);
            }
        }

        /*let gridDirectorProyecto = this.gridDirectorProyecto.getrows();
        let getSize2 = gridDirectorProyecto.length;
        if (getSize2 > 0) {
            for (let index = 0; index < gridDirectorProyecto.length; index++) {
                objDocenteParticipantes = {};
                objDocenteParticipantes.id = gridDirectorProyecto[index].idProyectoDocente;
                objDocenteParticipantes.idDocente = gridDirectorProyecto[index].idDocente;
                objDocenteParticipantes.idTipoUsuarioProyecto = gridDirectorProyecto[index].idTipoUsuarioProyecto;
                objDocenteParticipantes.usuarioIngresoId = this.usuarioActual.id;
                objDocenteParticipantes.estado = 'A';
                arrDocenteParticipantes.push(objDocenteParticipantes);
            }
        }*/

        //proyecto estudiante
        let gridEstudianteParticipantes = this.gridEstudianteParticipantes.getrows();
        let getSize3 = gridEstudianteParticipantes.length;
        if (getSize3 > 0) {
            for (let index = 0; index < gridEstudianteParticipantes.length; index++) {
                objProyectoEstudiante = {};
                objProyectoEstudiante.id = gridEstudianteParticipantes[index].idProyectoEstudiante;
                objProyectoEstudiante.idEstudiante = gridEstudianteParticipantes[index].idEstudiante;
                objProyectoEstudiante.idNivel = gridEstudianteParticipantes[index].idNivel;
                objProyectoEstudiante.idTipoUsuarioProyecto = gridEstudianteParticipantes[index].idTipoUsuarioProyecto;
                objProyectoEstudiante.usuarioIngresoId = this.usuarioActual.id;
                objProyectoEstudiante.estado = 'A';
                arrProyectoEstudiante.push(objProyectoEstudiante);
            }
        }
        /*let gridEstudianteCoordinador = this.gridEstudianteCoordinador.getrows();
        let getSize4 = gridEstudianteCoordinador.length;
        if (getSize4 > 0) {
            for (let index = 0; index < gridEstudianteCoordinador.length; index++) {
                objProyectoEstudiante = {};
                objProyectoEstudiante.id = gridEstudianteCoordinador[index].idProyectoEstudiante;
                objProyectoEstudiante.idEstudiante = gridEstudianteCoordinador[index].idEstudiante;
                objProyectoEstudiante.idNivel = gridEstudianteCoordinador[index].idNivel;
                objProyectoEstudiante.idTipoUsuarioProyecto = gridEstudianteCoordinador[index].idTipoUsuarioProyecto;
                objProyectoEstudiante.usuarioIngresoId = this.usuarioActual.id;
                objProyectoEstudiante.estado = 'A';
                arrProyectoEstudiante.push(objProyectoEstudiante);
            }
        }*/
        datosProyecto.proyectoDocente = arrDocenteParticipantes;
        datosProyecto.proyectoEstudiante = arrProyectoEstudiante;
        //json
        return datosProyecto;
    }

}