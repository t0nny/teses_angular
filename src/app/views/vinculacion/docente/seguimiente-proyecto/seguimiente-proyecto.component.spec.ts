import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimienteProyectoComponent } from './seguimiente-proyecto.component';

describe('SeguimienteProyectoComponent', () => {
  let component: SeguimienteProyectoComponent;
  let fixture: ComponentFixture<SeguimienteProyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimienteProyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimienteProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
