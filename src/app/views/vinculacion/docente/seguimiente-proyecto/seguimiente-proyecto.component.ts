import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';
import { _ParseAST } from '@angular/compiler';
import { TabsComponent } from './plantilla-tab-dinamica/tabs.component';
import { DatosGeneralesComponent } from './secciones-tab/datos-generales/datos-generales.component';
import { AsignarPersonalComponent } from './secciones-tab/asignar-personal/asignar-personal.component';
import { InstitucionComponent } from './secciones-tab/institucion/institucion.component';
import { FileService } from '../../../../services/file/file.service';
import { BeneficiariosComponent } from './secciones-tab/beneficiarios/beneficiarios.component';
import { MarcoLogicoComponent } from './secciones-tab/marco-logico/marco-logico.component';
import { ModalProyectoComponent } from '../modal-proyecto/modal-proyecto.component';

@Component({
  selector: 'app-seguimiente-proyecto',
  templateUrl: './seguimiente-proyecto.component.html',
  styleUrls: ['./seguimiente-proyecto.component.scss']
})
export class SeguimienteProyectoComponent implements OnInit {

  @ViewChild(DatosGeneralesComponent, { static: false }) datosGenerales: DatosGeneralesComponent;
  @ViewChild(AsignarPersonalComponent) asignarPersonal: AsignarPersonalComponent;
  @ViewChild(InstitucionComponent) institucion: InstitucionComponent;
  @ViewChild(BeneficiariosComponent) beneficiarios: BeneficiariosComponent;
  @ViewChild(MarcoLogicoComponent) marcoLogico: MarcoLogicoComponent;

  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

  @ViewChild(ModalProyectoComponent) formModalProyecto: ModalProyectoComponent;
  //tabs dinamico masters Santy
  @ViewChild('DatosGenerales') datoGeneralesSeccion1;
  @ViewChild('Beneficiarios') beneficiariosSeccion2;
  @ViewChild('AsignarPersonal') asignarPersonalSeccion3;
  @ViewChild('Institucion') institucionSeccion4;
  @ViewChild('MarcoLogico') marcoLogicoSeccion5;
  @ViewChild(TabsComponent) tabsComponent;

  //Variable paa cargar datos del objeto 
  _sub: Subscription;
  //variables para comunicar seguimientos de proyectos con la seccion datos generales
  proyecto: any = {};
  proyectoVersion: any = {};
  proyectoPresupuesto: any = {};
  proyectoAreaGeografica: any = {};
  proyectoAsignatura: any = {};
  dataSemestre: any = {};
  //variables para comunicar la seguimientos de proyectos con la seccion beneficiarios
  proyectoPersona: any = {};
  institucionBeneficiaria: any = [];
  //variables para comunicar la seguimientos de proyectos con la seccion institucion
  proyectoLineasInv: any = {}; //para llenar la LI que ya tiene el proyecto
  proyectoDominiosAcad: any = {};
  proyectoAreaUns: any = {};

  idDepOferta: any;
  idProyecto: any;

  constructor(private route: ActivatedRoute, private router: Router,
    private periodoConvocatoria: VinculacionService, private fileArchivo: FileService) {
  }

  ngOnInit() {
    this.cargarSemestres();
    setTimeout(() => {
      this.tabsComponent.openTab('Datos Generales', this.datoGeneralesSeccion1, {}, true);
      this.tabsComponent.openTab('Beneficiarios', this.beneficiariosSeccion2, {}, true);
      this.tabsComponent.openTab('AsignarPersonal', this.asignarPersonalSeccion3, {}, true);
      this.tabsComponent.openTab('Institucion', this.institucionSeccion4, {}, true);
      this.tabsComponent.openTab('Marco Lógico', this.marcoLogicoSeccion5, {}, true);
      this.tabsComponent.activeTab();
    });
   
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.getProyectoById();
    });
  }

  getProyectoById() {
    this._sub = this.route.params.subscribe(valor => {
      //RECUPERAR EL VALOR DEL PARAMETRO PASADO EN LA RUTA
      this.idProyecto = valor['idProyecto'];//ID DE PROYECTO
      let idDepartamento = valor['idDepartamento'];//ID DE DEPARTAMENTO
      this.idDepOferta = valor['idDepOferta'];//ID DE DEPARTAMENTO_OFERTA
      console.log("idPr " + this.idProyecto + " " + idDepartamento + " " + this.idDepOferta);
      this.listarLineasInvestigacion();
      //EVALUA SI EL PARAMETRO ID SE PASO
      if ((this.idProyecto != null)) {
        //TRATA DE OBTENER LA ASIGNATURA CON EL ID DADO
        this._sub = this.periodoConvocatoria.buscarProyectoPorId(this.idProyecto).subscribe((informacion: any) => {
          if (informacion) {
            if (informacion.estado == 'A') {
              console.log("proe " + JSON.stringify(informacion));
              this.editaProyecto(informacion);
            } else {
              this.myModal.alertNext({
                title: 'Advertencia',
                msg: 'No se encontro la información solicitada.',
                result: (p) => {
                  if (p) {
                    this.gotoList();
                  }
                }
              })
            }
          } else {
            this.myModal.alertNext({
              title: 'Advertencia',
              msg: 'No se pudo recuperar la información solicitada, contactese con el administrador del sistema.',
              result: (p) => {
                if (p) {
                  this.gotoList();
                }
              }
            })
          }
        })
      }
    });
  }

  /*************************************************************************************************************** */
  /*****************Carga datos del proyect**********************************************************/

  editaProyecto(informacion: any) {
    this.proyecto = {};//esto indica que estoy vaciando la variable asignaturas
    this.proyecto.id = informacion.id;
    this.proyecto.idPrograma = informacion.idPrograma;
    this.proyecto.idProyectoConvocatoria = informacion.idProyectoConvocatoria;
    this.proyecto.titulo = informacion.titulo;
    this.proyecto.descripcion = informacion.descripcion;
    this.proyecto.codigo = informacion.codigo;
    this.proyecto.estadoProyecto = informacion.estadoProyecto;
    this.proyecto.fechaDesde = informacion.fechaDesde;
    this.marcoLogico.date= new jqx.date(new Date(this.proyecto.fechaDesde));//establece la fecha de inicio en el scheluder
    this.proyecto.fechaHasta = informacion.fechaHasta;
    if (informacion.proyectoDocente[0] != undefined) {
      this.proyecto.proyectoDocente = [];
      /*let DOCENTE_DIRECTOR = informacion.proyectoDocente.filter((dats) => { return dats.idTipoUsuarioProyecto == 1 && dats.estado == 'A' })
      let DOCENTE_PARTICIPANTE = informacion.proyectoDocente.filter((dats) => { return dats.idTipoUsuarioProyecto == 2 && dats.estado == 'A' })
      if (DOCENTE_DIRECTOR.length > 0) {
        for (let index = 0; index < DOCENTE_DIRECTOR.length; index++) {
          this.asignarPersonal.procesaPassDirectorProyecto(DOCENTE_DIRECTOR[index].idDocente);
        }
      }
      if (DOCENTE_PARTICIPANTE.length > 0) {
        for (let index = 0; index < DOCENTE_PARTICIPANTE.length; index++) {
          this.asignarPersonal.procesaPassGripDocentesProyecto(DOCENTE_PARTICIPANTE[index].idDocente);
        }
      }*/
      this.asignarPersonal.procesaPassGripDocentesParticipantes(this.idProyecto);
    }

    if (informacion.proyectoEstudiante[0] != undefined) {
      this.proyecto.proyectoEstudiante = [];
      let ESTUDIANTE_COODINADOR = informacion.proyectoEstudiante.filter((dats) => { return dats.idTipoUsuarioProyecto == 3 && dats.estado == 'A' })
      let ESTUDIANTE_PARTICIPANTE = informacion.proyectoEstudiante.filter((dats) => { return dats.idTipoUsuarioProyecto == 4 && dats.estado == 'A' })
      /*if (ESTUDIANTE_COODINADOR.length > 0) {
        for (let index = 0; index < ESTUDIANTE_COODINADOR.length; index++) {
          this._sub = this.periodoConvocatoria.buscarEstudianteSemestre('5', '0', ESTUDIANTE_COODINADOR[index].idNivel, '0', ESTUDIANTE_COODINADOR[index].idEstudiante).subscribe(data => {
            console.log(JSON.stringify(data));
            this.proyecto.proyectoEstudiante.push(ESTUDIANTE_COODINADOR[index]);
            this.asignarPersonal.procesaPassGripEstudianteProyectoCo(data[0]);
          }, error => console.error("Error al llenar grid estudiante colaborador " + error));
        }
      }
      if (ESTUDIANTE_PARTICIPANTE.length > 0) {
        for (let index = 0; index < ESTUDIANTE_PARTICIPANTE.length; index++) {
          this._sub = this.periodoConvocatoria.buscarEstudianteSemestre('5', '0', ESTUDIANTE_PARTICIPANTE[index].idNivel, '0', ESTUDIANTE_PARTICIPANTE[index].idEstudiante).subscribe(data => {
            console.log(JSON.stringify(data));
            this.proyecto.proyectoEstudiante.push(ESTUDIANTE_PARTICIPANTE[index]);
            this.asignarPersonal.procesaPassGripEstudianteProyectoPa(data);
          }, error => console.error("Error al llenar grid estudiante participante " + error));
        }
      }*/
      this._sub = this.periodoConvocatoria.estudianteParticipantesProyecto(this.idProyecto).subscribe(data => {
        console.log('estudianteParticipantesProyecto'+JSON.stringify(data));
        this.asignarPersonal.procesaPassGripEstudianteProyectoPa(data);
      }, error => console.error("Error al llenar grid estudiante participante " + error));
    }

    if (informacion.proyectoVersion[0] != undefined) {
      this.proyectoVersion = informacion.proyectoVersion[0];
    }

    if (informacion.proyectoPresupuesto[0] != undefined) {
      this.proyectoPresupuesto = informacion.proyectoPresupuesto[0];
    }

    if (informacion.institucionBeneficiaria[0] != undefined) {
      this.institucionBeneficiaria = informacion.institucionBeneficiaria[0];
      //llenar seccion beneficiarios
      if (informacion.institucionBeneficiaria[0].proyectoAreaGeografica != undefined) {
        this.proyectoAreaGeografica = informacion.institucionBeneficiaria[0].proyectoAreaGeografica;
        this.datosGenerales.procesaPassCombosAreaGeo(informacion.institucionBeneficiaria[0].proyectoAreaGeografica.idAreaGeografica)
        this.datosGenerales.procesaPassCheckBoxParroquia(informacion.institucionBeneficiaria[0].proyectoAreaGeografica.barrioComuna)
      }
      if (informacion.institucionBeneficiaria[0].proyectoPersona != undefined) {
        this.proyectoPersona = informacion.institucionBeneficiaria[0].proyectoPersona;
      }
    }

    if (informacion.proyectoAsignatura[0] != undefined) {
      this.proyectoAsignatura = informacion.proyectoAsignatura[0];
      let PROYECTO_ASIGNATURA = informacion.proyectoAsignatura.filter((dats) => { return dats.estado == 'A' });
      if (PROYECTO_ASIGNATURA.length > 0) {
        this.datosGenerales.procesaPassGridAsignaturas(this.idProyecto)
      }
    }

    if (informacion.proyectoAreaUnesco[0] != undefined) {
      this.proyectoAreaUns = informacion.proyectoAreaUnesco[0];
      this.institucion.procesaPassCombosUnesco(informacion.proyectoAreaUnesco[0].idAreaUnesco)
    }

    if (informacion.proyectoDominiosAcademicos[0] != undefined) {
      this.proyectoDominiosAcad = informacion.proyectoDominiosAcademicos[0];
      this.institucion.procesaPassDropDownDominioAcad(informacion.proyectoDominiosAcademicos[0].idDominiosAcademicos)
    }

    if (informacion.proyectoLineasInv[0] != undefined) {
      this.proyectoLineasInv = informacion.proyectoLineasInv[0];
      this.institucion.procesaPassCombosLineasInv(informacion.proyectoLineasInv[0].idSubLineaInv)
    }

    this.marcoLogico.ListarGridComponentes(this.idProyecto);
  }

  /********************************************************************************************************************************** */
  /*********************************Cargar combo semestres*******************************************************************/

  cargarSemestres() {
    this._sub = this.periodoConvocatoria.listarNiveles().subscribe(data => {
      this.dataSemestre = data;
    }, error => console.error("Error al obtener los semestres " + error));
  }

  /********************************************************************************************************************************** */
  /**************************************SERVICIOS PARA LLENAR COMBOS**************************************************************/

  listarLineasInvestigacion() {
    this._sub = this.periodoConvocatoria.lineasYSubLineasInv(this.idDepOferta).subscribe(data => {
      this.institucion.procesaPasslistarLineasInvestigacion(data);
    });
  }

  /*************************************************************************************************************** */
  /*************************************************jqxMenu************************************************************** */

  //datos de jqxMenu
  data = [
    {
      'id': '1',
      'text': 'Grabar',
      'parentid': '-1',
      'subMenuWidth': '25%'
    },
    {
      'text': 'Cancelar',
      'id': '2',
      'parentid': '-1',
      'subMenuWidth': '25%'
    },
  ];
  // prepare the data
  source1 = {
    datatype: 'json',
    datafields: [
      { name: 'id' },
      { name: 'parentid' },
      { name: 'text' },
      { name: 'subMenuWidth' }
    ],
    id: 'id',
    localdata: this.data
  };

  getAdapter(source1: any): any {
    // create data adapter and perform data
    return new jqx.dataAdapter(this.source1, { autoBind: true });
  };

  menus = this.getAdapter(this.source1).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
  /*************************************************************************************************************** */
  /*************************************************evento del jqxMenu************************************************************** */

  itemclick(event: any): void {
    var opt = event.args.innerText;
    switch (opt) {
      case 'Grabar':
        if (this.allValidation()) {
          console.log('graabr')
          let datosProyecto = this.formarJsonProyecto();
          let fileProyecto= this.datosGenerales.enviarFileProyecto();

          this.guardar(datosProyecto, fileProyecto);
           
          
        } else {
          console.log('error al validar los datos')
        }
        break;
      case 'Cancelar':
        this.gotoList();
        break;
      default:
      //default code block
    }
  }
  procesarSioNo(event:any){
    if(event.val=='Si'){
      console.log('si')
      this._sub = this.periodoConvocatoria.updateEstadoProyecto(this.idProyecto).subscribe(data => {
        //console.log("titulo "+ JSON.stringify(data));
        this.myModal.alertMessage({ title: 'Proyecto', msg: 'El proyecto se ha guardado y enviado al Departamento de Vinculación!' });
        this.gotoList();  
      }, error => console.error("h " + error));
      
    }
    if(event.val=='No'){
      console.log('no')
      localStorage.setItem('detalles', 'true')
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'El proyecto se ha guardado pero no ha sido enviado al Departamento de Vinculación!' });
      this.gotoList();
    }
  }

  allValidation() {
    let bam = false;
    //validaciones de la seccion datos generales
    //proyecto
    bam =this.datosGenerales.funValidar();
    if (this.datosGenerales.funValidar()) {
      console.log('seccion datos generales validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //area geografica
    bam =this.datosGenerales.seccionAreaGeografica();
    if (this.datosGenerales.seccionAreaGeografica()) {
      console.log('seccion area geografica validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //area geografica
    bam =this.datosGenerales.seccionAsignaturasProycto();
    if (this.datosGenerales.seccionAsignaturasProycto()) {
      console.log('seccion asignaturas proyectos validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //validaciones de la seccion beneficiarios
    //organizacion 
    bam =this.beneficiarios.seccionOrganizacionBeneficiaria();
    if (this.beneficiarios.seccionOrganizacionBeneficiaria()) {
      console.log('seccion organizacion beneficiaria validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //representante legal
    bam =this.beneficiarios.seccionRepresententeLegal();
    if (this.beneficiarios.seccionRepresententeLegal()) {
      console.log('seccion Representente Legal validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //validaciones de la seccion Asignar personal
    bam =this.asignarPersonal.seccionAsigPersonal();
    if (this.asignarPersonal.seccionAsigPersonal()) {
      console.log('seccion asignacion de peersonal validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //validaciones de la seccion institucion
    bam =this.institucion.seccionInstitucion();
    if (this.institucion.seccionInstitucion()) {
      console.log('seccion Institucion validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    //validaciones de la seccion marco logico
    bam =this.marcoLogico.seccionMarcoLogico();
    if (this.marcoLogico.seccionMarcoLogico()) {
      console.log('seccion Marco Lógico validadas corectamente')
      bam = true;
    }else{
      return bam
    }
    return bam;
  }

  formarJsonProyecto() {
    let datosProyecto: any = this.datosGenerales.formarJsonDatosGenerales();
    let datosAsignarPersonal: any = this.asignarPersonal.formarJsonAsignarPersonal();
    let datosInstitucion: any = this.institucion.formarJsonInstitucion();

    datosProyecto.proyectoDocente = datosAsignarPersonal.proyectoDocente;
    datosProyecto.proyectoEstudiante = datosAsignarPersonal.proyectoEstudiante;
    datosProyecto.proyectoDominiosAcademicos = datosInstitucion.proyectoDominiosAcademicos;
    datosProyecto.proyectoLineasInv = datosInstitucion.proyectoLineasInv;
    datosProyecto.proyectoAreaUnesco = datosInstitucion.proyectoAreaUnesco;
    return datosProyecto;
  }

  guardar(datosProyecto: any, fileSelecionado: any) {
    try {
      var err: boolean = false;
      if (fileSelecionado != '') {
        this._sub = this.fileArchivo.pushFileToStoragePP(fileSelecionado).subscribe(result => {
          console.log('resuk ' + result);
        }, error => {
          console.error("Error al subir archivo al servidor " + error);
          this.myModal.alertMessage({ title: 'Proyecto', msg: 'Error al subir archivo al servidor!' });
          err = true;
        });
      }
      if (err == false) {
        this._sub = this.periodoConvocatoria.grabarProyecto(datosProyecto).subscribe(result => {
          this.myModal.alertMessage({ title: 'Proyecto', msg: 'Datos Guardados Correctamente!' });
          this.formModalProyecto.questionSioNo({ title: 'Proyecto', msg: '¿Desea enviar el proyecto al Departamento de Vinculación?' });         
        }, error => {
          console.error("h " + error)
          this.myModal.alertMessage({ title: 'Proyecto', msg: 'Error al grabar proyecto!' });
        });
      }
    }
    catch (error) {
      console.error('Here is the error message', error);
    }
  }

  gotoList(): void {
    this.router.navigate(['vinculacion/docente/registro-proyecto']);
  }
}
