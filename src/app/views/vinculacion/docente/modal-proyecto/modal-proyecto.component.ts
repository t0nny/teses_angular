import { Component, OnInit, ViewChild, Output, EventEmitter, Input, ElementRef } from '@angular/core';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';
import { ModalDirective, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { jqxValidatorComponent } from 'jqwidgets-ng/jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxInputComponent } from 'jqwidgets-ng/jqxinput';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { jqxTextAreaComponent } from 'jqwidgets-ng/jqxtextarea';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { FileService } from '../../../../services/file/file.service';
import { AccountService } from '../../../../services/auth/account.service';

@Component({
  selector: 'app-modal-proyecto',
  templateUrl: './modal-proyecto.component.html',
  styleUrls: ['./modal-proyecto.component.scss']
})
export class ModalProyectoComponent implements OnInit {

  @ViewChild(ModalComponentComponent) modalComponent: ModalComponentComponent;
  @ViewChild('template') private template: ModalDirective;
  @ViewChild('templateNuevaVersion') private templateNuevaVersion: ModalDirective;
  @ViewChild('templateSioNo') private templateSioNo: ModalDirective;
  @ViewChild('formNuevo') private modalForm: ModalDirective;
  @ViewChild('dateTimeInput') dateTimeInput: jqxDateTimeInputComponent;
  @ViewChild('myValidator') myValidator: jqxValidatorComponent;
  @ViewChild('dropDownPrograma') dropDownPrograma: jqxDropDownListComponent;
  @ViewChild('dropDownCarrera') dropDownCarrera: jqxDropDownListComponent;
  @ViewChild('checkBoxPrograma') checkBoxPrograma: jqxCheckBoxComponent;
  @ViewChild('myHabFechas') myHabFechas: jqxCheckBoxComponent;
  @ViewChild('myfechaDesde') myfechaDesde: jqxDateTimeInputComponent;
  @ViewChild('myfechaHasta') myfechaHasta: jqxDateTimeInputComponent;
  @ViewChild('myinputDescripcion') myinputDescripcion: jqxTextAreaComponent;
  @ViewChild('myCedula') myCedula: jqxInputComponent;
  @ViewChild('myinputTitulo') myinputTitulo: jqxInputComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

  //@ViewChild('myFileUpload') myFileUpload: jqxFileUploadComponent;
  @ViewChild('myFileUpload') myFileUpload: ElementRef;
  @ViewChild('myFileNuevaVersion') myFileNuevaVersion: ElementRef;
  @Output() cedulaIngresada = new EventEmitter(); // para comunicar el hijo con el padre
  @Output() procesarSioNo = new EventEmitter(); // para comunicar el hijo con el padre
  @Output() PassMethods: EventEmitter<any> = new EventEmitter();

  @Input() idDepOfertaH: any; // para comunicar el padre con el hijo
  @Input() idConvProy: any;

  modalProyectoSubscription: Subscription;
  idPrograma: any;
  ban: boolean = true;
  fileSelecionado: any = '';
  nuevaVersionStrg: any = '';
  validar: boolean;
  idUserProyecto: any;
  idDocente: any = null;
  fileNuevaVersion: any = '';
  url: string = '';
  nuevaVersionNum: number = null;
  banProyectoVersion: Boolean = false;

  constructor(protected modalService: BsModalService,
    private router: Router,
    private periodoConvocatoria: VinculacionService,
    private fileArchivo: FileService, private accountService: AccountService
  ) { }
  protected a: string;
  opcion: any = {};
  idDepOfertaProyecto: Number = null;
  idProyectoDocente: Number = null;
  idProyectoVersion: Number = null;
  /**Permite controlar que no salga del modal sin presionar algun boton de éste */
  protected config = { backdrop: true, ignoreBackdropClick: true };
  usuarioActual: any = {};//variable para guardar el usuario actual


  ngOnInit() {
    //validar, si el usuario logeado pertenece al dep de vinculacion, al momento de guardar proyecto
    //el id docente del json esta vacio si el usuario del departamento no esta registrado como docente
    //preguntar si el director y evaluador de vinculacion pueden crear proyectos
    //si la respuesta es positiva crear opciones de agragr director de proyecto si el funcianario de 
    //vinculacion no esta registrado como docente de alguna carrera

    //this.nameField.nativeElement.focus() 
    this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
    this.idUserProyecto = JSON.parse(localStorage.getItem('idTipoUserProyecto'));
    this.buscarIdDocente(this.usuarioActual.id);
  }

  ngOnDestroy() {
    console.log("destruimp");
    if (this.modalProyectoSubscription) {
      this.modalProyectoSubscription.unsubscribe();
    }

  }

  buscarIdDocente(id: any) {
    this.modalProyectoSubscription = this.periodoConvocatoria.buscarIdDocente(id).subscribe(data => {
      //console.log(JSON.stringify(data));
      this.idDocente = data.idDocente;
    });
  }

  onUploadStart(event: FileList): void {
    console.log('asi');
    let fileOld = event.item(0);
    //this.fileSelecionado=new File(fileOld,'FACSISTEL_TELE',fileOld.type);
    let fileNew: FormData = new FormData();
    let namefileSinExtension: string = fileOld.name.split('?')[0].split('.').shift();
    let fileExtension: string = fileOld.name.split('?')[0].split('.').pop();
    this.url = namefileSinExtension + '_V1.' + fileExtension;
    fileNew.append('file', fileOld, this.url);
    this.fileSelecionado = fileNew;
  };

  selectFileNuevaVersion(event: FileList): void {
    console.log('nuevaVersion');
    let fileOld = event.item(0);
    let fileNew: any = new FormData();
    let namefileSinExtension: string = fileOld.name.split('?')[0].split('.').shift();
    let fileExtension: string = fileOld.name.split('?')[0].split('.').pop();
    //preguntar si estoy editando o creando
    this.url = namefileSinExtension + '_' + this.nuevaVersionStrg + '.' + fileExtension;
    this.opcion.url;
    fileNew.append('file', fileOld, this.url);
    this.fileNuevaVersion = fileNew;
  };

  verFileDelServer(nombreFile: string) {
    this.fileArchivo.descargarFile(nombreFile).subscribe((blob: Blob) => {
      let fileNew: any = new FormData();
      fileNew.append('file', blob, nombreFile);
      this.fileNuevaVersion = fileNew;
    });

  }

  gotoList() {
    this.router.navigate(['/docente/registro-proyecto']);
  }

  ingresarNuevaVersion(idProyecto: any): void {
    this.banProyectoVersion = true
    this.opcion.id = idProyecto;
    this.buscarLaVersionProyecto(idProyecto);
    this.templateNuevaVersion.config = this.config;//Establece las configuraciones para el modal
    this.templateNuevaVersion.show();
  }

  editandoNuevaVersion(idProyecto: any): void {
    this.opcion.id = idProyecto;
    this.buscarLaVersionProyecto(idProyecto);
    this.templateNuevaVersion.config = this.config;//Establece las configuraciones para el modal
    this.templateNuevaVersion.show();

  }

  buscarLaVersionProyecto(idProyecto: any) {
    this.modalProyectoSubscription = this.periodoConvocatoria.buscarVersionProyecto(idProyecto).subscribe(data => {
      //console.log(JSON.stringify(data));
      if (data.length) {
        let nameVersion = data[0].url;
        this.url = data[0].url;
        if (!this.banProyectoVersion) {
          this.opcion.url = this.url;
          this.verFileDelServer(this.url);
          this.idProyectoVersion = data[0].idProyectoVersion;
          this.banProyectoVersion = true;
        }
        let namefileSinExtension: string = nameVersion.split('?')[0].split('.').shift();
        let fileExtension: string = namefileSinExtension.split('?')[0].split('_').pop();
        let version = fileExtension.split('').pop();
        this.nuevaVersionNum = Number(version) + 1;
        this.nuevaVersionStrg = 'V' + this.nuevaVersionNum;
        console.log('nuevaVersion ' + this.nuevaVersionNum);
        // let version: string = nose.split('?')[0].split('_').pop();
      }

    });
  }

  formularioNuevo(): void {
    console.log("yase" + this.idDepOfertaH)
    console.log("idConvPro " + this.idConvProy)
    console.log("idDepa " + this.idDepOfertaH)
    this.ListarAllProgramas(this.idDepOfertaH);
    //this.usuarioActual = JSON.parse(localStorage.getItem('usuarioActual')); //captura del usuario actual
    console.log(" aqui " + this.usuarioActual[0]);
    console.log(" aqui1 " + localStorage.getItem('usuarioActual'));
    //this.loadData(idParametricaPar, codigoTablaPar);
    //var opt = event.args.innerText;
    let fecha = new Date()
    this.myfechaDesde.value(fecha);
    let fechaFin = new Date(fecha.setMonth(fecha.getMonth() + 12))
    this.myfechaHasta.value(fechaFin);
    this.modalForm.show();//Muesta el modal
    this.modalForm.config = this.config;//Establece las configuraciones para el modal
    /*this._sub = this.PassMethods.subscribe((h) =>{
      if(h.val == 'ok'){
        this.myValidator.validate(document.getElementById('formParametrica'));
        console.log("descrip : "+this.txtDescripcion.onChange)
        if (this.validaDatos()) { 
          console.log("descrip : "+this.txtDescripcion.val())
          this.myValidator.hide();
         this.modalComponent.alertQuestion({
            title: 'Registro de Paramétricas',
            msg: 'Desea grabar este registro?',
            result: (k) => {
              if (k) {
                //this.grabarTablaParametrica(this.parametricaAng, codigoTablaPar) 
              }
            }
          })
        } else {
          this.modalComponent.alertMessage({
            title: 'Registro de Paramétricas',
            msg: 'Verifique que todos los campos esten llenos correctamente!'
          });
        }
      };
    }); */
  }



  //**************************listar combox programas*************************************************************
  ListarAllProgramas(idDepOferta: any) {
    this.modalProyectoSubscription = this.periodoConvocatoria.listarProgramaIdDepOferta(idDepOferta).subscribe(data => {
      //console.log(JSON.stringify(data));
      this.sourceProgramasRegistrados.localdata = data;
      this.dataAdapterPrograma.dataBind();
    });
  }

  sourceProgramasRegistrados: any = {
    datatype: 'json',
    id: 'idPrograma',
    localdata: [
      { name: 'estado', type: 'string' },
      { name: 'tituloPrograma', type: 'string' },
      { name: 'idPrograma', type: 'int' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterPrograma: any = new jqx.dataAdapter(this.sourceProgramasRegistrados);
  /*************************************************************************************************************** */

  /**
   * Evento de botón al presionar `Aceptar` en el modalQuestion
  */

  protected Submit() {
    //this.a = 'ok';
    //this.PassMethods.emit({ val: this.a });
    //this.modalForm.hide();
    //this.formNuevo.hide();
    if (this.funValidar() == true) {
      let datosProyecto = this.formarJsonProyecto();
      this.guardar(datosProyecto, this.fileSelecionado);
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Datos Guardados Correctamente!' });
      //this.formNuevo.hide();      
      this.modalForm.ngOnDestroy();
    }
  }

  funValidar() {
    this.validar = true;
    if (this.idPrograma == null) {
      this.validar = false;
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Seleccione un Programa!' });
    }
    if (this.opcion.titulo == "") {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese un título!' });
      this.validar = false;
    }
    if (this.opcion.descripcion == "") {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese una descripción!' });
      this.validar = false;
    }
    if (this.opcion.fechaDesde == null) {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese una Fecha Inicio!' });
      this.validar = false;
    }
    if (this.opcion.fechaHasta == null) {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Ingrese una Fecha Fin!' });
      this.validar = false;
    }
    if (this.fileSelecionado == '') {
      this.myModal.alertMessage({ title: 'Proyecto', msg: 'Seleccione un Archivo!' });
      this.validar = false;
    }
    if ((this.opcion.fechaDesde != null) && (this.opcion.fechaHasta != null)) {
      var dateFechadesde = this.opcion.fechaDesde;
      var dateFechaHasta = this.opcion.fechaHasta;
      //convierte las fechasen numeros
      var d = Date.parse(dateFechadesde);
      var h = Date.parse(dateFechaHasta);
      if (d >= h) {
        console.log("mayor " + dateFechadesde);
        this.myModal.alertMessage({ title: 'Proyecto', msg: 'Fecha Fin debe ser mayor a fecha Inicial!' });
        this.validar = false;
      }
    }
    return this.validar;
  }

  grabarFileVersion() {
    if (this.fileNuevaVersion != null) {
      //subir y enviar a vinculacion
      console.log('si' + this.fileNuevaVersion.fileName)
      let datosProyecto: any = {};
      let arrProyectoVersion: any = [];
      let objProyectoVersion: any = {};

      objProyectoVersion.id = this.idProyectoVersion;
      objProyectoVersion.url = this.url;
      objProyectoVersion.estadoProyectoVersion = 'REVISIÓN';
      objProyectoVersion.version = this.nuevaVersionNum;
      objProyectoVersion.usuarioIngresoId = this.usuarioActual.id;
      objProyectoVersion.estado = 'A';
      arrProyectoVersion.push(objProyectoVersion);

      datosProyecto.id = this.opcion.id;
      datosProyecto.estadoProyecto = 'REVISIÓN';
      datosProyecto.proyectoVersion = arrProyectoVersion;

      this.guardar(datosProyecto, this.fileNuevaVersion);
      this.limpiar();
      this.templateNuevaVersion.ngOnDestroy();
    } else {
      this.myModal.alertMessage({ title: 'Nueva Versión', msg: 'Seleccione un archivo!' });
    }
  }

  subirFileVersion() {
    if (this.fileNuevaVersion != null) {
      //subir 
      console.log('si' + this.fileNuevaVersion.fileName)
      let datosProyecto: any = {};
      let arrProyectoVersion: any = [];
      let objProyectoVersion: any = {};

      objProyectoVersion.id = this.idProyectoVersion;
      objProyectoVersion.url = this.url;
      objProyectoVersion.estadoProyectoVersion = 'INICIAL';
      objProyectoVersion.version = this.nuevaVersionNum;
      objProyectoVersion.usuarioIngresoId = this.usuarioActual.id;
      objProyectoVersion.estado = 'A';
      arrProyectoVersion.push(objProyectoVersion);

      datosProyecto.id = this.opcion.id;
      datosProyecto.proyectoVersion = arrProyectoVersion;

      this.guardar(datosProyecto, this.fileNuevaVersion);
      this.limpiar();
      this.templateNuevaVersion.ngOnDestroy();

    } else {
      this.myModal.alertMessage({ title: 'Nueva Versión', msg: 'Seleccione un archivo!' });
    }
  }

  guardar(datosProyecto: any, fileSelecionado: any) {
    console.log("datoaProyecto " + JSON.stringify(datosProyecto));
    try {
      var err: boolean = false;
      this.modalProyectoSubscription = this.fileArchivo.pushFileToStoragePP(fileSelecionado)
        .subscribe(result => {
          console.log('resuk ' + result);
        }, error => {
          console.error("Error al subir file al server " + error)
          this.myModal.alertMessage({ title: 'Proyecto', msg: 'Error al subir file al server!' });
          err = true;
        });

      if (err == false) {
        if (this.banProyectoVersion == true) {
          this.modalProyectoSubscription = this.periodoConvocatoria.grabarProyectoVersion(datosProyecto).subscribe(result => {
            //para actulizar grid del padre(evaluar_proyecto)
            this.a = "actualizar";
            this.PassMethods.emit({ val: this.a });
            this.myModal.alertMessage({ title: 'Nueva Versión', msg: 'Versión guardada correctamente!' });
          }, error => console.error("h " + error));
        } else {
          this.modalProyectoSubscription = this.periodoConvocatoria.grabarProyecto(datosProyecto).subscribe(result => {
            //para actulizar grid del padre(registro_proyecto)
            this.a = "actualizar";
            this.PassMethods.emit({ val: this.a });
            this.myModal.alertMessage({ title: 'Proyecto', msg: 'Proyecto guardado correctamente!' });
          }, error => console.error("h " + error));
        }
      }
      this.limpiar();
    }
    catch (error) {
      console.error('Here is the error message', error);
    }
    console.log('Execution continues');
  }

  formarJsonProyecto() {
    let datosProyecto: any = {};
    let depOfertaProyecto1: any = [];
    let depOfertaProyecto: any = {};
    let proyectoDocente1: any = [];
    let proyectoDocente: any = {};
    let arrProyectoVersion: any = [];
    let objProyectoVersion: any = {};

    objProyectoVersion.id = this.idProyectoVersion;
    objProyectoVersion.url = this.url;
    objProyectoVersion.estadoProyectoVersion = 'INICIAL';
    objProyectoVersion.version = 1;
    objProyectoVersion.usuarioIngresoId = this.usuarioActual.id;
    objProyectoVersion.estado = 'A';
    arrProyectoVersion.push(objProyectoVersion);

    proyectoDocente.id = this.idProyectoDocente;
    proyectoDocente.idDocente = this.idDocente;
    proyectoDocente.idTipoUsuarioProyecto = this.idUserProyecto;
    proyectoDocente.version = 1;
    proyectoDocente.usuarioIngresoId = this.usuarioActual.id;
    proyectoDocente.estado = 'A';
    proyectoDocente1.push(proyectoDocente);

    datosProyecto.id = this.opcion.id;
    depOfertaProyecto.id = this.idDepOfertaProyecto;
    depOfertaProyecto.idDepartamentoOferta = this.idDepOfertaH;
    depOfertaProyecto.version = 1;
    depOfertaProyecto.usuarioIngresoId = this.usuarioActual.id;
    depOfertaProyecto.estado = 'A';
    depOfertaProyecto1.push(depOfertaProyecto);
    //datosProyecto.idPeriodoAcademico = this.idPeriodoAcademico;
    datosProyecto.descripcion = this.opcion.descripcion;
    datosProyecto.titulo = this.opcion.titulo;
    datosProyecto.idPrograma = this.idPrograma;
    datosProyecto.codigo = 'NO-APR';
    datosProyecto.estadoProyecto = 'INICIAL';
    datosProyecto.idProyectoConvocatoria = this.idConvProy;
    datosProyecto.version = 1;
    datosProyecto.fechaDesde = this.opcion.fechaDesde;
    datosProyecto.fechaHasta = this.opcion.fechaHasta;
    datosProyecto.estado = 'A';
    datosProyecto.usuarioIngresoId = this.usuarioActual.id;
    datosProyecto.depOfertaProyecto = depOfertaProyecto1;
    datosProyecto.proyectoDocente = proyectoDocente1;
    datosProyecto.proyectoVersion = arrProyectoVersion;
    //json
    return datosProyecto;
  }

  limpiar() {
    console.log('limpia');
    this.opcion.descripcion = "";
    this.opcion.titulo = "";
    this.opcion.url = "";
    this.opcion.fechaDesde = null;
    this.opcion.fechaHasta = null;
    this.dropDownPrograma.clearSelection();
    this.opcion.id = null;
    this.idDepOfertaProyecto = null;
    this.idProyectoDocente = null;
    this.idProyectoVersion = null;
    this.idPrograma = null;
    this.myfechaDesde = null;
    this.myfechaHasta = null;
    this.myinputTitulo.val('');
    this.myFileUpload.nativeElement.value = "";
    this.myFileNuevaVersion.nativeElement.value = "";
    this.url = '';
    this.nuevaVersionStrg = '';
    this.nuevaVersionNum = null;
    this.banProyectoVersion = false;
  }

  checkChange(event: any): void {
    let checked = event.args.checked;
    this.dropDownPrograma.disabled(!this.ban);
    if (checked) {
      this.dropDownPrograma.clearSelection();
      this.dropDownPrograma.unselectIndex(0);
      this.dropDownPrograma.getItem(0).value = false;
      //this.dropDownPrograma.enableSelection(this.ban);
    }
    else {
      console.log("nada");
    }
  }

  itemSeleccionado(event: any): void {
    if (event.args) {
      let item = event.args.item;
      if (item) {
        this.idPrograma = item.value;
        console.log("idPr " + this.idPrograma + " o " + item.value);
      }
    }
  };

  //Reglas de validación formulario
  /* rules = [
     { input: '.descripcionInput', message: 'Descripción requerida!', action: 'keyup, blur', rule: 'required' },
     { input: '.descripcionInput', message: 'Descripción deben tener entre 5 a 100 caracteres!', action: 'keyup', rule: 'length=5,100' },
     {
       input: '.descripcionInput', message: 'Campo descripción no admite el caracter ingresado!', action: 'keyup, blur',
       rule: (): any => {//regla para evitar el ingreso de caracteres especiales 
         // return this.validarCaracter(this.txtDescripcion.val());
       }
     }
   ];*/

  cancelar() {
    this.modalProyectoSubscription.unsubscribe();
    this.limpiar();
    this.modalForm.hide();
  }

  enviarCedula() {
    let regEx = new RegExp(/^[0-9]\d{6,10}$/);
    if (!regEx.test(this.myCedula.ngValue)) {
      console.error(this.myCedula.ngValue + ' isn\'t a phone number with area code!');
    } else {
      console.log('Cedula Correcta ');
      this.cedulaIngresada.emit({ val: this.myCedula.ngValue });
    }
    //console.log("1 " + myCedula);
    //console.log("2 " + this.myCedula.ngValue);
    this.template.hide();
  }

  questionSioNo(opt: any): void {
    if (opt.title == undefined || opt.title == '') {
      this.opcion.title = 'Mensaje Usuario'
    } else {
      this.opcion.title = opt.title;//Establece el titulo para el modal
    }

    if (opt.msg == undefined || opt.msg == '') {
      this.opcion.msg = 'Alerta formulario'
    } else {
      this.opcion.msg = opt.msg;//Establece el mensaje para el modal
    }
    this.templateSioNo.config = this.config;//Establece las configuraciones para el modal
    this.templateSioNo.show();

  }
  Si() {
    this.procesarSioNo.emit({ val: 'Si' });
    this.templateSioNo.hide();
  }

  No() {
    this.procesarSioNo.emit({ val: 'No' });
    this.templateSioNo.hide();
  }

  solicitarCedula(): void {
    this.template.config = this.config;//Establece las configuraciones para el modal
    this.template.show();

  }

  validarCaracter(text) {
    var test = /^([A-z0-9 ñáéíóú":.,'´\-]{3,100})$/i;
    var nomText = new RegExp(test);
    return nomText.test(text);
  }
}
