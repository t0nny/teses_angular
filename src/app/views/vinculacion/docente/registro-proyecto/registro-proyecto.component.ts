import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, Optional } from '@angular/core';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';
import { ModalProyectoComponent } from '../modal-proyecto/modal-proyecto.component';
import { Router, ActivatedRoute } from '@angular/router';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { jqxTabsComponent } from 'jqwidgets-ng/jqxtabs';
import { MallasService } from '../../../../services/mallas/mallas.service';
import { jqxButtonComponent } from 'jqwidgets-ng/jqxbuttons';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { jqxInputComponent } from 'jqwidgets-ng/jqxinput';
import { jqxTextAreaComponent } from 'jqwidgets-ng/jqxtextarea';
import { Subscription } from 'rxjs';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { varGlobales } from '../../VarGlobales/varGlobales';
import { DistributivosService } from '../../../../services/distributivos/distributivos.service';
import { jqxMenuComponent } from 'jqwidgets-ng/jqxmenu';
import { DatePipe } from '@angular/common';
import { AccountService } from '../../../../services/auth/account.service';
import { DatosGeneralesComponent } from '../seguimiente-proyecto/secciones-tab/datos-generales/datos-generales.component';
import { AsignarPersonalComponent } from '../seguimiente-proyecto/secciones-tab/asignar-personal/asignar-personal.component';
import { Constants } from '../../../../util/constants';


@Component({
  selector: 'app-registro-proyecto',
  templateUrl: './registro-proyecto.component.html',
  styleUrls: ['./registro-proyecto.component.scss']
})

export class RegistroProyectoComponent implements OnInit, AfterViewInit {
  //Variable paa cargar datos del objeto 
  rowindex: number = -1;
  editaConvocatoria: any[];
  localization: any = Constants.getLocalization('es');
  regristroProyectoSubscription: Subscription;
  idDepOfertaP: any;
  //Variable paa cargar datos del objeto 
  listaProgramas: Array<any>;
  Titulo: string;
  directorProyecto: string;
  tipo: string;
  //varaiables para llenar convocatoriaProyecto
  convDescripcion: string;
  convFechaDesde: string;
  convFechaHasta: string;
  idConvocatoriaProyecto: any;

  //idDepartamentoP: any;
  env = varGlobales;
  convocatoria = true;
  public mostrarFacultad = false;
  public mostrarTitulo = false;
  public mostrarPrograma = false;
  public mostrarMenu = false;
  selected: any;
  idCarreraSelecionada: any = null;

  usuarioActual: any = {};//variable para guardar el usuario actual
  pr: any;
  //usuActual: import("c:/Users/pedri/git/siia-project/src/app/model/auth/usuario").Usuario;


  constructor(private router: Router, private route: ActivatedRoute, private mallasService: MallasService,
    private periodoConvocatoria: VinculacionService, private distributivosService: DistributivosService,
    private datePipe: DatePipe, private accountService: AccountService) {
    //this.tipoUsuario()
  }

  @ViewChild(DatosGeneralesComponent) datosGenerales: DatosGeneralesComponent;
  @ViewChild(AsignarPersonalComponent) asignarPersonal: AsignarPersonalComponent;

  @ViewChild('dropDownPeriodo') dropDownPeriodo: jqxDropDownListComponent;
  @ViewChild('dropDownCarrera') dropDownCarrera: jqxDropDownListComponent;
  @ViewChild('dropDownProgramas') dropDownProgramas: jqxDropDownListComponent;
  @ViewChild('gridProyectoRegistrados') gridProyectoRegistrados: jqxGridComponent;
  @ViewChild('gridProgramaRegistrados') gridProgramaRegistrados: jqxGridComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;
  @ViewChild(ModalProyectoComponent) formModalProyecto: ModalProyectoComponent;
  @ViewChild('jqxTabsPrincipal') jqxTabsPrincipal: jqxTabsComponent;
  @ViewChild('listaProyecto') listaProyecto: ElementRef;
  @ViewChild('jqxTabsProyecto') jqxTabsProyecto: jqxTabsComponent;
  @ViewChild('jqxTabsPrograma') jqxTabsPrograma: jqxTabsComponent;
  @ViewChild('buttonVolver') buttonVolver: jqxButtonComponent;

  @ViewChild('myMenu') myMenu: jqxMenuComponent;

  @ViewChild('myHabFechas') myHabFechas: jqxCheckBoxComponent;
  @ViewChild('myfechaDesde') myfechaDesde: jqxDateTimeInputComponent;
  @ViewChild('myfechaHasta') myfechaHasta: jqxDateTimeInputComponent;
  @ViewChild('myinputDescripcion') myinputDescripcion: jqxTextAreaComponent;
  @ViewChild('myinputTitulo') private myinputTitulo: jqxInputComponent;

  @ViewChild('gridDirectorProyecto') gridDirectorProyecto: jqxGridComponent;
  @ViewChild('gridDocenteParticipantes') gridDocenteParticipantes: jqxGridComponent;
  @ViewChild('gridEstudianteCoordinador') gridEstudianteCoordinador: jqxGridComponent;
  @ViewChild('gridEstudianteParticipantes') gridEstudianteParticipantes: jqxGridComponent;



  ngOnInit() {
    this.usuarioActual = this.accountService.getUsuario();
    this.directorProyecto = this.usuarioActual.nombres + ' ' + this.usuarioActual.apellidos;
    localStorage.setItem('detalles', 'false')
    // console.log('entre',this.usuarioActual);
    this.regristroProyectoSubscription = this.periodoConvocatoria.cargarTipoUsuario(this.usuarioActual.id).subscribe(data => {
      console.log("tipoUser " + JSON.stringify(data));
      /*this.pr=data;
       return this.pr
       */
      if (data.length > 0) {
        localStorage.setItem('tipoUserProyecto', JSON.stringify(data))
        this.mostrarTipoMenu(JSON.parse(localStorage.getItem('tipoUserProyecto')));
        //this.mostarDirectorProyecto();   
      }
    }, error => console.error("h " + error));
    // this.MostrarTitulo();
    this.MostrarConvocatoria();


    /* if(this.pr != undefined){
       this.mostrarTipoMenu(this.pr)
       
     }else{
       console.log('aun no carga');
     } */
    //this.ListarPeriodo();

    //this.cedula='0912539848';

  }
  mostarDirectorProyecto() {
    //this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
    this.directorProyecto = this.usuarioActual.firstName + ' ' + this.usuarioActual.lastName
  }



  ngAfterViewInit(): void {
    console.log('con ' + this.convocatoria);
    if (!this.convocatoria) {
      this.myMenu.disable('1', true);
      this.myMenu.disable('4', true);
    } else {
      console.log('akisss');
    }
    //this.mostrarTipoMenu(JSON.parse(localStorage.getItem('tipoUserProyecto'))); 

  }

  /*ngAfterContentInit() {
    console.log("entre seguimient")
    this.cargarDatos();
  }
  cargarDatos() {
    this.regristroProyectoSubscription= this.route.params.subscribe(valor => {
      //RECUPERAR EL VALOR DEL PARAMETRO PASADO EN LA RUTA
      const idDepOferta = valor['idDepOferta'];//ID DE ASIGNATURA
      this.dropDownCarrera.val(idDepOferta);
    
      console.log('aqui estoy cargo??');
      //EVALUA SI EL PARAMETRO ID SE PASO
      
    });
  }*/

  /*tipoUsuario() {
    this.usuarioActual=JSON.parse(localStorage.getItem('usuarioActual')); //captura del usuario actual
    console.log('idusu ' + this.usuarioActual.id)
    this.regristroProyectoSubscription = this.periodoConvocatoria.cargarTipoUsuario(this.usuarioActual.id).subscribe(data => {
      console.log("titulo "+ JSON.stringify(data));
      
        /*this.pr=data;
        return this.pr
        
      if (data.length > 0) {
        //return  this.pr=data;
        this.mostrarTipoMenu(data);
      }
    }, error => console.error("h " + error));
    //this.columnsProyectos=this.columnsProyectos1;
  }*/
  mostrarTipoMenu(data: any) {
    //console.log("titulo " + JSON.stringify(data));
    switch (data[0].codigo) {
      //Opciones de Crud  
      case 'DOD':
        console.log('DOCENTE DIRECTOR')

        //redirecciona a formulario de edicion nuevo
        this.columnsProyectos = this.columnsProyectos2;
        this.MostrarTitulo();
        this.mostrarFacultad = true;
        this.tipo = 'Director de Proyecto';
        this.mostrarPrograma = true;
        break;
      case 'DOP':
        console.log('DOCENTE PARTICIPADOR')
        this.columnsProyectos = this.columnsProyectos3;
        this.MostrarTitulo();
        this.mostrarPrograma = true;
        //this.mostrarMenu = true;
        this.mostrarFacultad = true;
        this.tipo = 'Docente Participante';
        break;
      case 'DOC':
        console.log('DOCENTE COORDINADOR')
        this.columnsProyectos = this.columnsProyectos2;
        this.MostrarTitulo();
        this.mostrarPrograma = false;
        //this.mostrarMenu = true;
        this.mostrarFacultad = true;
        this.tipo = 'Docente Coordinador';
        break;
      case 'VID':
        // this.dataMenu('disable', 'productLITagID', true)
        //this.jqxMenu('disable', 'productLITagID', true);

        this.ListarFacultades();
        console.log('DIRECTOR VINCULACION')
        this.mostrarTitulo = true;
        //this.mostrarMenu = true;
        this.columnsProyectos = this.columnsProyectos1;
        this.tipo = 'Director de Vinculación';
        break;
      default:
      //default code block
    }
  }

  ngOnDestroy(): void {
    this.Titulo = ' ';
    this.directorProyecto = ' ';
    /// this.idDepartamentoP = null;
    this.convDescripcion = ' ';
    this.convFechaDesde = ' ';
    this.convFechaHasta = ' ';
    this.idConvocatoriaProyecto = ' ';
    console.log("destruipProyecto");
    localStorage.removeItem('vin_idDep');
    if (this.regristroProyectoSubscription) {
      this.regristroProyectoSubscription.unsubscribe();
    }

  }

  MostrarTitulo() {
    let usuarioActual = JSON.parse(localStorage.getItem('tipoUserProyecto'));
    //this.env.nameUsuario=usuarioActual[0].identificacion;
    //aqui debe ir un metodo para obtener el id_periodoAcademico actual
    //llamar la tabla proyecto-convocatoria y obtener el id de peridoacademico de essa tabla
    this.regristroProyectoSubscription = this.periodoConvocatoria.cargarTituloFacultad('4', usuarioActual[0].identificacion).subscribe(data => {
      console.log("titulo " + JSON.stringify(data));
      if (data.length > 0) {
        this.Titulo = data[0].nombre;
        this.env.idDepartamento = data[0].idDepartamento;
        console.log("setlo " + data[0].idDepartamento);
        localStorage.setItem('vin_idDep', data[0].idDepartamento);
        console.log("setlo " + localStorage.getItem('vin_idDep'));
        if (this.mostrarPrograma) {
          this.sourceCarrera.localdata = data;
          this.dataAdapterCarrera.dataBind();
        } else {
          this.ListarCarreras(data[0].idDepartamento);
        }
      }
    }, error => console.error("h " + error));

    console.log("identeifi " + usuarioActual[0].identificacion);
    /*if ((this.env.nameUsuario).length == 0) {
      this.formModalProyecto.solicitarCedula();
    } else {
      this.listarTitulo(this.env.nameUsuario);
    }*/
  }
  /*
    listarTitulo(cedula: string) {
      if ((cedula).length == 0) {
        this.formModalProyecto.solicitarCedula();
      } else {
  
        //aqui debe ir un metodo para obtener el id_periodoAcademico actual
        this.regristroProyectoSubscription = this.periodoConvocatoria.cargarTituloFacultad('4', cedula).subscribe(data => {
          //console.log("titulo "+ JSON.stringify(data));
          this.Titulo = data.nombre;
          this.env.idDepartamento = data.idDepartamento;
          console.log("setlo " +data.idDepartamento);
          localStorage.setItem('vin_idDep', data.idDepartamento);
          console.log("setlo " +localStorage.getItem('vin_idDep'));
          this.ListarCarreras(data.idDepartamento);
        }, error => console.error("h " + error));
      }
    }*/

  MostrarConvocatoria() {

    console.log("aqui1 ");
    //aqui debe ir un metodo para obtener el id_periodoAcademico actual
    this.regristroProyectoSubscription = this.periodoConvocatoria.listarUltimaConvocatoria().subscribe(data => {
      // console.log("aqui " +JSON.stringify(data));

      this.convDescripcion = data.descripcion;
      this.convFechaDesde = data.fechaDesde;
      this.convFechaHasta = data.fechaHasta;
      this.idConvocatoriaProyecto = data.id;
      this.disableConvocatoria(data.fechaHasta)


    }, error => console.error("h " + error));

  }
  disableConvocatoria(fechaHasta: any) {
    let fechaActual = Date.now()
    console.log('date ' + this.datePipe.transform(fechaActual, "yyyy-MM-dd"))
    //convierte las fechasen numeros
    //let d=Date.parse(fechaActual);
    console.log('d +' + fechaActual)
    console.log('d *' + fechaHasta)

    //let h = Date.parse(fechaHasta);
    ///console.log('d -' + h)
    if (fechaActual >= fechaHasta) {
      this.convocatoria = false;
      console.log("mayor " + fechaActual);
      this.ngAfterViewInit();

    } else { this.convocatoria = true; }
    /* var Fecha = new Date(fechaHasta);
     Fecha.setDate(Fecha.getDate()+ 16)
     console.log('datedd ' + this.datePipe.transform(Fecha,"yyyy-MM-dd"))*/
  }



  /* itemclick(event: any): void {
     //this.ngAfterViewInit();
     //console.log(event);
     //var opt = this.jqxTabsProyecto.getTitleAt(event.args.item);
     //this.jqxTabsProyecto.getContentAt((event.args.item)-1);
     var opt = event.args.innerText;
     //event.args.hidden=true
     //this.jqxMenu.disable(event.args.id ,true);
     //console.log(opt);
     let opt1 = event.args.innerText;
     //var opt = this.jqxTabsProyecto.focus()
     //this.eventLog.nativeElement.innerHTML = 'Id: ' + event.args.id + ', Text: ' + event.args.innerText;
     // captura el id seleccionado
     //const selectedrowindex =this.gridProyectosRegistrados.getselectedrowindex();
     //const idProyectoSel=this.gridProyectosRegistrados.getcellvalue(selectedrowindex,'idProyecto');
     if (this.validad(opt) == true) {
       if (opt != 'Nuevo') {
         var getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
         var estadoProyecto = getRowObject.estadoProyecto;
         var idProyecto = getRowObject.idProyecto;
       }
       switch (opt) {
         //Opciones de Crud  
         case 'Nuevo':
        
           break;
         case 'Enviar Vinculación':
        
           break;
         case 'Eliminar':
           
           break;
         case 'Seguimiento':
         
           break;
         default:
         //default code block
       }
     }
   }*/


  /*validad(opt: string): boolean {
    let validar = true;
    console.log("opt " + opt)
    if (opt == 'Nuevo') {
      if (this.idCarreraSelecionada == null) {
        validar = false;
        this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione una Carrera!' });
      }
    } else {
      console.log("opt1 " + this.idCarreraSelecionada)
      if (this.idCarreraSelecionada == null) {
        validar = false;
        this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione una Carrera!' });
      } else {
        if (this.rowindex == -1) {
          validar = false;
          this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione un Proyecto!' });
        }
      }
      console.log("validar " + validar)
    }
    return validar;
  }

  /*****************llenar combo periodoAcademico**********************************************************/
  ListarPeriodo() {
    this.regristroProyectoSubscription = this.mallasService.listarPeriodo().subscribe(data => {
      this.sourcePeriodo.localdata = data;
      this.dataAdapterPeriodo.dataBind();
    });
  };

  sourcePeriodo: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'estado', type: 'string' },
      { name: 'descripcion', type: 'string' },
      { name: 'id', type: 'int' }
    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterPeriodo: any = new jqx.dataAdapter(this.sourcePeriodo);
  /*************************************************************************************************************** */

  /*****************llenar combo carreras**********************************************************/
  ListarCarreras(id: any) {
    this.regristroProyectoSubscription = this.periodoConvocatoria.cargarCarreras('4', id).subscribe(data => {
      //console.log(JSON.stringify(data));        
      this.sourceCarrera.localdata = data;
      this.dataAdapterCarrera.dataBind();
      localStorage.setItem('dataCarrera', JSON.stringify(data));

    }, error => console.error("h " + error));

  }
  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceCarrera: any = {
    datatype: 'json',
    id: 'id',
    localdata: [
      { name: 'id', type: 'int' },
      { name: 'descripcion', type: 'string' },
      { name: 'estado', type: 'string' }

    ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
  dataAdapterCarrera: any = new jqx.dataAdapter(this.sourceCarrera);
  /*************************************************************************************************************** */

  //**************************listar grid proyectos*************************************************************
  ListarAllProyectos(idDepOferta: any) {
    this.regristroProyectoSubscription = this.periodoConvocatoria.listarProyectosIdDepOferta(idDepOferta).subscribe(data => {
      // console.log(JSON.stringify(data));
      if (data.length > 0) {
        let usuarioActual = JSON.parse(localStorage.getItem('tipoUserProyecto'));
        if (usuarioActual.length > 0) {
          let VID = usuarioActual.filter((dats) => { return dats.codigo == "VID" }) //DIRECTOR DE VINCULACION
          let DOD = usuarioActual.filter((dats) => { return dats.codigo == "DOD" }) //DOCENTE DIRECTOR
          let DOP = usuarioActual.filter((dats) => { return dats.codigo == "DOP" }) //DOCENTE PARTICIPANTE
          let DOC = usuarioActual.filter((dats) => { return dats.codigo == "DOC" }) //DOCENTE COORDINADOR
          if (VID.length > 0) {
            //director de vinculacion
            console.log('director de vinculacion');
            let data1 = data.filter((dats) => { return dats.estadoProyecto.trim() != "MODIFICAR" && dats.estadoProyecto.trim() != "INICIAL" })
            this.sourceProyectosRegistrados.localdata = data1;
            this.dataAdapterProyectoRegistrados.dataBind();
            localStorage.setItem('idTipoUserProyecto', VID[0].idTipoUsuarioProyecto);
          } else {
            if (DOD.length > 0 && DOP.length > 0 && DOC.length > 0) {
              console.log('cOORDINADOR ,director y partipante de proyectos ');
              let data1 = data.filter((dats) => { return dats.estadoProyecto.trim() != "CULMINADO" })
              this.sourceProyectosRegistrados.localdata = data1;
              this.dataAdapterProyectoRegistrados.dataBind();
              localStorage.setItem('idTipoUserProyecto', DOC[0].idTipoUsuarioProyecto);
            } else {
              if (DOD.length > 0 && DOP.length > 0) {
                //director y partipante de proyectos 
                console.log('director y partipante de proyectos ');
                let data1 = data.filter((dats) => { return dats.estadoProyecto.trim() != "CULMINADO" })
                this.sourceProyectosRegistrados.localdata = data1;
                this.dataAdapterProyectoRegistrados.dataBind();
                localStorage.setItem('idTipoUserProyecto', DOD[0].idTipoUsuarioProyecto);
              } else {
                if (DOC.length > 0) {
                  console.log('Coordinador de proyectos ');
                  let data1 = data.filter((dats) => { return dats.estadoProyecto.trim() != "CULMINADO" })
                  this.sourceProyectosRegistrados.localdata = data1;
                  this.dataAdapterProyectoRegistrados.dataBind();
                  localStorage.setItem('idTipoUserProyecto', DOC[0].idTipoUsuarioProyecto);
                } else {
                  if (DOD.length > 0) {
                    //director  de proyectos 
                    console.log('director de proyectos ');
                    let data1 = data.filter((dats) => { return dats.estadoProyecto.trim() != "CULMINADO" })
                    this.sourceProyectosRegistrados.localdata = data1;
                    this.dataAdapterProyectoRegistrados.dataBind();
                    localStorage.setItem('idTipoUserProyecto', DOD[0].idTipoUsuarioProyecto);
                  } else {
                    console.log('partipante de proyectos ');
                    //partipante de proyectos 
                    localStorage.setItem('idTipoUserProyecto', DOP[0].idTipoUsuarioProyecto);
                  }
                }
              }
            }
          }
        }
      } else {
        this.myModal.alertMessage({ title: 'Menu Proyectos', msg: 'Carrera sin Proyecto!' });
        this.sourceProyectosRegistrados.localdata = [];
        this.dataAdapterProyectoRegistrados.dataBind();
      }
    });
    this.idDepOfertaP = idDepOferta;
  }


  sourceProyectosRegistrados: any =
    {
      datatype: 'array',
      id: 'idProyecto',
      datafields:
        [
          { name: 'idProyecto', type: 'int' },
          { name: 'tituloProyecto', type: 'string' },
          { name: 'codigoProyecto', type: 'string' },
          { name: 'tituloPrograma', type: 'string' },
          { name: 'descripcion', type: 'string' },
          { name: 'fechaDesde', type: 'date', format: 'd' },
          { name: 'fechaHasta', type: 'date', format: 'd' },
          { name: 'estadoPrograma', type: 'string' },
          { name: 'estadoProyecto', type: 'string' },
        ]
    };
  dataAdapterProyectoRegistrados: any = new jqx.dataAdapter(this.sourceProyectosRegistrados);

  columnsProyectos: any[] = [];

  columnsProyectos1: any[] =
    [
      { text: 'Id Proyecto', datafield: 'idProyecto', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Proyecto', datafield: 'tituloProyecto', width: '21%' },
      { text: 'Codigo', datafield: 'codigoProyecto', width: '10%' },
      { text: 'Programa', datafield: 'tituloPrograma', width: '20%' },
      { text: 'Período Académico', datafield: 'descripcion', width: '13%' },
      { text: 'Fecha Desde', datafield: 'fechaDesde', width: '10%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
      { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '10%', cellsformat: 'yyyy-MM-dd' },
      { text: 'Estado Proyecto', datafield: 'estadoProyecto', width: '8%' },
      {
        text: 'Evaluar', columntype: 'button', width: '8%',
        cellsrenderer: (): string => {
          return 'Evaluar';
        },
        buttonclick: (row: number): void => {
          console.log('entrecreo');
          let getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
          let idProyecto = getRowObject.idProyecto;
          let estadoProyecto = getRowObject.estadoProyecto;
          if (estadoProyecto.trim() == 'REVISIÓN') {
            this.router.navigate(['vinculacion/evaluador/evaluar-proyecto/', idProyecto, 1]);
          } else {
            switch (estadoProyecto.trim()) {
              case 'MODIFICAR':
                this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El proyecto fue enviado a revisión!' });
                this.gridProyectoRegistrados.clearselection();

                break;
              case 'APROBADO':
                this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto esta aprobado!' });
                this.gridProyectoRegistrados.clearselection();
                break;
              case 'CULMINADO':
                this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto ha culminado!' });
                this.gridProyectoRegistrados.clearselection();
                break;
              default:
            }
          }
        },

      },
    ];

  columnsProyectos2: any[] =
    [
      { text: 'Id Proyecto', datafield: 'idProyecto', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Proyecto', datafield: 'tituloProyecto', width: '19%' },
      // { text: 'Codigo', datafield: 'codigoProyecto', width: '10%' },
      { text: 'Programa', datafield: 'tituloPrograma', width: '19%' },
      { text: 'Período Académico', datafield: 'descripcion', width: '13%' },
      { text: 'Fecha Desde', datafield: 'fechaDesde', width: '10%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
      { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '10%', cellsformat: 'yyyy-MM-dd' },
      { text: 'Estado Proyecto', datafield: 'estadoProyecto', width: '8%' },
      {
        text: 'Detalles', columntype: 'button', width: '10%', cellsalign: 'center', center: 'center',
        cellsrenderer: (): string => {
          return 'Detalles';
        },
        buttonclick: (row: number): void => {
          console.log('entrecreo');
          let getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
          let idProyecto = getRowObject.idProyecto;
          let estadoProyecto = getRowObject.estadoProyecto;
          if (this.convocatoria) {
            if (estadoProyecto.trim() == 'INICIAL' || estadoProyecto.trim() == 'MODIFICAR') {
              this.router.navigate(['vinculacion/docente/seguimiento-proyecto', idProyecto, this.env.idDepartamento, this.idCarreraSelecionada]);
            } else {
              switch (estadoProyecto.trim()) {
                case 'REVISIÓN':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El proyecto fue enviado a revisión!' });
                  this.gridProyectoRegistrados.clearselection();

                  break;
                case 'APROBADO':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto esta aprobado!' });
                  this.gridProyectoRegistrados.clearselection();
                  break;
                case 'CULMINADO':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto ha culminado!' });
                  this.gridProyectoRegistrados.clearselection();
                  break;
                default:
              }
            }
          } else {
            this.myModal.alertMessage({ title: 'Registro Proyecto', msg: 'Período convocatoria ha finalizado' });
            this.gridProyectoRegistrados.clearselection();
          }
        },
      },
      {
        text: 'Nueva Version', columntype: 'button', width: '10%', cellsalign: 'center', center: 'center',
        cellsrenderer: (): string => {
          return 'Nueva Version';
        },
        buttonclick: (row: number): void => {
          console.log('entrecreo');
          let getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
          let idProyecto = getRowObject.idProyecto;
          let estadoProyecto = getRowObject.estadoProyecto;
          if (this.convocatoria) {
            if (estadoProyecto.trim() == 'MODIFICAR') {
              this.router.navigate(['vinculacion/evaluador/evaluar-proyecto/', idProyecto, 2]);
            } else {
              switch (estadoProyecto.trim()) {
                case 'REVISIÓN':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El proyecto fue enviado a vinculación!' });
                  this.gridProyectoRegistrados.clearselection();

                  break;
                case 'APROBADO':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto esta aprobado!' });
                  this.gridProyectoRegistrados.clearselection();
                  break;
                case 'CULMINADO':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto ha culminado!' });
                  this.gridProyectoRegistrados.clearselection();
                  break;
                case 'INICIAL':
                  this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El Proyecto aún no enviado a vinculación!' });
                  this.gridProyectoRegistrados.clearselection();
                  break;
                default:
              }
            }
          } else {
            this.myModal.alertMessage({ title: 'Registro Proyecto', msg: 'Período convocatoria ha finalizado' });
            this.gridProyectoRegistrados.clearselection();
          }
        },

      },
    ];

  columnsProyectos3: any[] =
    [
      { text: 'Id Proyecto', datafield: 'idProyecto', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Proyecto', datafield: 'tituloProyecto', width: '19%' },
      { text: 'Codigo', datafield: 'codigoProyecto', width: '10%' },
      { text: 'Programa', datafield: 'tituloPrograma', width: '19%' },
      { text: 'Período Académico', datafield: 'descripcion', width: '13%' },
      { text: 'Fecha Desde', datafield: 'fechaDesde', width: '10%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
      { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '10%', cellsformat: 'yyyy-MM-dd' },
      { text: 'Estado Proyecto', datafield: 'estadoProyecto', width: '8%' },
      {
        text: 'Tareas', columntype: 'button', width: '10%',
        cellsrenderer: (): string => {
          return 'Tareas';
        },
        buttonclick: (row: number): void => {
          console.log('entrecreo');
          let getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
          let idProyecto = getRowObject.idProyecto;
          //this.router.navigate(['vinculacion/evaluador/evaluar-proyecto/', idProyecto,2]);
          // let dataRecord = this.gridDocentesRegistrados.getrowdata(row);
          //this.descargaHojaDeVida(dataRecord.idDocente);  
        },

      },
    ];


  /*************************************************************************************************************** */
  //**************************listar grid programas*************************************************************

  ListarAllProgramas(idDepOferta: any) {
    this.regristroProyectoSubscription = this.periodoConvocatoria.listarProgramaIdDepOferta(idDepOferta).subscribe(data => {
      //console.log(JSON.stringify(data));
      this.sourceProgramasRegistrados.localdata = data;
      this.dataAdapterProgramaRegistrados.dataBind();
    });
  }

  sourceProgramasRegistrados: any =
    {
      datatype: 'array',
      id: 'id',
      datafields:
        [
          { name: 'idPrograma', type: 'int' },
          { name: 'tituloPrograma', type: 'string' },
          { name: 'codigoPrograma', type: 'string' },
          { name: 'descripcion', type: 'string' },
          { name: 'fechaDesde', type: 'date', format: 'd' },
          { name: 'fechaHasta', type: 'date', format: 'd' },
          { name: 'estado', type: 'string' },
          { name: 'estadoPrograma', type: 'string' },
        ]
    };
  dataAdapterProgramaRegistrados: any = new jqx.dataAdapter(this.sourceProgramasRegistrados);

  columnsPrograma: any[] =
    [
      { text: 'Id Programa', datafield: 'idPrograma', width: '5%', filtertype: 'none', hidden: true },
      { text: 'Programa', datafield: 'titulo', width: '20%' },
      { text: 'Codigo', datafield: 'codigo', width: '10%' },
      { text: 'Período Académico', datafield: 'descripcion', width: '10%' },
      { text: 'Fecha Desde', datafield: 'fechaDesde', width: '10%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
      { text: 'Fecha Hasta', datafield: 'fechaHasta', width: '10%', cellsformat: 'yyyy-MM-dd' },
      { text: 'Estado', datafield: 'estado', width: '5%' },
      { text: 'Estado Programa', datafield: 'estadoPrograma', width: '5%' },
      {
        text: 'Evaluar', columntype: 'button', width: '10%',
        cellsrenderer: (): string => {
          return 'Evaluar';
        },
        buttonclick: (row: number): void => {

          // let dataRecord = this.gridDocentesRegistrados.getrowdata(row);
          //this.descargaHojaDeVida(dataRecord.idDocente);  
        },

      },
    ];
  /*************************************************************************************************************** */

  /**************************evento recibido desde el hijo(modalProyecto)*************************************************************/
  /* cedulaRecibida(cedula) {
     console.log("cedula" + cedula.val)
     varGlobales.nameUsuario = cedula.val;
     this.listarTitulo(cedula.val);
   }*/

  //actualizaal padre desde el hijo
  procesaPassMethods(h) {
    if (h.val == 'actualizar') {
      console.log("aq")
      this.ListarAllProyectos(this.idDepOfertaP);
    }
  }

  //**************************evento del comboCarreras*************************************************************
  ListarProyectos() {
    console.log("debes traeeeee" + this.dropDownCarrera.val())
    this.idCarreraSelecionada = this.dropDownCarrera.val();
    this.ListarAllProgramas(this.dropDownCarrera.val());
    this.ListarAllProyectos(this.dropDownCarrera.val());
  }

  eliminarProyecto(idProyecto: number) {
    /* this.DocentesService.borrarDocente(idPersona).subscribe(result => {
    }, error => console.error(error)); */
  }

  //**************************metodo para obtner el id de la fila seleccionada*************************************************************
  Rowselect(event: any): void {
    if (event.args) {
      this.rowindex = event.args.rowindex;
    }
  }

  LiClick(event: any): void {
    console.log("entre al li del proyecto")

  }

  //**************************definición de estructura de datos del jqxMenu*************************************************************
  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Eliminar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Seguimiento',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '4',
      'text': 'Enviar Vinculación',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
  ];

  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };

  getAdapter(sourceMenu: any): any {
    return new jqx.dataAdapter(sourceMenu, { autoBind: true });
  };
  menuProyecto = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
  /*************************************************************************************************************** */

  /*
    controlIsCollapsed() {
      this.router.navigate(['vinculacion/docente/proyecto-edicion', 0]);
  
    }
  */
  ListarOfertas(event: any) {
    console.log('eve' + event.args.item.value);
    this.ListarCarreras(event.args.item.value);


  }

  ListarFacultades() {
    this.distributivosService.getAllFacultades().subscribe(data => {
      console.log(JSON.stringify(data));
      this.sourceComboFacultad.localdata = data;
      this.dataAdapterFacultad.dataBind();
    })
  }
  //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
  sourceComboFacultad: any =
    {
      datatype: 'json',
      id: 'idFacultad',
      localdata:
        [
          { name: 'idFacultad', type: 'int' },
          { name: 'facultad', type: 'string' },
          { name: 'estado', type: 'string' }

        ],
    };
  //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
  dataAdapterFacultad: any = new jqx.dataAdapter(this.sourceComboFacultad);


  rendertoolbar = (toolbar: any): void => {
    let id = '0';
    let container = document.createElement('div');
    container.id = id;
    container.style.margin = '0.5%';
    let nuevoButtonContainer = document.createElement('div')
    nuevoButtonContainer.id = 'nuevoButtonContainer' + id;
    nuevoButtonContainer.style.cssText = 'float: left';
    container.appendChild(nuevoButtonContainer);
    let eliminarButtonContainer = document.createElement('div')
    eliminarButtonContainer.id = 'elimnarButtonContainer' + id;
    eliminarButtonContainer.style.cssText = 'float: left';
    container.appendChild(eliminarButtonContainer);
    //let seguimientoButtonContainer = document.createElement('div')
    //seguimientoButtonContainer.id = 'seguimientoButtonContainer' + id;
    //seguimientoButtonContainer.style.cssText = 'float: left';
    //container.appendChild(seguimientoButtonContainer);
    let vinculacionButtonContainer = document.createElement('div')
    vinculacionButtonContainer.id = 'vinculacionButtonContainer' + id;
    vinculacionButtonContainer.style.cssText = 'float: left';
    container.appendChild(vinculacionButtonContainer);
    if (toolbar[0] == '' || toolbar[0] != null) {
      toolbar[0].appendChild(container);
      let settingNuevo = {
        width: '10%',
        height: 25,
        disabled: false,
        value: 'Nuevo',
        theme: 'ui-redmond'
      }
      let settingEliminar = {
        width: '10%',
        height: 25,
        disabled: false,
        value: 'Eliminar',
        theme: 'ui-redmond'
      }
      /*let settingSeguimiento = {
        width: '10%',
        height: 25,
        disabled: false,
        value: 'Seguimiento',
        theme: 'ui-redmond'
      }*/
      let settingVinculacion = {
        width: '15%',
        height: 25,
        disabled: false,
        value: 'Enviar vinculación',
        theme: 'ui-redmond'
      }
      let nuevoRowButton = jqwidgets.createInstance('#nuevoButtonContainer' + id, 'jqxButton', settingNuevo);
      nuevoRowButton.addEventHandler('click', () => {
        console.error(this.convocatoria)
        if (this.convocatoria) {
          if (this.idCarreraSelecionada == null) {
            this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione una Carrera!' });
          } else {
            console.log('si entre al nuevo')
            //redirecciona a formulario de edicion nuevo
            this.formModalProyecto.formularioNuevo();
          }
        } else {
          this.myModal.alertMessage({ title: 'Registro Proyecto', msg: 'Período convocatoria ha finalizado' });

        }
      })
      let eliminarRowButton = jqwidgets.createInstance('#elimnarButtonContainer' + id, 'jqxButton', settingEliminar);
      eliminarRowButton.addEventHandler('click', () => {
        if (this.idCarreraSelecionada == null) {
          this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione una Carrera!' });
        } else {
          if (this.rowindex == -1) {
            this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione un Proyecto!' });
          } else {
            console.log('si entre al eliminar');
            this.formModalProyecto.formularioNuevo();
            //this.mostrarDatos = !this.mostrarDatos;
            /*if (estadoProyecto.trim() == 'INICIAL') {
              this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'ESTE PROYECTO SE VA AELIMINAR!' });
            } else {
              this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Solo puede eliminar cuando el proyecto esta en estado INICIAL!' });
            }*/
          }
        }
      })
      /*let seguimientoRowButton = jqwidgets.createInstance('#seguimientoButtonContainer' + id, 'jqxButton', settingSeguimiento);
      seguimientoRowButton.addEventHandler('click', () => {
        if (this.idCarreraSelecionada == null) {
          this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione una Carrera!' });
        } else {
          if (this.rowindex == -1) {
            this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione un Proyecto!' });
          } else {
            console.log('si entre al seguimiento')
            var getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
            var estadoProyecto = getRowObject.estadoProyecto;
            var idProyecto = getRowObject.idProyecto;
            if (estadoProyecto.trim() == 'APROBADO') {
              this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'ESTE PROYECTO SE VA A SEGUIMIENTO!' });
            } else {
              this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'El proyecto aún no ha sido aprobado!' });
            }
          }
        }
      })*/
      let vinculacionRowButton = jqwidgets.createInstance('#vinculacionButtonContainer' + id, 'jqxButton', settingVinculacion);
      vinculacionRowButton.addEventHandler('click', () => {
        var getRowObject = this.gridProyectoRegistrados.getrowdata(this.rowindex);
        var estadoProyecto = getRowObject.estadoProyecto;
        var idProyecto = getRowObject.idProyecto;
        console.log('si entre al Vinculación');
        if (this.idCarreraSelecionada == null) {
          this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione una Carrera!' });
        } else {
          if (this.rowindex == -1) {
            this.myModal.alertMessage({ title: 'Mantenimiento de Proyectos', msg: 'Seleccione un Proyecto!' });
          } else {
            //debe tener todos los detalles antes de enviar a vinculacion
            console.log('si entre al eliminar');
            this.formModalProyecto.formularioNuevo();
            switch (estadoProyecto.trim()) {
              case 'INICIAL':
                //LLAMAR SERVICIO ACTUALIZAR ESTDO
                let ban = localStorage.getItem('detalles')
                if (ban == 'true') {
                  this.regristroProyectoSubscription = this.periodoConvocatoria.updateEstadoProyecto(idProyecto).subscribe(data => {
                    //console.log("titulo "+ JSON.stringify(data));
                    this.myModal.alertMessage({ title: 'Estado del Proyecto', msg: 'El Proyecto ha sido enviado a vinculación!' });
                    this.ListarAllProyectos(this.dropDownCarrera.val());
                    this.gridProyectoRegistrados.clearselection();
                    localStorage.setItem('detalles', 'false')
                  }, error => console.error("h " + error));
                } else {
                  this.myModal.alertMessage({ title: 'Estado del Proyecto', msg: 'Debe agregar la información general del Proyecto!' });

                }

                break;
              case 'APROBADO':
                this.myModal.alertMessage({ title: 'Estado del Proyecto', msg: 'El Proyecto esta aprobado!' });
                break;
              case 'CULMINADO':
                this.myModal.alertMessage({ title: 'Mantenimiento de Proyecto', msg: 'El Proyecto ha culminado!' });
                break;
              case 'MODIFICAR':
                this.myModal.alertMessage({ title: 'Mantenimiento de Proyecto', msg: 'Primero debe agregar una nueva versión!' });
                break;
              case 'REVISIÓN':
                this.myModal.alertMessage({ title: 'Mantenimiento de Proyecto', msg: 'El Proyecto esta en proceso de revisión!' });
                break;
              default:
            }
          }
        }

      })
    }
  }


}
