import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ControlAutenticacion } from '../../security/control-autenticacion';
import { RegistroProgramaComponent } from './director/registro-programa/registro-programa.component';
import { ConvocatoriaComponent } from './director/convocatoria/convocatoria.component';
import { ModalProgramaComponent } from './director/modal-programa/modal-programa.component';
import { RegistroProyectoComponent } from './docente/registro-proyecto/registro-proyecto.component';
import { ProyectoEdicionComponent } from './docente/proyecto-edicion/proyecto-edicion.component';
import { SeguimienteProyectoComponent } from './docente/seguimiente-proyecto/seguimiente-proyecto.component';
import { MatrizExAnteComponent } from './director/matriz-ex-ante/matriz-ex-ante.component';
import { EvaluarProyectoComponent } from './evaluar-proyecto/evaluar-proyecto.component';
import { CronogramaTareasComponent } from './componentes/cronograma-tareas/cronograma-tareas.component';
import { AsignarTareasComponent } from './componentes/asignar-tareas/asignar-tareas.component';
import { GenerarInformeComponent } from './componentes/generar-informe/generar-informe.component';
import { ConsolidarInformeComponent } from './componentes/consolidar-informe/consolidar-informe.component';
import { SeguimientoActividadComponent } from './director/seguimiento-actividad/seguimiento-actividad.component';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Vinculacion',
  },
  children: [{
    path: '',
    redirectTo: 'vinculacion',
    pathMatch: 'full'
  },
  {
    path: 'director/registro-programa',
    component: RegistroProgramaComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Registro Programa'
    }
  },
  {
    path: 'director/convocatoria',
    component: ConvocatoriaComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Registro de convocatorias'
    }
  },
  {
    path: 'director/seguimiento-actividad',
    component: SeguimientoActividadComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Consolidado de Actividades'
    }
  },
  {
    path: 'director/modal-programa',
    component: ModalProgramaComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Registro mant Programa'
    }
  },
  {
    path: 'docente/registro-proyecto',
    component: RegistroProyectoComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Registro Proyecto'
    }
  },
  {
    path: 'docente/proyecto-edicion/:idProyecto'
    ///:idOferta'
    ,
    component: ProyectoEdicionComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Edición de Proyecto'
    }
  },
  {
    path: 'docente/seguimiento-proyecto/:idProyecto/:idDepartamento/:idDepOferta'
    ///:idOferta'
    ,
    component: SeguimienteProyectoComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Seguimiento de Proyecto'
    }
  },
  {
    path: 'director/matriz-ex-ante/:idProyecto'
    ///:idOferta'
    ,
    component: MatrizExAnteComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Evaluacion de Proyecto'
    }
  },
  {
    path: 'evaluador/evaluar-proyecto/:idProyecto/:idTipoUsuario',
    component: EvaluarProyectoComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Evaluar Proyecto'
    }
  },
  {
    path: 'componentes/cronograma-tareas/:idProyecto',
    component: CronogramaTareasComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Cronogramas de tareas'
    }
  },
  {
    path: 'componentes/asignar-tareas',
    component: AsignarTareasComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Asignación de Tareas'
    }
  },
  {
    path: 'componentes/generar-informe',
    component: GenerarInformeComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Generar Informe'
    }
  },
  {
    path: 'componentes/consolidar-informe',
    component: ConsolidarInformeComponent,
    canActivate: [ControlAutenticacion],
    data: {
      title: 'Consolidar Informe'
    }
  },
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VinculacionRoutingModule { }