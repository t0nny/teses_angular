import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { ModalConvocatoriaComponent } from '../modal-convocatoria/modal-convocatoria.component';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';
import { varGlobales } from '../../VarGlobales/varGlobales';

@Component({
  selector: 'app-convocatoria',
  templateUrl: './convocatoria.component.html',
  styleUrls: ['./convocatoria.component.scss']
})

export class ConvocatoriaComponent implements OnInit {

  //Shift + Alt + F para arregral tu codogo
  //ctrl + k ctrl + c comentar ctrl + k ctrl + u descpomentar
  //ctrl + shift +p manual de atajos
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
  @ViewChild(ModalConvocatoriaComponent) formModalConvocatoria: ModalConvocatoriaComponent;
  @ViewChild('gridConvocatoriasRegistrados') gridConvocatoriasRegistrados: jqxGridComponent;

  //Variable paa cargar datos del objeto 
  convocatoriaSubscription: Subscription;
  rowindex: number = -1;
  idPeriodoConvocatoria: any;
  editaConvocatoria: any[];
  env = varGlobales;

  constructor(private periodoConvocatoria: VinculacionService) { }

  ngOnInit() {
    this.ListarConvocatorias();
    console.log(this.env.nameUsuario);
  }

  //---------------------------------------------------- EVENTOS DEL jqxGrid ----------------------------------------------------
  //metodo para obtner el id de la fila seleccionada
  Rowselect(event: any): void {
    if (event.args) {
      this.rowindex = event.args.rowindex;
    }
  }
  //------------------------------------------------------------------------------------------------------------------------------

  //actualizaal padre desde el hijo
  procesaPassMethods(h) {
    if (h.val == 'actualizar') {
      console.log("aq")
      this.ListarConvocatorias();
    }
  }

  itemclick(event: any): void {
    //const selectedrowindex = this.gridConvocatoriasRegistrados.getselectedrowindex();
    //const idPersonaSel = this.gridConvocatoriasRegistrados.getcellvalue(selectedrowindex, 'idConvocatoria');
    //this.idmallaOferta = ordersbyid[event.args.rowindex].id;
    //this.eventLog.nativeElement.innerHTML = 'Id: ' + event.args.id + ', Text: ' + event.args.innerText;
    let opt = event.args.innerText;
    switch (opt) {
      //Opciones de Crud  
      case 'Nuevo':
        this.convocatoriaSubscription = this.periodoConvocatoria.buscarFechaFinConvocatoria().subscribe((data: any) => {
          let fechaActual = Date.now();
          if (fechaActual >= data.fechaHasta) {
            this.formModalConvocatoria.formularioNuevo();
          }else{
            this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Existe una Convocatoria Vigente!' });
          }
        });
        
        break;

      case 'Editar':
        /*//funcional me trae el id de la fila seleccionada
        const selectedrowindex  = this.gridConvocatoriasRegistrados.getselectedrowindex();
        console.log("selew " + selectedrowindex)
        if(selectedrowindex >= 0){
          let idDepartamento = this.gridConvocatoriasRegistrados.getcellvalue(selectedrowindex,'idPeriodoConvocatoria')
          console.log(JSON.stringify("sle " + idDepartamento));
        }*/

        if (this.rowindex != -1) {
          let getRowObject = this.gridConvocatoriasRegistrados.getrowdata(this.rowindex);
          let id = getRowObject.id;
          this.convocatoriaSubscription = this.periodoConvocatoria.buscarPorPeriodoConvocatoriaId(id).subscribe((data: any) => {
            this.editaConvocatoria = data;
            this.formModalConvocatoria.formularioEditar(this.editaConvocatoria);
            this.gridConvocatoriasRegistrados.clearselection();
            this.rowindex = -1;
            this.convocatoriaSubscription.unsubscribe();
          });
        }
        else {
          this.myModal.alertMessage({ title: 'Mantenimiento de Convocatoria', msg: 'Seleccione una Convocatoria!' });
        }
        break;
      case 'Eliminar':
        let selectedrowindex = this.gridConvocatoriasRegistrados.getselectedrowindex();
        let idConvocatoria = this.gridConvocatoriasRegistrados.getcellvalue(selectedrowindex, 'idConvocatoriaProyecto');

        if (idConvocatoria) {
          this.myModal.alertQuestion({
            title: 'Registro de Convocatorias',
            msg: 'Desea eliminar este registro?',
            result: (k) => {
              if (k) {
                this.convocatoriaSubscription = this.periodoConvocatoria.borrarConvocatoria(idConvocatoria).subscribe(result => {
                  this.gridConvocatoriasRegistrados.deleterow(idConvocatoria);
                  this.gridConvocatoriasRegistrados.clearselection();
                  this.rowindex = -1;
                  this.convocatoriaSubscription.unsubscribe();
                }, error => console.error(error));
                this.myModal.alertMessage({ title: 'Registro de Convocatorias', msg: 'Convocatoria eliminado correctamente!' });
              }
            }
          });

        } else {
          this.myModal.alertMessage({ title: 'Registro de Convocatorias', msg: 'Seleccione una Convocatorias!' });
        }
        break;
      default:
    }
  };

  ListarConvocatorias() {
    this.convocatoriaSubscription = this.periodoConvocatoria.listarPeriodoConvocatoria().subscribe(data => {
      // console.log(JSON.stringify(data));
      this.sourceConvocatoriasRegistrados.localdata = data;
      this.dataAdapterConvocatoriasRegistrados.dataBind();
      this.convocatoriaSubscription.unsubscribe();
    });

  }

  //definición de estructura de datos del jqxGrid
  //datos de jqxGrid
  sourceConvocatoriasRegistrados: any =
    {
      datatype: 'array',
      id: 'id',
      datafields:
        [
          { name: 'id', type: 'int' },
          { name: 'descripcion', type: 'string' },
          { name: 'fechaDesde', type: 'date', format: 'd' },
          { name: 'fechaHasta', type: 'date', format: 'd' },
          { name: 'descripcionPeriodo', type: 'string' },
        ]
    };
  dataAdapterConvocatoriasRegistrados: any = new jqx.dataAdapter(this.sourceConvocatoriasRegistrados);

  columnsConvocatorias: any[] =
    [
      { text: 'Id Convocatoria', datafield: 'idConvocatoriaProyecto', width: '5%', filtertype: 'none', hidden: true },
      { text: 'Descripción', datafield: 'descripcion', width: '35%' },
      { text: 'Fecha Inicio', datafield: 'fechaDesde', width: '20%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
      { text: 'Fecha Fin', datafield: 'fechaHasta', width: '20%', cellsformat: 'yyyy-MM-dd' },
      { text: 'Período Académico', datafield: 'descripcionPeriodo', width: '16%' },
      {
        text: 'Proyectos', columntype: 'button', width: '10%',
        cellsrenderer: (): string => {
          return 'Proyectos';
        },
        buttonclick: (row: number): void => {
          this.myModal.alertMessage({ title: 'Listado de Proyectos', msg: 'Redirigiendo!' });
          // let dataRecord = this.gridDocentesRegistrados.getrowdata(row);
          //this.descargaHojaDeVida(dataRecord.idDocente);  
        },

      },
    ];

  //definición de estructura de datos del jqxMenu
  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Editar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Eliminar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },

  ];

  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };

  getAdapter(sourceMenu: any): any {
    return new jqx.dataAdapter(this.sourceMenu, { autoBind: true });
  };
  menuConvocatoria = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);


}
