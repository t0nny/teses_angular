import { DatePipe } from "@angular/common";
import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { jqxDateTimeInputComponent } from "jqwidgets-ng/jqxdatetimeinput";
import { jqxDropDownListComponent } from "jqwidgets-ng/jqxdropdownlist";
import { jqxTextAreaComponent } from "jqwidgets-ng/jqxtextarea";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { AccountService } from "../../../../services/auth/account.service";
import { MallasService } from "../../../../services/mallas/mallas.service";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";

@Component({
    selector: 'app-modal-seguimiento-actividad',
    templateUrl: './modal-seguimiento-actividad.component.html',
    //styleUrls: ['./modal-seguimiento-actividad.component.scss']
})

export class ModalSeguimientoActividadComponent implements OnInit {
    @ViewChild('modalSeguimientoActividad') private modalSeguimientoActividad: ModalDirective;
    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild('dropDownPeriodo') dropDownPeriodo: jqxDropDownListComponent;
    @ViewChild('myfechaDesde') myfechaDesde: jqxDateTimeInputComponent;
    @ViewChild('myfechaHasta') myfechaHasta: jqxDateTimeInputComponent;
    @ViewChild('myinputDescripcion') myinputDescripcion: jqxTextAreaComponent;

    /**Permite controlar que no salga del modal sin presionar algun boton de éste */
    protected config = { backdrop: true, ignoreBackdropClick: true };
    @Output() PassMethods: EventEmitter<any> = new EventEmitter();
    opcion: any = {};
    _sub: Subscription;
    usuarioActual: any = {};//variable para guardar el usuario actual

    constructor(private mallasService: MallasService,
        private periodoConvocatoria: VinculacionService, private accountService: AccountService) { }

    ngOnInit(): void {
        this.usuarioActual = this.accountService.getUsuario(); //captura del usuario actual
        this.ListarPeriodo();

    }

    formularioNuevoSeguimientoActividad(): void {
        let fecha = new Date()
        this.myfechaDesde.value(fecha);
        this.myfechaHasta.value(fecha);
        this.modalSeguimientoActividad.show(); //Muesta el modal
        this.modalSeguimientoActividad.config = this.config; //Establece las configuraciones para el modal
    }

    formularioEditarSeguimientoActividad(opt: any): void {
        debugger
        if (opt === void 0) { opt = {}; }
        this.opcion = opt;
        this.modalSeguimientoActividad.show(); //Muesta el modal
        this.modalSeguimientoActividad.config = this.config; //Establece las configuraciones para el modal
        this.dropDownPeriodo.val(this.opcion.idPeriodoAcademico);
    }

    ListarPeriodo() {
        this._sub = this.mallasService.listarPeriodo().subscribe((data) => {
            //console.log(JSON.stringify(data));
            this.sourcePeriodo.localdata = data;
            this.dataAdapterPeriodo.dataBind();
            this._sub.unsubscribe();
        });
    }

    sourcePeriodo: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'estado', type: 'string' },
            { name: 'descripcion', type: 'string' },
            { name: 'id', type: 'int' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
    dataAdapterPeriodo: any = new jqx.dataAdapter(this.sourcePeriodo);


    protected Submit() {
        if (this.funValidar() == true) {
            /*this.myModal.alertQuestion({
                title: 'Grabar',
                msg: '¿Realmente desea grabar?',
                result: (k) => {
                  if (k) {
                    this.guardar();
                   this.modalSeguimientoActividad.ngOnDestroy();
                  }
                }
              });*/
            this.guardar();
            this.modalSeguimientoActividad.ngOnDestroy();
        }
    }

    protected cancelar() {
        this.limpiar();
        this.modalSeguimientoActividad.ngOnDestroy();
    }

    limpiar() {
        this.opcion.descripcion = null;
        this.myinputDescripcion.placeHolder('Descripción General del Proyecto')
        this.opcion.fechaDesde = null;
        this.opcion.fechaHasta = null;
        this.dropDownPeriodo.clearSelection();
        this.dropDownPeriodo.unselectIndex(0);
        this.dropDownPeriodo.selectItem
        this.opcion.id = null;
        let fecha = new Date();
        (fecha.toLocaleDateString("es",{ month: "long" })).toUpperCase();
        fecha.getMonth()
        this.myfechaDesde.value(fecha);
        this.myfechaHasta.value(fecha);
    }

    guardar() {
        try {
            let fecha = new Date(this.opcion.fechaHasta);
            var datosConvocatoria: any = {};
            datosConvocatoria.id = this.opcion.id;
            datosConvocatoria.idPeriodoAcademico = this.dropDownPeriodo.val();
            datosConvocatoria.descripcion = this.opcion.descripcion;
            datosConvocatoria.version = 1;
            datosConvocatoria.fechaDesde = this.opcion.fechaDesde;
            datosConvocatoria.fechaHasta = this.opcion.fechaHasta;
            datosConvocatoria.estado = 'A';
            datosConvocatoria.usuarioIngresoId = this.usuarioActual.id;
            //json
            console.log(JSON.stringify(datosConvocatoria));
            this._sub = this.periodoConvocatoria.grabarSeguimientoActividad(datosConvocatoria).subscribe(result => {
                this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Datos Guardados Correctamente!' });
                this.limpiar();
                //para actulizar grid del padre
                //this.a = "actualizar";
                this.PassMethods.emit({ val: "actualizar" });
                this._sub.unsubscribe();
            }, function (error) { return this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Error al guardar!' }) });
            this.limpiar();
        }
        catch (error) {
            console.error('Here is the error message', error);
        }
        console.log('Execution continues');
    }

    funValidar() {
        let validar = true;
        if (this.dropDownPeriodo.val() == "") {
            this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Seleccione un Periodo Académico!' });
            return validar = false;
        }
        if (this.myinputDescripcion.val() == '') {
            this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Ingrese una descripción!' });
            return validar = false;
        }
        if (this.opcion.fechaDesde == null) {
            this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Ingrese una Fecha Inicio!' });
            return validar = false;
        }
        if (this.opcion.fechaHasta == null) {
            this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Ingrese una Fecha Fin!' });
            return validar = false;
        }
        if ((this.opcion.fechaDesde != null) && (this.opcion.fechaHasta != null)) {
            var dateFechadesde = this.opcion.fechaDesde;
            var dateFechaHasta = this.opcion.fechaHasta;
            //convierte las fechasen numeros
            var d = Date.parse(dateFechadesde);
            var h = Date.parse(dateFechaHasta);
            if (d >= h) {
                console.log("mayor " + dateFechadesde);
                this.myModal.alertMessage({ title: 'Registro de Seguimiento', msg: 'Fecha Fin debe ser mayor a fecha Inicial!' });
                return validar = false;
            }
        }
        return validar;
    }

    onChange(){
        var fecha = new Date(this.myfechaHasta.val());
        if(this.myfechaHasta.val() !=undefined){
            this.opcion.descripcion='CONSOLIDADO MES DE';
            this.opcion.descripcion=this.opcion.descripcion +' '+(fecha.toLocaleDateString("es",{ month: "long" })).toUpperCase();
            
        }
        
    }
}