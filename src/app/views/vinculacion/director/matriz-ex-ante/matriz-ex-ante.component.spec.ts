import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatrizExAnteComponent } from './matriz-ex-ante.component';

describe('MatrizExAnteComponent', () => {
  let component: MatrizExAnteComponent;
  let fixture: ComponentFixture<MatrizExAnteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatrizExAnteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatrizExAnteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
