import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { jqxPanelComponent } from 'jqwidgets-ng/jqxPanel';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { Subscription } from 'rxjs';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AccountService } from '../../../../services/auth/account.service';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { ModalProyectoComponent } from '../../docente/modal-proyecto/modal-proyecto.component';

@Component({
  selector: 'app-matriz-ex-ante',
  templateUrl: './matriz-ex-ante.component.html',
  styleUrls: ['./matriz-ex-ante.component.scss']
})
export class MatrizExAnteComponent implements OnInit {

  @ViewChild('myPanelGrid') myPanelGrid: jqxPanelComponent;
  @ViewChild('checkBoxModificar') checkBoxModificar: jqxCheckBoxComponent;
  @ViewChild('checkBoxAprobar') checkBoxAprobar: jqxCheckBoxComponent;
  @ViewChild('checkBoxNoAprobar') checkBoxNoAprobar: jqxCheckBoxComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;
  @ViewChild('myfechaEntrega') myfechaEntrega: jqxDateTimeInputComponent;
  @ViewChild('formEvaluar') private formEvaluar: ModalDirective;
  @ViewChild(ModalProyectoComponent) formModalProyecto: ModalProyectoComponent;
 
  usuarioActual: any = {};//variable para guardar el usuario actual
  public mostrarFechaRecepcion = true;
  public mostrarCodigo = true; 
  _sub: Subscription;
  cabMatriz: any = {};
  arrayGrids: any[] = [];
  proyecto: any = {};
  preguntaInstrumento: Array<any>;
  opcionRespuesta: Array<any>;
  categoriaPregunta: Array<any>;
  tabsCategoriaPregunta: jqwidgets.jqxTabs;
  prueba: any[];
  idProyectoVersion: any;
  estadoMatriz: string;

  @Output() PassMethods: EventEmitter<any> = new EventEmitter();
  detMatriz: any;

    /**Permite controlar que no salga del modal sin presionar algun boton de éste */
    protected config = {backdrop: true,ignoreBackdropClick: true};
  constructor(private periodoConvocatoria: VinculacionService,
     private router: Router,private accountService: AccountService) { }

  ngOnInit() {
    console.log('inicialice');
    setTimeout(() => {
    this.listarPreguntasInstrumentoVin(10);
    this.usuarioActual = this.accountService.getUsuario();//captura del usuario actual
    });    
  }

  ngOnDestroy() {
    console.log("matriz");
    if (this._sub) {
      this._sub.unsubscribe();
    }

  } 

  formularioEvaluar(idProyectoVersion: number): void {
    //this.idProyectoVersion=null;
   // this.createStructureForDynamicsTabs();
    //this.tabsCategoriaPregunta = jqwidgets.createInstance('#myTabs', 'jqxTabs', this.configurationTabs);
    console.log('1.1');
    this.cabMatriz.id=null;
    this.cabMatriz.fechaDesde=null;
    this.cabMatriz.fechaHasta=null;
    if (this.preguntaInstrumento.length > 0 || this.preguntaInstrumento != undefined) {
      console.log("idProyectoVersion " + idProyectoVersion);
      console.log('1.2');
      this.formEvaluar.show();//Muesta el modal
      this.formEvaluar.config = this.config;//Establece las configuraciones para el modal    
      this.llenarCapMatriz(idProyectoVersion);
      this.idProyectoVersion = idProyectoVersion;
      this.proyecto.id = idProyectoVersion;
      console.log('Editar detalle matriz');
      this.listarDetMatriz(this.idProyectoVersion);
      this.listarCodigoBd(this.idProyectoVersion);
    }
  }

  listarCodigoBd(idProyectoVersion:any) {
    this._sub = this.periodoConvocatoria.buscarCodigoProyecto(idProyectoVersion).subscribe(data => {
      console.log("codigo"+data);
      if (data.codigo != 'NO-APR') {
        this.checkBoxAprobar.checked(true);
        this.mostrarCodigo = false;
        this.proyecto.codigo = data.codigo
      }
    }, error => console.error("Error al cargar codigo de proyecto " + error));
  }

  listarDetMatriz(idCabMatriz: any) {
    this._sub = this.periodoConvocatoria.buscarDetMatriz(idCabMatriz).subscribe(data => {
      console.log("detalle"+JSON.stringify(data));
      if (data.length > 0) {
        console.log('1.4');
        this.detMatriz=data;
        this.cabMatriz.id=data[0].idCabMatriz;
        this.cabMatriz.estadoMatriz=data[0].estadoMatriz;
        this.cabMatriz.fechaDesde = data[0].fechaDesde;
        this.cabMatriz.fechaHasta = data[0].fechaHasta;
        this.llenarDetMatriz(data);
      }else{
        console.log('1.5');
        this.tabsCategoriaPregunta = jqwidgets.createInstance('#myTabs', 'jqxTabs', this.configurationTabs);
    
      }
    }, error => console.error("Error al cargar detalle matriz " + error));
  }


  llenarCapMatriz(idProyectoVersion: number) {
    this._sub = this.periodoConvocatoria.buscarCabProyecto(idProyectoVersion).subscribe(data => {
      console.log("cabecera"+JSON.stringify(data));
      if (data.length > 0) {
        console.log('1.3');
        this.cabMatriz.departamento = data[0].departamento;
        this.cabMatriz.departamento.toUpperCase();
        this.cabMatriz.nombreProyecto = data[0].titulo;
        this.cabMatriz.oferta = data[0].oferta;
        this.cabMatriz.fechaDesdeINTZ = data[0].fechaDesde;
        this.cabMatriz.directorProyecto = data[0].nombres;
        this.cabMatriz.fechaHastaINFZ = data[0].fechaHasta;
        this.cabMatriz.idCabMatriz = data[0].idCabMatriz;
        console.log('ddd ' + this.cabMatriz.departamento);
      }
    }, error => console.error("h " + error));
  }

  /*************************************************************************************************************** */
  //**************************lLISTAR PREGUNTAS INSTRUMENTO*************************************************************

  listarPreguntasInstrumentoVin(idInstrumento: any) {
    this._sub = this.periodoConvocatoria.buscarPreguntasInstrumentoVin(idInstrumento).subscribe(data => {
      console.log('PREGUNTAS'+JSON.stringify(data));
      if (data.length > 0) {
        //pasamos las preguntas
        console.log('1');
        this.preguntaInstrumento = data;
        this.listarCategoriaPregunta(data);
        this.createStructureForDynamicsTabs();
        //this.tabsCategoriaPregunta = jqwidgets.createInstance('#myTabs', 'jqxTabs', this.configurationTabs);
        

      }
    }, error => console.error("h " + error));

  }

  /**Retorna las opciones con sus respuesta para la evaluacion del instrumento  */
  /*mostrarOpcionesRespuesta(data: any) {
    var jsonObjectArray = [];
    for (let i = 0; i < data.length; i++) {
      var resultado = data[i].respuesta;
      var vectorRespuesta = resultado.split(",");

      for (let j = 0; j < vectorRespuesta.length; j++) {
        var vectorOpcion = vectorRespuesta[j];
        var vector = vectorOpcion.split("=");
        var v_respuesta = vector[0];
        var v_valor = vector[1];

        if (jsonObjectArray.length > 0) {

          var contadorSI_NO = 0;
          var contador = 0;
          for (let k = 0; k < jsonObjectArray.length; k++) {
            //valida si la respuesta de SI y NO ya estan en el arreglo de opciones
            if (vectorOpcion.trim().toLowerCase() == jsonObjectArray[k].descripcion.trim().toLowerCase()) {
              contadorSI_NO++;
            } else {
              if (v_valor == jsonObjectArray[k].valor && v_respuesta.trim().toLowerCase() == jsonObjectArray[k].descripcion.trim().toLowerCase()) {
                contador++;
              }
            }
          }

          if (vectorOpcion.trim().toLowerCase() == "si" || (vectorOpcion.trim().toLowerCase() == "no") || (vectorOpcion.trim().toLowerCase() == "parcial")) {
            if (contadorSI_NO == 0) {
              jsonObjectArray.push({ valor: vectorOpcion.trim(), descripcion: vectorOpcion.trim() });
            }
          } else {
            if (contador == 0) {
              jsonObjectArray.push({ valor: v_valor, descripcion: v_respuesta.trim() });
            }
          }
        } else {

          if (vectorOpcion.trim().toLowerCase() == "si" || (vectorOpcion.trim().toLowerCase() == "no") || (vectorOpcion.trim().toLowerCase() == "parcial")) {
            jsonObjectArray.push({ valor: vectorOpcion.trim(), descripcion: vectorOpcion.trim() });
          } else {
            jsonObjectArray.push({ valor: v_valor, descripcion: v_respuesta.trim() });
          }
        }
      }
    }
    this.opcionRespuesta = jsonObjectArray;
  }*/

  listarCategoriaPregunta(data: any) {
    console.log('2');
    var jsonObjectArray = [];
    for (let i = 0; i < data.length; i++) {
      var idCategoria = data[i].idCategoriaPregunta;
      var categoria = data[i].categoria;
      if (jsonObjectArray.length > 0) {
        var contador = 0;
        for (let j = 0; j < jsonObjectArray.length; j++) {
          if (categoria.trim().toLowerCase() == jsonObjectArray[j].categoria.trim().toLowerCase()) {
            contador++;
          }
        }
        if (contador == 0) {
          jsonObjectArray.push({ idCategoriaPregunta: idCategoria, categoria: categoria.trim() });
        }
      } else {
        jsonObjectArray.push({ idCategoriaPregunta: idCategoria, categoria: categoria.trim() });
      }
    }
    this.categoriaPregunta = jsonObjectArray;
  }

  createStructureForDynamicsTabs() {
    console.log('3');
    let _ul = document.createElement('ul')
    let _principal = document.getElementById('myTabs')
    document.getElementById('myTabs').appendChild(_ul)

    for (let i = 0; i < this.categoriaPregunta.length; i++) {
      let _itemName = this.categoriaPregunta[i].categoria
      let _li = _ul.appendChild(document.createElement('li'))
      _li.innerHTML = _itemName;
      let _div = _principal.appendChild(document.createElement('div'))
      _div.id = "contentGridComponent" + i;
      let _ContentGrid = document.getElementById("contentGridComponent" + i).appendChild(document.createElement('div'))
      _ContentGrid.id = "jqxGrid" + i;
      _div.style.cssText = "overflow: hidden;margin: 10px 10px;";     
    }
    setTimeout(() => {
      console.log('4');
      this.tabsCategoriaPregunta = jqwidgets.createInstance('#myTabs', 'jqxTabs', this.configurationTabs);   
    });    
  }

  configurationTabs: jqwidgets.TabsOptions = {
    theme: 'darkblue',
    selectionTracker: false,
    initTabContent: (tab) => {
      console.log('5');
      this.initGrid(tab);
    }
  }

  initGrid(tab: any) {
    console.log('6');
    let source: any = {};
    let _Grid = jqwidgets.createInstance("#jqxGrid" + tab, 'jqxGrid');
    let dataFusion = this.filtrarPreguntaByCategoria(tab);

    source = {
      datafields: [
        { name: 'idDetMatriz', type: 'number' },
        { name: 'idPregunta', type: 'number' },
        { name: 'codigo', type: 'string' },
        { name: 'descripcion', type: 'string' },
        { name: 'idCategoriaPregunta', type: 'number' },
        { name: 'categoria', type: 'string' },
        { name: 'nota', type: 'string' },
        { name: 'opciones', type: 'array' },
        { name: 'observacion', type: 'string' }
      ],
      localdata: dataFusion
    };

    let dataAdapter = new jqx.dataAdapter(source);

    let settingGrid = {
      source: dataAdapter,
      width: "100%",
      editable: true,
      theme: 'darkblue',
      autoheight: true,
      autorowheight: true,
      columnsresize: true,
      columns: [
        {
          text: 'N°', editable: false, datafield: 'indice', columntype: 'number', width: '5%', cellsalign: 'center', align: 'center',
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:8px;'>" + (value + 1) + "</div>";
          }
        },
        { text: 'Id', datafield: 'idDetMatriz', editable: false, hidden: true },
        { text: 'Id', datafield: 'idPregunta', editable: false, hidden: true },
        { text: 'Codigo', datafield: 'codigo', hidden: true },
        { text: 'Pregunta', datafield: 'descripcion', width: "50%", editable: false, align: 'center' },
        { text: 'idCategoria', datafield: 'idCategoriaPregunta', hidden: true },
        { text: 'Categoria', datafield: 'categoria', hidden: true },
        {
          text: 'Opciones', datafield: 'nota', width: '15%', cellsalign: 'center', align: 'center', columntype: 'dropdownlist',
          enableHover: true,
          initeditor: (row: number, value: any, editor: any): void => {
            var opcionesByRow = dataAdapter._source.localdata[row].opciones;
            editor.jqxDropDownList({
              autoDropDownHeight: true,
              source: opcionesByRow,
              autoOpen: true,
              displayMember: 'descripcion',
              valueMember: 'valor',
              animationType: 'slide', enableHover: true, autoItemsHeight: true
            });
          },
          cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
            console.log(row);
          },

          /*cellsrenderer: function (row, column, value) {
            return "<div style='padding:5px;border:1px solid #004a73;border-radius:5px;width:auto;height:auto;'>" + value + "</div>";
          }*/
        },
        { text: 'Observacion', datafield: 'observacion', width: '29%' }
      ]
    }

    _Grid.setOptions(settingGrid);

    let arrayTabs: any = {};
    arrayTabs['grid'] = _Grid;
    arrayTabs['idTab'] = this.categoriaPregunta[tab].idCategoriaPregunta;
    arrayTabs['nameTab'] = this.categoriaPregunta[tab].categoria;
    this.arrayGrids.push(arrayTabs);
    if(this.detMatriz != undefined){
      this.llenarDetMatriz(this.detMatriz);
    }
  }

  filtrarPreguntaByCategoria(tab: any): any[] {
    console.log('6');
    let arrayObject = [];
    let idCategoria = this.categoriaPregunta[tab].idCategoriaPregunta.toString();
    for (let i = 0; i < this.preguntaInstrumento.length; i++) {
      if (this.preguntaInstrumento[i].idCategoriaPregunta == idCategoria) {
        var resultado = this.preguntaInstrumento[i].respuesta;
        var vectorRespuesta = resultado.split(",");
        let arrayOpcionesObject = [];
        for (let j = 0; j < vectorRespuesta.length; j++) {
          var vectorOpcion = vectorRespuesta[j];
          vectorOpcion = vectorOpcion.split("/");


          if (vectorOpcion[0].trim().toLowerCase() == "si" || (vectorOpcion[0].trim().toLowerCase() == "no") || (vectorOpcion[0].trim().toLowerCase() == "parcial")) {
            arrayOpcionesObject.push({ valor: vectorOpcion[1].trim(), descripcion: vectorOpcion[0].trim() });
          } else {
            var vector = vectorOpcion.split("=");
            var v_respuesta = vector[0];
            var v_valor = vector[1];
            arrayOpcionesObject.push({ valor: v_valor, descripcion: v_respuesta.trim() });
          }
        }
        arrayObject.push({
          idPregunta: this.preguntaInstrumento[i].idPregunta,
          codigo: this.preguntaInstrumento[i].codigo,
          descripcion: this.preguntaInstrumento[i].descripcion,
          idCategoriaPregunta: this.preguntaInstrumento[i].idCategoriaPregunta,
          categoria: this.preguntaInstrumento[i].categoria,
          nota: 'Seleccione',
          opciones: arrayOpcionesObject
        });
      }
    }
    return arrayObject;
  }

  /*************************************************************************************************************** */
  //*************************DATA PARA EL MENU *************************************************************
  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Grabar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'text': 'Regresar',
      'id': '2',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'text': 'llenar si',
      'id': '3',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
  ];

  // prepare the data
  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };
  getAdapter(sourceMenu: any): any {
    // create data adapter and perform data
    return new jqx.dataAdapter(this.sourceMenu, { autoBind: true });
  };

  menus = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
  itemclick(event: any): void {
    var opt = event.args.innerText;
    switch (opt) {
      case 'Grabar':
        if(!this.checkBoxAprobar.checked() && !this.checkBoxModificar.checked() && !this.checkBoxNoAprobar.checked()){
          let jsonCabMatriz=this.formarJson();
          this.saveMatrizExAnte(jsonCabMatriz);
        }else{
          this.formModalProyecto.questionSioNo({ title: 'Matriz Ex-Ante', msg: '¿Desea enviar la matriz evaluada al Director de Proyecto?' });         
        }        
      break;
      case 'Regresar':
        //this.myModal.alertQuestion({title:'Regresar',msg:'Está seguro que quiere abandonar la evalución?'});//REDIRECCIONA AL METODO --> show();
        //this.formEvaluar.dismissReason()
        //this.formEvaluar.nativeElement.click();
        this.formEvaluar.ngOnDestroy();
      break;
      case 'llenar si':
        this.llenarSi();
      break;
      default:

    }
  }
  saveMatrizExAnte(jsonCabMatriz: { id: any; usuarioIngresoId: any; estado: string; fechaDesde: any; fechaHasta: any; estadoMatriz: string; idProyectoVersion: any; detalleMatriz: any[]; }) {
    this._sub = this.periodoConvocatoria.grabarCabMatriz(jsonCabMatriz).subscribe(() => {
      this.myModal.alertMessage({ title: 'Matriz Ex-Ante', msg: "Información guardada con exito!" });
      this.proyecto={};
      this.PassMethods.emit({ val: 'actualizar' });
      this.formEvaluar.ngOnDestroy();
    }, error => {
      this.myModal.alertMessage({ title: 'Error', msg: "Ha ocurrido un error al momento de registrar la evaluación del docente, por favor intente mas tarde o contáctese con el administrador del sistema." });
    })
  }
  

  procesarSioNo(event:any){
    if(event.val=='Si'){      
      if (this.checkBoxAprobar.checked()) {
        this.estadoMatriz = 'APROBADO';
        this.proyecto.estadoProyecto = 'APROBADO';
        let jsonCabMatriz=this.formarJson();
        this.saveMatrizExAnte(jsonCabMatriz);
      }
      if (this.checkBoxModificar.checked()) {
        this.proyecto.estadoProyecto = 'MODIFICAR';
        this.estadoMatriz = 'MODIFICAR';
        let jsonCabMatriz=this.formarJson();
        this.saveMatrizExAnte(jsonCabMatriz);
      }
      this.guardarCodigoProyecto();         
    }
    if(event.val=='No'){
      this.proyecto.estadoProyecto = null;
      this.guardarCodigoProyecto(); 
      this.estadoMatriz = 'REVISAR';
      let jsonCabMatriz=this.formarJson();
      this.saveMatrizExAnte(jsonCabMatriz);
    }
  }

  guardarCodigoProyecto() {
    this._sub = this.periodoConvocatoria.grabarCodigoProyecto(this.proyecto).subscribe(() => {      
    }, error => {
      this.myModal.alertMessage({ title: 'Error', msg: "Ha ocurrido un error al momento de registrar el código del proyecto, por favor intente mas tarde o contáctese con el administrador del sistema." });
    })
  }


  /*************************************************************************************************************** */
  //*************************GUARDAR MATRIZ EX-ANTE *************************************************************
  llenarSi() {
    var tabGrid = null;
    var tabsDiv = document.getElementById('myTabs');
    var divUl = tabsDiv.getElementsByTagName('ul');
    var divLi = divUl[0].children;

    var jsonDetMatriz = [];

    for (let i = 0; i < divLi.length; i++) {
      var componente = divLi[i].textContent;
      var contador = 0;
      for (let j = 0; j < this.arrayGrids.length; j++) {
        var nameTab = this.arrayGrids[j].nameTab
        console.log(this.arrayGrids[j].nameTab);
        if (componente == nameTab) {
          tabGrid = this.arrayGrids[j].grid;
          contador++;
        }
      }
      if (contador > 0) {
        let _grid: jqwidgets.jqxGrid = tabGrid
        let rows = _grid.getrows();
        //peter
        for (let indice = 0; indice < rows.length; indice++) {
          //console.log('1 ' + rows[indice].nota);
          rows[indice].nota = 'si';
          //console.log('2 ' + rows[indice].nota);
        }
        //fin peter---
        for (let indice = 0; indice < rows.length; indice++) {
          var valorSeleccionado = rows[indice].nota.trim();
          if (valorSeleccionado == null || valorSeleccionado == 'Seleccione' || valorSeleccionado == '') {
            var mensaje = 'En la pestaña "' + componente + '" de la pregunta "N° ' + (indice + 1) + '" no ha seleccionado un valor. \n Por favor seleccione.';
            this.myModal.alertMessage({ title: 'Información', msg: mensaje });
            return;
          } else {
            var respuesta = null;
            if (valorSeleccionado.toLowerCase() == "si" || valorSeleccionado.toLowerCase() == "no" || valorSeleccionado.toLowerCase() == "parcial") {
              respuesta = valorSeleccionado;
              valorSeleccionado = null;
            }

            jsonDetMatriz.push({
              id: null,
              idCabEvaluacionDocente: null,
              idOpcionPregunta: rows[indice].idOpcionPregunta,
              valor: valorSeleccionado,
              observacion: rows[indice].observacion,
              respuestaSiNo: respuesta,
              estado: 'A',
              usuarioIngresoId: this.usuarioActual.id
            });
          }
        }
      } else {
        this.myModal.alertMessage({ title: 'Información', msg: "Debe contestar las pregunta de la pestaña " + componente });
        return;
      }
    }
    this.prueba = this.arrayGrids;

  }
  //llamar esta funcion cuando da check aprobado o modificar
  formarJson() {
    var tabGrid = null;
    var tabsDiv = document.getElementById('myTabs');
    var divUl = tabsDiv.getElementsByTagName('ul');
    var divLi = divUl[0].children;

    var jsonDetMatriz = [];

    for (let i = 0; i < divLi.length; i++) {
      var componente = divLi[i].textContent;
      var contador = 0;
      for (let j = 0; j < this.arrayGrids.length; j++) {
        var nameTab = this.arrayGrids[j].nameTab
        console.log(this.arrayGrids[j].nameTab);
        if (componente == nameTab) {
          tabGrid = this.arrayGrids[j].grid;
          contador++;
        }
      }
      if (contador > 0) {
        let _grid: jqwidgets.jqxGrid = tabGrid
        let rows = _grid.getrows();
        for (let indice = 0; indice < rows.length; indice++) {
          var valorSeleccionado = rows[indice].nota.trim();
          var respuesta = null;
          //var valorSeleccionado1 = rows[indice].opciones;
          if (valorSeleccionado == null || valorSeleccionado == 'Seleccione' || valorSeleccionado.toLowerCase() == '') {
          }else{
            switch (valorSeleccionado.toLowerCase()) {
              case 'si':
                respuesta = rows[indice].opciones[0].valor;
                valorSeleccionado = null;
                break;
              case 'no':
                respuesta = rows[indice].opciones[1].valor;
                valorSeleccionado = null;
                break;
              case 'parcial':
                respuesta = rows[indice].opciones[2].valor;
                valorSeleccionado = null;
                break;
            }
           
            jsonDetMatriz.push({
              id: rows[indice].idDetMatriz,
              idOpcionPregunta: respuesta,
              observacion: rows[indice].observacion,
              estado: 'A',
              usuarioIngresoId: this.usuarioActual.id
            });
          }
        }
      } 
    
    }
    if(this.estadoMatriz == undefined){
      this.estadoMatriz = "INICIAL"
    }    
    if (this.cabMatriz.fechaDesde == null) {
      let fecha = new Date()
      this.cabMatriz.fechaDesde = fecha;
    }
    if (this.cabMatriz.fechaHasta == null) {
      let fecha = new Date()
      this.cabMatriz.fechaHasta = fecha;
    }
    if (this.checkBoxAprobar.checked()) {
      let fecha = new Date()
      this.cabMatriz.fechaHasta = fecha;
    }
    if (this.checkBoxModificar.checked()) {
      this.cabMatriz.fechaHasta = this.myfechaEntrega.value();
    }

    var jsonCabMatriz = {
      id: this.cabMatriz.id,
      usuarioIngresoId: this.usuarioActual.id,
      estado: 'A',
      fechaDesde: this.cabMatriz.fechaDesde,
      fechaHasta: this.cabMatriz.fechaHasta,
      estadoMatriz: this.estadoMatriz,
      idProyectoVersion: this.idProyectoVersion,
      detalleMatriz: jsonDetMatriz
    };
    console.log(jsonCabMatriz);
   // this.listarCategoriaPregunta(10);
    //this.formEvaluar.setPristine();
   
    if(jsonDetMatriz.length>0){
      /*this._sub = this.periodoConvocatoria.grabarCabMatriz(jsonCabMatriz).subscribe(() => {

      this.myModal.alertMessage({ title: 'Matriz Ex-Ante', msg: "Información guardada con exito!" });
      this.PassMethods.emit({ val: 'actualizar' });
      this.formEvaluar.ngOnDestroy();
    }, error => {
      this.myModal.alertMessage({ title: 'Error', msg: "Ha ocurrido un error al momento de registrar la evaluación del docente, por favor intente mas tarde o contáctese con el administrador del sistema." });
    })*/
    }else{
      this.myModal.alertMessage({ title: 'Información', msg: "Debe contestar las pregunta " });
      return;
    }
    return jsonCabMatriz;
  }

  /*************************************************************************************************************** */
  //************************* Llena detalle matriz opcion editar *************************************************************

  llenarDetMatriz(data: any) {
    var tabGrid = null;
    var tabsDiv = document.getElementById('myTabs');
    var divUl = tabsDiv.getElementsByTagName('ul');
    var divLi = divUl[0].children;

   
    for (let i = 0; i < divLi.length; i++) {
      var componente = divLi[i].textContent;
      var contador = 0;
      for (let j = 0; j < this.arrayGrids.length; j++) {
        var nameTab = this.arrayGrids[j].nameTab
        console.log(this.arrayGrids[j].nameTab);
        if (componente == nameTab) {
          tabGrid = this.arrayGrids[j].grid;
          contador++;
        }
      }
      if (contador > 0) {
        let _grid: jqwidgets.jqxGrid = tabGrid
        let rows = _grid.getrows();
        for (let indice = 0; indice < rows.length; indice++) {
          for (let indOpciones = 0; indOpciones < rows[indice].opciones.length; indOpciones++) {
            for (let contIndice = 0; contIndice < data.length; contIndice++) {
              if (rows[indice].opciones[indOpciones].valor == data[contIndice].idOpcionPregunta) {
                rows[indice].nota = rows[indice].opciones[indOpciones].descripcion;
                rows[indice].idDetMatriz=data[contIndice].id;
                rows[indice].observacion=data[contIndice].observacion;
                break;
              }
              
            }
            
          }
        }
      }
    }
    /*//sirve para llenar si
    var contIndice = -1;
    for (let j = 0; j < this.arrayGrids.length; j++) {
      console.log(this.arrayGrids[j].nameTab);
      tabGrid = this.arrayGrids[j].grid;
      let _grid: jqwidgets.jqxGrid = tabGrid
      let rows = _grid.getrows();
      for (let indice = 0; indice < rows.length; indice++) {
        contIndice = contIndice + 1;
        for (let indOpciones = 0; indOpciones < rows[indice].opciones.length; indOpciones++) {
          if (rows[indice].opciones[indOpciones].valor == data[contIndice].idOpcionPregunta) {
            rows[indice].nota = rows[indice].opciones[indOpciones].descripcion;
          }
        }
      }
    }*/
    /*
    var tabsDiv = document.getElementById('myTabs');
    var divUl = tabsDiv.getElementsByTagName('ul');
    var divLi = divUl[0].children;
    var contIndice=-1;
    for (let i = 0; i < divLi.length; i++) {
      var componente = divLi[i].textContent;
      var contador = 0;
      for (let j = 0; j < this.arrayGrids.length; j++) {
        var nameTab = this.arrayGrids[j].nameTab
        console.log(this.arrayGrids[j].nameTab);
        if (componente == nameTab) {
          tabGrid = this.arrayGrids[j].grid;
          contador++;
        }
      }
      if (contador > 0) {
        let _grid: jqwidgets.jqxGrid = tabGrid
        let rows = _grid.getrows();
        for (let indice = 0; indice < rows.length; indice++) {
          contIndice=contIndice+1;
          for (let indOpciones = 0; indOpciones < rows[indice].opciones.length; indOpciones++) {
            if(rows[indice].opciones[indOpciones].valor==data[contIndice].idOpcionPregunta){
              rows[indice].nota=rows[indice].opciones[indOpciones].descripcion;
            }
          }        
        }
      }
    }*/
  }
  /*************************************************************************************************************** */
  //************************* eventos de check aprobado modificado *************************************************************

  checkAprobado(event: any): void {
    let checked = event.args.checked;
    //this.dropDownPrograma.disabled(!this.ban);
    if (checked) {
      let aprobado = this.aprobarProyecto(checked);      
      if(aprobado == 'no_si'){
        this.checkBoxAprobar.checked(!checked);
      }else{
        this.mostrarCodigo = !aprobado;
        this.checkBoxAprobar.checked(aprobado);
      }
    }
  }

  checkModificar(event: any): void {
    let checked = event.args.checked;
    //this.dropDownPrograma.disabled(!this.ban);
    if (checked) {
      let aprobado = this.aprobarProyecto(checked);
      if(aprobado == 'no_si'){
        this.checkBoxModificar.checked(!checked);
      }else{
        this.mostrarFechaRecepcion = aprobado;
        this.checkBoxModificar.checked(!aprobado);
      }
      
    }
  }

  aprobarProyecto(checked:boolean) {
    let aprobar;
    var tabGrid = null;
    var tabsDiv = document.getElementById('myTabs');
    var divUl = tabsDiv.getElementsByTagName('ul');
    var divLi = divUl[0].children;
    
    for (let i = 0; i < divLi.length; i++) {
      var componente = divLi[i].textContent;
      var contador = 0;
      for (let j = 0; j < this.arrayGrids.length; j++) {
        var nameTab = this.arrayGrids[j].nameTab
        //console.log(this.arrayGrids[j].nameTab);
        if (componente == nameTab) {
          tabGrid = this.arrayGrids[j].grid;
          contador++;
        }
      }
      if (contador > 0) {
        let _grid: jqwidgets.jqxGrid = tabGrid
        let rows = _grid.getrows();
        for (let indice = 0; indice < rows.length; indice++) {
          var valorSeleccionado = rows[indice].nota.trim();
          //var valorSeleccionado1 = rows[indice].opciones;
          if (valorSeleccionado.toLowerCase() != 'si') {
            this.myModal.alertMessage({ title: 'Información', msg: 'El proyecto no cumple con los parámetros necesarios para ser aprobado' });
            //this.mostrarFechaRecepcion = !checked;
            //this.mostrarCodigo = checked;
            //this.checkBoxAprobar.checked(!checked);
            //this.checkBoxModificar.checked(checked);
            this.checkBoxNoAprobar.checked(!checked);
            this.estadoMatriz = "MODIFICAR"
            return aprobar=false;
          }else{
            if(componente==divLi[divLi.length-1].textContent){
              this.myModal.alertMessage({ title: 'Información', msg: 'El proyecto puede ser aprobado' });
              aprobar=true;
              //this.mostrarFechaRecepcion = checked;
             // this.mostrarCodigo = !checked;
             // this.checkBoxAprobar.checked(checked);
              //this.checkBoxModificar.checked(!checked);
              this.checkBoxNoAprobar.checked(!checked);
              this.estadoMatriz = "APROBADO"
            }else{
              aprobar='no_si';
              //this.checkBoxAprobar.checked(!checked);
              //this.checkBoxModificar.checked(!checked);
              //this.mostrarFechaRecepcion = checked;
            }
          }
        }
      } else {
        this.myModal.alertMessage({ title: 'Información', msg: "Diríjase a la sección: " + componente });
        aprobar='no_si';
        //this.checkBoxAprobar.checked(false);
        //this.checkBoxModificar.checked(false);
        //this.mostrarFechaRecepcion = true;
        return aprobar;
      }

    }
    return aprobar;
  }

  

}
