import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-ng/jqxvalidator';
import { jqxTextAreaComponent } from 'jqwidgets-ng/jqxtextarea';
import { MallasService } from '../../../../services/mallas/mallas.service';
import { VinculacionService } from '../../../../services/vinculacion/vinculacion.service';
import { ModalComponentComponent } from '../../../modal-view/modal-component/modal-component.component';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-modal-convocatoria',
    templateUrl: './modal-convocatoria.component.html',
    styleUrls: ['./modal-convocatoria.component.scss']
})
export class ModalConvocatoriaComponent implements OnInit {

    @ViewChild('dropDownPeriodo') dropDownPeriodo: jqxDropDownListComponent;
    @ViewChild('formNuevo') private formNuevo: ModalDirective;
    @ViewChild('myValidator') myValidator: jqxValidatorComponent;
    @ViewChild('myfechaDesde') myfechaDesde: jqxDateTimeInputComponent;
    @ViewChild('myfechaHasta') myfechaHasta: jqxDateTimeInputComponent;
    @ViewChild('myinputDescripcion') myinputDescripcion: jqxTextAreaComponent;
    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias

    @Output() PassMethods: EventEmitter<any> = new EventEmitter();
    opcion: any = {};
    /**Permite controlar que no salga del modal sin presionar algun boton de éste */
    protected config = { backdrop: true, ignoreBackdropClick: true };
    idPeriodoAcademico: any;
    modalConvocatoriaSubscription: Subscription;
    validar: boolean;

    constructor(
        // private router: Router, 
        //private route: ActivatedRoute,
        private mallasService: MallasService,
        private periodoConvocatoria: VinculacionService,
        private datePipe: DatePipe
        //private evaluacionService: DesempenoDocenteService,

    ) { }

    ngOnInit(): void {
        this.ListarPeriodo();
        //this.limpiar();
    }

    limpiar() {
        this.opcion.descripcion = " ";
        this.opcion.fechaDesde = null;
        this.opcion.fechaHasta = null;
        this.dropDownPeriodo.clearSelection();
        this.opcion.id = null;
        this.idPeriodoAcademico = null;
        this.myfechaDesde = null;
        this.myfechaHasta = null;
    }

    ListarPeriodo() {
        this.modalConvocatoriaSubscription = this.mallasService.listarPeriodo().subscribe( (data)=> {
            //console.log(JSON.stringify(data));
            this.sourcePeriodo.localdata = data;
            this.dataAdapterPeriodo.dataBind();
            this.modalConvocatoriaSubscription.unsubscribe();
        });
    }

    sourcePeriodo: any = {
        datatype: 'json',
        id: 'id',
        localdata: [
            { name: 'estado', type: 'string' },
            { name: 'descripcion', type: 'string' },
            { name: 'id', type: 'int' }
        ],
    };
    //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
    dataAdapterPeriodo: any = new jqx.dataAdapter(this.sourcePeriodo);


    protected Submit() {
        if (this.funValidar() == true) {
            this.guardar();
            this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Datos Guardados Correctamente!' });
            //this.formNuevo.hide();      
            this.formNuevo.ngOnDestroy();
        }
    }

    protected cancelar() {
        this.limpiar();
        this.formNuevo.ngOnDestroy();
    }

    guardar() {
        try {
            var datosConvocatoria: any = {};
            datosConvocatoria.id = this.opcion.id;
            datosConvocatoria.idPeriodoAcademico = this.idPeriodoAcademico;
            datosConvocatoria.descripcion = this.opcion.descripcion;
            datosConvocatoria.idDepartamento=15;
            datosConvocatoria.version = 1;
            datosConvocatoria.fechaDesde = this.opcion.fechaDesde;
            datosConvocatoria.fechaHasta = this.opcion.fechaHasta;
            datosConvocatoria.estado = 'A';
            datosConvocatoria.usuarioIngresoId = '1';
            //json
            console.log(JSON.stringify(datosConvocatoria));
            this.modalConvocatoriaSubscription = this.periodoConvocatoria.grabarConvocatoria(datosConvocatoria).subscribe(result =>  {
                //para actulizar grid del padre
                //this.a = "actualizar";
                this.PassMethods.emit({ val: "actualizar" });
                this.modalConvocatoriaSubscription.unsubscribe();
            }, function (error) { return console.error("h " + error); });
            this.limpiar();
        }
        catch (error) {
            console.error('Here is the error message', error);
        }
        console.log('Execution continues');
    }

    funValidar() {
        this.validar = true;
        if (this.idPeriodoAcademico == null) {
            this.validar = false;
            this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Seleccione un Periodo Académico!' });
        }
        if (this.opcion.descripcion == " ") {
            this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Ingrese una descripción!' });
            this.validar = false;
        }
        if (this.opcion.fechaDesde == null) {
            this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Ingrese una Fecha Inicio!' });
            this.validar = false;
        }
        if (this.opcion.fechaHasta == null) {
            this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Ingrese una Fecha Fin!' });
            this.validar = false;
        }
        if ((this.opcion.fechaDesde != null) && (this.opcion.fechaHasta != null)) {
            var dateFechadesde = this.opcion.fechaDesde;
            var dateFechaHasta = this.opcion.fechaHasta;
            //convierte las fechasen numeros
            var d = Date.parse(dateFechadesde);
            var h = Date.parse(dateFechaHasta);
            if (d >= h) {
                console.log("mayor " + dateFechadesde);
                this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Fecha Fin debe ser mayor a fecha Inicial!' });
                this.validar = false;
            }
        }
        return this.validar;
    }

    itemSeleccionado(event: any): void {
        if (event.args) {
            var item = event.args.item;
            if (item) {
                this.idPeriodoAcademico = item.value;
            }
        }
    }
    onChange(){
        console.log('fecha '+this.myfechaDesde.val());
        var Fecha = new Date(this.myfechaDesde.val());
        if(this.myfechaDesde.val() !=undefined){
            Fecha.setDate(Fecha.getDate()+ 16)
            this.myfechaHasta.val(this.datePipe.transform(Fecha,"yyyy-MM-dd"));
            console.log('datedd ' + this.datePipe.transform(Fecha,"yyyy-MM-dd"))
        }
        
    }

    formularioNuevo(): void {
        this.opcion.fechaDesde = null;
        this.opcion.fechaHasta = null;
        this.formNuevo.show(); //Muesta el modal
        this.formNuevo.config = this.config; //Establece las configuraciones para el modal
    }

    formularioEditar(opt:any): void {
        if (opt === void 0) { opt = {}; }
        this.opcion.fechaDesde = null;
        this.opcion.fechaHasta = null;
        this.opcion = opt;
        this.formNuevo.show(); //Muesta el modal
        this.formNuevo.config = this.config; //Establece las configuraciones para el modal
        this.dropDownPeriodo.val(this.opcion.idPeriodoAcademico);
    }
}
