import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConvocatoriaComponent } from './modal-convocatoria.component';

describe('ModalConvocatoriaComponent', () => {
  let component: ModalConvocatoriaComponent;
  let fixture: ComponentFixture<ModalConvocatoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConvocatoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConvocatoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
