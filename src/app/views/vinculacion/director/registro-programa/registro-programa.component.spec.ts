import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroProgramaComponent } from './registro-programa.component';

describe('RegistroProgramaComponent', () => {
  let component: RegistroProgramaComponent;
  let fixture: ComponentFixture<RegistroProgramaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroProgramaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroProgramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
