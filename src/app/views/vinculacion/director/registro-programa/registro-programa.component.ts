import { Component, OnInit, ViewChild } from '@angular/core';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { Router } from '@angular/router';

import { ModalProgramaComponent } from '../modal-programa/modal-programa.component';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';

@Component({
  selector: 'app-registro-programa',
  templateUrl: './registro-programa.component.html',
  styleUrls: ['./registro-programa.component.scss']
})
export class RegistroProgramaComponent implements OnInit {

  constructor(private router: Router) { }

  @ViewChild('dropDownFacultad')        dropDownFacultad:         jqxDropDownListComponent;
  @ViewChild('dropDownCarrera')         dropDownCarrera:         jqxDropDownListComponent ;
   
  @ViewChild('gridProgramasRegistrados') gridProgramasRegistrados: jqxGridComponent;
 // @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;
  @ViewChild(ModalProgramaComponent)  formModalPrograma: ModalProgramaComponent;

  //Variable paa cargar datos del objeto 
  listaProgramas: Array<any>;
  ngOnInit() {
    this.ListarAllProgramas();
    this.ListarFacultades();
    this.ListarCarreras();
  }

  ListarFacultades() {
    /* this.mallasService.listarPeriodo().subscribe(data => {
     // this.periodo = data;
      this.sourceFacultad.localdata = data;
      this.dataAdapterFacultad.dataBind();
    }) */
  }

  sourceFacultad: any = {
      datatype: 'json',
      id:'id',
      localdata: [
          { name: 'estado', type:'string'},
          { name: 'descripcion', type:'string'},
          { name: 'id', type: 'int' }
      ],
  };
  //CARGAR ORIGEN DEL COMBOBOX DE PERIODO
  dataAdapterFacultad: any = new jqx.dataAdapter(this.sourceFacultad);
  ListarCarreras(){
    /* this.mallasService.listarTipoOferta().subscribe(data =>  {
      this.sourceTipoOferta.localdata = data;
      this.dataAdapterTipoOferta.dataBind();
    }) */
  }
   //FUENTE DE DATOS PARA EL COMBOBOX DE OFERTA
   sourceCarrera: any ={
       datatype: 'json',
       id:'id',
       localdata:  [
        { name: 'id', type: 'int' },
        { name: 'descripcion', type:'string'},
        { name: 'estado', type:'string'}
           
       ],
   };
   //CARGAR ORIGEN DEL COMBOBOX DE OFERTA
   dataAdapterCarrera: any = new jqx.dataAdapter(this.sourceCarrera);



  //definición de estructura de datos del jqxMenu
  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Editar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Eliminar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },

  ];

  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };

  getAdapter(sourceMenu: any): any {
    return new jqx.dataAdapter(this.sourceMenu, { autoBind: true });
  };
  menuPrograma = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

  
  itemclick(event: any): void {
    //this.eventLog.nativeElement.innerHTML = 'Id: ' + event.args.id + ', Text: ' + event.args.innerText;
    var opt=event.args.innerText;
    switch (opt) {
        //Opciones de Crud  
        case 'Nuevo':
             //redirecciona a formulario de edicion nuevo
            // this.formModalPrograma.formularioNuevo(this.datosMalla);
            this.formModalPrograma.formularioNuevo();
          break;
          break;
        case 'Editar':
       
    
        /*   if (this.idmallaOferta==undefined)
          {  
          //  alert(this.idmallaOferta);
            this.formMantenedorMallaModal.alertMessage({title:'Mantenimiento de Mallas',msg:'Seleccione un registro para editar !'});
          }
          else {
            this.editaMalla="";
            this.mallasService.buscarMallaPorId(this.idmallaOferta).subscribe(data => {
              this.editaMalla = data;
       //       this.source.localdata = data;
             // this.dataAdapter.dataBind();       
          //    alert(JSON.stringify(this.editaMalla));
         this.formMantenedorMallaModal.formularioEditar(this.editaMalla);
           });    
              } */
          break; 
        case 'Eliminar':
          break;  
        default:
        //default code block
    }   
  };


  ListarAllProgramas() {
   /*  this.DocentesService.getListarTodosDocenteRegistrados().subscribe(data => {
      //alert(JSON.stringify(data));
      ;
      this.sourceDocentesRegistrados.localdata = data;
      this.dataAdapterDocentesRegistrados.dataBind();
    })
 */
  }

  eliminarPrograma(idPrograma:number){
    /* this.DocentesService.borrarDocente(idPersona).subscribe(result => {
    }, error => console.error(error)); */
  }

  columnsProgramas: any[] =
    [
      { text: 'Id Programa', datafield: 'idPrograma', width: '5%', filtertype: 'none', hidden: true },
      { text: 'Nombres', datafield: 'nombresCompletos', width: '40%' },
      { text: 'Fecha Inicio', datafield: 'fechaInicio', width: '12%', cellsalign: 'center', center: 'center' },
      { text: 'Fecha Fin', datafield: 'fechaFin', width: '33%' },
      /* { text: 'Id Docente', datafield: 'idDocente', width: '5%', hidden: true, filtertype: 'none' },
      { text: 'Hoja de vida',columntype: 'button', width: '15%',
        cellsrenderer: (): string => {
          return 'Descargar';
        },
        buttonclick: (row: number): void => {
            // let dataRecord = this.gridDocentesRegistrados.getrowdata(row);
            //this.descargaHojaDeVida(dataRecord.idDocente);  
        } , 
    
      }, */
    ];

}
