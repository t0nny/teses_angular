import { Component, OnInit, ViewChild,ViewEncapsulation, Output, EventEmitter, OnDestroy} from '@angular/core';




import { Router, ActivatedRoute } from '@angular/router';

import { jqxValidatorComponent } from 'jqwidgets-ng/jqxvalidator';
import { jqxInputComponent } from 'jqwidgets-ng/jqxinput';



//import { ListaParametricaComponent } from '../../../../../views/desempeno-docente/mantenimiento/parametrica/lista-parametrica/lista-parametrica.component';
import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';

import { jqxTextAreaComponent } from 'jqwidgets-ng/jqxtextarea';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { jqxDateTimeInputComponent } from 'jqwidgets-ng/jqxdatetimeinput';

@Component({
  selector: 'app-modal-programa',
  templateUrl: './modal-programa.component.html',
  styleUrls: ['./modal-programa.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ModalProgramaComponent implements OnInit,OnDestroy {
  
  //@ViewChild(ModalComponentComponent) modalComponent: ModalComponentComponent;
  
  
  @ViewChild('template') private template : ModalDirective;
  @ViewChild('templateQuestion') private templateQuestion : ModalDirective;
  @ViewChild('formEditar') private formEditar : ModalDirective;
  @ViewChild('formNuevo') private modalForm : ModalDirective;
  @ViewChild('myinputTitulo') private myinputTitulo : jqxInputComponent;
  
  @ViewChild('myValidator') myValidator: jqxValidatorComponent;
 
  

  @ViewChild('myHabFechas')        myHabFechas:        jqxCheckBoxComponent;
  @ViewChild('myfechaDesde')       myfechaDesde:       jqxDateTimeInputComponent;
  @ViewChild('myfechaHasta')       myfechaHasta:       jqxDateTimeInputComponent;
  @ViewChild('myinputDescripcion') myinputDescripcion: jqxTextAreaComponent;
  @Output() 
  
  protected PassMethods = new EventEmitter();//variable para escuchar eventos del componente

  constructor(protected modalService: BsModalService,
              //public messagerService: MessagerService,
              private router: Router, 
              private route: ActivatedRoute,
              //private evaluacionService: DesempenoDocenteService,
             // private validadorService: ValidadorService
  ) { }
  protected a:string;

  /**Permite controlar que no salga del modal sin presionar algun boton de éste */
  protected config = {backdrop: true,ignoreBackdropClick: true};
  //_sub: Subscription;
  parametricaAng: any = {};
  parametricaGeneral = {};
  usuarioActual: any = {};//variable para guardar el usuario actual
  labelOpcion: string;
  labelNombreTabla: string;
  ocultarComboRegla: boolean= true
  ocultarValorMin: boolean= true;
  ocultarValorMax: boolean= true;
  ocultarValor: boolean= true;
  //txtDescripcion ='';
   //variables estaticas para identificar las tablas parametricas
  static PAR_REGLA_CALIFICACION: string = 'REGCAL';
  static PAR_CATEGORIA_PREGUNTA: string = 'CATPRE';
  static PAR_INSTRUMENTO: string = 'INSTRU';
  static PAR_TIPO_PREGUNTA: string = 'TIPPRE';
  static PAR_TIPO_RECURSO_AP: string = 'TIPREC';
  static PAR_OPCION_RESPUESTA: string = 'OPCRES';
  ngOnInit() {
    //this.nameField.nativeElement.focus() 
   // this.clearParametros();
    //this.listarReglamento();
    //this.usuarioActual=JSON.parse(localStorage.getItem('usuarioActual')); //captura del usuario actual
  }
  ngOnDestroy(){}
  gotoList() {
    this.router.navigate(['/director/registro-programa']);
  }

  

  formularioNuevo(): void {
    //this.loadData(idParametricaPar, codigoTablaPar);
    //var opt = event.args.innerText;
    this.modalForm.show();//Muesta el modal
   /*  this.modalForm.config = this.config;//Establece las configuraciones para el modal
    this._sub = this.PassMethods.subscribe((h) =>{
      if(h.val == 'ok'){
        this.myValidator.validate(document.getElementById('formParametrica'));
        console.log("descrip : "+this.txtDescripcion.onChange)
        if (this.validaDatos()) { 
          console.log("descrip : "+this.txtDescripcion.val())
          this.myValidator.hide();
         this.modalComponent.alertQuestion({
            title: 'Registro de Paramétricas',
            msg: 'Desea grabar este registro?',
            result: (k) => {
              if (k) {
                //this.grabarTablaParametrica(this.parametricaAng, codigoTablaPar) 
              }
            }
          })
        } else {
          this.modalComponent.alertMessage({
            title: 'Registro de Paramétricas',
            msg: 'Verifique que todos los campos esten llenos correctamente!'
          });
        }
      };
    }); */
  }
  protected Submit(){
    
    this.a = 'ok';
    this.PassMethods.emit({val:this.a});
    this.formEditar.hide();
    //this.formNuevo.hide();
  }
  listOnSelect(event: any): void {
   /*  if (event.args) {
        let item = event.args.item;
        if (item) {
        //this.idofertaMalla=item.value  
        this.inputDetalle.val("MALLA DE "+item.label)
        }
    } */
};
checkChange(event: any): void {
};

  Change(event: any): void 
    {
      //console.log(this.txtDescripcion.val())
    //this.validaDatos()
    }

  nuevoRegistro() {
    //PARA ARMAR EL JASON 
    this.parametricaAng.id = null
    this.parametricaAng.estado = "A"
    this.parametricaAng.usuarioIngresoId = "1"
    //this.parametricaAng.usuarioIngresoId = this.usuarioActual.id
  }

  sourceTablasParametricas = [
   /*  { "tabla": "Regla Calificación Escala", "codigo": MantParametricaComponent.PAR_REGLA_CALIFICACION},
    { "tabla": "Categoria Pregunta", "codigo": MantParametricaComponent.PAR_CATEGORIA_PREGUNTA },
    { "tabla": "Instrumento de Evaluación", "codigo": MantParametricaComponent.PAR_INSTRUMENTO }, 
    { "tabla": "Tipo Pregunta", "codigo": MantParametricaComponent.PAR_TIPO_PREGUNTA }, 
    { "tabla": "Tipo Recurso Apelación", "codigo": MantParametricaComponent.PAR_TIPO_RECURSO_AP },
    { "tabla": "Opcion Respuesta", "codigo": MantParametricaComponent.PAR_OPCION_RESPUESTA }
     */
  ];

  mostrarLabelParametrica(codigoTabla:String){
    for (let i = 0; i < this.sourceTablasParametricas.length; i++) {
      if(this.sourceTablasParametricas[i].codigo == codigoTabla)
        this.labelNombreTabla= this.sourceTablasParametricas[i].tabla
    }
    console.log(this.labelNombreTabla);
  }
  //FUENTE DE DATOS PARA EL COMBOBOX DE TIPOS DE REGLAMENTO
  sourceReglamento: any =
  {
    datatype: 'json',
    id: 'id',
    localdata:
      [
        { name: 'id', type: 'int' },
        { name: 'nombre', type: 'string' }
      ]
  };

  //CARGAR ORIGEN DEL COMBOBOX DE TIPOS DE INGRESO
  dataAdapterReglamento: any = new jqx.dataAdapter(this.sourceReglamento);

  listarReglamento() {
   /*  this.evaluacionService.getReglamentos().subscribe(data => {
      this.sourceReglamento.localdata = data;
      this.dataAdapterReglamento.dataBind();
    }) */
  }

  recuperarDatosParametricas(idParametrica: number, codigoTabla:String) {
    console.log("idParametrica : "+idParametrica)
    switch (codigoTabla) {
     /*  case MantParametricaComponent.PAR_REGLA_CALIFICACION:       
        this.buscarReglaCalificacionEscala(idParametrica)
        break;
      case MantParametricaComponent.PAR_CATEGORIA_PREGUNTA:
        this.buscarCategoriaPregunta(idParametrica)        
        break;    
      case MantParametricaComponent.PAR_INSTRUMENTO:
        this.buscarInstrumento(idParametrica)
        break;
      case MantParametricaComponent.PAR_TIPO_PREGUNTA:
        this.buscarTipodePregunta(idParametrica)
        break;      
      case MantParametricaComponent.PAR_TIPO_RECURSO_AP:
        this.buscarTipoRecurso(idParametrica)
        break;  
      case MantParametricaComponent.PAR_OPCION_RESPUESTA:
        this.buscarOpcionRespuesta(idParametrica)
        break;    */
    }    
  }

  buscarReglaCalificacionEscala(idParametrica: number) {
    //console.log(this.evaluacionService.getBuscarReglaCalificacionEscala(idParametrica))
   /*  this.evaluacionService.getBuscarReglaCalificacionEscala(idParametrica).subscribe(data => {
      if (data) {            
        this.parametricaAng = data  
        this.comboReglamento.val(data.idReglamento)
      }
    }) */
  }
  
  buscarCategoriaPregunta(idParametrica: number) {
   /*  this.evaluacionService.getBuscarCategoriaPregunta(idParametrica).subscribe(data => {
      if (data){
        this.parametricaAng = data
        console.log(data)
      }      
    }) */
  }

  buscarInstrumento(idParametrica: number) {
   /*  this.evaluacionService.getBuscarInstrumento(idParametrica).subscribe(data => {
      if (data)
      this.parametricaAng= data
    }) */
  }

  buscarTipodePregunta(idParametrica: number) {
   /*  this.evaluacionService.getBuscarTipoPregunta(idParametrica).subscribe(data => {
      if (data)
      this.parametricaAng= data
    }) */
  }

  buscarTipoRecurso(idParametrica: number) {
    /* this.evaluacionService.getBuscarTipoRecursoApelacion(idParametrica).subscribe(data => {
      if (data)
        this.parametricaAng= data
    }) */
  }

  buscarOpcionRespuesta(idParametrica: number) {
   /*  this.evaluacionService.getBuscarOpcionRespuesta(idParametrica).subscribe(data => {
      if (data)
        this.parametricaAng= data
    }) */
  }
  grabarTablaParametrica(parametrica: any, codigoTabla:String) {
   //armar json
  /*  switch (codigoTabla) {
    case MantParametricaComponent.PAR_REGLA_CALIFICACION:       
        console.log("imprime json a enviar : ")
        console.log(parametrica)
        let parametricaReglaCalif:any = {};
        parametricaReglaCalif['id'] =parametrica.id;
        parametricaReglaCalif['valorMinimo'] =parametrica.valorMinimo;
        parametricaReglaCalif['valorMaximo'] =parametrica.valorMaximo;  
        parametricaReglaCalif['usuarioIngresoId'] =parametrica.usuarioIngresoId   ;
              
        let calEscala :any ={};
        calEscala['id'] = parametrica.idCalificacionEscala;
        calEscala['descripcion']=parametrica.descripcion;
        calEscala['usuarioIngresoId'] =parametrica.usuarioIngresoId  ; 
        parametricaReglaCalif['calificacionEscala'] = calEscala

        let reglamento :any ={};
        reglamento['id'] = parametrica.idReglamento;
        reglamento['idTipoReglamento'] = null;
        reglamento['nombre'] = parametrica.descripcionRegla;
        reglamento['fechaAprobacionIes'] = null;
        reglamento['fechaHasta'] = null;
        reglamento['reglamentoNacional'] = null ;   
        reglamento['usuarioIngresoId'] = parametrica.usuarioIngresoId  ;
        parametricaReglaCalif['reglamento'] = reglamento;

    /* let reglamentoCal :any ={};
      reglamentoCal['Reglamento'] =[]
      reglamentoCal['Reglamento'].push({'id':parametrica.idReglamento})
      */
       /*  console.log("nuevo json")
        //alert(JSON.stringify(parametricaReglaCalif))
        console.log(parametricaReglaCalif)
        console.log("fin") 
        this.evaluacionService.grabarReglaCalificacionEscala(parametricaReglaCalif).subscribe(result => {
        this.cancelar()
        this.gotoList();
        return
      }, error => {
        console.log(error);
        this.showAlert(error.error);
      }); 
      break; 
    case MantParametricaComponent.PAR_CATEGORIA_PREGUNTA:
      this.evaluacionService.grabarCategoriaPregunta(parametrica).subscribe(result => {
        this.cancelar()
        this.gotoList() 
        return
      }, error => { 
        this.showAlert(error.error);
      });
      break;
    case MantParametricaComponent.PAR_INSTRUMENTO:
      this.evaluacionService.grabarInstrumento(parametrica).subscribe(result => {
        this.cancelar()
        this.gotoList();
      }, error => {
        this.showAlert(error.error);
      });
      break;
      case MantParametricaComponent.PAR_TIPO_PREGUNTA:
          this.evaluacionService.grabarTipoPregunta(parametrica).subscribe(result => {
          this.cancelar()
          this.gotoList();
      }, error => {
        this.showAlert(error.error);
      });
      break;
      case MantParametricaComponent.PAR_TIPO_RECURSO_AP:
          this.evaluacionService.grabarTipoRecursoApelacion(parametrica).subscribe(result => {
          this.cancelar()
          this.gotoList();
        }, error => {
          this.showAlert(error.error);
        });
      break;
      case MantParametricaComponent.PAR_OPCION_RESPUESTA:
          this.evaluacionService.grabarOpcionRespuesta(parametrica).subscribe(result => {
          this.cancelar()
          this.gotoList();
        }, error => {
          this.showAlert(error.error);
        });
      break;
    default: 
    }   */
  }
  
  //Reglas de validación formulario
  rules = [
      { input: '.descripcionInput', message: 'Descripción requerida!', action: 'keyup, blur', rule: 'required' },
      { input: '.descripcionInput', message: 'Descripción deben tener entre 5 a 100 caracteres!', action: 'keyup', rule: 'length=5,100' },
      { input: '.descripcionInput', message: 'Campo descripción no admite el caracter ingresado!', action: 'keyup, blur' , 
        rule:(): any =>{//regla para evitar el ingreso de caracteres especiales 
         // return this.validarCaracter(this.txtDescripcion.val());
        }
      }  
  ];

  

  
  /**
   * Evento de botón al presionar `Aceptar` en el modalQuestion
   */
 /*protected submit(){    
    this.a = 'ok';
    this.PassMethods.emit({val:this.a});
    //this.modalForm.hide();
  }*/
  protected submit(){
    this.PassMethods.emit({val:'ok'});
    //this._sub.unsubscribe();
    //this.modalForm.hide();
  } 
  protected cancelar(){  
    //this.clearParametros();
   // this._sub.unsubscribe(); 
    this.modalForm.hide();    
  }

  loadData(idMantenedor: number, codigoComboParametrica:String){
    console.log(idMantenedor,codigoComboParametrica)
    this.ocultarJqw(codigoComboParametrica)
    this.mostrarLabelParametrica(codigoComboParametrica);
    if (idMantenedor != 0) {
      this.labelOpcion = 'Edición '
      this.recuperarDatosParametricas(idMantenedor,codigoComboParametrica)
    } else {
      //this.clearParametros()
      this.labelOpcion = 'Registro ' 
      this.nuevoRegistro()
    }
  }
  ocultarJqw(codigoParametrica: String) {
    /*  switch (codigoParametrica) {
      case MantParametricaComponent.PAR_REGLA_CALIFICACION:       
          this.ocultarValorMin=false;
          this.ocultarValorMax=false;
          this.ocultarValor=true;   
          this.ocultarComboRegla=false;       
          this.listarReglamento();
        break; 
      case MantParametricaComponent.PAR_CATEGORIA_PREGUNTA:
          this.ocultarValorMin=true;
          this.ocultarValorMax=true;
          this.ocultarValor=true;   
          this.ocultarComboRegla=true;

        break;
      case MantParametricaComponent.PAR_INSTRUMENTO:
          this.ocultarValorMin=true;
          this.ocultarValorMax=true;
          this.ocultarComboRegla=true;
          this.ocultarValor=true;   
        break;
        case MantParametricaComponent.PAR_TIPO_PREGUNTA:
          this.ocultarValorMin=true;
          this.ocultarValorMax=true;
          this.ocultarComboRegla=true;
          this.ocultarValor=true;   
        break;
        case MantParametricaComponent.PAR_TIPO_RECURSO_AP:
          this.ocultarValorMin=true;
          this.ocultarValorMax=true;
          this.ocultarComboRegla=true;
          this.ocultarValor=true;  
        break;
        case MantParametricaComponent.PAR_OPCION_RESPUESTA:
          this.ocultarValorMin=true;
          this.ocultarValorMax=true;
          this.ocultarComboRegla=true;
          this.ocultarValor=false;   
        break;
      default:
    } */
  }
  validarCaracter(text){
    var test = /^([A-z0-9 ñáéíóú":.,'´\-]{3,100})$/i;
    var nomText = new RegExp(test);
    return nomText.test(text);
  }
}
