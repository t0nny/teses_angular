import { Component, OnInit, ViewChild } from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import { Subscription } from "rxjs";
import { VinculacionService } from "../../../../services/vinculacion/vinculacion.service";
import { ModalComponentComponent } from "../../../modal-view/modal-component/modal-component.component";
import { ModalSeguimientoActividadComponent } from "../modal-seguimiento-actividad/modal-seguimiento-actividad.component";

@Component({
    selector: 'app-seguimiento-actividad',
    templateUrl: './seguimiento-actividad.component.html',
    //styleUrls: ['./seguimiento-actividad.component.scss']
})

export class SeguimientoActividadComponent implements OnInit {
    rowindex: number = -1;
    _sub: Subscription;

    @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent; //modal para mensajes y advertencias
    @ViewChild(ModalSeguimientoActividadComponent) formModalSeguimientoActividad: ModalSeguimientoActividadComponent;
    @ViewChild('gridConsolidado') gridConsolidado: jqxGridComponent;

    constructor(private periodoConvocatoria: VinculacionService) { }

    ngOnInit(): void {
        this.ListarSeguimientoActividad()
    }

    //actualizaal padre desde el hijo
    procesaPassMethods(h) {
        if (h.val == 'actualizar') {
            console.log("aq")
            this.ListarSeguimientoActividad();
        }
    }
    //---------------------------------------------------- EVENTOS DEL jqxGrid ----------------------------------------------------
    //metodo para obtner el id de la fila seleccionada
    Rowselect(event: any): void {
        if (event.args) {
            this.rowindex = event.args.rowindex;
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------
    ListarSeguimientoActividad() {
        this._sub = this.periodoConvocatoria.listarSeguimientoActividad().subscribe(data => {
             console.log(JSON.stringify(data));
            this.sourceConsolidado.localdata = data;
            this.dataAdapterConsolidado.dataBind();
            this._sub.unsubscribe();
        });

    }

    //definición de estructura de datos del jqxGrid
    //datos de jqxGrid
    sourceConsolidado: any =
        {
            datatype: 'array',
            id: 'id',
            datafields:
                [
                    { name: 'id', type: 'int' },
                    { name: 'descripcion', type: 'string' },
                    { name: 'fechaDesde', type: 'date', format: 'd' },
                    { name: 'fechaHasta', type: 'date', format: 'd' },
                    { name: 'descripcionPeriodo', type: 'string' },
                ]
        };
    dataAdapterConsolidado: any = new jqx.dataAdapter(this.sourceConsolidado);

    columnsConsolidado: any[] =
        [
            { text: 'Id Convocatoria', datafield: 'idConvocatoriaProyecto', width: '5%', filtertype: 'none', hidden: true },
            { text: 'Descripción', datafield: 'descripcion', width: '35%' },
            { text: 'Fecha Inicio', datafield: 'fechaDesde', width: '20%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
            { text: 'Fecha Fin', datafield: 'fechaHasta', width: '20%', cellsformat: 'yyyy-MM-dd' },
            { text: 'Período Académico', datafield: 'descripcionPeriodo', width: '16%' },
            {
                text: 'Proyectos', columntype: 'button', width: '10%',
                cellsrenderer: (): string => {
                    return 'Proyectos';
                },
                buttonclick: (row: number): void => {
                    this.myModal.alertMessage({ title: 'Listado de Proyectos', msg: 'Redirigiendo!' });
                    // let dataRecord = this.gridDocentesRegistrados.getrowdata(row);
                    //this.descargaHojaDeVida(dataRecord.idDocente);  
                },

            },
        ];

    rendertoolbar = (toolbar: any): void => {
        let id = '0';
        let container = document.createElement('div');
        container.id = id;
        container.style.margin = '0.5%';

        let nuevoButtonContainer = document.createElement('div')
        nuevoButtonContainer.id = 'nuevoButtonContainer' + id;
        nuevoButtonContainer.style.cssText = 'float: left';
        container.appendChild(nuevoButtonContainer);

        let editarButtonContainer = document.createElement('div')
        editarButtonContainer.id = 'seguimientoButtonContainer' + id;
        editarButtonContainer.style.cssText = 'float: left';
        container.appendChild(editarButtonContainer);

        let eliminarButtonContainer = document.createElement('div')
        eliminarButtonContainer.id = 'elimnarButtonContainer' + id;
        eliminarButtonContainer.style.cssText = 'float: left';
        container.appendChild(eliminarButtonContainer);

        if (toolbar[0] == '' || toolbar[0] != null) {
            toolbar[0].appendChild(container);
            let settingNuevo = {
                width: '10%',
                height: 25,
                disabled: false,
                value: 'Nuevo',
                theme: 'ui-redmond'
            }
            let settingEliminar = {
                width: '10%',
                height: 25,
                disabled: false,
                value: 'Eliminar',
                theme: 'ui-redmond'
            }
            let settingEditar = {
                width: '10%',
                height: 25,
                disabled: false,
                value: 'Editar',
                theme: 'ui-redmond'
            }
            let nuevoRowButton = jqwidgets.createInstance('#nuevoButtonContainer' + id, 'jqxButton', settingNuevo);
            nuevoRowButton.addEventHandler('click', () => {
                /*this._sub = this.periodoConvocatoria.buscarFechaFinConvocatoria().subscribe((data: any) => {
                    let fechaActual = Date.now();
                    if (fechaActual >= data.fechaHasta) {
                        this.formModalSeguimientoActividad.formularioNuevoSeguimientoActividad();
                    } else {
                        this.myModal.alertMessage({ title: 'Convocatoria', msg: 'Existe una Convocatoria Vigente!' });
                    }
                });*/this.formModalSeguimientoActividad.formularioNuevoSeguimientoActividad();
            })
            let eliminarRowButton = jqwidgets.createInstance('#elimnarButtonContainer' + id, 'jqxButton', settingEliminar);
            eliminarRowButton.addEventHandler('click', () => {
                let selectedrowindex = this.gridConsolidado.getselectedrowindex();
                let idConvocatoria = this.gridConsolidado.getcellvalue(selectedrowindex, 'id');

                if (idConvocatoria) {
                    this.myModal.alertQuestion({
                        title: 'Registro de Convocatorias',
                        msg: 'Desea eliminar este registro?',
                        result: (k) => {
                            if (k) {
                                this._sub = this.periodoConvocatoria.borrarConvocatoria(idConvocatoria).subscribe(result => {
                                    this.gridConsolidado.deleterow(idConvocatoria);
                                    this.gridConsolidado.clearselection();
                                    this.rowindex = -1;
                                    this._sub.unsubscribe();
                                }, error => console.error(error));
                                this.myModal.alertMessage({ title: 'Registro de Convocatorias', msg: 'Convocatoria eliminado correctamente!' });
                            }
                        }
                    });

                } else {
                    this.myModal.alertMessage({ title: 'Registro de Convocatorias', msg: 'Seleccione una Convocatorias!' });
                }
            })
            let editarRowButton = jqwidgets.createInstance('#seguimientoButtonContainer' + id, 'jqxButton', settingEditar);
            editarRowButton.addEventHandler('click', () => {
                if (this.rowindex != -1) {
                    let getRowObject = this.gridConsolidado.getrowdata(this.rowindex);
                    let id = getRowObject.id;
                    this._sub = this.periodoConvocatoria.buscarPorSeguimientoActividadId(id).subscribe((data: any) => {
                        //this.editaConvocatoria = data;
                        this.formModalSeguimientoActividad.formularioEditarSeguimientoActividad(data);
                        this.gridConsolidado.clearselection();
                        this.rowindex = -1;
                        this._sub.unsubscribe();
                    });
                }
                else {
                    this.myModal.alertMessage({ title: 'Mantenimiento de Convocatoria', msg: 'Seleccione una Convocatoria!' });
                }
            })
        }
    }

}