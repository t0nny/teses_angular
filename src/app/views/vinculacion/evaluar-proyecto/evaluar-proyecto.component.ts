import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { jqxButtonComponent } from 'jqwidgets-ng/jqxbuttons';
import { jqxDropDownListComponent } from 'jqwidgets-ng/jqxdropdownlist';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { NgxExtendedPdfViewerComponent } from 'ngx-extended-pdf-viewer';
import { Subscription } from 'rxjs';
import { FileService } from '../../../services/file/file.service';
import { VinculacionService } from '../../../services/vinculacion/vinculacion.service';
import { ModalComponentComponent } from '../../modal-view/modal-component/modal-component.component';
import { MatrizExAnteComponent } from '../director/matriz-ex-ante/matriz-ex-ante.component';
import { ModalProyectoComponent } from '../docente/modal-proyecto/modal-proyecto.component';

@Component({
  selector: 'app-evaluar-proyecto',
  templateUrl: './evaluar-proyecto.component.html',
  styleUrls: ['./evaluar-proyecto.component.scss']
})
export class EvaluarProyectoComponent implements OnInit {

  @ViewChild('dropDownFacultad') dropDownFacultad: jqxDropDownListComponent;
  @ViewChild('dropDownCarrera') dropDownCarrera: jqxDropDownListComponent;
  @ViewChild('dropDownProgramas') dropDownProgramas: jqxDropDownListComponent;
  @ViewChild('dropDownProyectos') dropDownProyectos: jqxDropDownListComponent;
  @ViewChild('gridProyectosRegistrados') gridProyectosRegistrados: jqxGridComponent;
  @ViewChild('myGridProyectoVersion') myGridProyectoVersion: jqxGridComponent;
  @ViewChild(ModalComponentComponent) myModal: ModalComponentComponent;
  @ViewChild(MatrizExAnteComponent) formEvaluarProyecto: MatrizExAnteComponent;
  @ViewChild(ModalProyectoComponent) formModalProyecto: ModalProyectoComponent;
  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('buttonVolver') buttonVolver: jqxButtonComponent;

  isCollapsedPdf: boolean = true;
  pdfSrc: string = '';
  filename: string = "";
  _sub: Subscription;
  rowindex: number = -1;
  idProyecto: any;

  /**Permite controlar que no salga del modal sin presionar algun boton de éste */
  protected config = { backdrop: true, ignoreBackdropClick: true };
  idTipoUsuario: any;

  constructor(
    private route: ActivatedRoute, private router: Router,
    private fileArchivo: FileService, private periodoConvocatoria: VinculacionService) { }

  ngOnInit() {

  }

  ngAfterContentInit() {
    console.log("entre evalua proyecto")
    this.getProyectoById();
  }

  Rowselect(event: any): void {
    if (event.args) {
      this.rowindex = event.args.rowindex;
    }
  }

  //**************************lee los parametro que llegan del router *************************************************************
  getProyectoById() {
    this.idProyecto = null;
    this._sub = this.route.params.subscribe(valor => {
      //RECUPERAR EL VALOR DEL PARAMETRO PASADO EN LA RUTA
      this.idProyecto = valor['idProyecto'];//ID DE PROYECTO
      this.idTipoUsuario = valor['idTipoUsuario'];//ID DE TIPOUSUARIO
      this.tipoMenu(this.idTipoUsuario);
      console.log("idPr " + this.idProyecto + " " + this.idTipoUsuario);
      this.ListarVersionesProyectos(this.idProyecto);
    });
  }

  tipoMenu(idTipoUsuario: any) {
    if (idTipoUsuario == 1) {
      //evaluador
      this.menuMatriz = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
    } else {
      //director de proyectos
      this.menuMatriz = this.getAdapter(this.sourceMenu1).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
    }
  }

  /*************************************************************************************************************** */
  //**************************evento del comboCarreras para agg docentes al combo *************************************************************


  nestedGrids: any[] = new Array();

  // create nested grid.
  initRowDetails = (index: number, parentElement: any, gridElement: any, record: any): void => {
    let id = record.uid.toString();
    console.log('vv1 ' + id)
    let nestedGridContainer = parentElement.children[0];
    this.nestedGrids[index] = nestedGridContainer;
    let filtergroup = new jqx.filter();
    let filter_or_operator = 1;
    let filtervalue = id;
    let filtercondition = 'equal';
    let filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
    let orders = this.dataAdapterMatriz.records;
    let ordersbyid = [];
    for (let i = 0; i < orders.length; i++) {
      let result = filter.evaluate(orders[i]['idProyectoVersion']);
      if (result && orders[i].idCabMatriz != null) {
        switch (orders[i].estadoMatriz.trim()) {
          case 'REVISAR':
            orders[i].descripcion = 'Matriz lista para ser enviada';
            break;
          case 'INICIAL':
            orders[i].descripcion = 'Matriz incompleta';
            break;
          case 'MODIFICAR':
            orders[i].descripcion = 'Matriz enviada a ser revisada';
            break;
          case 'APROBADO':
            orders[i].descripcion = 'Proyecto aprobado';
            break;
          default:
          //default code block
        }
        ordersbyid.push(orders[i]);
        // alert('cc ' + JSON.stringify(ordersbyid));
      }
      //alert('ff ' + JSON.stringify(orders[i]));
    }

    let sourceMatriz = {
      datafields: [
        { name: 'idCabMatriz', type: 'int' },
        { name: 'estadoMatriz', type: 'string' },
        { name: 'descripcion', type: 'string' },
        { name: 'version', type: 'int' },
        { name: 'fechaDesde', type: 'date', format: 'd' },
        { name: 'fechaHasta', type: 'date', format: 'd' },
        { name: 'id', type: 'int' },//id de proyecto        
        { name: 'idProyectoVersion', type: 'int' },
      ],
      id: 'idCabMatriz',
      localdata: ordersbyid
    }
    let nestedGridAdapter = new jqx.dataAdapter(sourceMatriz);
    // alert(JSON.stringify(sourceCarrera));
    if (nestedGridContainer != null) {
      let settings = {
        width: "'90%'",
        height: "100%",
        theme: "darkblue",
        source: nestedGridAdapter,
        columngroups: [
          {
            text: 'Matrices', cellsalign: 'center',
            align: 'center', name: 'matrizExAnte'
          },
        ],
        columns: [
          { text: 'Id Matriz', datafield: 'idCabMatriz', width: '5%', hidden: true },
          { text: 'Estado', datafield: 'estadoMatriz', width: '10%', hidden: true },
          { text: 'Descripción', datafield: 'descripcion', width: '29%' },
          { text: 'Fecha Revisión', datafield: 'fechaDesde', width: '20%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
          { text: 'Entegar hasta', datafield: 'fechaHasta', width: '20%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },
          {
            text: 'Ver', cellsalign: 'center', align: 'center', columngroup: 'matrizExAnte', columntype: 'button', width: '5%', height: '0%',
            cellsrenderer: (): string => {
              return 'Ver';
            },
            buttonclick: (row: number): void => {
              //get the data and append in to the inputs
              // this.editrow = row;
              //console.log(ordersbyid[this.editrow].idOferta);
              console.log(ordersbyid[row].idProyectoVersion);
              //this.desabilitarCombos();
              this.verEvaluacionMatrizExAnte(ordersbyid[row].idProyectoVersion)
              //  ordersbyid[this.editrow].idOferta,
              //  ordersbyid[this.editrow].versionMalla); 
              this.myGridProyectoVersion.virtualmode(false);
            },
          },
          {
            text: 'Descargar', cellsalign: 'center', align: 'center', columngroup: 'matrizExAnte', columntype: 'button', width: '12%', height: '0%',
            cellsrenderer: (): string => {
              return 'Descargar';
            },
            buttonclick: (row: number): void => {
              //get the data and append in to the inputs
              /*  this.editrow = row;
               console.log(ordersbyid[this.editrow].idOferta);
               console.log(ordersbyid[this.editrow].versionMalla);
               this.descargaMallaVersionPorPeriodo(ordersbyid[this.editrow].idPeriodoAcademico,
                 ordersbyid[this.editrow].idOferta,
                 ordersbyid[this.editrow].versionMalla); */
              const selectedrowindex = this.myGridProyectoVersion.getselectedrowindex();
              const url = this.myGridProyectoVersion.getcellvalue(selectedrowindex, 'idProyectoVersion');
              console.log('idProyectoVersion ' + url)
              this.descargarEvaluacionMatrizExAnte(ordersbyid[row].idProyectoVersion);

            },

          },
          {
            text: 'Enviar Matriz', cellsalign: 'center', align: 'center', columngroup: 'matrizExAnte', columntype: 'button', width: '14%', height: '0%',
            cellsrenderer: (): string => {
              return 'Enviar Matriz';
            },
            buttonclick: (row: number): void => {
              let rowData = this.myGridProyectoVersion.getrowdata(row);
              console.log('row ' + rowData.estadoMatriz);
              console.log('idProyectoVersion ' + rowData.idProyectoVersion);
              // let selectedrowindex = this.myGridProyectoVersion.getselectedrowindex();
              // let idDocente = this.myGridProyectoVersion.getcellvalue(row, 'idCabMatriz');
              // this.initRowDetails(index, parentElement, gridElement, record);
              //this.myGridProyectoVersion.deleterow(row);
              //let selectedrowindex1 = this.myGridProyectoVersion.getselectedcell();
              //get the data and append in to the inputs
              /*  this.editrow = row;
               console.log(ordersbyid[this.editrow].idOferta);
               console.log(ordersbyid[this.editrow].versionMalla);
               this.descargaMallaVersionPorPeriodo(ordersbyid[this.editrow].idPeriodoAcademico,
                 ordersbyid[this.editrow].idOferta,
                 ordersbyid[this.editrow].versionMalla); */
              /*var selectedrowindex = this.myGridProyectoVersion.getselectedrowindex();  
              let getRowObject = this.myGridProyectoVersion.getrowdata(selectedrowindex);
              let idProyectoVersion = getRowObject.idProyectoVersion;
              let idCabMatriz = getRowObject.idCabMatriz;*/
              let estadoMatriz = rowData.estadoMatriz;
              let idProyectoVersion = rowData.idProyectoVersion;
              switch (estadoMatriz.trim()) {
                case 'INICIAL':
                  //cargar la matriz para ser editada
                  console.log('editando');
                  //this.formEvaluarProyecto.formularioEvaluar(idProyectoVersion);
                  this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'La matriz aún no calificada!' });
                  this.myGridProyectoVersion.refreshdata();
                  this.myGridProyectoVersion.clearselection();
                  this.myGridProyectoVersion.removegroupat(row);
                  break;
                case 'REVISAR':
                  this.listarCodigoBd(idProyectoVersion);
                  this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'Matriz enviada al director del proyecto!' });
                  this.myGridProyectoVersion.clearselection();
                  this.myGridProyectoVersion.removegroupat(row);
                  break;
                case 'APROBADO':
                  this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'Matriz aprobado!' });
                  this.myGridProyectoVersion.clearselection();
                  this.myGridProyectoVersion.removegroupat(row);
                  break;
                case 'MODIFICAR':
                  this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'Matriz ya ha sido enviada al director del proyecto!' });
                  this.myGridProyectoVersion.clearselection();
                  this.myGridProyectoVersion.removegroupat(row);
                  break;
                default:
              }
            },

          },
        ]
      };
      jqwidgets.createInstance(`#${nestedGridContainer.id}`, 'jqxGrid', settings);
    }
  }
  ready = (): void => {
    this.myGridProyectoVersion.showrowdetails(0);
  };

  verEvaluacionMatrizExAnte(idProyectoVersion: number) {
    let formato = "pdf";
    this._sub = this.periodoConvocatoria.getReportEvaluacionMatrizExAnte(idProyectoVersion, formato).subscribe((blob: Blob) => {
      if (this.isCollapsedPdf != false) {
        this.habilitarPdf();
        console.log('entro')
      } else {
        console.log('no entro')
      }
      this.filename = this.nombreArchivo();
      const objectUrl = window.URL.createObjectURL(blob);
      const enlace = document.createElement('a');
      enlace.href = objectUrl;
      this.pdfSrc = "";
      this.pdfSrc = enlace.href;
      this.nombreArchivoDescarga(this.filename);
    });
  }

  descargarEvaluacionMatrizExAnte(idProyectoVersion: number) {
    let formato = "pdf";
    this._sub = this.periodoConvocatoria.getReportEvaluacionMatrizExAnte(idProyectoVersion, formato).subscribe((blob: Blob) => {
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
      }
      let filename = "";
      filename = this.nombreArchivo();
      const objectUrl = window.URL.createObjectURL(blob);
      const enlace = document.createElement('a');
      enlace.href = objectUrl;
      enlace.download = objectUrl;
      enlace.setAttribute('download', filename);
      enlace.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
      setTimeout(function () {
        window.URL.revokeObjectURL(objectUrl);
      });
    });
  }

  listarCodigoBd(idProyectoVersion: any) {
    let proyecto: any = {};
    let cabeceraMatriz: any = {};
    this._sub = this.periodoConvocatoria.buscarCodigoProyecto(idProyectoVersion).subscribe(data => {
      console.log("codigo" + data);
      if (data.codigo != 'NO-APR') {
        proyecto.id = idProyectoVersion;
        proyecto.estadoProyecto = 'APROBADO';
        cabeceraMatriz.idProyectoVersion = idProyectoVersion;
        cabeceraMatriz.estadoMatriz = 'APROBADO';
        this.guardarCodigoProyecto(proyecto);
        this.guardarEstadoMatriz(cabeceraMatriz);
      } else {
        proyecto.id = idProyectoVersion;
        proyecto.estadoProyecto = 'MODIFICAR';
        cabeceraMatriz.idProyectoVersion = idProyectoVersion;
        cabeceraMatriz.estadoMatriz = 'MODIFICAR';
        this.guardarCodigoProyecto(proyecto);
        this.guardarEstadoMatriz(cabeceraMatriz);
      }
      this.sourcedataAdapterProyectoVersion.localdata = [];
      this.dataAdapterProyectoVersion.dataBind();
      this.sourceMatriz.localdata = [];
      this.dataAdapterMatriz.dataBind();
      this.ListarVersionesProyectos(this.idProyecto);
      this.initRowDetails
    }, error => console.error("Error al cargar codigo de proyecto " + error));
  }
  guardarEstadoMatriz(cabeceraMatriz: any) {
    this._sub = this.periodoConvocatoria.updateEstadoMatriz(cabeceraMatriz).subscribe(() => {
    }, error => {
      this.myModal.alertMessage({ title: 'Error', msg: "Ha ocurrido un error al momento de registrar la actualización de la Matriz, por favor intente mas tarde o contáctese con el administrador del sistema." });
    })
  }

  guardarCodigoProyecto(proyecto: any) {
    this._sub = this.periodoConvocatoria.grabarCodigoProyecto(proyecto).subscribe(() => {
    }, error => {
      this.myModal.alertMessage({ title: 'Error', msg: "Ha ocurrido un error al momento de registrar el código del proyecto, por favor intente mas tarde o contáctese con el administrador del sistema." });
    })
  }

  //**************************evento del matriz ex ante*************************************************************

  //actualizar padre desde el hijo(modal_proyecto)
  procesaPassMethods(event: any) {
    this.newMethod(event);
  }

  //actualizar padre desde el hijo(matriz_ex_ante)
  show(event: any) {
    this.newMethod(event);
  }

  private newMethod(event: any) {
    if (event.val == 'actualizar') {
      console.log("aq " + event.val);
      this.ListarVersionesProyectos(this.idProyecto);
    }
  }

  //**************************l*************************************************************************
  //**************************listar grid proyectos version y matrice*************************************************************
  ListarVersionesProyectos(idProyecto: any) {
    this._sub = this.periodoConvocatoria.buscarproyectoVersionMatriz(idProyecto).subscribe(data => {
      console.log('versiones' + JSON.stringify(data));
      this.sourcedataAdapterProyectoVersion.localdata = data;
      this.dataAdapterProyectoVersion.dataBind();
      this.sourceMatriz.localdata = data;
      this.dataAdapterMatriz.dataBind();
    });
  }

  sourcedataAdapterProyectoVersion: any =
    {
      datatype: 'json',
      id: 'idProyectoVersion',
      localdata: [],
    };
  dataAdapterProyectoVersion: any = new jqx.dataAdapter(this.sourcedataAdapterProyectoVersion);

  //**************************estilo para las celdas del grip*************************************************************
  renderer = (row: number, column: any, value: string): string => {
    return '<span style="margin-left: 4px; margin-top: 9px; float: left;">' + value + '</span>';
  }

  /*************************************************************************************************************** */
  //**************************evento para expandir mtriz*************************************************************
  rowdetailstemplate: any = {
    rowdetails: '<div  id="nestedGrid" style="margin: 10px; "></div>', rowdetailshidden: true, autorowheight: true
  };

  /*************************************************************************************************************** */

  columns: any[] =
    [
      { text: 'id', datafield: 'idProyectoVersion', width: '5%', cellsrenderer: this.renderer, hidden: true },
      { text: 'Descripción', datafield: 'titulo', width: '70%', cellsrenderer: this.renderer },
      { text: 'Versión', datafield: 'version', width: '10%', cellsrenderer: this.renderer },
      { text: 'url', datafield: 'url', width: '5%', cellsrenderer: this.renderer, hidden: true },
      { text: 'estadoProyectoVersion', datafield: 'estadoProyectoVersion', width: '5%', cellsrenderer: this.renderer },
      {
        text: 'ver', columngroup: 'versionesProyecto', cellsalign: 'center', align: 'center', columntype: 'button', width: '10%', cellsrenderer: (): string => {
          return 'ver';
        },
        buttonclick: (row: number): void => {
          // captura el nombre del file seleccionado(url)
          const selectedrowindex = this.myGridProyectoVersion.getselectedrowindex();
          const url = this.myGridProyectoVersion.getcellvalue(selectedrowindex, 'url');
          console.log('url ' + url)
          this.verFileDelServer(url);
          this.myGridProyectoVersion.virtualmode(false);
        },
      },

      {
        text: 'Descargar', columngroup: 'versionesProyecto', columntype: 'button', cellsalign: 'center', align: 'center', width: '10%', cellsrenderer: (): string => {
          return 'Descargar';
        },
        buttonclick: (row: number): void => {
          // captura el nombre del file seleccionado(url)
          const selectedrowindex = this.myGridProyectoVersion.getselectedrowindex();
          const url = this.myGridProyectoVersion.getcellvalue(selectedrowindex, 'url');
          console.log('url ' + url)
          this.descargarFileDelServer(url);
        },
      },
    ];

  columngroups: any[] =
    [
      {
        text: 'Versiones de Proyectos', cellsalign: 'center', align: 'center',
        name: 'versionesProyecto'
      },
    ];

  sourceMatriz: any = {
    //localData: [],
    id: 'idProyectoVersion',
    dataType: 'array',
    dataFields: [
      { name: 'idCabMatriz', map: 'idCabMatriz', type: 'int' },
      { name: 'estadoMatriz', map: 'estadoMatriz', type: 'string' },
      { name: 'descripcion', map: 'descripcion', type: 'string' },
      { name: 'fechaDesde', map: 'fechaDesde', type: 'date', format: 'd' },
      { name: 'fechaHasta', map: 'fechaHasta', type: 'date', format: 'd' },
      { name: 'idProyectoVersion', map: 'idProyectoVersion', type: 'int' },
      { name: 'id', map: 'id', type: 'int' },//id_proyecto
    ]
  };

  dataAdapterMatriz: any = new jqx.dataAdapter(this.sourceMatriz);

  /*************************************************************************************************************** */
  //**************************definición de estructura de datos del jqxMenu para matriz Ex-ANTE*************************************************************

  //datos de jqxMenu
  dataMenu = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Editar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Cancelar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },

  ];
  sourceMenu =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu
    };

  getAdapter(Menu: any): any {
    return new jqx.dataAdapter(Menu, { autoBind: true });
  };
  menuMatriz = ''
  //this.menuMatriz = this.getAdapter(this.sourceMenu).getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);

  /*************************************************************************************************************** */
  //**************************definición de estructura de datos del jqxMenu para matriz Ex-ANTE*************************************************************
  //datos de jqxMenu
  dataMenu1 = [
    {
      'id': '1',
      'text': 'Nuevo',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '2',
      'text': 'Editar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
    {
      'id': '3',
      'text': 'Cancelar',
      'parentid': '-1',
      'subMenuWidth': '250px'
    },
  ];
  sourceMenu1 =
    {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'text' },
        { name: 'subMenuWidth' }
      ],
      id: 'id',
      localdata: this.dataMenu1
    };

  /*************************************************************************************************************** */
  //**************************evento del clip del menu*************************************************************

  itemclick(event: any): void {
    var opt = event.args.innerText;
    var selectedrowindex = this.myGridProyectoVersion.getselectedrowindex();
    switch (opt) {
      case 'Nuevo':
        this.myGridProyectoVersion.refreshdata();
        if (this.idTipoUsuario == 1) {
          console.log('si entre al nuevo MATRIZ')
          if (selectedrowindex == -1) {
            this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'Seleccione una  versión del Proyecto!' });
          } else {
            let getRowObject = this.myGridProyectoVersion.getrowdata(selectedrowindex);
            let idProyectoVersion = getRowObject.idProyectoVersion;
            let idCabMatriz = getRowObject.idCabMatriz;
            if (idCabMatriz == null) {
              this.formEvaluarProyecto.formularioEvaluar(idProyectoVersion);
            } else {
              this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'La versión de Proyecto ya tiene una matriz!' });
              this.myGridProyectoVersion.clearselection();
            }
          }
        } else {
          console.log('nueva version');
          this.estadoVersion();
        }
        break;
      case 'Editar':
        this.myGridProyectoVersion.refreshdata();
        if (selectedrowindex == -1) {
          this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'Seleccione una  versión del Proyecto!' });
        } else {
          if (this.idTipoUsuario == 1) {
            let getRowObject = this.myGridProyectoVersion.getrowdata(selectedrowindex);
            let idProyectoVersion = getRowObject.idProyectoVersion;
            let idCabMatriz = getRowObject.idCabMatriz;
            let estadoMatriz = getRowObject.estadoMatriz;
            if (idCabMatriz != null) {
              switch (estadoMatriz.trim()) {
                case 'INICIAL':
                  //cargar la matriz para ser editada
                  console.log('editando');
                  this.formEvaluarProyecto.formularioEvaluar(idProyectoVersion);
                  this.myGridProyectoVersion.clearselection();
                  break;
                case 'REVISAR':
                  //cargar la matriz para ser editada
                  console.log('editando');
                  this.formEvaluarProyecto.formularioEvaluar(idProyectoVersion);
                  this.myGridProyectoVersion.clearselection();
                  break;
                case 'APROBADO':
                  this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'El Proyecto esta aprobado!' });
                  this.myGridProyectoVersion.clearselection();
                  break;
                case 'MODIFICAR':
                  this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'Matriz enviada a modificar!' });
                  this.myGridProyectoVersion.clearselection();
                  break;
                default:
              }
            } else {
              this.myModal.alertMessage({ title: 'Evaluación de Proyectos', msg: 'La versión de Proyecto no tiene una matriz!' });
              this.myGridProyectoVersion.clearselection();
            }
          } else {
            console.log('editar nueva version');
            //preguntar por el estado sea inicialb
            let getRowObject = this.myGridProyectoVersion.getrowdata(selectedrowindex);
            let idProyectoVersion = getRowObject.idProyectoVersion;
            this._sub = this.periodoConvocatoria.buscarEstadoVersion1(idProyectoVersion).subscribe(data => {
              console.log('estadoVersion' + JSON.stringify(data));
              if (data.estado.trim() == 'INICIAL') {
                this.formModalProyecto.editandoNuevaVersion(this.idProyecto);
              } else {
                this.myModal.alertMessage({ title: 'Versiones de Proyectos', msg: 'Esta versión no puede ser editada!' });
              }
            });
          }
        }
        break;
      case 'Enviar Versión':
        if (selectedrowindex == -1) {
          this.myModal.alertMessage({ title: 'Versiones de Proyectos', msg: 'Seleccione una  versión del Proyecto!' });
        } else {
          let getRowObject = this.myGridProyectoVersion.getrowdata(selectedrowindex);
          let estadoProyectoVersion = getRowObject.estadoProyectoVersion;
          let idProyectoVersion = getRowObject.idProyectoVersion;
          let url = getRowObject.url;
          if (estadoProyectoVersion.trim() == 'INICIAL') {
            let datosProyecto: any = {};
            let arrProyectoVersion: any = [];
            let objProyectoVersion: any = {};

            objProyectoVersion.id = String(idProyectoVersion);
            objProyectoVersion.estadoProyectoVersion = 'REVISIÓN';
            objProyectoVersion.url = url;
            arrProyectoVersion.push(objProyectoVersion);

            datosProyecto.id = this.idProyecto;
            datosProyecto.estadoProyecto = 'REVISIÓN';
            datosProyecto.proyectoVersion = arrProyectoVersion;

            this.guardar(datosProyecto);
            this.sourcedataAdapterProyectoVersion.localdata = [];
            this.dataAdapterProyectoVersion.dataBind();
            this.sourceMatriz.localdata = [];
            this.dataAdapterMatriz.dataBind();
            this.ListarVersionesProyectos(this.idProyecto);
            this.initRowDetails
            this.myGridProyectoVersion.clearselection();
          } else {
            this.myModal.alertMessage({ title: 'Versiones de Proyectos', msg: 'La versión de Proyecto ya fue enviada a vinculación!' });
            this.myGridProyectoVersion.clearselection();
          }
        }

        break;
      case 'Cancelar':
        this.gotoList();
        break;
      default:
      //default code block
    }
  };

  guardar(datosProyecto: any) {
    console.log("datoaProyecto " + JSON.stringify(datosProyecto));
    this._sub = this.periodoConvocatoria.grabarProyectoVersion(datosProyecto).subscribe(result => {
      this.myModal.alertMessage({ title: 'Nueva Versión', msg: 'Versión enviada al Vinculación!' });
    }, error => console.error("h " + error));

  }

  estadoVersion() {
    this._sub = this.periodoConvocatoria.buscarEstadoVersion(this.idProyecto).subscribe(data => {
      console.log('estadoVersion' + JSON.stringify(data));
      if (data.estado.trim() != 'INICIAL') {
        this.formModalProyecto.ingresarNuevaVersion(this.idProyecto);
      } else {
        this.myModal.alertMessage({ title: 'Versiones de Proyectos', msg: 'Ya se cuenta con una nueva versión creada!' });
      }
    });
  }

  gotoList(): void {
    this.router.navigate(['vinculacion/docente/registro-proyecto']);
  }

  descargarFileDelServer(nombreFile: string) {
    //CAPTURO EL ID DEL COMBOBOX DE LA CARRERA ---->OFERTA
    //   console.log(this.formato)
    //  let formato=this.dropDownFormato.val();
    // console.log(this.reporteServicio.getReportMallaVersionPorPeriodo(cboOferta,cboMallaVersion,cboPeriodo))
    this.fileArchivo.descargarFile(nombreFile).subscribe((blob: Blob) => {
      // Para navegadores de Microsoft.
      // console.log("formato 1 "+this.formato)
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
      }
      let filename = "";
      filename = this.nombreArchivo();
      const objectUrl = window.URL.createObjectURL(blob);
      const enlace = document.createElement('a');
      enlace.href = objectUrl;
      enlace.download = objectUrl;
      enlace.setAttribute('download', filename);
      enlace.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
      setTimeout(function () {
        window.URL.revokeObjectURL(objectUrl);
      });
    });
  }

  verFileDelServer(nombreFile: string) {
    //CAPTURO EL ID DEL COMBOBOX DE LA CARRERA ---->OFERTA
    let formato = "pdf";
    this.fileArchivo.descargarFile(nombreFile).subscribe((blob: Blob) => {
      if (this.isCollapsedPdf != false) {
        this.habilitarPdf();
        console.log('entro')
      } else {
        console.log('no entro')
      }
      let filename = "";
      this.filename = this.nombreArchivo();
      const objectUrl = window.URL.createObjectURL(blob);
      const enlace = document.createElement('a');
      enlace.href = objectUrl;
      this.pdfSrc = "";
      this.pdfSrc = URL.createObjectURL(blob);
      this.nombreArchivoDescarga(filename);
    });

  }
  habilitarPdf() {
    this.buttonVolver.disabled(false);
    this.isCollapsedPdf = !this.isCollapsedPdf;
  }

  nombreArchivo(): string {
    this.filename = "";
    var fecha = new Date();
    this.filename = fecha.getHours() + "_" + fecha.getMinutes() + "_" + fecha.getDate() + "_" + fecha.getMonth() + "_" + fecha.getFullYear()
    return this.filename;
  }


  nombreArchivoDescarga(filenameForDownload: string) {
    if (NgxExtendedPdfViewerComponent.ngxExtendedPdfViewerInitialized) {
      (<any>window).PDFViewerApplication.appConfig.filenameForDownload = filenameForDownload;
      (<any>window).PDFViewerApplication.appConfig.showZoomButtons = true;
      (<any>window).PDFViewerApplication.appConfig.showSecondaryToolbarButton = true;
      (<any>window).PDFViewerApplication.appConfig.showFindButton = true;
      (<any>window).PDFViewerApplication.appConfig.showPrintButton = true;
      (<any>window).PDFViewerApplication.appConfig.showPagingButtons = true;
      (<any>window).PDFViewerApplication.appConfig.showZoomButtons = true;
      (<any>window).PDFViewerApplication.appConfig.showDownloadButton = true;
      (<any>window).PDFViewerApplication.appConfig.showSidebarButton = true;
    }
  }

  //evnto del boton volver
  controlIsCollapsed() {
    this.pdfSrc = "";
    this.isCollapsedPdf = !this.isCollapsedPdf;
    this.buttonVolver.disabled(true);
  }

}
