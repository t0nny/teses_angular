import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import{VinculacionRoutingModule} from './vinculacion-routing.module';
import { FormsModule} from '@angular/forms';
import { NgxExtendedPdfViewerComponent, NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { CronogramaTareasComponent } from './componentes/cronograma-tareas/cronograma-tareas.component';
import { ModalTareasComponent } from './componentes/modal-tareas/modal-tareas.component';
import { MatrizExAnteComponent } from './director/matriz-ex-ante/matriz-ex-ante.component';
import { ModalConvocatoriaComponent } from './director/modal-convocatoria/modal-convocatoria.component';
import { ModalProyectoComponent } from './docente/modal-proyecto/modal-proyecto.component';
import { VinculacionService } from '../../services/vinculacion/vinculacion.service';
import { ValidadorVinculacionService } from './VarGlobales/validatorVinculacion.service';
import { RegistroProgramaComponent } from './director/registro-programa/registro-programa.component';
import { RegistroProyectoComponent } from './docente/registro-proyecto/registro-proyecto.component';
import { ConvocatoriaComponent } from './director/convocatoria/convocatoria.component';
import { ModalProgramaComponent } from './director/modal-programa/modal-programa.component';
import { ProyectoEdicionComponent } from './docente/proyecto-edicion/proyecto-edicion.component';
import { SeguimienteProyectoComponent } from './docente/seguimiente-proyecto/seguimiente-proyecto.component';
import { EvaluarProyectoComponent } from './evaluar-proyecto/evaluar-proyecto.component';
import { GlobalComponentModule } from '../global-component.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DatosGeneralesComponent } from './docente/seguimiente-proyecto/secciones-tab/datos-generales/datos-generales.component';
import { DynamicTabsDirective } from './docente/seguimiente-proyecto/plantilla-tab-dinamica/dynamic-tabs.directive';
import { TabComponent } from './docente/seguimiente-proyecto/plantilla-tab-dinamica/tab.component';
import { TabsComponent } from './docente/seguimiente-proyecto/plantilla-tab-dinamica/tabs.component';
import { BeneficiariosComponent } from './docente/seguimiente-proyecto/secciones-tab/beneficiarios/beneficiarios.component';
import { AsignarPersonalComponent } from './docente/seguimiente-proyecto/secciones-tab/asignar-personal/asignar-personal.component';
import { InstitucionComponent } from './docente/seguimiente-proyecto/secciones-tab/institucion/institucion.component';
import { MarcoLogicoComponent } from './docente/seguimiente-proyecto/secciones-tab/marco-logico/marco-logico.component';
import { ModalActividadesComponent } from './componentes/modal-actividades/modal-actividades.component';
import { AsignarTareasComponent } from './componentes/asignar-tareas/asignar-tareas.component';
import { GenerarInformeComponent } from './componentes/generar-informe/generar-informe.component';
import { ModalInformeActividadesComponent} from './componentes/modal-informe-actividades/modal-informe-actividades.component'
import { ModalDetallesParticipantesComponent } from './componentes/modal-detalles-participantes/modal-detalles-participantes.component';
import { ConsolidarInformeComponent } from './componentes/consolidar-informe/consolidar-informe.component';
import { SeguimientoActividadComponent } from './director/seguimiento-actividad/seguimiento-actividad.component';
import { ModalSeguimientoActividadComponent } from './director/modal-seguimiento-actividad/modal-seguimiento-actividad.component';

@NgModule({
  exports:[ ], 
  imports: [
    FormsModule,
    CommonModule,    
    VinculacionRoutingModule,   
    NgxExtendedPdfViewerModule,
    GlobalComponentModule,
    ModalModule,
  ],
  declarations: [
    RegistroProgramaComponent, 
    RegistroProyectoComponent, 
    EvaluarProyectoComponent, 
    ConvocatoriaComponent,
    MatrizExAnteComponent, 
    ModalConvocatoriaComponent,
    ModalProgramaComponent,
    ModalProyectoComponent,
    ProyectoEdicionComponent,
    SeguimienteProyectoComponent, 
    CronogramaTareasComponent,
    ModalTareasComponent,
    DatosGeneralesComponent,
    DynamicTabsDirective,
    TabComponent,
    TabsComponent,
    BeneficiariosComponent,
    AsignarPersonalComponent,
    InstitucionComponent,
    MarcoLogicoComponent,
    ModalActividadesComponent,
    AsignarTareasComponent,
    GenerarInformeComponent,
    ConsolidarInformeComponent,
    ModalInformeActividadesComponent,
    ModalDetallesParticipantesComponent,
    ModalSeguimientoActividadComponent,
    SeguimientoActividadComponent
  ],
  providers: [
    VinculacionModule,
    VinculacionService,
    ValidadorVinculacionService],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [TabComponent],
    
})
export class VinculacionModule { }
