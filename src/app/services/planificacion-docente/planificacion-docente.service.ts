import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../config/app-config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class PlanificacionDocenteService {

  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS_SILABOS = this.URL_SERVER + '/api/syllabus';

  constructor(private http: HttpClient) { }


  /**
   * Obtiene todas las facultades que le pertenecen a un usuario.
   * 
   * @param idUsuario Identificador del Usuario que hizo loggin
   */
  getFacultades(idUsuario:any): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS + '/getFacultades/' + idUsuario);
  }

  /**
   * Obtiene todas las carreras que le pertenezcan a una facultad
   * 
   * @param idDepartamento Identificador del Departamento o Facultad
   */
  getCarreras(idDepartamento:any): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS + '/getOfertas/' + idDepartamento);
  }

  /**
   * Obtiene todas las asignaturas que le pertenecen a una carrera
   * 
   * @param idOferta Identificador de la carrera u oferta
   */
  getAsignaturas(idOferta:any):Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS + '/getAsignaturas/' + idOferta);
  }

  /**
   * Obtiene la información de la asignatura para mostrar cuando se cree o edite un syllabus
   * 
   * @param idAsignatura Identificador de la asignatura
   * @param idOferta Identificador de la carrera u oferta
   */
  getAsiSyllabus(idAsignatura:any,idOferta:any):Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS + `/getAsiSyllabus/${idAsignatura}/${idOferta}`);
  }

   /**
   * Obtiene la información de la asignatura para mostrar cuando se cree o edite un syllabus
   * 
   * @param idMallaAsignatura Identificador de la asignatura
   */
  getSilabus(idMallaAsignatura:any):Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS + '/getSilabus/'+ idMallaAsignatura);
  }

  /**
   * Envia a guardar en la base de datos toda la información que posee el objeto `Syllabus`
   * 
   * @param Syllabus Objeto que contiene todos los datos a ser guardados.
   */
  saveSyllabus(Syllabus:any): Observable<any>{
    return this.http.post(this.URL_ROOT_WS_SILABOS + '/grabarSyllabus',Syllabus);
  }

  /**
   * Obtiene la información del syllabus y sus contenidos
   * 
   * @param idSyllabus Identificador de syllabus
   */
  getSyllabusContenidos(idSyllabus): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS + '/getSyllabusContenidos/'+idSyllabus);
  }

  /**
   * Valida la creación o no de un syllabus de una determinada asignatura
   * 
   * @param idAsignatura Identificador de la asignatura
   */
  validaAccesSyllabus(idAsignatura:any):Observable<any>{
    return this.http.get(this.URL_ROOT_WS_SILABOS+'/validaAccesSyllabus/'+idAsignatura);
  }
  
}
