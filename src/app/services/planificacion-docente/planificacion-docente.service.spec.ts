import { TestBed } from '@angular/core/testing';

import { PlanificacionDocenteService } from './planificacion-docente.service';

describe('PlanificacionDocenteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanificacionDocenteService = TestBed.get(PlanificacionDocenteService);
    expect(service).toBeTruthy();
  });
});
