import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable()
export class PersonasService {
  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS = this.URL_SERVER + '/persona';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarTodos');
  }
  //metodo de paginado
  getAllPage(pageNumber:string, size:string):Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPaginado?page=' + pageNumber +'&size=' +size);
  }
  //metodo de paginado y ordenado
   getAllPageSort(pageNumber:string, size:string, direction:string, sortfield:string ):Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPaginadoOrdenado?page=' + pageNumber +'&size=' +size+'&direction='+direction+'&sortfield='+sortfield);
  }

  //metodo de filtrado paginado y ordenado
  getFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarFiltradoPaginadoOrdenado',pageinformation);
    return result;
  }

  getUserFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarUserPaginadoOrdenado',pageinformation);
    return result;
  }
//metodo de filtrado por Id
  getBuscarIdFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarBuscarIdPaginadoOrdenado',pageinformation);
    return result;
  }

  get(id: string): Observable<any> {
    let result:Observable<Object>;
    result= this.http.get(this.URL_ROOT_WS + '/buscar/' + id);
    return result;
  }

  save(persona: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabar', persona); 
    return result;
   
  }

  savePersonaUsuario(persona: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarPersonaUsuario', persona); 
    return result;
   
  }

  remove(id: string) {
    return this.http.delete(this.URL_ROOT_WS + '/borrar/' + id);
  }

  removeUsuario(id: string) {
    return this.http.delete(this.URL_ROOT_WS + '/borrarUsuario/' + id);
  }
  /*
  saveUsuario(usuario: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarUsuario', usuario);
    return result;
  }
  */
  getByCedula(cedula: string) {
    return this.http.get(this.URL_ROOT_WS + '/buscarPorCedula/' + cedula);
  }

  getListModulos(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS + '/listaModulos');
  }

  getListDepartamentos(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS + '/listaDepartamentos');
  }
 
  getListRolesModulos(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS + '/listaRolesModulos');
  } 
}
