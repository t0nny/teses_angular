import { TestBed } from '@angular/core/testing';

import { MallasService } from './mallas.service';

describe('MallasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MallasService = TestBed.get(MallasService);
    expect(service).toBeTruthy();
  });
});
