import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MallasService {
  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_MALLAS = this.URL_SERVER + '/api/mallas';
  public URL_ROOT_WS1 = this.URL_SERVER + '/api/asignaturas';
  public URL_ROOT_WSNIVEL = this.URL_SERVER + '/api/nivel';
  public URL_ROOT_SECUENCIA = this.URL_SERVER + '/api/secuencia';
  public URL_ROOT_REQUSITO = this.URL_SERVER + '/api/requisito';
  public URL_ROOT_WS_periodo = this.URL_SERVER + '/api/periodoacademico';
  public URL_ROOT_WS_tipoOferta = this.URL_SERVER + '/api/tipooferta';
  public URL_ROOT_WS_DEPARTAMENTO = this.URL_SERVER + '/api/departamento';
  public URL_ROOT_WS_OFERTA = this.URL_SERVER + '/api/oferta';
  //componenteaprendizaje
  public URL_ROOT_WSCOMP_APRENDI = this.URL_SERVER + '/api/componenteaprendizaje';

  constructor(private http: HttpClient) {

  }

  //*************+Fin Interfaz Malla Carrera principal *************/
  validaExisteAsignaturaOferta(idOferta: number): Observable<any> {//LISTADO DE PERIODO ACADEMICO
    return this.http.get(this.URL_ROOT_MALLAS + '/validaExisteAsignaturaOferta/' + idOferta)
  }

  buscaReglamentoNsemanas(): Observable<any> {//LISTADO DE PERIODO ACADEMICO
    return this.http.get(this.URL_ROOT_MALLAS + '/reglamento')
  }

  buscaPeriodo(idPeriodo: number): Observable<any> {//LISTADO DE PERIODO ACADEMICO
    return this.http.get(this.URL_ROOT_MALLAS + '/periodo/' + idPeriodo)
  }

  listarPeriodo(): Observable<any> {//LISTADO DE PERIODO ACADEMICO
    return this.http.get(this.URL_ROOT_WS_periodo + '/buscarPeriodoAcademico')
  }

  listarTipoOferta(): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_tipoOferta + '/buscarTipoOferta')
  }

  listarDepartamentoPorTipoOferta(idTipoOferta: number): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS_DEPARTAMENTO + '/listaDepartamentoPorTipoOferta/' + idTipoOferta)
  }

  listarMallaVersionPorPeriodoTipoOferta(idTipoOferta: number, idPeriodo: number, idDepartamento: number): Observable<any> {//LISTADO DEDICACION
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarMallasVersionPorPeriodoTipoOferta/' + idTipoOferta + '/' + idPeriodo + '/' + idDepartamento)
  }

  listarComponenteAprendizajeForm(): Observable<any> {
    return this.http.get(this.URL_ROOT_WSCOMP_APRENDI + '/listarComponenteAprendizajeForm')
  }
  listarOfertaPorDepartamentoTipoOferta(idDepartamento: number, idTipoOferta: number): Observable<any> {//LISTADO DEDICACION
    return this.http.get(this.URL_ROOT_WS_OFERTA + '/filtrarPorDepartamentoTO/' + idDepartamento + '/' + idTipoOferta)
  }

  verificarMallaUsada(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WSNIVEL + '/verificarMallaUsada/' + idMalla);
  }

  getAll(): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarPorOferta');
  }
  getReglamentoVigente(): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarReglamentoRegimenAcademico');
  }
  getBuscaReglamento(idReglamento: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarReglamento/' + idReglamento);
  }

  getPeriodoDesde(fechaDesde: Date): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarPeriodoDesde/' + fechaDesde);
  }
  //Lista los niveles para ubicarlos como nombres de columnas del kanban en la agrupacion de materias po niveles
  getColumnaNiveles(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WSNIVEL + '/buscarNivelesColumna/' + idMalla);
  }
  //Lista las materias agrupadas por niveles (años y semestre) y las ubica en su correspondiente UOC - AB
  getlistarNivelesUnidades(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listarNivelesUnidades/' + idMalla);
  }

  get(id: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarporcampoformacion/' + id);
  }

  getlistarAsignaturaOferta(idMalla: string, idOferta: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarPorAsignaturaOferta/' + idMalla + '/' + idOferta);
  }

  //Lista las materias que estan en mallaasigantura y que pertenecen a una oferta y a una malla
  getMallaAsignatura(id: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarPorMallaAsignatura/' + id);
  }

  getcomponentesAprendizaje(id: string) {
    return this.http.get(this.URL_ROOT_WSCOMP_APRENDI + '/listarComponenteAprendizajeVigente/' + id);
  }

  getEnviaComponentesAprendizaje(id: string) {
    return this.http.get(this.URL_ROOT_WSCOMP_APRENDI + '/listarAsignaturaCompAprendizaje/' + id);
  }


  getMallaAsiganutraCampoF(id: string, idOferta: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarMateriasCampFormacion/' + id + '/' + idOferta);
  }

  //Listas materias asigandas a un nivel(semestre o año) por idmallaversion y idnivel AB
  getMallaAsiganutraNivel(id: string, idNivel: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarPorMallaAsignaturaNivel/' + id + '/' + idNivel);
  }

  // Listas materias asigandas a un nivel(semestre o año) por idmallaversion y idnivel - Solo si existen en asignatura organizacion AB
  getMallaAsignaturaNivelAo(id: number, idNivel: number) {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarPorMallaAsignaturaNivelAo/' + id + '/' + idNivel);
  }

  //BUSCA MATERIAS POR OFERTA, MALLA Y NOMBRE 09-05-2019 PARA Y BUSQUEDA DE MATERIAS AB
  getMallaAsiganutraIdNombre(id: string, nombre: string, idOferta: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarPorNombreAsignatura/' + id + '/' + nombre + '/' + idOferta);
  }

  //BUSCA MATERIAS POR OFERTA Y NOMBRE 09-05-2019 PARA FILTRADO Y BUSQUEDA DE MATERIAS AB
  getMallaAsiganutraIdNombreCF(id: string, nombre: string) {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarPorNombreAsignaturaCF/' + id + '/' + nombre);
  }

  getDeleteMallaAsiganutra(id: string) {
    return this.http.delete(this.URL_ROOT_MALLAS + '/borrarMallaAsignatura/' + id);
  }

  getDeleteAsiganutraAprendizaje(id: string) {
    return this.http.delete(this.URL_ROOT_MALLAS + '/borrarasiganturaOrganizacion/' + id);
  }

  //Graba en asignatura organizacion materias asociadas a una UOC -AB
  getasignaunidadcomponente(tipoTransaccion, idNivel: string, idComponente: string, idMalla: string) {
    return this.http.get(this.URL_ROOT_WS1 + '/GrabarAsignaturasOrga/' + tipoTransaccion + '/' + idNivel + '/' + idComponente + '/' + idMalla);
  }
  getAllMallaFacultad(): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarOfertaPorFacultades');
  }
  getAllMallaCarrera(): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarOfertaPorFacultadesTodos');
  }

  buscarMallaEnDistributivo(idMalla: number) {
    // alert(JSON.stringify(id));
    return this.http.get(this.URL_ROOT_MALLAS + '/listaMallaUtilizadaDistributivo/' + idMalla);
  }
  buscarMallaPorId(id: number) {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarPorMallasId/' + id);
  }

  buscarCalculoAsignatura(id: string) {
    // alert(JSON.stringify(id));
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarPorMallasAsigId/' + id);
  }
  //Lista las materias y las presenta agrupadas por nivel(semestre o años) en el 1er kanban de asginacion de UOC
  getAllMallaNivel(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarPorMallaNivel/' + idMalla);
  }

  //+++++++++Servicios para la interfaz de Asignación de Niveles+++++++++++++++++++++

  getRecuperarNombresCarreraFacultad(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/recuperarNombresCarreraFacultad/' + idMalla);
  }

  getListarAsignaturasComponentesOrganizacion(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listarAsignaturasComponentesOrganizacion/' + idMalla);
  }

  getListarAsignaturasAprendizajeMalla(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listarAsignaturasAprendizajeMalla/' + idMalla);
  }

  getListarAsignaturasAprendizajeDistributivo(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listarAsignaturasAprendizajeDistributivo/' + idMalla);
  }


  getMallaVersionId(id: string): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarMallasVersionId/' + id);
  }

  grabaEstadoMalla(mallaNueva: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_MALLAS + '/grabarMallaEstado', mallaNueva);
    return result;
  }

  eliminarMalla(mallaNueva: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_MALLAS + '/eliminarMalla', mallaNueva);
    return result;
  }


  savemallaCampoF(mallascampoF: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_MALLAS + '/grabarAsignaturaOrganizacion', mallascampoF);
    return result;
  }

  savemallaCompoA(mallascompoA: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_MALLAS + '/grabarComponenteAsignatura', mallascompoA);
    return result;
  }

  //SERVICIOS PARA PANTALLA ASIGNAR PREREQUISITOS Y CORREQUISITO
  getFilterPageSort(pageinformation: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_MALLAS + '/buscarFiltradoPaginadoOrdenado', pageinformation);
    return result;
  }
  /***************Para pantalla Secuencia*******************/
  buscarPerrequisitoAsignatura(idMallaAsignatura): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/buscarPerrequisitosAsignatura/' + idMallaAsignatura);
  }

  buscarCorequisitoAsignatura(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/buscarCorrequisitosAsignatura/' + idMallaAsignatura);
  }


  getAllAsignaturaporCarrera(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/listarAsignaturasEnMalla/' + idMalla);
  }

  recuperarNombresAsignaturaNivel(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/recuperarNombresAsignaturaNivel/' + idMallaAsignatura);
  }

  recuperarPerrequisitosAsignatura(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/recuperarPerrequisitosAsignatura/' + idMallaAsignatura);
  }

  recuperarCorrequisitosAsignatura(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/recuperarCorrequisitosAsignatura/' + idMallaAsignatura);
  }

  saveAsignaturaRelacion(materiasRelaciones: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_SECUENCIA + '/grabarPreCorrequisitos', materiasRelaciones);
    return result;
  }

  /***************Para pantalla Secuencia*******************/

  findMallaRequisitoById(id: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrarMallasRequisitoId/' + id);
  }


  recuperarJsonMallaAsignatura(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_SECUENCIA + '/recuperarJsonMallaAsignatura/' + idMallaAsignatura);
  }


  removeMallaRequisito(id: string): Observable<any> {
    return this.http.delete(this.URL_ROOT_MALLAS + '/borrarMallaRequisito/' + id);
  }

  getAllMallaRequisito(id: string): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/filtrar/' + id);
  }

  saveMallaRequisito(algo: any): Observable<any> {
    return this.http.post(this.URL_ROOT_MALLAS + '/grabarMallaRequisito', algo);
  }

  buscarMallaRequisitoIdMallaIdRequisito(idMalla: string, idRequisito: string): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarMallaRequisitoIdMallaIdRequisito/' + idMalla + '/' + idRequisito);
  }

  buscarMallaRequisitoId(idMallaRequisito: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/buscarMallaRequisitoId/' + idMallaRequisito);
  }
  //*************+Para Interfaz UOC *************/

  getListarUOC(): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listarUOC');
  }

  listaMallaAsignaturaJsonUOC(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listaMallaAsignaturaJsonUOC/' + idMalla);
  }

  listaMallaAsignaturaJsonAprendizajes(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listaMallaAsignaturaJsonAprendizajes/' + idMalla);
  }

  getListaMallaAsignaturaJsonComponentes(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listaMallaAsignaturaJsonComponentes/' + idMalla);
  }

  grabaComponentesAsignatura(listaMallasAsignaturas: any): Observable<any> {
    return this.http.post(this.URL_ROOT_MALLAS + '/grabaComponentesAsignatura', listaMallasAsignaturas);
  }
  //*************+Fin Interfaz UOC *************/

  //*************+Para Interfaz Componentes Aprendizaje *************/

  listAllAsignaturasAprendizaje(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listAllAsignaturasAprendizaje/' + idMalla);
  }

  getListaMallaAsignaturaParaInterfazComponente(idMalla: number) {
    return this.http.get(this.URL_ROOT_MALLAS + '/listaMallaAsignaturaParaInterfazComponente/' + idMalla);
  }
  //*************+Fin Interfaz UOC *************/

  //*************+Para Interfaz malla Requisito *************/
  listarRequisitos(): Observable<any> {
    return this.http.get(this.URL_ROOT_REQUSITO + '/buscarRequisitos')
  }

  listarRequisitosPermitidosAsignatura(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_REQUSITO + '/listarRequisitosPermitidosAsignatura/' + idMalla);
  }

  listaMallaAsignaturaRequisitoJson(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_MALLAS + '/listaMallaAsignaturaRequisitoJson/' + idMalla);
  }

  grabarMallaRequisitos(Mallajson: any): Observable<any> {
    return this.http.post(this.URL_ROOT_REQUSITO + '/grabarMallaRequisitos', Mallajson);
  }
  
  listarHorasAsignadasEnMalla(idMalla: number): Observable<any> {
    return this.http.get(this.URL_ROOT_REQUSITO + '/listarHorasAsignadasEnMalla/' + idMalla);
  }

  grabarMalla(Mallajson: any): Observable<any> {
    return this.http.post(this.URL_ROOT_MALLAS + '/grabarMalla', Mallajson);
  }
  //*************+Fin Interfaz malla Requisito *************/


}