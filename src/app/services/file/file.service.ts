import { APP_CONFIG } from '../../config/app-config';
import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class FileService {

  constructor(private http: HttpClient) {}

  pushFileToStorage(file: File): Observable<any> {
    const uploadData: FormData = new FormData();
    uploadData.append('file', file, file.name);
    return this.http.post(APP_CONFIG.restUrl + '/files/uploadFile', uploadData, {
      reportProgress: true, observe: 'events'
    });
  }

  download(url: string): Observable<Blob> {
    return this.http.get(url, {responseType: 'blob'});
  }

  //desarrollad by Peter
  //guarda archivo al server resive como parametro un file
  pushFileToStoragePP(file: FormData): Observable<any> {
    let result: Observable<Object>;
    const uploadData: FormData = new FormData();
    //uploadData.append('file', file, file.name);
    result = this.http.post(APP_CONFIG.restUrl + '/api/files/uploadFileP', file);
    //debugger;
    return result;
    
  }  

  //descarga archivo del serve
  descargarFile(fileName: string): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/files/uploadFileP/' + fileName , {responseType: 'blob'});
  }

}
