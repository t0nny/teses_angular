import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {//encabezados adicionales para operaciones
  headers: new HttpHeaders({//permite el filtrado adicional con caracteres especiales.
    'Content-Type': 'application/json;charset=utf-8'
  })
};

@Injectable()
export class AsignaturasService {

  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS_ASIGNATURAS = this.URL_SERVER + '/api/asignaturas';
  public URL_ROOT_WS_DEPARTAMENTOS = this.URL_SERVER + '/api/departamento'

  constructor(private http: HttpClient) { }

  listaDepartamento(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_DEPARTAMENTOS + '/listaDepartamento')
  }

  listarOfertasPorDepartamentos(idfacultad: any) {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/listarOfertasPorDepartamentos/' + idfacultad);
  }

  /**
   * @description Listado de carreras vigentes//agregar filtro por facultad.
   */
  listarOfertas(idFacultad: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/listarOfertas/' + idFacultad)
  }

  /**
  * @description Listado de carreras vigentes//agregar filtro por facultad.
  */
  listarCarreras(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/listarCarreras')
  }

  /**
   * @description Método que obtiene las asignaturas que estan presentes en al menos una carrera.
   */
  getAllOfertaAsignatura(idOferta: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscarOfertaAsignaturas/' + idOferta);
  }

  /**
   * @description Método que busca una Asignatura por su identificador para poder editar si se lo desea.
   * 
   * @param idAsignatura Identificador de asignatura
   */
  findAsignaturaById(idAsignatura: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscarAsignatura/' + idAsignatura);
  }

  /**
   * @description Busca una Asignatura cuyo nombre se pueda encontrar en una carrera de acuerdo al identificador del mismo.
   * 
   * @param descripcion nombre de la asignatura
   * @param idOferta identificador de la oferta
   */
  findAsignaturaByName(descripcion: string, idOferta: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscarDescripcionAsignatura/' + descripcion + '/' + idOferta, httpOptions);
  }

  /**
   * @description Graba o edita una Asignatura.
   * 
   * @param ofertaAsignatura cuerpo en formato JSON de una asignatura a grabar o editar
   */
  saveOfertaAsignatura(ofertaAsignatura: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_ASIGNATURAS + '/grabarAsignatura', ofertaAsignatura);
  }

  grabarAsignaturaV2(asignatura: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_ASIGNATURAS + '/grabarAsignaturaV2', asignatura);
  }

  /**
   * @description Quita una Asignatura agregando la fecha actual del sistema a la fecha de finalización
   * de la misma y además le cambia el estado A => I
   * 
   * @param id identificador de Asignatura
   */
  removeOfertaAsignatura(id: string): Observable<any> {
    return this.http.delete(this.URL_ROOT_WS_ASIGNATURAS + '/borrarAsignatura/' + id);
  }

  /**
   * @description Consulta si la asignatura en específico ya se encuentra en una malla publicada o no.
   * 
   * @param idAsignatura identificador de Asignatura
   */
  findPublishedSubject(idAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/consultaAsignaturaPublicada/' + idAsignatura);
  }

  //----------------- METODOS PARA ASIGNATURA COMUN ------------------------------
  //----- principal
  /**
   * Método que obtiene las asignaturas que estan presentes en al menos una carrera sin que ésta se repita en el listado.
   */
  getListOfertaAsignaturaComun(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscarOfertaAsignaturasComuns');
  }

  listaAsignaturasComunUtilizadaMalla(idAsignatura:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/listaAsignaturasComunUtilizadaMalla/' + idAsignatura);
  }


  //------------------------------------------------------------------------------
  //nuevo-edicion
  /**
   * @description Busca una asignatura y me devuelve un archivo JSON con todos los datos de la asignatura
   * y su relación con la oferta.
   * 
   * @param idAsignatura identificador de asignatura
   */
  findAsignaturaByIdComun(idAsignatura: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscaAsignaturaById/' + idAsignatura)
  }

  //CONSULTA SI EL CODIGO INGRESADO YA SE ENCUENTRA EN LA BASE DE DATOS
  /**
   * @description Consulta si el código ingresado ya existe, ésto es útil solo al momento de crear una nueva asignatura.
   * 
   * @param codigo codigo de la asignatura
   */
  buscarCodigoAsignatura(codigo: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscarCodigoAsignatura/' + codigo)
  }

  buscarOfertaById(idOferta: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIGNATURAS + '/buscarOfertaById/' + idOferta)
  }

}