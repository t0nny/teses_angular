import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../config/app-config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//

@Injectable()
export class ParametricasService {
  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS = this.URL_SERVER + '/api/parametricas';
  public URL_ROOT_PERIODOS = this.URL_SERVER + '/api/periodoacademico';
  public URL_ROOT_PERIODOSANIO = this.URL_SERVER + '/api/periodo';
  public URL_ROOT_MODALIDAD = this.URL_SERVER + '/api/modalidades';
  public URL_ROOT_TIPOS = this.URL_SERVER + '/api/tipooferta';
  public URL_ROOT_OFERTA = this.URL_SERVER + '/api/oferta';
  
  
  constructor(private http: HttpClient) { }
//llama desde siiarest
  getAll(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarParametrica');
  }
//metodo de paginado
  getAllPage(pageNumber:string, size:string):Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPaginado?page=' + pageNumber +'&size=' +size);
  }
//metodo de paginado y ordenado
   getAllPageSort(pageNumber:string, size:string, direction:string, sortfield:string ):Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPaginadoOrdenado?page=' + pageNumber +'&size=' +size+'&direction='+direction+'&sortfield='+sortfield);
  }

//metodo de filtrado paginado y ordenado
  getFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarFiltradoPaginadoOrdenado',pageinformation);
    return result;
  }

  getUserFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarUserPaginadoOrdenado',pageinformation);
    return result;
  }
//metodo de filtrado por Id
  getBuscarIdFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarBuscarIdPaginadoOrdenado',pageinformation);
    return result;
  }

  get(id: string) {
    return this.http.get(this.URL_ROOT_WS + '/buscarParametricaId/' + id);
  }
  // LISTAR PERIODO ACADEMICO POR ID
  getBuscarPeriodo() {
    return this.http.get(this.URL_ROOT_PERIODOSANIO + '/buscarPeriodosAnio');
  }
  getBuscarPeriodoId(id:any) {
    return this.http.get(this.URL_ROOT_PERIODOSANIO + '/buscarPeriodosId/' + id);
  }

  save(parametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarParametrica', parametrica);
    return result;
  }

  remove(id: string) {
    return this.http.delete(this.URL_ROOT_WS + '/borrar/' + id);
  }

  getListarPeriodosAcad() {
    return this.http.get(this.URL_ROOT_PERIODOS + '/buscarPeriodoAcademico');
  }

  // LISTAR PERIODO ACADEMICO POR ID
  getListarPeriodosAcadId(id:any) {
    return this.http.get(this.URL_ROOT_PERIODOS + '/filtrarPeriodoAcadId/' + id);
  }

  getListarPeriodos() {
    return this.http.get(this.URL_ROOT_PERIODOSANIO + '/buscarPeriodosAnio');
  }

  getListarPeriodosAnio(id) {
    return this.http.get(this.URL_ROOT_PERIODOSANIO + '/buscarPeriodosAnioId/' + id);
  }

  getListaModalidad() {
    return this.http.get(this.URL_ROOT_MODALIDAD + '/buscarModalidad');
  }

  getListaTipos() {
    return this.http.get(this.URL_ROOT_TIPOS + '/buscarTipoOferta');
  }


  grabarPeriodo(periodo: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_PERIODOSANIO + '/grabarPeriodo', periodo);
    return result;
  }

  // GRABAR PERIODO ACADEMICO, PERIODO MODALIDAD, PERIODO TIPO OFERTA Y DISTRIBUTIVO GENERAL

  grabarPeriodoAcademico(periodoAcaddemico: any): Observable<any> {
    let result: Observable<Object>;
  //  console.log(JSON.stringify(periodoAcaddemico),JSON.stringify(distibutivoGeneral));
    result = this.http.post(this.URL_ROOT_PERIODOS + '/grabarPeriodoAcademico', periodoAcaddemico);
    return result;
  }
  //
  getBuscarReglamentoRegimenAca() {
    return this.http.get(this.URL_ROOT_PERIODOSANIO + '/buscarPorReglamentoRegimenAca');
  }
  getBuscarIdReglamentoDistributivo(id) {
    return this.http.get(this.URL_ROOT_PERIODOSANIO + '/buscarPorTipoReglamento/' + 1);
  }
  
  eliminancionLogicaPeriodo(id: number) {
    return this.http.delete(this.URL_ROOT_PERIODOS + '/borrar/' + id);
  }
  //BUSCAR OFERTAS
  getBuscarOfertasActivas() {
    return this.http.get(this.URL_ROOT_OFERTA + '/buscarOfertasPre');
  }

}