import { TestBed } from '@angular/core/testing';

import { ParametricasService } from './parametricas.service';

describe('ParametricasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParametricasService = TestBed.get(ParametricasService);
    expect(service).toBeTruthy();
  });
});
