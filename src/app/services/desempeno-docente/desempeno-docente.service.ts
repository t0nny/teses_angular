import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

/*@Injectable({
  providedIn: 'root'
})*/
const httpOptions = {//encabezados adicionales para operaciones
  headers: new HttpHeaders({//permite el filtrado adicional con caracteres especiales.
    'Content-Type':  'application/json;charset=utf-8'
  })
};
@Injectable()
export class DesempenoDocenteService { 
  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS_OPC = this.URL_SERVER + '/api/opciones';
  public URL_ROOT_WS_INS = this.URL_SERVER + '/api/instrumentoPorEvaluacion';
  public URL_ROOT_WS_PEV = this.URL_SERVER + '/api/periodoEvaluacion';
  public URL_ROOT_WS_PRE = this.URL_SERVER + '/api/pregunta';
  public URL_ROOT_WS_HET = this.URL_SERVER + '/api/evaluaciones';
  public URL_ROOT_WS_PARAMETRICA = this.URL_SERVER + '/api/parametricasEvaluacion';
  public URL_ROOT_WS_EDG = this.URL_SERVER + '/api/evaluacionDocenteGeneral';
  public URL_ROOT_WS_TE = this.URL_SERVER + '/api/tipoEvaluacion';
 
  constructor(private http: HttpClient) {
  }
   
 /* getOpcionPregunta(id: number) : Observable<any>{
    return this.http.get(this.URL_ROOT_WS_OPC + '/listarOpcionPregunta/' + id);
  }*/

  getAllOpcionRespuestaPregunta(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_OPC + '/listarOpcionRespuestaPregunta');
  }

  getPreguntaInstrumento(id: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_INS + '/listarPeguntaInstrumento/' + id);
  }

  getMateriaDocentePorEstudiante(idEstudiante: number, idSemestre: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PEV + '/listarMateriaDocentePorEstudiante/' + idEstudiante + '/' + idSemestre);
  }

  getSemestrePorUsuarioEstudiante(usuario: String): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PEV + '/listarSemestreUsuarioEstudiante/' + usuario );
  }

  //GRABAR EVALUACION
  saveHeteroevaluacion(evaluacion: any): Observable<any>{
    return this.http.post(this.URL_ROOT_WS_HET + '/grabarHeteroevaluacion',evaluacion);
  }

  //metodos para las parametricas  
  getAllCalificacionEscala(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarCalificacionEscala');
  }
  getAllReglaCalificacionEscala(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarReglaCalificacion');
  }
  getAllReglaCalificacionConId(id: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarReglaCalificacionConId/' + id );
  }

  getAllCategoriaPregunta(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarCategoriaPregunta');
  }

  getAllFuncionEvaluacion(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarFuncionEvaluacion');
  }

  getAllTipoPregunta(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarTipoPregunta');
  }

  getAllTipoRecursoApelacion(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarTipoRecursoApelacion');
  }
  getAllOpcionRespuesta(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/listarOpcionRespuesta');
  }


  //recuperan por id
  getBuscarCalificacionEscala(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarCalificacionEscala/' + idParametrica);
  }

  getBuscarReglaCalificacionEscala(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarReglaCalificacionEscala/' + idParametrica);
  }

  getBuscarCategoriaPregunta(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarCategoriaPregunta/' + idParametrica);
  }

  getBuscarFuncionEvaluacion(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarFuncionEvaluacion/' + idParametrica);
  }

  getBuscarTipoPregunta(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarTipoPregunta/' + idParametrica);
  }

  getBuscarTipoRecursoApelacion(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarTipoRecursoApelacion/' + idParametrica);
  }

  getBuscarOpcionRespuesta(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarOpcionRespuesta/' + idParametrica);
  }

  //para grabar
  grabarCalificacionEscala(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarCalificacionEscala', pojoParametrica);
    return result;
  }
  grabarReglaCalificacionEscala(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarReglaCalificacionEscala', pojoParametrica);
    return result;
  }

  grabarCategoriaPregunta(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarCategoriaPregunta', pojoParametrica);
    return result;
  }

  grabarFuncionEvaluacion(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarFuncionEvaluacion', pojoParametrica);
    return result;
  }

  grabarTipoPregunta(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarTipoPregunta', pojoParametrica);
    return result;
  }
 
  grabarTipoRecursoApelacion(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarTipoRecursoApelacion', pojoParametrica);
    return result;
  }
  grabarOpcionRespuesta(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarOpcionRespuesta', pojoParametrica);
    return result;
  }

 //para eliminar un campo
  eliminarCalificacionEscala(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarCalificacionEscala/' + idParametrica );
  }

  eliminarReglaCalificacionEscala(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarReglaCalificacionEscala/' + idParametrica );
  }

  eliminarCategoriaPregunta(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarCategoriaPregunta/' + idParametrica );
  }

  eliminarFuncionEvaluacion(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarFuncionEvaluacion/' + idParametrica );
  }

  eliminarTipoPregunta(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarTipoPregunta/' + idParametrica );
  }

  eliminarTipoRecursoApelacion(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarTipoRecursoApelacion/' + idParametrica );
  }
  eliminarOpcionRespuesta(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarOpcionRespuesta/' + idParametrica );
  }
   //------------------------------------------------------------------------------------------//
  //       SERVICIO PARA MANTENEDORES DE PREGUNTAS E INSTRUMENTOS    //
  //------------------------------------------------------------------------------------------//

  getAllPreguntaCategoria(id: number) : Observable<any>{
    return this.http.get(this.URL_ROOT_WS_PRE + '/listarPreguntaCategoria/' + id);
  }

  getAllPregunta() : Observable<any>{
    return this.http.get(this.URL_ROOT_WS_PRE + '/listarPregunta');
  }
  
  getAllOpcionPregunta() : Observable<any>{
    return this.http.get(this.URL_ROOT_WS_OPC + '/listarOpcionPreguntaRespuesta');
  }
  getAllInstrumentos() : Observable<any>{
    return this.http.get(this.URL_ROOT_WS_INS + '/listarInstrumentos');
  }

  getBuscarInstrumento(idParametrica: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARAMETRICA + '/buscarInstrumento/' + idParametrica);
  }

  grabarInstrumento(pojoParametrica: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PARAMETRICA + '/grabarInstrumento', pojoParametrica);
    return result;
  }
  eliminarInstrumento(idParametrica: number) {
    return this.http.delete(this.URL_ROOT_WS_PARAMETRICA + '/eliminarInstrumento/' + idParametrica );
  }

  /**
   * @description Busca una pregunta cuyo nombre se pueda encontrar en un instrumento de acuerdo al identificador del mismo.
   * 
   * @param descripcion de pregunta
   * @param idOpcion identificador de la opcion
   */
  buscarPreguntaPorDescripcion(descripcion:string): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_PRE + '/buscarDescripcionPregunta/' + descripcion, httpOptions);
  }  
    /**
   * @description Graba o edita una Pregunta.
   * 
   * @param opcionPregunta cuerpo en formato JSON de una asignatura a grabar o editar
   */
  grabarOpcionPregunta(opcionPregunta: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PRE + '/grabarPregunta', opcionPregunta);
    return result;
  }

  /**
   * @description Quita una Asignatura agregando la fecha actual del sistema a la fecha de finalización
   * de la misma y además le cambia el estado A => I
   * 
   * @param id identificador de Asignatura
   */
  eliminarPreguntaInstrumento(id: string): Observable<any>{
    return this.http.delete(this.URL_ROOT_WS_INS + '/borrarPreguntaInstrumento/' + id);
  }
    
  //------------------------------------------------------------------------------------------//
  //       SERVICIO PARA MANTENEDORES DE EVALUACION DOCENTE GENERAL, PERIODO EVALUACION       //
  //------------------------------------------------------------------------------------------//

  /**
   * Método que me permite obtener registros de Evaluación Docente General activos
   */
  getAllEvalDocGral(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_EDG + '/getAllEvalDocGral');
  }

  listarInstrumentos(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_EDG + '/listarInstrumentos');
  }

  /**
   * Método que me permite conocer si un registro de evlauación docente general se encuentra en un período de evaluación
   * @param idEvalDocGral Identificador de Evaluación Docente General
   */
  // getPEvalDoc(idEvalDocGral:any): Observable<any>{
  //   return this.http.get(this.URL_ROOT_WS_EDG + '/getPEvalDoc/'+idEvalDocGral);
  // }

  /**
   * Método que me permite obtener todos los reglamentos
   */
  getReglamentos(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_EDG + '/getReglamentos');
  }

  /**
   * Método que me permite grabar o editar una Evaluación Docente general
   * @param evalDocGral Objeto de tipo Evaluacion Docente General
   */
  saveEvalDocenteGeneral(evalDocGral:any): Observable<any>{
    return this.http.post(this.URL_ROOT_WS_EDG + '/saveEvalDocenteGeneral',evalDocGral)
  }

  // /**
  //  * Método que me permite obtener una evaluación docente general por medio de un identificador.
  //  * @param idEvalDocGral Identificador de Evaluación Docente General
  //  */
  // getEvalDocGralById(idEvalDocGral:any): Observable<any>{
  //   return this.http.get(this.URL_ROOT_WS_EDG + '/getEvalDocGralById/' + idEvalDocGral);
  // }

  /**
   * Método que me permite eliminar una Evaluación Docente General por medio de un identificador.
   * @param idEvalDocGral Identificador de Evaluación Docente General
   */
  deleteEvaluacionDocenteGeneral(idEvalDocGral: any): Observable<any>{
    return this.http.delete(this.URL_ROOT_WS_EDG + '/deleteEvaluacionDocenteGeneral/' + idEvalDocGral);
  }


  /**
   * Busca y lista todos los períodos académicos vigentes
   */
  buscarPeriodoAcademico(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_EDG + '/findPeriodosAcademicosVigentes')
  }


  //------------------------------------------------------------------------------------------//
  //           SERVICIO PARA MANTENEDORES DE TIPO_EVALUACION - REGLA_COMPONENTE               //
  //------------------------------------------------------------------------------------------//

  findComponente(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_TE + '/findComponentes')
  }

  findFuncionEvaluacion(): Observable<any>{
    return this.http.get(this.URL_ROOT_WS_TE + '/findFuncionEvaluacion')
  }

  saveReglaComponente(reglaComponente: any): Observable<any>{
    return this.http.post(this.URL_ROOT_WS_TE + '/saveReglaComponente',reglaComponente);
  }

}
