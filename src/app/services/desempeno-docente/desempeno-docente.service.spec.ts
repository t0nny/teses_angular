import { TestBed } from '@angular/core/testing';

import { DesempenoDocenteService } from './desempeno-docente.service';

describe('DesempenoDocenteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DesempenoDocenteService = TestBed.get(DesempenoDocenteService);
    expect(service).toBeTruthy();
  });
});
