import { APP_CONFIG } from '../../config/app-config';
import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable()
export class OpcionesAsignaturaService {


  constructor(private http: HttpClient) { }

  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS_periodo = this.URL_SERVER + '/api/periodoacademico';
  public URL_ROOT_WS_docente = this.URL_SERVER + '/api/docentes';
  public URL_ROOT_WS_asignatura = this.URL_SERVER + '/api/asignaturas';
  public URL_ROOT_WS_calificacion = this.URL_SERVER + '/api/calificacion';
  public URL_ROOT_WS_PLANIFICACION_DOCENTE = this.URL_SERVER + '/api/planificacionDocente';

  listarPeriodo(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_periodo + '/buscarPeriodoAcademico')
  }

  periodoAcademico(id: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_periodo + '/buscarPeriodoAcademicoId/' + id)
  }

  docente(id: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_docente + '/buscarDocentePorId/' + id)
  }

  asignatura(id: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_asignatura + '/buscarAsignaturaIdMallaAsig/' + id)
  }

  paralelo(id: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/buscarParaleloPorId/' + id)
  }

  listarDocente(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaDocente')
  }
  listarDocenteAsignatura(idPeriodo: number, idDocente: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaDocenteAsignatura/' + idPeriodo + '/' + idDocente)
  }
  listarResumenSilaboAsignatura(idPeriodo: number, idOferta: number, idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaResumenSilaboAsignatura/' + idPeriodo + '/' + idOferta + '/' + idMallaAsignatura)
  }
  listarSilaboAsignaturaCab(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaSilaboAsignaturaCab/' + idMallaAsignatura)
  }
  listarSilaboAsignaturaDet(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaSilaboAsignaturaDet/'+ idMallaAsignatura)
  }

  listarCiclos(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/listaCiclosPorPeriodo')
  }

  listarEstudiantes(idPeriodoAcademico: number, idAsignatura: number, idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/listarEstudiantesPorPeriodoAsignaturaMateria/' + idPeriodoAcademico + '/' + idAsignatura + '/' + idParalelo)
  }

  listarComponentesAprendizaje(idPeriodoAcademico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/listarComponenteAprendizaje/' + idPeriodoAcademico)
  }

  listarEstudianteRegistroCalificaciones(idPeriodoAcademico: number, idAsignatura: number, idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/listar2/' + idPeriodoAcademico + '/' + idAsignatura + '/' + idParalelo)
  }

  columnasComponenteAprendizaje(idPeriodoAcademico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/getColumnasComponenteAprendizaje/' + idPeriodoAcademico)
  }

  grupoColumnaCiclo(idPeriodoAcademico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/getGrupoColumnaCiclo/' + idPeriodoAcademico)
  }

  listaCalificacionesEstudiante(idPeriodoAcademico: number, idAsignatura: number, idParalelo: number): Observable<any> {
    console.log(idPeriodoAcademico + "   " + idAsignatura)
    return this.http.get(this.URL_ROOT_WS_calificacion + '/listaCalificacionesEstudiante/' + idPeriodoAcademico + '/' + idAsignatura + '/' + idParalelo)
  }

  grabar(acta: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_calificacion + '/grabarActa', acta)
  }

  grabarPromedios(arregloEstudianteAsignatura: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_calificacion + '/grabarPromedioEstudiante', arregloEstudianteAsignatura)
  }

  cerrarActa(acta: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_calificacion + '/cerrarActa', acta)
  }

  cicloVigente(idPeriodoAcademico: any, idDocenteAsignaturaApren: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_calificacion + '/cicloVigente/' + idPeriodoAcademico + '/' + idDocenteAsignaturaApren)
  }

  //Para pantalla de registro de plan de clases
  getListaMetodologias(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaMetodologiasEnsenanza')
  }

  getListaEvaluaciones(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaEstrategiasEvaluacion')
  }

  getRecuperarPlanClaseId(idPlanClase: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/recuperarPlanClaseId/' + idPlanClase)
  }

  getRecuperarContenidoId(idContenido: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/recuperarContenidoId/' + idContenido)
  }

  getListaAutores(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaAutores')
  }

  getListaEditoriales(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaEditoriales')
  }

  getListaTiposBibliografias(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaTiposBibliografias')
  }

  getListaBibliografias(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaBibliografias')
  }

  getListarAreasConocimiento(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listarAreasConocimiento')
  }

  getListaTodasBibliografias(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaTodasBibliografias')
  }

  getListarAsignaturas(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listarAsignaturas')
  }

  getListaPlanesClase(idMallaAsignatura: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/listaPlanesClase/'  + idMallaAsignatura)
  }

  getRecuperarRecursoBibliograficoId(idRecursoBibliografico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/recuperarRecursoBibliograficoId/' + idRecursoBibliografico)
  }

  getRecuperarHoras(idContenido: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/recuperarHoras/' + idContenido)
  }

  borrarBibliografia(idPersona: number) {
    return this.http.delete(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/borrarBibliografia/' + idPersona);
  }

  borrarPlanClase(idPersona: number) {
    return this.http.delete(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/borrarPlanClase/' + idPersona);
  }

  grabarPlanClase(planClase: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/grabarPlanClase', planClase)
  }

  grabarBibliografia(recursoBibliografico: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/grabarBibliografia', recursoBibliografico)
  }

  getReporteSylabos(idPaciente: number): Observable<Blob> {
    return this.http.get(this.URL_ROOT_WS_PLANIFICACION_DOCENTE + '/getReporteSylabos/' + idPaciente, { responseType: 'blob' });
  }


}
