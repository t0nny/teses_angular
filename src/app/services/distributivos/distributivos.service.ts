import { APP_CONFIG } from '../../config/app-config';
import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()

export class DistributivosService {

  public URL_SERVER = APP_CONFIG.restUrl;
  //Metodos Pantalla Distributivo General
  public URL_ROOT_WS = this.URL_SERVER + '/api/distributivoGeneral';
  public URL_ROOT_WS1 = this.URL_SERVER + '/api/actividadesDocencia';
  public URL_ROOT_WS_MallaAsigFusion = this.URL_SERVER + '/api/mallaAsignaturaFusion';
  //Metodos Pantalla Configuracion Distributivo
  public URL_ROOT_WS2 = this.URL_SERVER + '/api/periodoacademico';

  public URL_ROOT_WS3 = this.URL_SERVER + '/api/configuracionDistributivo';
  public URL_ROOT_WSDOCENTES = this.URL_SERVER + '/api/docentes';
  public URL_ROOT_WS_tipoOferta = this.URL_SERVER + '/api/tipooferta';
  //Metodos Pantalla Editar Distributivo
 
  
 
  constructor(private http: HttpClient) {

  }

  //metodo de filtrado paginado y ordenado
  getFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarFiltradoPaginadoOrdenado',pageinformation);
    return result;
  }

  //metodos distributivo general FINDALL
  getAllDistributivoGeneral(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/recuperarDistributivoGeneral ');
  }
  
  //metodos distributivo general ID
  getIdDistributivoGeneral(idDistributivoGeneral:string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarDistributivoGeneralId/' + idDistributivoGeneral);
    }


  //Activades de docencia configuracion general
    getAllActividadesDocencia(): Observable<any> {
      return this.http.get(this.URL_ROOT_WS1 + '/buscarActividadesDocencia');
    }
    listarTipoOferta(): Observable<any> {//LISTADO TIPO OFERTA
      return this.http.get(this.URL_ROOT_WS_tipoOferta + '/buscarTipoOferta')
    }

//PANTALLA CONFIGURACION DISTRIBUTIVO
recuperarPeriodoAcademico(id:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS2 + '/buscarPeriodoAcademicoId/' + id);
  }
/*
getAllOfertasporCarreraDistributivo(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDistributivosOfertasId/');
}*/

getAllOfertasporCarreraDistributivoPeriodo(idDistributivoGeneralVer:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDistributivosOfertasId/' + idDistributivoGeneralVer);
}

getValidarDistDocenteVacio(idDistributivoOfertaVer:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/validarDistDocenteVacio/' + idDistributivoOfertaVer);
}
getValidarDistDocenteMallaAsig(idPeriodoAcademico:number,idDistributivoOfertaVer:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/validarDistDocenteMallaAsig/' +idPeriodoAcademico+'/'+ idDistributivoOfertaVer);
}


//*********FIN Pantalla configuracion distributivo */


//PANTALLA DISTRIBUTIVO DETALLE
recuperarNombreOfertaDistributivo(id:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperarNombreOfertaDistributivo/' + id);
}
listarCantDocenteDed(id:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarCantDocenteDed/' + id);
}
periodoDistributivoOferta(idDistributivoOferta:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/periodoDistributivo/' + idDistributivoOferta);
}

listarCabeceraDistributivoOfertaDocente( idDistributivoOfertaVersion:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/getSPCabeceraDistributivoOfertaDocente/'+idDistributivoOfertaVersion);
}

listarDetalleDistributivoOfertaDocente( idDistributivoOfertaVersion:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/getSPDetalleDistributivoOfertaDocente/'+idDistributivoOfertaVersion);
}

listarDocentesDistributivo(idPeriodoAcademico: number, idDistributivoOfertaVersion:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDocentesDedicacion/' + idPeriodoAcademico+'/'+idDistributivoOfertaVersion);
}

detalleDocentesDedicacionID(idDocente:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDetalleDocentesDistributivoID/' + idDocente);
}

detalleDocentesDedicacion(idDistributivoOfertaVersion:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDetalleDocentesDistributivo/'+ idDistributivoOfertaVersion);
}

recuperarComponentesAsignaturaId(idAsignatura:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperarComponentesAsignaturaId/' + idAsignatura);
}
//*********FIN Pantalla distributivo Detalle */


//PANTALLA EDITAR DISTRIBUTIVO 

// getAllJornadas(): Observable<any> {
//   return this.http.get(this.URL_ROOT_WS3 + '/cargarJornadas ');
// }

getDocente(idDistributivoOfertaVersion: number , idDocente: number , idPeriodo: number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarDocente/'+ idDistributivoOfertaVersion+'/'+idDocente+'/'+ idPeriodo);
}

getAllDocentes(idDistributivoOfertaVersion: number ,  idPeriodo: number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarTodosDocentes/'+idDistributivoOfertaVersion+'/'+ idPeriodo);
}

 getAllDedicacionDocente(): Observable<any> {
   return this.http.get(this.URL_ROOT_WS3 + '/cargarDedicaciones ');
 }



getAllAsignaturas(idPeriodo:number,idOferta:number,idDistOfertaVersion:number,idDistributivoDoc:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listaOfertaAsignaturaAprendizaje/ '+ idPeriodo + '/' + idOferta + '/' + idDistOfertaVersion + '/' +idDistributivoDoc);
}


getDistributivoDocenteId(idDistributivoDocente:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/buscarDistributivoDocente/' + idDistributivoDocente);
}
getDistributivoOfertaVersion(idDistributivoOfertaVersion:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/buscarDistributivoOfertaVersion/' + idDistributivoOfertaVersion);
}

getDocenteAsigAprendizajeOtroDist(idDocente:number, idDistributivoDocente:number, idDistributivoGeneralVer:number, fechaDesde :Date, fechaHasta :Date): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/buscarDocenteAsigAprendOtroDis/'+idDocente+'/' + idDistributivoDocente+'/'+idDistributivoGeneralVer+'/'+fechaDesde+'/'+fechaHasta);
  
}

getDocenteAsigAprendizaje(idDistributivoOfertaVersion : number, idDistributivoDocente:number , idDocente: number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/getSPDetalleDistributivoOfertaDocenteAsignatura/'+idDistributivoOfertaVersion+'/'+idDistributivoDocente+'/'+idDocente);
  
}
getDedicacionDistributivoOfertaV(idDistributivoOfertaVersion : number, idDistributivoDocente:number , idDocente: number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/getSPDedicacionDistributivoOfertaDocente/'+idDistributivoOfertaVersion+'/'+idDistributivoDocente+'/'+idDocente);
  
}
getAsignaturaParaleloNoUtilizadaDist(idPeriodoAcademico : number, idDistributivoOfertaVersion:number ): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/asignaturaParaleloNoUtilizadaDist/'+idPeriodoAcademico+'/'+idDistributivoOfertaVersion);
  
}



/* 
getDistDedicacionOtroDist(idDocente:number, idDistributivoDocente:number, idDistributivoGeneralVer:number, fechaDesde :Date, fechaHasta :Date): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/buscarDocenteDedicacionOtroDis/'+idDocente+'/' + idDistributivoDocente+'/'+idDistributivoGeneralVer+'/'+fechaDesde+'/'+fechaHasta); 
}
getDocActividadOtroDist(idDocente:number, idDistributivoDocente:number, fechaDesde :Date, fechaHasta :Date, idDistributivoGeneralVer:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/buscarDocenteActividadOtroDis/'+idDocente+'/' + idDistributivoDocente+'/'+fechaDesde+'/'+fechaHasta+'/'+idDistributivoGeneralVer); 
} */

getDistributivoGeneralVer(idPeriodo: number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarDistributivoGen/'+ idPeriodo);
}
getAllFacultades(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarFacultad');
}
getFacultad(idDistributivoGeneralVer: number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarFacultades/'+ idDistributivoGeneralVer);
}
getFacultadDis(idDistributivoGeneralVer: number, usuario:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarFacultad/'+ idDistributivoGeneralVer+'/'+usuario);
}

getAllCarreras(idFacultad:string, idDistributivoGeneralver:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarCarreras/ '+ idFacultad + '/'+ idDistributivoGeneralver);

}

getDistributivosCarrera(idOferta:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDistributivosOfertasId/ '+ idOferta);

}

periodoValidoCrearDistributivo(idDistributivoOferta:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/periodoValidoCrearDistributivo/ '+ idDistributivoOferta);

}



/* recuperarIdDocenteAsignaturaAprend(idAsignaturaAprendizaje:string,idDocente:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperarIdDocenteAsignaturaAprend/ '+ idAsignaturaAprendizaje+ '/'+ idDocente);
  
} */

saveDistributivoDocente(distributivoDocente: any): Observable<any> {
  let result: Observable<Object>;
  result = this.http.post(this.URL_ROOT_WS3 + '/grabarDistributivo', distributivoDocente);
  return result;
}


 editaDistributivoOfertaVersion(editaDistributivoOfertaVersion: any): Observable<any> {
  let result: Observable<Object>;
  result = this.http.post(this.URL_ROOT_WS + '/grabarEdicionDistributivoOfertaVersion', editaDistributivoOfertaVersion);
  return result;
}


grabarElimancionDistributivoOfertaVersion(editaDistributivoOfertaVersion: any): Observable<any> {
  let result: Observable<Object>;
  result = this.http.post(this.URL_ROOT_WS + '/grabarElimancionDistributivoOfertaVersion', editaDistributivoOfertaVersion);
  return result;
}


 borrarDocenteDeDistributivo(idDistributivoOfertaVersion:number, idDocente:number){
  return  this.http.delete(this.URL_ROOT_WS3 + '/borrarDocenteDeDistributivo/'+ idDistributivoOfertaVersion + '/'+ idDocente);
}
eliminacionlogicaDistributivo(idDistributivoDocente:number){
  return  this.http.delete(this.URL_ROOT_WS3 + '/eliminacionLogicaDistDoc/'+ idDistributivoDocente);
}
/*
borrarDedicacionesDocente(idAsignatura:string, idDistributivoDocente:string){
  return  this.http.delete(this.URL_ROOT_WS3 + '/borrarDedicacionDocente/'+ idAsignatura + '/'+ idDistributivoDocente);
} */

getReportDistributivoPorDocente(idDocente:number): Observable<Blob> {
  return this.http.get(this.URL_ROOT_WS3 + '/getReportDistributivoPorDocente/ '+ idDocente ,{responseType:'blob' });

}

//recupera todas las actividades personal
getListaActividad(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperaListaActividad ');
}
getListaActividadDetalle(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperaListaActividadDetalle');
}

//recupera todas las actividades personal
getAllActividadPersonal(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/cargarTodasActividadesPersonal ');
}

//recupera los detalles por id Distributivo docente
recuperaActividadDetalleId(idDistributivoDocente:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperaActividadDetalleId/ '+ idDistributivoDocente );
}

buscarCantAsigParalelo(idPeriodo:number, idDepartamentoOferta:number ): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/buscarCantAsigParalelo/ '+ idPeriodo +'/'+ idDepartamentoOferta);
}



  //recupera los detalles por id Distributivo docente
  getRecuperaComponentesAsignaturaId(idAsignatura:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperaComponentesAsignaturaId/ '+ idAsignatura );
  }

   //recupera los detalles por id Distributivo docente
   getListarDetalleDocentesDistributivoOfertaID(idPeriodoAcademico:number, idDepartamento:number, idOferta:number, idDistributivoOfertaVersion:number, idDocente:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS3 + '/listarDetalleDocentesDistributivoOfertaID/ '+ idPeriodoAcademico + '/' + idDepartamento + '/' + idOferta + '/' + idDistributivoOfertaVersion + '/' + idDocente );
    }
 /*   getListarDetalleDocentesDistributivoOfertaID(idPeriodoAcademico:number, idDepartamento:number, idOferta:number, idDistributivoOfertaVersion:number, idDocente:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS3 + '/listarDetalleDocentesDistributivoOfertaID/ '+ idPeriodoAcademico + '/' + idDepartamento + '/' + idOferta + '/' + idDistributivoOfertaVersion + '/' + idDocente );
    } */
 /*   
  //presenta las dedicaciones de otras facultades por docente AB
    getOtrasDedicacionesDocente(idPeriodoAcademico:number, idOferta:number, idDistributivoOfertaVersion:number,idDocente:number): Observable<any> {
  //  alert(JSON.stringify('/listarDetalleDocentesDistributivoOfertaID/ '+ idPeriodoAcademico + '/0/' + idOferta + '/' + idDistributivoOfertaVersion + '/' + idDocente));
      return this.http.get(this.URL_ROOT_WS3 + '/listarDetalleDocentesDistributivoOfertaID/ '+ idPeriodoAcademico + '/0/' + idOferta + '/' + idDistributivoOfertaVersion + '/' + idDocente );
      }
*/
//*********FIN Pantalla Editar distributivo */


// filtrarApellidoDocentesDistributivo(idDistributivoOferta:string,apellidos:string): Observable<any> {
//   return this.http.get(this.URL_ROOT_WS3 + '/filtrarApellidoDocentesDistributivo/' + idDistributivoOferta + '/'+ apellidos );
// }

recuperarIdDedicacionDocente(idDocente:number,fechaDesde :Date, fechaHasta: Date): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/DocenteDedicacion/'+ idDocente+'/'+fechaDesde+'/'+fechaHasta);
}

recuperarCabezeraDistributivoOferta(idDistributivoOferta:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperarCabezeraDistributivoOferta/'+ idDistributivoOferta);
}
  
grabarDocentesDistributivo(distributivosDocentesDedicacion: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS3 + '/grabarDocentesDistributivo',distributivosDocentesDedicacion);
    return result;
  }
  borrarDocenteDistributivoDedicacion(idDistributivoOferta :string,idDocente :string){
  return  this.http.delete(this.URL_ROOT_WS3 + '/borrarDocenteDistributivoDedicacion/'+ idDistributivoOferta + '/'+ idDocente );
}  



borrarDedicacionDocente(idDistributivoOferta :string,idDocente :string,idDedicacion :string){
  return  this.http.delete(this.URL_ROOT_WS3 + '/borrarDedicacionDocente/'+ idDistributivoOferta + '/'+ idDocente + '/'+ idDedicacion );
}  
  

/* grabarDistributivoGeneralActividades(distributivosActividades: any): Observable<any> {
  let result: Observable<Object>;
  result = this.http.post(this.URL_ROOT_WS + '/grabarDistributivoGeneralActividades',distributivosActividades);
  return result;
} */

recuperarUltimoPeriodoVigente(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/recuperarUltimoPeriodoVigente ');
}

borrarDistributivoActividades(idDistributivoActividad :string, parametro:string){
  return  this.http.delete(this.URL_ROOT_WS + '/borrarDistributivoActividades/'+ idDistributivoActividad + '/' + parametro);
} 


//Pantalla Asignatura Docente
getDedicacionHorasClases(idDedicacion:number,idCategoria:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/recuperaDedicacionHorasClases/' +idDedicacion+'/'+idCategoria);
}



getAllDocente(idDistributivo:string): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listarDocentesDistributivo/' +idDistributivo);
}


recuperarAsignturas(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/listaAsignatura ');
}

recuperarParalelos(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/listaParalelo ');
}

replicaDistributivoVersion(idOferta:number,idDistributivo:number,idPeriodoDuplicar:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/replicaDistributivoVersion/' +idOferta+'/'+idDistributivo+'/'+idPeriodoDuplicar);
}

getPeriodoVigente(): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/cargarPeriodoVigente');
}

 

getPeriodosVigentes(idOferta : number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/cargarPeriodosVigentes/'+idOferta);
}
getPeriodosVigenteFuturo(idOferta : number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/cargarPeriodosVigentesFuturos/'+idOferta);
}

//Pantalla Configurar Paralelo

getRecuperarConfiguracionParalelo(idPeriodo:number,idOferta:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS + '/recuperarConfiguracionParalelo/' +idPeriodo+'/'+idOferta);
  }

  getRecuperarAsigParaleloUtil(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscaAsigParaleloUtil/' +idPeriodo+'/'+idOferta);
    }

  /** INICIO SERVICIOS UTILIZADOS EN MALLA ASIGNATURA FUSIÓN */
  getRecuperarMallaAsignatura(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsignatura/' +idPeriodo+'/'+idOferta);
  }
  getRecuperarMallaAsigOtraCar(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsignaturaOtraCar/' +idPeriodo+'/'+idOferta);
  }
  getMallaAsigParaleloOtraCar(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsigParaleloOtraCar/' +idPeriodo+'/'+idOferta);
  }

  getRecuperarMallaAsignaturaComp(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsignaturaComp/' +idPeriodo+'/'+idOferta);
  }

  
  getMallaAsigParalelo(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsigParalelo/' +idPeriodo+'/'+idOferta);
  }
  getMallaAsignaturaFunsionCab(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsignaturaFusionCab/' +idPeriodo+'/'+idOferta);
  }

  getMallaAsignaturaFunsionDet(idPeriodo:number,idOferta:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsignaturaFusionDet/' +idPeriodo+'/'+idOferta);
  }

  grabarMallaAsignaturaFusion(mallaAsignaturaFusion: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_MallaAsigFusion + '/grabarMallaAsignaturaFusion',mallaAsignaturaFusion);
    return result;
  }

    /** FIN SERVICIOS UTILIZADOS EN MALLA ASIGNATURA FUSIÓN */



  getRecuperarMallaAsignaturaParalelo(idPeriodo:number,idOferta:number): Observable<any> {
      return this.http.get(this.URL_ROOT_WS_MallaAsigFusion + '/buscarMallaAsignaturaParalelo/' +idPeriodo+'/'+idOferta);
  }

  actualizarMallaAsignatura(mallaAsignaturaTmp: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarMallaAsignatura',mallaAsignaturaTmp);
    return result;
  }

 grabarPlanificacaionParalelo(planificacionParalelo: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarPlanificacionParalelo',planificacionParalelo);
    return result;
  }

  //nuevos servicio para el distributivo
  getMallaAsigPorOferta(idPeriodo:number,idOferta:number,idDistOfertaVersion:number,idDistributivoDoc:number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS3 + '/listaOfertaMallaAsig/' +idPeriodo+'/'+idOferta+'/'+idDistOfertaVersion+'/'+idDistributivoDoc);
}

getMallaAsigCompPorOferta(idPeriodo:number,idOferta:number,idDistOfertaVersion:number,idDistributivoDoc:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listaOfertaMallaAsigComp/' +idPeriodo+'/'+idOferta+'/'+idDistOfertaVersion+'/'+idDistributivoDoc);
}

getMallaAsigParalelosPorOferta(idPeriodo:number,idOferta:number,idDistOfertaVersion:number,idDistributivoDoc:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listaOfertaMallaAsigParalelo/' +idPeriodo+'/'+idOferta+'/'+idDistOfertaVersion+'/'+idDistributivoDoc);
}
getMallaAsignaturaComp(idPeriodo:number,idOferta:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listaOfertaMallaAsigCompMul/' +idPeriodo+'/'+idOferta);
}
getMallaAsigCompMult(idPeriodo:number,idOferta:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listaMallaAsigCompMultidocente/' +idPeriodo+'/'+idOferta);
}
grabarAsignaturaAprendizajeMult(asignaturaAprendizaje: any): Observable<any> {
  let result: Observable<Object>;
  result = this.http.post(this.URL_ROOT_WS3 + '/grabarAsignaturaAprenMultidocente',asignaturaAprendizaje);
  return result;
}
getAsignaturaUtilDist(idPeriodo:number,idMalla:number): Observable<any> {
  return this.http.get(this.URL_ROOT_WS3 + '/listaAsignaturaUtilDist/' +idPeriodo+'/'+idMalla);
}

}

