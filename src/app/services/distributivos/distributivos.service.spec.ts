import { TestBed } from '@angular/core/testing';

import { DistributivosService } from './distributivos.service';

describe('DistributivosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DistributivosService = TestBed.get(DistributivosService);
    expect(service).toBeTruthy();
  });
});
