import { APP_CONFIG } from '../../config/app-config';
import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ReportService {

  constructor(private http: HttpClient) {}

  getReport(id:number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/reports/getReport/'+id, {responseType: 'blob'});
  }

  public URL_SERVER=APP_CONFIG.restUrl;
  public URL_ROOT_WS_periodo = this.URL_SERVER+ '/api/periodoacademico';
  public URL_ROOT_WS_areas = this.URL_SERVER+ '/api/areas';
  public URL_ROOT_WS_oferta = this.URL_SERVER+ '/api/oferta';
  public URL_ROOT_WS_tipoOferta = this.URL_SERVER+ '/api/tipooferta';
  public URL_ROOT_WS_categoria = this.URL_SERVER+ '/api/categoria';
  public URL_ROOT_WS_configuracionDist = this.URL_SERVER+ '/api/configuracionDistributivo';
  public URL_ROOT_WS_mallas = this.URL_SERVER+ '/api/mallas';
  public URL_ROOT_WS_asignatura = this.URL_SERVER+ '/api/asignaturas';
  public URL_ROOT_WS_excel = this.URL_SERVER+ '/api/excel';

  listarPeriodo(): Observable<any> {//LISTADO DE PERIODO ACADEMICO
    return this.http.get(this.URL_ROOT_WS_periodo + '/buscarPeriodoAcademico')
  }
  
  listarDepartamento(): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS_areas + '/listaDepartamento')
  }

  listarDepartamentoPorTipoOferta(idTipoOferta:number): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS_areas + '/listaDepartamentoPorTipoOferta/'+ idTipoOferta)
  }
  listarOfertas(): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_oferta + '/filtrarOfertaDepatamento')
  }
  listarOferta(idDepartamento: number): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_oferta + '/filtrarPorDepartamento/'+idDepartamento)
  }
  listarOfertaPorTipoOferta( idTipoOferta:number): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_oferta + '/filtrarPorTipoOferta/'+ idTipoOferta)
  }

  listarDistributivoOferta( idPeriodo:number ): Observable<any> {//LISTADO DISTRIBUTIVO OFERTA
    return this.http.get(this.URL_ROOT_WS_oferta + '/filtrarDistributivoOferta/'+ idPeriodo)
  }


  listarTipoOferta(): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_tipoOferta + '/buscarTipoOferta')
  }

  listarCategoria(): Observable<any> {//LISTADO CATEGORIA
    return this.http.get(this.URL_ROOT_WS_categoria + '/buscarCategoria')
  }

  listarDedicacion(): Observable<any> {//LISTADO DEDICACION
    return this.http.get(this.URL_ROOT_WS_configuracionDist + '/cargarDedicaciones')
  }

  listarMallaVersion(idOferta: number, idPeriodo:number): Observable<any> {//LISTADO DEDICACION
    return this.http.get(this.URL_ROOT_WS_mallas + '/filtrarMallasVersionyPeriodo/'+idOferta+'/'+idPeriodo)
  }
  listarMallaVersionPorPeriodoTipoOferta(idTipoOferta: number, idPeriodo:number): Observable<any> {//LISTADO DEDICACION
    return this.http.get(this.URL_ROOT_WS_mallas + '/filtrarMallasVersionPorPeriodoTipoOferta/'+idTipoOferta+'/'+idPeriodo)
  }

  listarPeriodoDocente( asignatura:string): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_asignatura + '/buscaDocenteByPeriodoAndDescripcion/'+ asignatura)
  }
  listarPeriodoDocenteAsignatura(idPeriodo: number, asignatura:string): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_asignatura + '/buscaDocenteByPeriodoAndAsignatura/'+idPeriodo+ '/'+ asignatura)
  }

  listarPeriodoDistributivoDocente(idPeriodo: number, tipoOferta:number): Observable<any> {//LISTADO TIPO OFERTA
    return this.http.get(this.URL_ROOT_WS_excel + '/buscarDistributivoDocente/'+idPeriodo+ '/'+ tipoOferta)
    
  }


  getReportDocenteNoCumplenNumHoras(idPeriodo:number,idDepartamento:number,idOferta:number,idCategoria:number, idDedicacion:number , numHoras:number, aux:boolean): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportDocenteNoCumplenNumHoras/'+idPeriodo+'/'+idDepartamento+'/'+idOferta+'/'+ idCategoria+'/'+idDedicacion+'/'+numHoras+'/'+aux, {responseType: 'blob'});
  }
 
  getReportIndicadoresCalidadPorDedicacion(idPeriodo:number, idTipoOferta:number, idDepartamento: number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportIndicadoresCalidadPorDedicacion/'+idPeriodo +'/'+ idTipoOferta +'/'+ idDepartamento, {responseType: 'blob'});
  }
  getReportIndicadoresCalidadPorCategoria(idPeriodo:number, idTipoOferta:number, idDepartamento: number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportIndicadoresCalidadPorCategoria/'+idPeriodo +'/'+ idTipoOferta+'/'+ idDepartamento, {responseType: 'blob'});
  }
  getReportIndicadoresCalidadPorGenero(idPeriodo:number, idTipoOferta:number, idDepartamento: number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportIndicadoresCalidadPorGenero/'+idPeriodo +'/'+ idTipoOferta+'/'+ idDepartamento, {responseType: 'blob'});
  }
  getReportIndicadoresCalidadPorActividad(idPeriodo:number, idTipoOferta:number, idDepartamento: number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportIndicadoresCalidadPorActividad/'+idPeriodo +'/'+ idTipoOferta+'/'+ idDepartamento, {responseType: 'blob'});
  }
  getReportIndicadoresCalidadPromedioHorasClases(idPeriodo:number,idTipoOferta:number, idDepartamento: number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportIndicadoresCalidadPromedioHorasClases/'+idPeriodo+'/'+ idTipoOferta+'/'+ idDepartamento, {responseType: 'blob'});
  }
  getReportIndicadoresCalidadPromedioEstudiantes(idPeriodo:number,idTipoOferta:number, idDepartamento: number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportIndicadoresCalidadPromedioEstudiante/'+idPeriodo+'/'+ idTipoOferta+'/'+ idDepartamento, {responseType: 'blob'});
  }
 /* getReportDistributivoPorCarrera(idPeriodo:number,idOferta:number, idTipoOferta:number, formato:string): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/reports/getReportDistributivoDocentePorCarrera/'+idPeriodo+'/'+idOferta+'/'+idTipoOferta+'/'+formato, {responseType: 'blob'});
  }
  getReportDistributivoPorFacultad(idPeriodo:number,idDepartamento:number, idTipoOferta:number, formato:string): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/reports/getReportDistributivoDocentePorFacultad/'+idPeriodo+'/'+idDepartamento+'/'+idTipoOferta+'/'+formato, {responseType: 'blob'});
  }*/
  getReportDistributivo(idPeriodo:number,idDepartamento:number, idOferta:number,idDistributivo : number, formato:string): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportDistributivoDocente/'+idPeriodo+'/'+idDepartamento+'/'+idOferta+'/'+idDistributivo+'/'+formato, {responseType: 'blob'});
  }
  getReportDistributivoPorOferta(idDepartamento:number, idDistributivoGeneral:number,idDistributivoOferta : number, formato:string): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportDistributivoDocPorOferta/'+idDepartamento+'/'+idDistributivoGeneral+'/'+idDistributivoOferta+'/'+formato, {responseType: 'blob'});
  }
  
  getReportMallaVersionPorPeriodo(idOferta:number, idMallaVersion:number,idPeriodo:number, formato:string ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportMallaPorCarrera/'+idOferta+'/'+idMallaVersion+'/'+idPeriodo+'/'+formato, {responseType: 'blob'});
  }
  getReportMallaVersionRequisitoPorPeriodo(idOferta:number, idMallaVersion:number,idPeriodo:number, formato:string ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportMallaPorCarreraRequisito/'+idOferta+'/'+idMallaVersion+'/'+idPeriodo+'/'+formato, {responseType: 'blob'});
  }

  getReportMallaHorasSemanalesPorPeriodo(idDepartamento:number,idPeriodo:number, formato:string ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportHorasSemanalesPorFacultad/'+idDepartamento+'/'+idPeriodo+'/'+formato, {responseType: 'blob'});
  }

  getExportarExcel( ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/excel/getExportarExcel', {responseType: 'blob'});
  }

  getReporteResumenNivelAcademico(idPeriodo:number,idDepartamento:number,idOferta:number ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportResumenDocenteNivelAcademico/'+idPeriodo+'/'+idDepartamento+'/'+idOferta, {responseType: 'blob'});
  }
  getReportDetalleNivelAcademico(idPeriodo:number,idDepartamento:number,idOferta:number ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportDetalleDocenteNivelAcademico/'+idPeriodo+'/'+idDepartamento+'/'+idOferta, {responseType: 'blob'});
  }



}
