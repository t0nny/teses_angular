import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()

export class RolesService {

  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS = this.URL_SERVER + '/roles';

  constructor(private http: HttpClient) {

  }

  getAll(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarRoles');
  }

  getRolesByUserModulo(usuario:string,moduloId:string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarRolesUsuarioModulos/' + usuario +'/'+moduloId);
  }
}