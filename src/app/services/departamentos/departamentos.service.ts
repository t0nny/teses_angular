import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
//

@Injectable()
export class DepartamentosService {
  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS = this.URL_SERVER + '/areas';

  constructor(private http: HttpClient) { }
//llama desde siiarest
  getAll(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarDepartamento');
  }
  //metodo de paginado
  getAllPage(pageNumber:string, size:string):Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPaginado?page=' + pageNumber +'&size=' +size);
  }
  //metodo de paginado y ordenado
   getAllPageSort(pageNumber:string, size:string, direction:string, sortfield:string ):Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPaginadoOrdenado?page=' + pageNumber +'&size=' +size+'&direction='+direction+'&sortfield='+sortfield);
  }

  //metodo de filtrado paginado y ordenado
  getFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarFiltradoPaginadoOrdenado',pageinformation);
    return result;
  }

  getUserFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarUserPaginadoOrdenado',pageinformation);
    return result;
  }
//metodo de filtrado por Id
  getBuscarIdFilterPageSort(pageinformation:any):Observable<any> {
    let result:Observable<Object>;
    result= this.http.post(this.URL_ROOT_WS + '/buscarBuscarIdPaginadoOrdenado',pageinformation);
    return result;
  }

  get(id: string) {
    return this.http.get(this.URL_ROOT_WS + '/buscarDepartamentoId/' + id);
  }

  save(departamento: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarDepartamento', departamento);
    return result;
  }

  remove(id: string) {
    return this.http.delete(this.URL_ROOT_WS + '/borrarDepartamento/' + id);
  }
}