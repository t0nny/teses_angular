import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../config/app-config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocentesService {
  public URL_SERVER = APP_CONFIG.restUrl;

   //Metodos Pantalla Docentes
  public URL_ROOT_WS = this.URL_SERVER + '/api/docentes';
  public URL_ROOT_WS_Departamento = this.URL_SERVER + '/api/areas';
  public URL_ROOT_WS_Oferta = this.URL_SERVER + '/api/oferta';
  public URL_ROOT_WS_Mallas = this.URL_SERVER + '/api/mallas';
  constructor(private http: HttpClient) { }

  listarDepartamento(): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS_Departamento + '/listaDepartamento')
  }
  
  listarOferta(idDepartamento: Number): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS_Oferta + '/filtrarPorDepartamento/' + idDepartamento)
  }

  getListarDocenteRegistrados(idOferta: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarDocenteRegistrados/' + idOferta);
  }

  borrarDocente(idPersona: number) {
    return this.http.delete(this.URL_ROOT_WS + '/borrarDocente/' + idPersona );
  }

  getListarTodosDocenteRegistrados(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarTodosDocenteRegistrados/' );
  }
  getBuscarDocente(idPersona: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarDocente/' + idPersona);
  }

  
  getBuscarDocentePorCedula(cedula: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarDocentePorCedula/' + cedula);
  }

  grabarDocente(docentePojo: any): Observable<any> {
    /*let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarDocente', DocentePojo);
    return result;*/  
    return this.http.post(this.URL_ROOT_WS + '/grabarDocente', docentePojo);
  }

  getRecuperarMallaCarreraFacultad(idOferta: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_Mallas + '/recuperarMallaCarreraFacultad/' + idOferta);
  }


  getListaFormacionProfesional(idPersona: number): Observable<any> {
    
    return this.http.get(this.URL_ROOT_WS + '/listarFormacionProfesionalPorIdPersona/' + idPersona);
  }

  getListaActualizacionProfesionalPorIdPersona(idPersona: number): Observable<any> {
    
    return this.http.get(this.URL_ROOT_WS + '/listarActualizacionProfesionalPorIdPersona/' + idPersona);
  }

  buscarFormacionProfesionalPorId(id: string) {
    return this.http.get(this.URL_ROOT_WS + '/buscarFormacionProfesionalPorId/' + id);
  }

  getAllUniversidades(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarUniversidades');
  }

  getAllNiveles(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarNivelesAcademicos');
  }

  getAllAreas(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarAreasConocimiento');
  }

  grabarlistaFormacion(formaciones:any): Observable<any> {
    /*let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarDocente', DocentePojo);
    return result;*/  
    console.log(formaciones);
    return this.http.post(this.URL_ROOT_WS + '/grabarlistaFormacion', formaciones);
  }
    
  //PANTALLA ESTUDIANTES *********
  getListaDocenteHistorial(idDocente: number): Observable<any> {
    console.log(idDocente);
    return this.http.get(this.URL_ROOT_WS + '/listarDocenteHistorial/' + idDocente);
  }
  getAllDocentesCategorias(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarDocenteCategoria');
  }

  getReportHojaDeVida(idDocente:number): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl + '/api/reports/getReportDocenteHojaDeVida/'+idDocente, {responseType: 'blob'});
  }



  
}
