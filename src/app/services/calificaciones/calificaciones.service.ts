import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../config/app-config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalificacionesService {
  public URL_SERVER = APP_CONFIG.restUrl;

   //Metodos Calificaciones
   public URL_ROOT_WS = this.URL_SERVER + '/api/calificacion';

  constructor(private http: HttpClient) { }

  listarCiclos(): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS + '/listaCiclosPorPeriodo')
  }

  listarEstudiantes(idPeriodoAcademico: number, idAsignatura: number,idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarEstudiantesPorPeriodoAsignaturaMateria/'+idPeriodoAcademico+'/'+idAsignatura+'/'+idParalelo)
  }
  
  listarComponentesAprendizaje(idPeriodoAcademico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarComponenteAprendizaje/'+idPeriodoAcademico)
  }

  /*listarRegistroCalificaciones(idPeriodoAcademico: number,idAsignatura: number,idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listar/'+idPeriodoAcademico+'/'+idAsignatura+'/'+idParalelo)
  }*/

  listarEstudianteRegistroCalificaciones(idPeriodoAcademico: number,idAsignatura: number,idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listar2/'+idPeriodoAcademico+'/'+idAsignatura+'/'+idParalelo)
  }

  listarCompo(idPeriodoAcademico: number,idAsignatura: number,idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listacom/'+idPeriodoAcademico+'/'+idAsignatura+'/'+idParalelo)
  }

  listarCompo_(idPeriodoAcademico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listacom/'+idPeriodoAcademico)
  }

  listarCiclo_(idPeriodoAcademico: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listaciclos/'+idPeriodoAcademico)
  }

  listarEst(idPeriodoAcademico: number, idAsignatura: number,idParalelo: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/listarEst/'+idPeriodoAcademico+'/'+idAsignatura+'/'+idParalelo)
  }

  grabar(acta: any): Observable<any> {
    return this.http.post(this.URL_ROOT_WS + '/grabarActa',acta)
  }
  
  cicloVigente(idPeriodoAcademico: any): Observable<any> {//LISTADO DEPARTAMENTO
    return this.http.get(this.URL_ROOT_WS + '/cicloVigente/'+idPeriodoAcademico)
  }

  
}
