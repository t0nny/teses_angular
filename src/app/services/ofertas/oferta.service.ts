import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class OfertaService {

    public URL_SERVER = APP_CONFIG.restUrl;
    public URL_ROOT_WS = this.URL_SERVER + '/api/oferta';
    public URL_ROOT_WS1 = this.URL_SERVER + '/api/perrequisitoCorrequisito';
    public URL_ROOT_WS2 = this.URL_SERVER + '/api/mallas';
    public URL_ROOT_WSAREAS = this.URL_SERVER + '/api/departamento';
    public URL_ROOT_WSDOCENTES = this.URL_SERVER + '/api/docentes';
    
  
    constructor(private http: HttpClient) {
  
    }
  
    getAllOfertas(): Observable<any> {
      return this.http.get(this.URL_ROOT_WS + '/buscarOfertas ');
    }
  
    getOfertas(id: string) {
      return this.http.get(this.URL_ROOT_WS + '/buscarOferta/' + id);
    }
  
    saveOfertas(ofertas: any): Observable<any> {
      let result: Observable<Object>;
      result = this.http.post(this.URL_ROOT_WS + '/grabarOferta', ofertas);
      return result;
    }
  

    //metodo de filtrado paginado y ordenado
    getFilterPageSort(pageinformation:any):Observable<any> {
      let result:Observable<Object>;
      result= this.http.post(this.URL_ROOT_WS + '/buscarFiltradoPaginadoOrdenado',pageinformation);
      return result;
    }
     

    //Pantalla Prerequisitos Cambiar los servicios cuando funcionane la pantalla anterior
    buscarPerrequisitoAsignatura(id:string): Observable<any> {
      return this.http.get(this.URL_ROOT_WS1 + '/buscarPerrequisitosAsignatura/' + id);
      }
    
      buscarCorequisitoAsignatura(id:string): Observable<any> {
        return this.http.get(this.URL_ROOT_WS1 + '/buscarCorrequisitosAsignatura/' + id);
        }
      getAllAsignaturaporCarrera(): Observable<any> {
        return this.http.get(this.URL_ROOT_WS1 + '/listarAsignaturasEnMalla ');
      }
      getAll(): Observable<any> {
        return this.http.get(this.URL_ROOT_WS2 + '/buscarPorOferta');
      }
  
      recuperarNombresAsignaturaNivel(id:string): Observable<any> {
          return this.http.get(this.URL_ROOT_WS1 + '/recuperarNombresAsignaturaNivel/' + id);
          }
     
      recuperarPerrequisitosAsignatura(id:string): Observable<any> {
            return this.http.get(this.URL_ROOT_WS1 + '/recuperarPerrequisitosAsignatura/' + id);
            }
  
      recuperarCorrequisitosAsignatura(id:string): Observable<any> {
              return this.http.get(this.URL_ROOT_WS1 + '/recuperarCorrequisitosAsignatura/' + id);
      }
  
      saveAsignaturaRelacion(materiasRelaciones: any): Observable<any> {
        /// alert(mallasvaen);
        let result: Observable<Object>;
        result = this.http.post(this.URL_ROOT_WS1 + '/grabarPreCorrequisitos',materiasRelaciones);
        //  alert(JSON.stringify(result)); 
        return result;
      }
  
      recuperarCabeceraMallaAsignatura(id:string): Observable<any> {
        return this.http.get(this.URL_ROOT_WS1 + '/recuperarCabeceraMallaAsignatura/' + id);
  }
  
  borrarPreCorrequisito(maId:string, marId:string){
    return  this.http.delete(this.URL_ROOT_WS1 + '/borrarPreCorrequisito/'+ maId + '/'+ marId);
  }

  // LISTAR FACULTADES
  listarFacultades(){
    return  this.http.get(this.URL_ROOT_WSAREAS + '/listaDepartamento');
    
  }

  listarFacultadesPorTipoOfe(id:number){
    return  this.http.get(this.URL_ROOT_WSAREAS + '/listaDepartamentoPorTipoOferta/'+id);
  }
  //listar Docentes
  listarDocentes(id:number): Observable<any> {
    return  this.http.get(this.URL_ROOT_WSDOCENTES + '/listarDocenteSinAsignarOferta/' + id );
  }
  listarDocentesDistributivo(idDepartamento:number, idPeriodoAcademico:number): Observable<any> {
    return  this.http.get(this.URL_ROOT_WS + '/filtrarDocenteDistributivo/' + idDepartamento+'/'+idPeriodoAcademico );
  }

    //listar Docentes
    listarDocentesOferta(idperiodo:number,idDepart:number): Observable<any> {
      return  this.http.get(this.URL_ROOT_WSDOCENTES + '/listarOfertaDocente/'+ idperiodo + '/'+ idDepart);
    }

  //listar carreras como columnas del Kanban
  listarColumnasCarreras(idFac:number,idTipOf:number):Observable<any>{
    return  this.http.get(this.URL_ROOT_WS + '/listarCarrerascolumnas/'+ idFac + '/'+ idTipOf);
  }

  //graba en OfertaDocente
/*   grabarDocentefertas(ofertaDocente: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WSDOCENTES + '/grabarOfertaDocente', ofertaDocente);
    return result;
  } */

  grabarOfertaDocente(ofertaDocente: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WSDOCENTES + '/grabarOfertaDocenteJson',ofertaDocente);
    return result;
  }
  
  borraItemOfertaDocente(id:number){
    return  this.http.delete(this.URL_ROOT_WSDOCENTES + '/eliminaOfertaDocente/'+ id );
  }

}
