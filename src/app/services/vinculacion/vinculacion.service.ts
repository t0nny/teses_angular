import { APP_CONFIG } from '../../config/app-config';
import { Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class VinculacionService {

  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS = this.URL_SERVER + '/api/ConvocatoriaProyecto';
  public URL_ROOT_WS_PROY = this.URL_SERVER + '/api/proyecto';
  public URL_ROOT_WS_PROG = this.URL_SERVER + '/api/programa';
  public URL_ROOT_WS_NIVEL = this.URL_SERVER + '/api/nivel';
  public URL_ROOT_WS_PARL = this.URL_SERVER + '/api/distributivoGeneral';
  public URL_ROOT_WS_CABMATRIZ = this.URL_SERVER + '/api/cabeceraMatriz';
  public URL_ROOT_WS_AREA_GEOGRAFICA= this.URL_SERVER + '/api/areageografica';
  public URL_ROOT_WS_ASIG_PROY= this.URL_SERVER + '/api/proyectoAsignatura';
  public URL_ROOT_WS_PROY_LINEAS_INV= this.URL_SERVER + '/api/lineasInvestigacion';
  public URL_ROOT_WS_PROY_DOMINIOS_ACAD= this.URL_SERVER + '/api/dominiosAcademicos';
  public URL_ROOT_WS_PROY_AREA_UNES= this.URL_SERVER + '/api/areaUnesco';
  public URL_ROOT_WS_PROY_DOCENTE= this.URL_SERVER + '/api/proyectoDocente';
  public URL_ROOT_WS_PROY_ESTUD= this.URL_SERVER + '/api/proyectoEstudiante';
  public URL_ROOT_WS_MARCO_LOGIC= this.URL_SERVER + '/api/marcoLogico';
  public URL_ROOT_WS_REPORT_VIN= this.URL_SERVER + '/api/reportsVinculacion';
  public URL_ROOT_WS_PROY_VERSION= this.URL_SERVER + '/api/proyectoVersion';
  public URL_ROOT_WS_ASIG_ACTIV= this.URL_SERVER + '/api/asignacionActividades';
  public URL_ROOT_WS_ACT_PART= this.URL_SERVER + '/api/actividadParticipante';
  public URL_ROOT_WS_SEGU_ACTIV= this.URL_SERVER + '/api/seguimientoActividad';

  constructor(private http: HttpClient) { }

  /***********************Servicios para /api/proyectoVersion*********************************** */
  buscarEstadoVersion(idProyecto: string): Observable<any> {//LISTADO DE PERIODO CONVOCATORIA
    return this.http.get(this.URL_ROOT_WS_PROY_VERSION + '/buscarEstadoProyectoVersion/' + idProyecto)
  }

  buscarEstadoVersion1(idProyectoVersion: string): Observable<any> {//LISTADO DE PERIODO CONVOCATORIA
    return this.http.get(this.URL_ROOT_WS_PROY_VERSION + '/buscarEstadoProyectoVersion1/' + idProyectoVersion)
  }

  /***********************Servicios para /api/periodoConvocatoria*********************************** */
  listarPeriodoConvocatoria(): Observable<any> {//LISTADO DE PERIODO CONVOCATORIA
    return this.http.get(this.URL_ROOT_WS + '/buscarPeriodoConvocatoria')
  }

  listarUltimaConvocatoria(): Observable<any> {//LISTADO DE PERIODO CONVOCATORIA
    return this.http.get(this.URL_ROOT_WS + '/buscarUltimaConvocatoria')
  }

  buscarFechaFinConvocatoria(): Observable<any> {//LISTADO DE PERIODO CONVOCATORIA
    return this.http.get(this.URL_ROOT_WS + '/buscarFechaFinConvocatoria')
  }

  buscarPorPeriodoConvocatoriaId(id: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarPorPeriodoConvocatoriaId/' + id);
  }

  borrarConvocatoria(idConvocatoria: string) {
    return this.http.delete(this.URL_ROOT_WS + '/borrar/' + idConvocatoria);
  }

  grabarConvocatoria(convocatoria: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabarPeriodoConvocatoria', convocatoria);
    return result;
  }

  cargarTituloFacultad(idPeriodoAca: string, cedula: string): Observable<any> {
    console.log("id" + idPeriodoAca + " cedula " + cedula);
    return this.http.get(this.URL_ROOT_WS + '/cargarFacultad/' + idPeriodoAca + '/' + cedula);
  }

  cargarCarreras(id: string, idFacultad: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/cargarCarreras/' + id + '/' + idFacultad);
  }

  cargarTipoUsuario(idUsuario: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/cargarTipoUsuario/' + idUsuario);
  }
  /**************************************************************************************************** */
  /***********************Servicios para /api/proyectos************************************************ */

  listarSoloProyectos(): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarSoloProyectos')
  }

  buscarAsignaturasProyectos(idDepOferta: string,idSemestre: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarAsignaturasProyectos/'+idDepOferta +'/' + idSemestre)
  }

  buscarAsignaturasProyectosGrid(idProyecto: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarAsignaturasProyectosGrid/'+idProyecto )
  }

  buscarDirectorProyecto(idDocente: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/directorProyectoTitulos/'+ idDocente)
  }

  buscarVersionProyecto(idProyecto: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarVersionProyecto/'+ idProyecto)
  }

  buscarIdDocente(idUsere: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarIdDocente/'+ idUsere)
  }

  updateEstadoProyecto(idProyecto: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/updateEstadoProyecto/'+ idProyecto)
  }

  buscarEstudianteSemestre(idPerAcademico: string,idDepOferta: string,idSemestre: Number,idParalelo: string,idEstudiante: Number): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/estudianteSemestre/'+ idPerAcademico +'/'+idDepOferta+'/'+idSemestre+'/'+idParalelo+'/'+idEstudiante)
  }

  buscarPreguntasInstrumentoVin(idInstrumento: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/fnListarPreguntaInstrumentoVin/'+ idInstrumento)
  }

  buscarEstudianteProyecto(idEstudiante: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarEstudianteProyecto/'+ idEstudiante)
  }

  listarEstudianteOferta(idDepOferta: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/listarEstudianteOferta/'+ idDepOferta)
  }

  buscarDocentesPorIdDepOferta(idPerAca: string,idDepOferta: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarDocentesPorIdDepOferta/'+ idPerAca +'/'+idDepOferta)
  }

  buscarTituloDirector(idDocente: string): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarTitulosDirector/'+ idDocente)
  }

  
  buscarproyectoVersionMatriz(idProyecto: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY + '/proyectoVersionMatriz/' + idProyecto);
  }

  buscarCabProyecto(idProyectoVersion: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY + '/proyectoCabMatriz/' + idProyectoVersion);
  }

  buscarProyectoPorId(idProyecto: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarProyectoPorId/' + idProyecto);
  }

  buscarCodigoProyecto(idProyectoVersion: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY + '/buscarCodigoProyecto/' + idProyectoVersion);
  }

  grabarProyecto(proyecto: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PROY + '/grabarProyecto', proyecto);
    return result;
  }

  grabarProyectoVersion(proyecto: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PROY + '/grabarProyectoVersion', proyecto);
    return result;
  }
  grabarCodigoProyecto(proyecto: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PROY + '/updateCodigoProyecto', proyecto);
    return result;
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/cabeceraMatriz************************************************ */
 
  grabarCabMatriz(cabMatriz: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_CABMATRIZ + '/grabarCabMatriz', cabMatriz);
    return result;
  }
 
  buscarDetMatriz(idCabMatriz: number): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_CABMATRIZ + '/buscarDetMatriz/' + idCabMatriz);
  }

  updateEstadoMatriz(cabeceraMatriz: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_CABMATRIZ + '/updateEstadoMatriz', cabeceraMatriz);
    return result;
  }
  /**************************************************************************************************** */
  /***********************Servicios para /api/programas************************************************ */
  listarSoloProgramas(): Observable<any> {//LISTADO DE PROYECTOS
    return this.http.get(this.URL_ROOT_WS_PROG + '/buscarTodosProgramas')
  }

  listarProyectosIdDepOferta(idDepOferta: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROG + '/buscarProyectosIdDepOferta/' + idDepOferta);
  }

  listarProgramaIdDepOferta(idDepOferta: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROG + '/buscarProgramasIdDepOferta/' + idDepOferta);
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/nivel************************************************ */
  listarNiveles(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_NIVEL + '/buscarNiveles');
  }

  listarParalelos(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PARL + '/listaParalelo');
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/areageografica************************************************ */
 
  buscarListaAreaGeografica(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_AREA_GEOGRAFICA + '/buscarListaAreaGeografica');
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/proyectoAsignatura************************************************ */
 
  borrarAsignaturaGrid(idProyectoAsignatura: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIG_PROY + '/borrarAsignaturaGrid/' + idProyectoAsignatura);
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/lineasInvestigacion************************************************ */
  
  buscarLineasInvestigacion(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_LINEAS_INV + '/buscarLineasInvestigacion');
  }

  lineasYSubLineasInv(idDepOferta: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_LINEAS_INV + '/lineasYSubLineasInv/' + idDepOferta);
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/dominiosAcademicos************************************************ */
  
  buscarDominiosAcademicos(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_DOMINIOS_ACAD + '/buscarDominiosAcademicos');
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/areaUnesco************************************************ */
  
  buscarListaAreaUnesco(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_AREA_UNES + '/buscarListaAreaUnesco');
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/proyectoDocente************************************************ */
  
  borrarProyectoDocenteGrid(idProyectoDocente: any) {
    return this.http.get(this.URL_ROOT_WS_PROY_DOCENTE + '/borrarProyectoDocenteGrid/' + idProyectoDocente);
  }

  listarDocentesProyecto(idProyecto: any) {
    return this.http.get(this.URL_ROOT_WS_PROY_DOCENTE + '/docenteProyecto/' + idProyecto);
  }
  
  /**************************************************************************************************** */
  /***********************Servicios para /api/proyectoEstudiante************************************************ */
  
  borrarProyectoEstudianteGrid(idProyectoEstudiante: any) {
    return this.http.get(this.URL_ROOT_WS_PROY_ESTUD + '/borrarProyectoEstudianteGrid/' + idProyectoEstudiante);
  }

  listarEstudianteProyecto(idProyecto: any) {
    return this.http.get(this.URL_ROOT_WS_PROY_ESTUD + '/estudianteProyecto/' + idProyecto);
  }

  estudianteParticipantesProyecto(idProyecto: any) {
    return this.http.get(this.URL_ROOT_WS_PROY_ESTUD + '/estudianteParticipantesProyecto/' + idProyecto);
  }

  /**************************************************************************************************** */
  /***********************Servicios para /api/marcoLogico*********************************** */
  
  listarGridComponentes(idProyecto: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MARCO_LOGIC + '/buscarObjetivoGlobal/' + idProyecto);
  }

  buscarActividad(idItemMarcoLogico: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MARCO_LOGIC + '/buscarActividad/' + idItemMarcoLogico);
  }

  grabarItemMarcoLogico(proyecto: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_MARCO_LOGIC + '/grabarItemMarcoLogico', proyecto);
    return result;
  }

  buscarObjetivoTareas(idProyecto: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MARCO_LOGIC + '/buscarObjetivoTareas/' + idProyecto);
  }

  grabarObjetivoTarea(ObjetivoTarea: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_MARCO_LOGIC + '/grabarObjetivoTarea', ObjetivoTarea);
    return result;
  }

  listarActividadesProyecto(idProyecto: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_MARCO_LOGIC + '/buscarObjetivoActividadYTareas/' + idProyecto);
  }

  /***********************Servicios para /api/reportes vinculacion*********************************** */
  getReportEvaluacionMatrizExAnte(idProyectoVersion:number,formato:string ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl +'/api/reportsVinculacion/getReportEvaluacionMatrizExAnte/'+idProyectoVersion+'/'+formato, {responseType: 'blob'});
  }

  getReportDetallePresupuestoTareas(idProyecto:number,formato:string ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl +'/api/reportsVinculacion/getReportDetallePresupuestoTareas/'+idProyecto, {responseType: 'blob'});
  }

  /***********************Servicios para /api/asigancionActividades*********************************** */
  borrarParticipanteProyectoGrid(idAsignacion: string): Observable<any> {
    return this.http.delete(this.URL_ROOT_WS_ASIG_ACTIV + '/borrarAsignacionActividades/' + idAsignacion);
  }
  
  grabarAsignacionActividades(AsignacionActividades: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_ASIG_ACTIV + '/grabarAsignacionActividades', AsignacionActividades);
    return result;
  }
    
  listarAsignacionesTareas(idProyecto: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ASIG_ACTIV + '/buscarAsignacionActividades/' + idProyecto);
  }
  
  /***********************Servicios para /api/actividadesParticipantes*********************************** */
  grabarActividadParticipante(ActividadParticipante: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_ACT_PART + '/grabarActividadParticipante', ActividadParticipante);
    return result;
  }

  buscarActividadesRealizadas(idProyecto: any,idDepOfertaActividades:any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ACT_PART + '/buscarActividadesRealizadas/' + idProyecto+'/'+idDepOfertaActividades);
  }

  buscarDepOfertaActividad(idDepartamentoOferta: any,idSeguimientoActividad:any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ACT_PART + '/buscarDepOfertaActividad/' + idDepartamentoOferta+'/'+idSeguimientoActividad);
  }

  editarActividadesRealizadas(idActividadParticipante: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ACT_PART + '/editarActividadesRealizadas/' + idActividadParticipante);
  }

  borrarActividadesRealizadas(idActividadRealizada: string): Observable<any> {
    return this.http.delete(this.URL_ROOT_WS_ACT_PART + '/borrarActividadesRealizadas/' + idActividadRealizada);
  }

  updateEstadoDepOfetaActividades(idDepOfetaActividades: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_ACT_PART + '/updateEstadoDepOfetaActividades/' + idDepOfetaActividades);
  }

  /***********************Servicios para /api/seguimientoActividad*********************************** */
  listarSeguimientoActividad(): Observable<any> {//LISTADO DE PERIODO CONVOCATORIA
    return this.http.get(this.URL_ROOT_WS_SEGU_ACTIV + '/buscarSeguimientoActividad')
  }
  grabarSeguimientoActividad(seguimientoActividad: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_SEGU_ACTIV + '/grabarSeguimientoActividad', seguimientoActividad);
    return result;
  }

  buscarPorSeguimientoActividadId(idSeguimientoActividad: any): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_SEGU_ACTIV + '/buscarPorSeguimientoActividadId/' + idSeguimientoActividad);
  }
}
