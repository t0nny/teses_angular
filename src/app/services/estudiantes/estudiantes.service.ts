import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EstudiantesService {

  public URL_SERVER = APP_CONFIG.restUrl;
  public URL_ROOT_WS = this.URL_SERVER + '/estudiante';

  constructor(private http: HttpClient) {

  }

  getAll(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS + '/buscarTodos');
  }

  get(id: string) {
    return this.http.get(this.URL_ROOT_WS + '/buscar/' + id);
  }

  save(estudiante: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS + '/grabar', estudiante);
    return result;
  }

  remove(id: string) {
    return this.http.delete(this.URL_ROOT_WS + '/borrar/' + id);
  }

}
