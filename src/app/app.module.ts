import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, DatePipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { IconModule, IconSetModule, IconSetService } from '@coreui/icons-angular';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { Base64Util } from './util/base-64-util';
import { AuthService } from './services/auth/auth.service';
import { AccountService } from './services/auth/account.service';
import { TokenService } from './services/auth/token.service';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { CacheService } from './services/auth/cache.service';
import { OpcionesService } from './services/opciones/opciones.service';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ControlAutenticacion } from './security/control-autenticacion';
import { PersonasService } from './services/personas/personas.service';
import { MallasService } from './services/mallas/mallas.service';
import { DistributivosService } from './services/distributivos/distributivos.service';
import { FileService } from './services/file/file.service';
import { ParametricasService } from './services/parametricas/parametricas.service';
import { AsignaturasService } from './services/asignaturas/asignaturas.service';
import { OpcionesAsignaturaService } from './services/opciones-asignatura/opciones-asignatura.service';
import { ReportService } from './services/report/report.service';
import { PlanificacionDocenteService } from './services/planificacion-docente/planificacion-docente.service';
import { OfertaService } from './services/ofertas/oferta.service';
import { DocentesModule } from './views/docentes/docentes.module';
import { FormsModule } from '@angular/forms';
import { DocentesService } from './services/docentes/docentes.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    IconModule,
    IconSetModule.forRoot(),
    HttpClientModule, 
    FormsModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent
  ],
  providers: [
    //DatePipe,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    IconSetService,
    Base64Util, AuthService, AccountService, TokenService, AuthGuardService, CacheService, OpcionesService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    // LVT Fin 
    ControlAutenticacion,
    PersonasService,
    MallasService,
    DistributivosService,
    FileService,
    ParametricasService,
    AsignaturasService,
    OpcionesAsignaturaService,
    ReportService,
    PlanificacionDocenteService,
    OfertaService,
    DocentesService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
